<?php
namespace App;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use \Carbon\Carbon;
use Illuminate\Support\Facades\Mail;

class CitySecuritySystem extends BaseInventoryModel
{
    protected $table = 'city_security_system';

    public static function getDT($p_Name, $p_City, $p_CreatedAt, $p_UpdatedAt, $p_Status, $p_Order, $p_Start, $p_Length, $p_Draw)
    {
        $v_Query = CitySecuritySystem::join('city', 'city.id', '=', 'city_security_system.city_id')->join('revision_status', 'revision_status.id', '=', 'city_security_system.revision_status_id')
            ->select(DB::raw('SQL_CALC_FOUND_ROWS city_security_system.id, city_security_system.city_id, city_security_system.nome_fantasia, city.name as municipio, city_security_system.created_at, city_security_system.updated_at, revision_status.name as status'));

        if(UserType::isMunicipio())
            $v_Query->where('city.id', Auth::user()->city_id);
        else if(UserType::isCircuito())
            $v_Query->whereIn('city.id', TouristicCircuitCities::getUserCircuitCities());

        if($p_Name != '')
            $v_Query->where('city_security_system.nome_fantasia', 'like', '%' . $p_Name . '%');

        if($p_City != '')
            $v_Query->where('city.name', 'like', '%' . $p_City . '%');

        if($p_CreatedAt != '')
        {
            $v_StartDate = Carbon::createFromFormat('d/m/Y', substr($p_CreatedAt, 0, 10));
            $v_Query->where('city_security_system.created_at', '>=', $v_StartDate->startOfDay()->format('Y-m-d H:i:s'));
            $v_EndDate = Carbon::createFromFormat('d/m/Y', substr($p_CreatedAt, 13, 23));
            $v_Query->where('city_security_system.created_at', '<=', $v_EndDate->endOfDay()->format('Y-m-d H:i:s'));
        }

        if($p_UpdatedAt != '')
        {
            $v_StartDate = Carbon::createFromFormat('d/m/Y', substr($p_UpdatedAt, 0, 10));
            $v_Query->where('city_security_system.updated_at', '>=', $v_StartDate->startOfDay()->format('Y-m-d H:i:s'));
            $v_EndDate = Carbon::createFromFormat('d/m/Y', substr($p_UpdatedAt, 13, 23));
            $v_Query->where('city_security_system.updated_at', '<=', $v_EndDate->endOfDay()->format('Y-m-d H:i:s'));
        }

        if($p_Status != '')
            $v_Query->where('city_security_system.revision_status_id',  $p_Status);

        if($p_Order != null)
        {
            if($p_Order["column"] == 0)
                $v_Query->orderBy('city_security_system.nome_fantasia', $p_Order["dir"]);
            if($p_Order["column"] == 1)
                $v_Query->orderBy('city.name', $p_Order["dir"]);
            if($p_Order["column"] == 2)
                $v_Query->orderBy('city_security_system.created_at', $p_Order["dir"]);
            if($p_Order["column"] == 3)
                $v_Query->orderBy('city_security_system.updated_at', $p_Order["dir"]);
            if($p_Order["column"] == 4)
                $v_Query->orderBy('revision_status.name', $p_Order["dir"]);
        }

        if($p_Length != -1)
            $v_Query->take($p_Length)->skip($p_Start);

        $v_QueryRes = $v_Query->get()->toArray();
        $v_Data = [];
        $v_IsParceiro = UserType::isParceiro();
        for($c_Index = 0 ; $c_Index < sizeof($v_QueryRes) ; $c_Index++)
        {
            array_push($v_Data, [
                $v_QueryRes[$c_Index]['nome_fantasia'],
                $v_QueryRes[$c_Index]['municipio'],
                Carbon::createFromFormat('Y-m-d H:i:s', $v_QueryRes[$c_Index]['created_at'])->format('d/m/Y - H:i'),
                Carbon::createFromFormat('Y-m-d H:i:s', $v_QueryRes[$c_Index]['updated_at'])->format('d/m/Y - H:i'),
                $v_QueryRes[$c_Index]['status'],
                '<div class="actions-div">' .
                '<a href="' . url('admin/inventario/sistemas-seguranca/editar/' . $v_QueryRes[$c_Index]['id']) . '" title="Editar" type="button" class="btn btn-success"><i class="fa fa-edit"></i></a>' .
                '<a href="' . url('admin/inventario/sistemas-seguranca/historico/' . $v_QueryRes[$c_Index]['id']) . '" title="Histórico" type="button" class="btn btn-success"><i class="fa fa-history"></i></a>' .
                ($v_IsParceiro ? '' : '<a href="' . url('admin/inventario/sistemas-seguranca/excluir/' . $v_QueryRes[$c_Index]['id']) . '" title="Excluir" type="button" class="btn btn-success delete-btn"><i class="fa fa-trash-o"></i></a>') .
                '</div>'
            ]);
        }

        $v_DataTableAjax = new \stdClass();
        $v_DataTableAjax->draw = $p_Draw;
        $v_DataTableAjax->recordsFiltered = BaseInventoryModel::getTotalRows();

        if(UserType::isMunicipio())
            $v_DataTableAjax->recordsTotal = CitySecuritySystem::where('city_id', Auth::user()->city_id)->count();
        else if(UserType::isCircuito())
            $v_DataTableAjax->recordsTotal = CitySecuritySystem::whereIn('city_id', TouristicCircuitCities::getUserCircuitCities())->count();
        else
            $v_DataTableAjax->recordsTotal = CitySecuritySystem::count();
        $v_DataTableAjax->data = $v_Data;
        return json_encode($v_DataTableAjax);
    }

    public static function post($p_Id, $p_Data)
    {
        array_walk($p_Data, function (&$c_Item) {
            $c_Item = ($c_Item === '') ? null : $c_Item;
        });

        $v_PreviousStatus = 0;
        if($p_Id != null)
            $v_PreviousStatus = CitySecuritySystem::find($p_Id)->revision_status_id;

        $v_Item = CitySecuritySystem::updateOrCreate(['id' => $p_Id], $p_Data);

        if($p_Id != null && $v_PreviousStatus != 4){
            $v_City = City::find($v_Item->city_id)->name;
            $v_Subject = null;
            $v_Message = null;
            if($v_Item->revision_status_id == 4) {
                $v_Subject = 'Alterações aprovadas';
                $v_Message = '<p style="font-size: 16px;line-height: 24px;padding: 25px 0;">As alterações do item A4 - ' . $v_City . ' foram aprovadas.</p><p id="call" style="font-size: 16px;line-height: 24px;color: #f26522;text-align: center;padding: 0;">Consulte-o no <a href="' . url('/admin') . '" style="color: #f26522;text-decoration: underline;">Descubra Mato Grosso</a>.</p>';
            }
            else if($v_Item->revision_status_id == 5) {
                $v_Subject = 'Alterações rejeitadas';
                $v_Message = '<p style="font-size: 16px;line-height: 24px;padding: 25px 0;">As alterações do item A4 - ' . $v_City . ' foram rejeitadas.</p>';
                if(!empty($v_Item->comentario_revisao))
                    $v_Message .= '<p style="font-size: 16px;line-height: 24px;padding: 25px 0;">Motivo: ' . $v_Item->comentario_revisao . '</p>';
                $v_Message .= '<p id="call" style="font-size: 16px;line-height: 24px;color: #f26522;text-align: center;padding: 0;">Consulte-o no <a href="' . url('/admin') . '" style="color: #f26522;text-decoration: underline;">Descubra Mato Grosso</a>.</p>';
            }
            if($v_Message != null) {
                $v_Users = User::getItemUsers($v_Item->city_id, 'A4', $v_Item->id);
                foreach($v_Users as $c_User) {
                    Mail::send('emails.generic', ['p_Title' => $v_Subject, 'p_Msg' => $v_Message],
                        function ($message) use ($c_User, $v_Subject)
                        {
                            $message->to($c_User->email, $c_User->name)->subject('Descubra Mato Grosso - ' . $v_Subject);
                        });
                }
            }
        }
    }

    public static function getReport($p_Data)
    {
        $v_CircuitId = $p_Data['touristic_circuit_id'];
        $v_RegionId = $p_Data['region_id'];

        $v_Query = CitySecuritySystem::leftJoin('city', 'city.id', '=', 'city_security_system.city_id')
                                     ->leftJoin('district', 'city_security_system.district_id', '=', 'district.id')
                                     ->leftJoin('touristic_circuit_cities', 'city_security_system.city_id', '=', 'touristic_circuit_cities.city_id')
                                     ->leftJoin('touristic_circuit', 'touristic_circuit_cities.touristic_circuit_id', '=', 'touristic_circuit.id')
                                     ->leftJoin('destination', 'destination.city_id', '=', 'city_security_system.city_id')
                                     ->leftJoin('region', 'destination.region_id', '=', 'region.id')
                                     ->leftJoin('economic_activity', 'city_security_system.economic_activity_id', '=', 'economic_activity.id')
                                     ->join('type', 'city_security_system.type_id', '=', 'type.id')
                                     ->selectRaw('city_security_system.*, city.name as cidade, district.name as distrito, touristic_circuit.nome as circuito, region.name as regiao, type.name as type, economic_activity.name as atividade_economica')
                                     ->groupBy('city_security_system.id');

        if(array_key_exists('city_id', $p_Data))
            $v_Query->whereIn('city_security_system.city_id', $p_Data['city_id']);

        if($v_CircuitId != '')
            $v_Query->where('touristic_circuit_cities.touristic_circuit_id', $v_CircuitId);

        if($v_RegionId != '')
            $v_Query->where('destination.region_id', $v_RegionId);

        $v_Results = $v_Query->get();

        $v_TemplateFile = public_path() . '/templates-relatorios/RelatorioA4.xlsx';
        \Excel::load($v_TemplateFile, function($v_Template) use($v_Results){
            $v_Template->sheet('Relatorio', function($sheet) use($v_Results) {
                $v_CurrentRow = 2;
                foreach($v_Results as $c_Result)
                {
                    $v_Data = [];
                    try {
                        array_push($v_Data, $c_Result->cidade);
                        array_push($v_Data, $c_Result->circuito);
                        array_push($v_Data, $c_Result->regiao);
                        array_push($v_Data, $c_Result->distrito);
                        array_push($v_Data, $c_Result->type);
                        $v_CNPJ = empty($c_Result->cnpj) ? '' : substr($c_Result->cnpj, 0, 2) . '.' . substr($c_Result->cnpj, 2, 3) . '.' . substr($c_Result->cnpj, 5, 3) . '/' . substr($c_Result->cnpj, 8, 4) . '-' . substr($c_Result->cnpj, 12, 2);
                        array_push($v_Data, $v_CNPJ);
                        array_push($v_Data, $c_Result->nome_fantasia);
                        array_push($v_Data, $c_Result->nome_juridico);
                        array_push($v_Data, $c_Result->atividade_economica);
                        array_push($v_Data, $c_Result->cep);
                        array_push($v_Data, $c_Result->bairro);
                        array_push($v_Data, $c_Result->logradouro);
                        array_push($v_Data, $c_Result->numero);
                        array_push($v_Data, $c_Result->complemento);
                        array_push($v_Data, $c_Result->telefone);
                        array_push($v_Data, $c_Result->site);
                        array_push($v_Data, $c_Result->email);
                        array_push($v_Data, $c_Result->latitude);
                        array_push($v_Data, $c_Result->longitude);

                        $v_FuncionamentoJSON = json_decode($c_Result->funcionamento_1,1);
                        $v_Funcionamento = '';
                        if($v_FuncionamentoJSON != null && count($v_FuncionamentoJSON['meses'])) {
                            foreach($v_FuncionamentoJSON['meses'] as $c_Index => $c_Month)
                                $v_Funcionamento .= ($c_Index > 0 ? ', ' : '') . $c_Month;
                            foreach($v_FuncionamentoJSON['dias'] as $c_Index => $c_Day) {
                                if($c_Day['fechado'])
                                    $v_Funcionamento .= '\n' . $c_Day['dia'] . ': Fechado';
                                else if($c_Day['funciona24horas'])
                                    $v_Funcionamento .= '\n' . $c_Day['dia'] . ': 24 horas';
                                else if(!empty($c_Day['horarioDe']) && !empty($c_Day['horarioAte']))
                                    $v_Funcionamento .= '\n' . $c_Day['dia'] . ': ' . $c_Day['horarioDe'] . ' - ' . $c_Day['horarioAte'];
                            }
                        }
                        array_push($v_Data, stripcslashes($v_Funcionamento));
                        $v_FuncionamentoJSON = json_decode($c_Result->funcionamento_2,1);
                        $v_Funcionamento = '';
                        if($v_FuncionamentoJSON != null && count($v_FuncionamentoJSON['meses'])) {
                            foreach($v_FuncionamentoJSON['meses'] as $c_Index => $c_Month)
                                $v_Funcionamento .= ($c_Index > 0 ? ', ' : '') . $c_Month;
                            foreach($v_FuncionamentoJSON['dias'] as $c_Index => $c_Day) {
                                if($c_Day['fechado'])
                                    $v_Funcionamento .= '\n' . $c_Day['dia'] . ': Fechado';
                                else if($c_Day['funciona24horas'])
                                    $v_Funcionamento .= '\n' . $c_Day['dia'] . ': 24 horas';
                                else if(!empty($c_Day['horarioDe']) && !empty($c_Day['horarioAte']))
                                    $v_Funcionamento .= '\n' . $c_Day['dia'] . ': ' . $c_Day['horarioDe'] . ' - ' . $c_Day['horarioAte'];
                            }
                        }
                        array_push($v_Data, stripcslashes($v_Funcionamento));
                        array_push($v_Data, $c_Result->funcionamento_observacao);
                        array_push($v_Data, $c_Result->informacoes_observacoes_complementares);
                        array_push($v_Data, $c_Result->equipe_responsavel_responsavel);
                        array_push($v_Data, $c_Result->equipe_responsavel_instituicao);
                        array_push($v_Data, $c_Result->equipe_responsavel_telefone);
                        array_push($v_Data, $c_Result->equipe_responsavel_email);
                        array_push($v_Data, $c_Result->equipe_responsavel_observacao);

                        $sheet->row($v_CurrentRow,$v_Data);
                        $v_CurrentRow++;
                    } catch(\Exception $e){
                    }
                    $c_Result = null;
                }
                $sheet->setBorder('A2:AB'.($v_CurrentRow-1), 'thin');
            });
        })->download('xlsx');
    }

    public static function getGeoReport($p_Data)
    {
        $v_Query = CitySecuritySystem::leftJoin('city', 'city.id', '=', 'city_security_system.city_id')
                                     ->leftJoin('touristic_circuit_cities', 'city_security_system.city_id', '=', 'touristic_circuit_cities.city_id')
                                     ->leftJoin('destination', 'destination.city_id', '=', 'city_security_system.city_id')
                                     ->whereNotNull('city_security_system.latitude')
                                     ->whereNotNull('city_security_system.longitude')
                                     ->selectRaw('city_security_system.nome_fantasia as nome, city_security_system.latitude, city_security_system.longitude, city.name as cidade')
                                     ->groupBy('city_security_system.id');

        if(array_key_exists('id', $p_Data))
            $v_Query->whereIn('city_security_system.city_id', $p_Data['id']);

        if(array_key_exists('touristic_circuit_id', $p_Data))
            $v_Query->whereIn('touristic_circuit_cities.touristic_circuit_id', $p_Data['touristic_circuit_id']);

        if(array_key_exists('region_id', $p_Data))
            $v_Query->whereIn('destination.region_id', $p_Data['region_id']);

        if(array_key_exists('revision_status_id', $p_Data))
            $v_Query->whereIn('city_security_system.revision_status_id', $p_Data['revision_status_id']);

        if(array_key_exists('published', $p_Data) && $p_Data['published'] == 1)
            $v_Query->where('city_security_system.id', 0); //nao existem itens publicados

        return $v_Query->get();
    }
}