<?php

namespace App;

use App\Http\Controllers\BaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use \Carbon\Carbon;
use Illuminate\Support\Facades\Mail;

class OtherTourismService extends BaseInventoryModel {

    protected $table = 'other_service';
       //Indica campos que podem ser salvos pelo função create/update
    protected $fillable = [
        'city_id',
        'tipo_atividade_cadastur',
        'revision_status_id',
        'tipos_viagem',
        'possui_espacos_eventos',
        'district_id',
        'type_id',
        'sub_type_id',
        'nome_fantasia',
        'nome_juridico',
        'cnpj',
        'registro_cadastur',
        'inicio_atividade',
        'natureza_entidade',
        'natureza_entidade_outras',
        'registro_filiacao_entidade',
        'registro_filiacao_numero',
        'cep',
        'bairro',
        'logradouro',
        'numero',
        'complemento',
        'telefone',
        'whatsapp',
        'site',
        'email',
        'localizacao',
        'latitude',
        'longitude',
        'descricao_arredores_distancia_pontos',
        'funcionamento_1',
        'funcionamento_2',
        'funcionamento_observacao',
        'taxa_ocupacao_media_mensal',
        'mes_maior_atendimento',
        'mes_menor_atendimento',
        'principais_atividades',
        'atendimento_bilingue',
        'atendimento_bilingue_idiomas',
        'descricoes_observacoes_complementares',
        'acessibilidade',
        'equipe_responsavel_responsavel',
        'equipe_responsavel_instituicao',
        'equipe_responsavel_telefone',
        'equipe_responsavel_email',
        'equipe_responsavel_observacao',
        'equipamento_fechado',
        'equipamento_fechado_motivo',
        'created_at',
        'updated_at',
        'comentario_revisao',
        'validade_cadastur',
    ];

    public static function getDT($p_Name, $p_City, $p_CreatedAt, $p_UpdatedAt, $p_Status, $p_Order, $p_Start, $p_Length, $p_Draw) {
        $v_Query = OtherTourismService::join('city', 'city.id', '=', 'other_service.city_id')->join('revision_status', 'revision_status.id', '=', 'other_service.revision_status_id')
                ->select(DB::raw('SQL_CALC_FOUND_ROWS other_service.id, other_service.city_id, other_service.nome_fantasia, city.name as municipio, other_service.created_at, other_service.updated_at, revision_status.name as status'));

        if (UserType::isMunicipio())
            $v_Query->where('city.id', Auth::user()->city_id);
        else if (UserType::isCircuito())
            $v_Query->whereIn('city.id', TouristicCircuitCities::getUserCircuitCities());

        if ($p_Name != '')
            $v_Query->where('other_service.nome_fantasia', 'like', '%' . $p_Name . '%');

        if ($p_City != '')
            $v_Query->where('city.name', 'like', '%' . $p_City . '%');

        if ($p_CreatedAt != '') {
            $v_StartDate = Carbon::createFromFormat('d/m/Y', substr($p_CreatedAt, 0, 10));
            $v_Query->where('other_service.created_at', '>=', $v_StartDate->startOfDay()->format('Y-m-d H:i:s'));
            $v_EndDate = Carbon::createFromFormat('d/m/Y', substr($p_CreatedAt, 13, 23));
            $v_Query->where('other_service.created_at', '<=', $v_EndDate->endOfDay()->format('Y-m-d H:i:s'));
        }

        if ($p_UpdatedAt != '') {
            $v_StartDate = Carbon::createFromFormat('d/m/Y', substr($p_UpdatedAt, 0, 10));
            $v_Query->where('other_service.updated_at', '>=', $v_StartDate->startOfDay()->format('Y-m-d H:i:s'));
            $v_EndDate = Carbon::createFromFormat('d/m/Y', substr($p_UpdatedAt, 13, 23));
            $v_Query->where('other_service.updated_at', '<=', $v_EndDate->endOfDay()->format('Y-m-d H:i:s'));
        }

        if ($p_Status != '')
            $v_Query->where('other_service.revision_status_id', $p_Status);

        if ($p_Order != null) {
            if ($p_Order["column"] == 0)
                $v_Query->orderBy('other_service.nome_fantasia', $p_Order["dir"]);
            if ($p_Order["column"] == 1)
                $v_Query->orderBy('city.name', $p_Order["dir"]);
            if ($p_Order["column"] == 2)
                $v_Query->orderBy('other_service.created_at', $p_Order["dir"]);
            if ($p_Order["column"] == 3)
                $v_Query->orderBy('other_service.updated_at', $p_Order["dir"]);
            if ($p_Order["column"] == 4)
                $v_Query->orderBy('revision_status.name', $p_Order["dir"]);
        }

        if ($p_Length != -1)
            $v_Query->take($p_Length)->skip($p_Start);

        $v_QueryRes = $v_Query->get()->toArray();
        $v_Data = [];
        $v_IsParceiro = UserType::isParceiro();
        for ($c_Index = 0; $c_Index < sizeof($v_QueryRes); $c_Index++) {
            array_push($v_Data, [
                $v_QueryRes[$c_Index]['nome_fantasia'],
                $v_QueryRes[$c_Index]['municipio'],
                Carbon::createFromFormat('Y-m-d H:i:s', $v_QueryRes[$c_Index]['created_at'])->format('d/m/Y - H:i'),
                Carbon::createFromFormat('Y-m-d H:i:s', $v_QueryRes[$c_Index]['updated_at'])->format('d/m/Y - H:i'),
                $v_QueryRes[$c_Index]['status'],
                '<div class="actions-div">' .
                '<a href="' . url('admin/inventario/outros-servicos-turisticos/editar/' . $v_QueryRes[$c_Index]['id']) . '" title="Editar" type="button" class="btn btn-success"><i class="fa fa-edit"></i></a>' .
                '<a href="' . url('admin/inventario/outros-servicos-turisticos/historico/' . $v_QueryRes[$c_Index]['id']) . '" title="Histórico" type="button" class="btn btn-success"><i class="fa fa-history"></i></a>' .
                ($v_IsParceiro ? '' : '<a href="' . url('admin/inventario/outros-servicos-turisticos/excluir/' . $v_QueryRes[$c_Index]['id']) . '" title="Excluir" type="button" class="btn btn-success delete-btn"><i class="fa fa-trash-o"></i></a>') .
                '</div>'
            ]);
        }

        $v_DataTableAjax = new \stdClass();
        $v_DataTableAjax->draw = $p_Draw;
        $v_DataTableAjax->recordsFiltered = BaseInventoryModel::getTotalRows();

        if (UserType::isMunicipio())
            $v_DataTableAjax->recordsTotal = OtherTourismService::where('city_id', Auth::user()->city_id)->count();
        else if (UserType::isCircuito())
            $v_DataTableAjax->recordsTotal = OtherTourismService::whereIn('city_id', TouristicCircuitCities::getUserCircuitCities())->count();
        else
            $v_DataTableAjax->recordsTotal = OtherTourismService::count();
        $v_DataTableAjax->data = $v_Data;
        return json_encode($v_DataTableAjax);
    }

    public static function post($p_Id, $p_Photos, $p_PhotoInfo, $p_DeletedPhotos, $p_Data) {
        $v_Data = $p_Data['formulario'];

        $v_Data['possui_espacos_eventos'] = array_key_exists('possui_espacos_eventos', $v_Data) ? 1 : 0;
        $v_Data['atendimento_bilingue'] = array_key_exists('atendimento_bilingue', $v_Data) ? 1 : 0;
        $v_Data['equipamento_fechado'] = array_key_exists('equipamento_fechado', $v_Data) ? 1 : 0;
        if (array_key_exists('mes_maior_atendimento', $p_Data) && $p_Data['mes_maior_atendimento'] != '') {
            $v_BusyMonths = $p_Data['mes_maior_atendimento'];
            $v_Months = '';
            foreach ($v_BusyMonths as $c_Month)
                $v_Months .= $c_Month . ';';
            $v_Data['mes_maior_atendimento'] = $v_Months;
        }
        if (array_key_exists('mes_menor_atendimento', $p_Data) && $p_Data['mes_menor_atendimento'] != '') {
            $v_SlowMonths = $p_Data['mes_menor_atendimento'];
            $v_Months = '';
            foreach ($v_SlowMonths as $c_Month)
                $v_Months .= $c_Month . ';';
            $v_Data['mes_menor_atendimento'] = $v_Months;
        }
        array_walk($v_Data, function (&$c_Item) {
            $c_Item = ($c_Item === '') ? null : $c_Item;
        });

        $v_PreviousStatus = 0;
        if ($p_Id != null)
            $v_PreviousStatus = OtherTourismService::find($p_Id)->revision_status_id;

        $v_Item = OtherTourismService::updateOrCreate(['id' => $p_Id], $v_Data);

        InventoryPhoto::updatePhotos('B7', $v_Item->id, $p_Photos, $p_PhotoInfo, $p_DeletedPhotos);

        if ($p_Id != null && $v_PreviousStatus != 4) {
            $v_Subject = null;
            $v_Message = null;
            if ($v_Item->revision_status_id == 4) {
                $v_Subject = 'Alterações aprovadas';
                $v_Message = '<p style="font-size: 16px;line-height: 24px;padding: 25px 0;">As alterações do item ' . $v_Item->nome_fantasia . ' foram aprovadas.</p><p id="call" style="font-size: 16px;line-height: 24px;color: #f26522;text-align: center;padding: 0;">Consulte-o no <a href="' . url('/admin') . '" style="color: #f26522;text-decoration: underline;">Descubra Mato Grosso</a>.</p>';
            } else if ($v_Item->revision_status_id == 5) {
                $v_Subject = 'Alterações rejeitadas';
                $v_Message = '<p style="font-size: 16px;line-height: 24px;padding: 25px 0;">As alterações do item ' . $v_Item->nome_fantasia . ' foram rejeitadas.</p>';
                if (!empty($v_Item->comentario_revisao))
                    $v_Message .= '<p style="font-size: 16px;line-height: 24px;padding: 25px 0;">Motivo: ' . $v_Item->comentario_revisao . '</p>';
                $v_Message .= '<p id="call" style="font-size: 16px;line-height: 24px;color: #f26522;text-align: center;padding: 0;">Consulte-o no <a href="' . url('/admin') . '" style="color: #f26522;text-decoration: underline;">Descubra Mato Grosso</a>.</p>';
            }
            if ($v_Message != null) {
                $v_Users = User::getItemUsers($v_Item->city_id, 'B7', $v_Item->id);
                foreach ($v_Users as $c_User) {
                    Mail::send('emails.generic', ['p_Title' => $v_Subject, 'p_Msg' => $v_Message], function ($message) use ($c_User, $v_Subject) {
                        $message->to($c_User->email, $c_User->name)->subject('Descubra Mato Grosso - ' . $v_Subject);
                    });
                }
            }
        }

        return $v_Item;
    }

    public static function deleteOtherTourismService($p_Id) {
        $v_Query = OtherTourismService::where('id', $p_Id);
        if (UserType::isMunicipio())
            $v_Query->where('city_id', Auth::user()->city_id);
        else if (UserType::isCircuito())
            $v_Query->whereIn('city_id', TouristicCircuitCities::getUserCircuitCities());
        $v_Item = $v_Query->firstOrFail();

        InventoryPhoto::deletePhotos('B7', $p_Id);

        $v_Item->equipe_responsavel_observacao = 'DELETED';
        $v_Item->save();
        $v_Item->delete();
    }

    public static function getReport($p_Data) {
        $v_CircuitId = $p_Data['touristic_circuit_id'];
        $v_RegionId = $p_Data['region_id'];
        $v_DistrictId = $p_Data['district_id'];
        $v_TypeId = $p_Data['type_id'];
        $v_SubTypeId = $p_Data['sub_type_id'];

        $v_Query = OtherTourismService::leftJoin('city', 'city.id', '=', 'other_service.city_id')
                ->leftJoin('district', 'other_service.district_id', '=', 'district.id')
                ->leftJoin('touristic_circuit_cities', 'other_service.city_id', '=', 'touristic_circuit_cities.city_id')
                ->leftJoin('touristic_circuit', 'touristic_circuit_cities.touristic_circuit_id', '=', 'touristic_circuit.id')
                ->leftJoin('destination', 'destination.city_id', '=', 'other_service.city_id')
                ->leftJoin('region', 'destination.region_id', '=', 'region.id')
                ->leftJoin('type', 'other_service.type_id', '=', 'type.id')
                ->leftJoin('type as sub_type', 'other_service.sub_type_id', '=', 'sub_type.id')
                ->selectRaw('other_service.*, city.name as cidade, district.name as distrito, touristic_circuit.nome as circuito, region.name as regiao, type.name as type, sub_type.name as subtype')
                ->groupBy('other_service.id');

        if (array_key_exists('city_id', $p_Data))
            $v_Query->whereIn('other_service.city_id', $p_Data['city_id']);

        if ($v_CircuitId != '')
            $v_Query->where('touristic_circuit_cities.touristic_circuit_id', $v_CircuitId);

        if ($v_RegionId != '')
            $v_Query->where('destination.region_id', $v_RegionId);

        if ($v_DistrictId != '')
            $v_Query->where('other_service.district_id', $v_DistrictId);

        if ($v_TypeId != '')
            $v_Query->where('other_service.type_id', $v_TypeId);

        if ($v_SubTypeId != '')
            $v_Query->where('other_service.sub_type_id', $v_SubTypeId);

        $v_Results = $v_Query->get();

        $v_TemplateFile = public_path() . '/templates-relatorios/RelatorioB7.xlsx';
        \Excel::load($v_TemplateFile, function($v_Template) use($v_Results) {
            $v_Template->sheet('Relatorio', function($sheet) use($v_Results) {
                $v_CurrentRow = 2;
                foreach ($v_Results as $c_Result) {
                    $v_Data = [];
                    try {
                        array_push($v_Data, $c_Result->cidade);
                        array_push($v_Data, $c_Result->circuito);
                        array_push($v_Data, $c_Result->regiao);
                        array_push($v_Data, $c_Result->distrito);
                        array_push($v_Data, $c_Result->type);
                        array_push($v_Data, $c_Result->subtype);
                        $v_CNPJ = empty($c_Result->cnpj) ? '' : substr($c_Result->cnpj, 0, 2) . '.' . substr($c_Result->cnpj, 2, 3) . '.' . substr($c_Result->cnpj, 5, 3) . '/' . substr($c_Result->cnpj, 8, 4) . '-' . substr($c_Result->cnpj, 12, 2);
                        array_push($v_Data, $v_CNPJ);
                        array_push($v_Data, $c_Result->nome_fantasia);
                        array_push($v_Data, $c_Result->nome_juridico);
                        array_push($v_Data, $c_Result->inicio_atividade);
                        array_push($v_Data, $c_Result->registro_cadastur);
                        array_push($v_Data, $c_Result->natureza_entidade);
                        array_push($v_Data, $c_Result->natureza_entidade_outras);
                        array_push($v_Data, $c_Result->registro_filiacao_entidade);
                        array_push($v_Data, $c_Result->registro_filiacao_numero);
                        array_push($v_Data, $c_Result->cep);
                        array_push($v_Data, $c_Result->bairro);
                        array_push($v_Data, $c_Result->logradouro);
                        array_push($v_Data, $c_Result->numero);
                        array_push($v_Data, $c_Result->complemento);
                        array_push($v_Data, $c_Result->telefone);
                        array_push($v_Data, $c_Result->whatsapp);
                        array_push($v_Data, $c_Result->site);
                        array_push($v_Data, $c_Result->email);
                        array_push($v_Data, $c_Result->latitude);
                        array_push($v_Data, $c_Result->longitude);
                        array_push($v_Data, $c_Result->localizacao);
                        array_push($v_Data, $c_Result->descricao_arredores_distancia_pontos);

                        $v_FuncionamentoJSON = json_decode($c_Result->funcionamento_1, 1);
                        $v_Funcionamento = '';
                        if ($v_FuncionamentoJSON != null && count($v_FuncionamentoJSON['meses'])) {
                            foreach ($v_FuncionamentoJSON['meses'] as $c_Index => $c_Month)
                                $v_Funcionamento .= ($c_Index > 0 ? ', ' : '') . $c_Month;
                            foreach ($v_FuncionamentoJSON['dias'] as $c_Index => $c_Day) {
                                if ($c_Day['fechado'])
                                    $v_Funcionamento .= '\n' . $c_Day['dia'] . ': Fechado';
                                else if ($c_Day['funciona24horas'])
                                    $v_Funcionamento .= '\n' . $c_Day['dia'] . ': 24 horas';
                                else if (!empty($c_Day['horarioDe']) && !empty($c_Day['horarioAte']))
                                    $v_Funcionamento .= '\n' . $c_Day['dia'] . ': ' . $c_Day['horarioDe'] . ' - ' . $c_Day['horarioAte'];
                            }
                        }
                        array_push($v_Data, stripcslashes($v_Funcionamento));
                        $v_FuncionamentoJSON = json_decode($c_Result->funcionamento_2, 1);
                        $v_Funcionamento = '';
                        if ($v_FuncionamentoJSON != null && count($v_FuncionamentoJSON['meses'])) {
                            foreach ($v_FuncionamentoJSON['meses'] as $c_Index => $c_Month)
                                $v_Funcionamento .= ($c_Index > 0 ? ', ' : '') . $c_Month;
                            foreach ($v_FuncionamentoJSON['dias'] as $c_Index => $c_Day) {
                                if ($c_Day['fechado'])
                                    $v_Funcionamento .= '\n' . $c_Day['dia'] . ': Fechado';
                                else if ($c_Day['funciona24horas'])
                                    $v_Funcionamento .= '\n' . $c_Day['dia'] . ': 24 horas';
                                else if (!empty($c_Day['horarioDe']) && !empty($c_Day['horarioAte']))
                                    $v_Funcionamento .= '\n' . $c_Day['dia'] . ': ' . $c_Day['horarioDe'] . ' - ' . $c_Day['horarioAte'];
                            }
                        }
                        array_push($v_Data, stripcslashes($v_Funcionamento));
                        array_push($v_Data, $c_Result->funcionamento_observacao);
                        array_push($v_Data, $c_Result->taxa_ocupacao_media_mensal);

                        $v_BusyMonths = explode(';', $c_Result->mes_maior_atendimento);
                        $v_Months = '';
                        foreach ($v_BusyMonths as $c_Index => $c_Month) {
                            if (!empty($c_Month))
                                $v_Months .= ($c_Index > 0 ? ', ' : '') . BaseController::$m_Months[$c_Month];
                        }
                        array_push($v_Data, $v_Months);

                        $v_SlowMonths = explode(';', $c_Result->mes_menor_atendimento);
                        $v_Months = '';
                        foreach ($v_SlowMonths as $c_Index => $c_Month) {
                            if (!empty($c_Month))
                                $v_Months .= ($c_Index > 0 ? ', ' : '') . BaseController::$m_Months[$c_Month];
                        }
                        array_push($v_Data, $v_Months);

                        array_push($v_Data, $c_Result->principais_atividades);

                        if ($c_Result->atendimento_bilingue) {
                            array_push($v_Data, 'Sim');

                            $v_IdiomasJSON = json_decode($c_Result->atendimento_bilingue_idiomas, 1);
                            $v_Idiomas = '';
                            foreach ($v_IdiomasJSON as $c_Index => $c_Idioma)
                                $v_Idiomas .= ($c_Index > 0 ? ', ' : '') . $c_Idioma;
                            array_push($v_Data, $v_Idiomas);
                        } else
                            array_push($v_Data, 'Não', '');


                        array_push($v_Data, $c_Result->descricoes_observacoes_complementares);

                        array_push($v_Data, $c_Result->possui_espacos_eventos ? 'Sim' : 'Não');

                        $v_AcessibilidadeJSON = json_decode($c_Result->acessibilidade, 1);
                        if ($v_AcessibilidadeJSON != null && $v_AcessibilidadeJSON['possui']) {
                            array_push($v_Data, 'Sim');
                            foreach ($v_AcessibilidadeJSON['campos'] as $c_Field) {
                                if ($c_Field['tipo'] == 'text')
                                    array_push($v_Data, $c_Field['valor']);
                                else {
                                    $v_Valor = '';
                                    $c_Field['valor'] = $c_Field['valor'] == null ? [] : $c_Field['valor'];
                                    foreach ($c_Field['valor'] as $c_Index => $c_Value)
                                        $v_Valor .= ($c_Index > 0 ? ', ' : '') . $c_Value;
                                    array_push($v_Data, $v_Valor);
                                }
                            }
                        } else
                            array_push($v_Data, 'Não', '', '', '', '', '', '', '', '', '', '', '');

                        array_push($v_Data, $c_Result->equipe_responsavel_responsavel);
                        array_push($v_Data, $c_Result->equipe_responsavel_instituicao);
                        array_push($v_Data, $c_Result->equipe_responsavel_telefone);
                        array_push($v_Data, $c_Result->equipe_responsavel_email);
                        array_push($v_Data, $c_Result->equipe_responsavel_observacao);

                        array_push($v_Data, $c_Result->equipamento_fechado ? 'Sim' : 'Não');
                        array_push($v_Data, $c_Result->equipamento_fechado ? $c_Result->equipamento_fechado_motivo : '');

                        $sheet->row($v_CurrentRow, $v_Data);
                        $v_CurrentRow++;
                    } catch (\Exception $e) {
                        
                    }
                    $c_Result = null;
                }
                $sheet->setBorder('A2:BF' . ($v_CurrentRow - 1), 'thin');
            });
        })->download('xlsx');
    }

    public function getEventBasicFields() {
        $v_Event = new EventService();
        $v_Event->city_id = $this->city_id;
        $v_Event->district_id = $this->district_id;
        $v_Event->cnpj = $this->cnpj;
        $v_Event->nome_fantasia = $this->nome_fantasia;
        $v_Event->nome_juridico = $this->nome_juridico;
        $v_Event->nome_rede = $this->nome_rede;
        $v_Event->inicio_atividade = $this->inicio_atividade;
        $v_Event->cep = $this->cep;
        $v_Event->bairro = $this->bairro;
        $v_Event->logradouro = $this->logradouro;
        $v_Event->numero = $this->numero;
        $v_Event->complemento = $this->complemento;
        $v_Event->telefone = $this->telefone;
        $v_Event->whatsapp = $this->whatsapp;
        $v_Event->site = $this->site;
        $v_Event->email = $this->email;
        $v_Event->latitude = $this->latitude;
        $v_Event->longitude = $this->longitude;
        $v_Event->descricao_arredores_distancia_pontos = $this->descricao_arredores_distancia_pontos;

        return $v_Event;
    }

    public static function getGeoReport($p_Data) {
        $v_Query = OtherTourismService::leftJoin('city', 'city.id', '=', 'other_service.city_id')
                ->leftJoin('touristic_circuit_cities', 'other_service.city_id', '=', 'touristic_circuit_cities.city_id')
                ->leftJoin('destination', 'destination.city_id', '=', 'other_service.city_id')
                ->join('type', 'other_service.type_id', '=', 'type.id')
                ->whereNotNull('other_service.latitude')
                ->whereNotNull('other_service.longitude')
                ->selectRaw('other_service.nome_fantasia as nome, other_service.latitude, other_service.longitude, other_service.type_id, other_service.sub_type_id, city.name as cidade')
                ->groupBy('other_service.id');

        if (array_key_exists('id', $p_Data))
            $v_Query->whereIn('other_service.city_id', $p_Data['id']);

        if (array_key_exists('touristic_circuit_id', $p_Data))
            $v_Query->whereIn('touristic_circuit_cities.touristic_circuit_id', $p_Data['touristic_circuit_id']);

        if (array_key_exists('region_id', $p_Data))
            $v_Query->whereIn('destination.region_id', $p_Data['region_id']);

        if (array_key_exists('revision_status_id', $p_Data))
            $v_Query->whereIn('other_service.revision_status_id', $p_Data['revision_status_id']);

        if (array_key_exists('published', $p_Data) && $p_Data['published'] == 1)
            $v_Query->where('other_service.id', 0); //nao existem itens publicados

        return $v_Query->get();
    }

}
