<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Intervention\Image\Facades\Image;

class DestinationPhoto extends Model
{
    public $timestamps = false;
    protected $table = 'destination_photo';


    //Relacionamento de 1 para 1
    public function Destination(){
        return $this->hasOne(Destination::class, 'id', 'destination_id');
    }

    public static function updatePhotos($p_DestinationId, $p_Photos, $p_PhotoInfo, $p_DeletedPhotos)
    {
        $v_Path = public_path() . '/imagens/';
        if(!\File::exists($v_Path))
            \File::makeDirectory($v_Path);
        $v_Path .=  '/destinos/';
        if(!\File::exists($v_Path))
            \File::makeDirectory($v_Path);

        foreach($p_DeletedPhotos as $c_PhotoId)
        {
            $v_Photo = DestinationPhoto::find($c_PhotoId);
            $v_OldFileName = explode('/', $v_Photo->url);
            $v_OldFileName = array_pop($v_OldFileName);
            $v_Photo->delete();
            \File::delete($v_Path . $v_OldFileName);
        }
        foreach($p_Photos as $c_Index => $c_File)
        {
            if($c_File != null)
            {
                $v_Photo = DestinationPhoto::findOrNew($p_PhotoInfo['id'][$c_Index]);
                $v_PhotoExtension = 'jpg';
                if($p_PhotoInfo['type'][$c_Index] == 'cover')
                {
                    $v_Photo->is_cover = 1;
                    $v_Photo->is_map = 0;
                    $v_Photo->is_testimonial = 0;
                }
                else if($p_PhotoInfo['type'][$c_Index] == 'map')
                {
                    $v_Photo->is_cover = 0;
                    $v_Photo->is_map = 1;
                    $v_Photo->is_testimonial = 0;
                    $v_PhotoExtension = 'png';
                }
                else if($p_PhotoInfo['type'][$c_Index] == 'testimonial')
                {
                    $v_Photo->is_cover = 0;
                    $v_Photo->is_map = 0;
                    $v_Photo->is_testimonial = 1;
                }
                else
                {
                    $v_Photo->is_cover = 0;
                    $v_Photo->is_map = 0;
                    $v_Photo->is_testimonial = 0;
                }

                $v_PhotoName =  time() . str_random(10) . '.' . $v_PhotoExtension;
                $v_Image = Image::make($c_File);
                $v_Image->widen(1920, function ($constraint){
                    $constraint->upsize();
                });
                if($v_PhotoExtension == 'jpg')
                    $v_Image->encode('jpg');
                $v_Image->save($v_Path . $v_PhotoName);
                if ($v_Photo->url != null)
                {
                    $v_OldFileName = explode('/', $v_Photo->url);
                    $v_OldFileName = array_pop($v_OldFileName);
                    \File::delete($v_Path . $v_OldFileName);
                }
                $v_Photo->url = url('/imagens/destinos/' . $v_PhotoName);
                $v_Photo->destination_id = $p_DestinationId;
                $v_Photo->save();
            }
        }
    }

    public static function getCoverPhoto($p_DestinationId)
    {
        return DestinationPhoto::where('is_cover', 1)->where('destination_id', $p_DestinationId)->first();
    }

    public static function getMapPhoto($p_DestinationId)
    {
        return DestinationPhoto::where('is_map', 1)->where('destination_id', $p_DestinationId)->first();
    }

    public static function getTestimonialPhoto($p_DestinationId)
    {
        return DestinationPhoto::where('is_testimonial', 1)->where('destination_id', $p_DestinationId)->first();
    }

    public static function getPhotoGallery($p_DestinationId, $p_WithCover = null)
    {
        $v_Query = DestinationPhoto::where('is_map', 0)->where('is_testimonial', 0);

        if($p_WithCover == null || $p_WithCover == 0)
            $v_Query->where('is_cover', 0);

        return $v_Query->where('destination_id', $p_DestinationId)->get();
    }
}