<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactCity extends Model
{
    public $timestamps = false;
    protected $table = 'contact_city';

    public static function getCitiesByState($p_State){
        return ContactCity::where('state', '=', $p_State)
            ->orderBy('name')
            ->lists('name', 'id')
            ->toArray();
    }

    public static function getStateList(){
        return ['' => trans('string.state')] + ContactCity::groupBy('state')
            ->orderBy('state')
            ->lists('state', 'state')
            ->toArray();
    }

}