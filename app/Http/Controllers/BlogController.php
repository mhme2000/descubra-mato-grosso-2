<?php

/* trata as requisições do site público do sistema relativas ao blog. Também lida com as requisições de listagem, edição, criação, remoção, busca, ativação e desativação dos artigos do blog. */

namespace App\Http\Controllers;

use App\BlogArticle;
use App\BlogArticleHashtag;
use App\Hashtag;
use \Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
use Validator;

class BlogController extends BaseController
{
    public function getBlog($p_Lang)
    {
        \App::setLocale($p_Lang);
        $v_Articles = BlogArticle::getArticlesPagination(10)->setPath(url('/' . $p_Lang . '/blog'))->appends(Input::except('page'));
        foreach($v_Articles as $c_Article)
            $c_Article->hashtags = array_keys(BlogArticleHashtag::getList($c_Article->id));

        $v_Hashtags = BlogArticleHashtag::getRandomHashtags(10);

        return view('public.blog')->with(['p_Articles' => $v_Articles,
                                          'p_Hashtags' => $v_Hashtags,
                                          'p_ArchiveMonths' => BlogArticle::getArchiveMonths(),
                                          'p_ArchiveYears' => BlogArticle::getArchiveYears(),
                                          'p_Language' => $p_Lang]);
    }

    public function getBlogSearch($p_Lang)
    {
        \App::setLocale($p_Lang);
        $v_Hashtag = Input::get('hashtag');
        $v_SearchString = Input::get('q');
        $v_Archive = Input::get('arquivo');
        $v_Articles = [];
        if($v_Hashtag != null && strlen($v_Hashtag) > 0)
            $v_Articles = BlogArticleHashtag::getHashtagArticles($v_Hashtag, 1000000);
        else if($v_SearchString != null && strlen(trim($v_SearchString)) > 0)
        {
	        $v_SearchString = trim($v_SearchString);
            $v_HashtagIds = Hashtag::getHashtagIds($v_SearchString);
            $v_Articles = BlogArticle::getArticlesFilter($v_HashtagIds, 1000000);
        }
        else if($v_Archive != null && strlen($v_Archive) > 0) {
            $v_Articles = BlogArticle::getArticlesArchive($v_Archive, 1000000);
            $v_Archive = explode('-', $v_Archive);
            $v_Archive = array_reverse($v_Archive);
            $v_Archive = join('/', $v_Archive);
        }

        foreach($v_Articles as $c_Article)
            $c_Article->hashtags = array_keys(BlogArticleHashtag::getList($c_Article->id));

        return view('public.blogSearchResults')->with(['p_Hashtag' => $v_Hashtag,
                                                       'p_SearchString' => $v_SearchString,
                                                       'p_Articles' => $v_Articles,
                                                       'p_Archive' => $v_Archive,
                                                       'p_Language' => $p_Lang]);
    }

    public function getArticle($p_Lang, $p_Slug)
    {
        \App::setLocale($p_Lang);

        $v_Article = BlogArticle::getArticle($p_Slug);
        $v_ArticleHashtags = BlogArticle::getArticleHashtags($v_Article->id);
        $v_Hashtags = BlogArticleHashtag::getRandomHashtags(10);

	    $v_HashtagIds = [];
	    foreach($v_ArticleHashtags as $c_Hashtag)
	        $v_HashtagIds = array_merge($v_HashtagIds, [$c_Hashtag->id]);

	    $v_RelatedArticles = BlogArticle::getArticlesFilter($v_HashtagIds, 3, $v_Article->id);
	    foreach($v_RelatedArticles as $c_Article)
		    $c_Article->hashtags = array_keys(BlogArticleHashtag::getList($c_Article->id));
        return view('public.blogArticle')->with(['p_Article' => $v_Article,
                                                 'p_ArticleHashtags' => $v_ArticleHashtags,
                                                 'p_Hashtags' => $v_Hashtags,
                                                 'p_RelatedArticles' => $v_RelatedArticles,
                                                 'p_ArchiveMonths' => BlogArticle::getArchiveMonths(),
                                                 'p_ArchiveYears' => BlogArticle::getArchiveYears(),
                                                 'p_Language' => $p_Lang]);
    }

    public function getArticles()
    {
        return view('admin.blog.list');
    }

    public function getDTArticles()
    {
        $v_Columns = Input::get('columns');
        $v_CreatedAt = $v_Columns[0]['search']['value'];
        $v_UpdatedAt = $v_Columns[1]['search']['value'];
        $v_Title = $v_Columns[2]['search']['value'];
        $v_Status = $v_Columns[3]['search']['value'];
        $v_Order = Input::get('order')[0];
        $v_Start = Input::get('start');
        $v_Length = Input::get('length');
        $v_Draw = Input::get('draw');
        return BlogArticle::getDT($v_CreatedAt, $v_UpdatedAt, $v_Title, $v_Status, $v_Order, $v_Start, $v_Length, $v_Draw);
    }

    public function editArticle($p_Id = null)
    {
        if ($p_Id != null)
        {
            $v_Article = BlogArticle::findOrFail($p_Id);
            $v_Hashtags = BlogArticleHashtag::getList($p_Id);
        }
        else
        {
            $v_Article = null;
            $v_Hashtags = [];
        }

        return view('admin.blog.edit')->with(['p_Article' => $v_Article,
                                              'p_Hashtags' => $v_Hashtags]);
    }

    public function postArticle()
    {
        $v_Id = Input::get('id');
        $v_Validator = Validator::make(Input::all(), $v_Id == null ? BlogArticle::$m_Rules : BlogArticle::$m_RulesEdit);
        if($v_Validator->fails())
            return redirect()->back()->withInput()->withErrors($v_Validator);

        $v_Image = Input::file('foto_capa');
        if($v_Image != null && $v_Image->isValid())
            $v_CoverPhoto = Image::make($v_Image->getRealPath());
        else
            $v_CoverPhoto = null;

        BlogArticle::post($v_Id, $v_CoverPhoto, Input::all());
        return redirect(url('/admin/blog'))->with('message', 'Artigo salvo com sucesso!');
    }

    public function toggleActive($p_Id)
    {
        $v_Article = BlogArticle::findOrFail($p_Id);
        $v_Article->active = !$v_Article->active;
        $v_Article->save();
        return redirect()->back()->with('message', 'Operação realizada com sucesso');
    }

    public function deleteArticle($p_Id)
    {
        BlogArticle::deleteArticle($p_Id);
        return redirect()->back()->with('message', 'Operação realizada com sucesso');
    }
}

