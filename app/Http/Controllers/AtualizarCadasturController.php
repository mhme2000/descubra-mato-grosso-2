<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Events\AtualizaCadasturEvent;
use Illuminate\Support\Facades\Auth;
use App\Parameter;
use App\Revision;
use App\AccomodationService;
use App\FoodDrinksService;
use App\TourismAgencyService;
use App\TourismTransportationService;
use App\EventService;
use App\RecreationService;
use App\OtherTourismService;

use App\Jobs\ReviewCadastur;
use App\Jobs\ReviewAttractions;

class AtualizarCadasturController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $this->dispatch(new ReviewCadastur());
	    $this->dispatch(new ReviewAttractions());
        return "Processo sendo executado.";
    }

}
#  su -c nohup php /var/www/minasgerais/artisan queue:work --daemon --sleep=3  --tries=3 & -s /bin/sh apache ...