<?php

/* classe base de controlador, com algumas variáveis estáticas utilizadas no projeto. */

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;

abstract class BaseController extends Controller
{
    public static $m_Languages = array
    (
        0 => 'pt',
        1 => 'en',
        2 => 'es',
        3 => 'fr'
    );

	public static $m_Months = array
	(
        'Todos' => 'Todos',
        '1' => 'Janeiro',
        '2' => 'Fevereiro',
        '3' => 'Março',
        '4' => 'Abril',
        '5' => 'Maio',
        '6' => 'Junho',
        '7' => 'Julho',
        '8' => 'Agosto',
        '9' => 'Setembro',
        '10' => 'Outubro',
        '11' => 'Novembro',
        '12' => 'Dezembro',
	);

	public static $m_Days = array
	(
        '1' => 'Segunda',
        '2' => 'Terça',
        '3' => 'Quarta',
        '4' => 'Quinta',
        '5' => 'Sexta',
        '6' => 'Sábado',
        '7' => 'Domingo',
        '8' => 'Feriados'
	);

    public static $m_Prices = array
    (
        '' => '',
        '0' => '$',
        '1' => '$$',
        '2' => '$$$',
        '3' => '$$$$',
        '4' => '$$$$$'
    );

//    public static function isAdmin()
//    {
//        return Auth::check() && Auth::user()->is_admin == 1;
//    }
//    public static function isCompany()
//    {
//        return Auth::check() && Auth::user()->company_id != null;
//    }
//    public static function isInterviewee()
//    {
//        return Auth::check() && Auth::user()->interviewee_id != null;
//    }
}