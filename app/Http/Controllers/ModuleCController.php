<?php

namespace App\Http\Controllers;

use App\Country;
use App\GastronomicPrimaryProduct;
use App\GastronomicTransformedProduct;
use App\GastronomicTypicalDish;
use App\InventoryPhoto;
use App\PaymentMethod;
use App\RevisionStatus;
use App\TouristicCircuitCities;
use App\TripType;
use App\UserType;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\City;
use App\ContemporaryRealization;
use App\CulturalAttraction;
use App\EconomicActivityAttraction;
use App\Language;
use App\NaturalAttraction;
use App\Type;
use \Illuminate\Support\Facades\Input;
use Validator;

class ModuleCController extends BaseController
{
	/*
	|--------------------------------------------------------------------------
	| C1 - NaturalAttraction
	|--------------------------------------------------------------------------
	*/

	public function getNaturalAttractions()
	{
		return view('admin.moduleC.naturalAttraction.list')->with(['p_Status' => RevisionStatus::getList()]);
	}

	public function getDTNaturalAttractions()
	{
		$v_Columns = Input::get('columns');
		$v_Name = $v_Columns[0]['search']['value'];
		$v_City = $v_Columns[1]['search']['value'];
		$v_CreatedAt = $v_Columns[2]['search']['value'];
		$v_UpdatedAt = $v_Columns[3]['search']['value'];
		$v_Status = $v_Columns[4]['search']['value'];
		$v_Order = Input::get('order')[0];
		$v_Start = Input::get('start');
		$v_Length = Input::get('length');
		$v_Draw = Input::get('draw');
		return NaturalAttraction::getDT($v_Name, $v_City, $v_CreatedAt, $v_UpdatedAt, $v_Status, $v_Order, $v_Start, $v_Length, $v_Draw);
	}

	public function editNaturalAttraction($p_Id = null)
	{
		if ($p_Id != null)
		{
			$v_Query = NaturalAttraction::where('id',$p_Id);
			if(UserType::isMunicipio())
				$v_Query->where('city_id', Auth::user()->city_id);
			else if(UserType::isCircuito())
				$v_Query->whereIn('city_id', TouristicCircuitCities::getUserCircuitCities());
			$v_Attraction = $v_Query->firstOrFail();
			$v_CoverPhoto = InventoryPhoto::getCoverPhoto('C1', $p_Id);
			$v_PhotoGallery = InventoryPhoto::getPhotoGallery('C1', $p_Id);
		}
		else
		{
			$v_Attraction = null;
			$v_CoverPhoto = null;
			$v_PhotoGallery = [];
		}

		if(UserType::isMunicipio())
			$v_Cities = City::where('id',Auth::user()->city_id)->lists('name', 'id')->toArray();
		else if(UserType::isCircuito())
			$v_Cities = City::whereIn('id',TouristicCircuitCities::getUserCircuitCities())->lists('name', 'id')->toArray();
		else
			$v_Cities = City::getList();

		return view('admin.moduleC.naturalAttraction.edit')->with(['p_Cities' => $v_Cities,
		                                                           'p_Attraction' => $v_Attraction,
		                                                           'p_Types' => Type::getInventoryTypeListByName('C1'),
		                                                           'p_CoverPhoto' => $v_CoverPhoto,
		                                                           'p_PhotoGallery' => $v_PhotoGallery,
		                                                           'p_LanguageNames' => Language::getNameList(),
		                                                           'p_Languages' => Language::getList(),
		                                                           'p_TripTypes' => TripType::getList()]);
	}

	public function postNaturalAttraction()
	{
		$v_PhotoFiles = Input::file('photo');
		$v_Photos = [];
		foreach($v_PhotoFiles['file'] as $c_Image)
		{
			if($c_Image != null && $c_Image->isValid())
				array_push($v_Photos, $c_Image);
			else
				array_push($v_Photos, null);
		}
		$v_PhotoInfo = Input::get('photo');
		$v_DeletedPhotos = Input::has('delete_photo') ? Input::get('delete_photo') : [];

		$v_Id = Input::get('id');
		$v_Eventos = false;
		if($v_Id != null){
			$v_Attraction = NaturalAttraction::find($v_Id);
			$v_Eventos = $v_Attraction->possui_espacos_eventos;
		}
		$v_Data = Input::all();
		$v_Attraction = NaturalAttraction::post($v_Id, $v_Photos, $v_PhotoInfo, $v_DeletedPhotos, $v_Data['formulario']);

		if(!$v_Eventos && array_key_exists('possui_espacos_eventos', $v_Data['formulario'])){
			Session::put('EventServiceBasicFields', $v_Attraction->getEventBasicFields());
			return redirect(url('/admin/inventario/servicos-eventos/editar?link=1'))->with('message', 'Opera��o realizada com sucesso. Cadastre agora o espa�o para eventos');
		}
		else
			return redirect(url('/admin/inventario/atrativos-naturais'))->with('message', 'Opera��o realizada com sucesso.');
	}

	public function getNaturalAttractionHistory($p_Id)
	{
		$v_Attraction = NaturalAttraction::findOrFail($p_Id);
		$v_Title = 'C1 - Atrativos naturais - Hist�rico - ' . City::find($v_Attraction->city_id)->name;
		$v_Subtitle = 'Nome: ' . $v_Attraction->nome_oficial;
		return view('admin.util.history')->with(['p_Title' => $v_Title,
		                                         'p_Subtitle' => $v_Subtitle,
		                                         'p_History' => $v_Attraction->revisionHistory]);
	}

	public function deleteNaturalAttraction($p_Id)
	{
		NaturalAttraction::deleteNaturalAttraction($p_Id);
		return redirect()->back()->with('message', 'Opera��o realizada com sucesso');
	}

	/*
	|--------------------------------------------------------------------------
	| C2 - CulturalAttraction
	|--------------------------------------------------------------------------
	*/

	public function getCulturalAttractions()
	{
		return view('admin.moduleC.culturalAttraction.list')->with(['p_Status' => RevisionStatus::getList()]);
	}

	public function getDTCulturalAttractions()
	{
		$v_Columns = Input::get('columns');
		$v_Name = $v_Columns[0]['search']['value'];
		$v_City = $v_Columns[1]['search']['value'];
		$v_CreatedAt = $v_Columns[2]['search']['value'];
		$v_UpdatedAt = $v_Columns[3]['search']['value'];
		$v_Status = $v_Columns[4]['search']['value'];
		$v_Order = Input::get('order')[0];
		$v_Start = Input::get('start');
		$v_Length = Input::get('length');
		$v_Draw = Input::get('draw');
		return CulturalAttraction::getDT($v_Name, $v_City, $v_CreatedAt, $v_UpdatedAt, $v_Status, $v_Order, $v_Start, $v_Length, $v_Draw);
	}

	public function editCulturalAttraction($p_Id = null)
	{
		if ($p_Id != null)
		{
			$v_Query = CulturalAttraction::where('id',$p_Id);
			if(UserType::isMunicipio())
				$v_Query->where('city_id', Auth::user()->city_id);
			else if(UserType::isCircuito())
				$v_Query->whereIn('city_id', TouristicCircuitCities::getUserCircuitCities());
			$v_Attraction = $v_Query->firstOrFail();
			$v_CoverPhoto = InventoryPhoto::getCoverPhoto('C2', $p_Id);
			$v_PhotoGallery = InventoryPhoto::getPhotoGallery('C2', $p_Id);
		}
		else
		{
			$v_Attraction = null;
			$v_CoverPhoto = null;
			$v_PhotoGallery = [];
		}

		if(UserType::isMunicipio())
			$v_Cities = City::where('id',Auth::user()->city_id)->lists('name', 'id')->toArray();
		else if(UserType::isCircuito())
			$v_Cities = City::whereIn('id',TouristicCircuitCities::getUserCircuitCities())->lists('name', 'id')->toArray();
		else
			$v_Cities = City::getList();

		return view('admin.moduleC.culturalAttraction.edit')->with(['p_Cities' => $v_Cities,
		                                                            'p_Attraction' => $v_Attraction,
		                                                            'p_Types' => Type::getInventoryTypeListByName('C2'),
		                                                            'p_Countries' => Country::getList(),
		                                                            'p_CoverPhoto' => $v_CoverPhoto,
		                                                            'p_PhotoGallery' => $v_PhotoGallery,
		                                                            'p_PaymentMethods' => PaymentMethod::getList(),
		                                                            'p_LanguageNames' => Language::getNameList(),
		                                                            'p_Languages' => Language::getList(),
		                                                            'p_TripTypes' => TripType::getList()]);
	}

	public function postCulturalAttraction()
	{
		$v_PhotoFiles = Input::file('photo');
		$v_Photos = [];
		foreach($v_PhotoFiles['file'] as $c_Image)
		{
			if($c_Image != null && $c_Image->isValid())
				array_push($v_Photos, $c_Image);
			else
				array_push($v_Photos, null);
		}
		$v_PhotoInfo = Input::get('photo');
		$v_DeletedPhotos = Input::has('delete_photo') ? Input::get('delete_photo') : [];

		$v_Id = Input::get('id');
		$v_Eventos = false;
		if($v_Id != null){
			$v_Attraction = CulturalAttraction::find($v_Id);
			$v_Eventos = $v_Attraction->possui_espacos_eventos;
		}
		$v_Data = Input::all();
		$v_Attraction = CulturalAttraction::post($v_Id, $v_Photos, $v_PhotoInfo, $v_DeletedPhotos, $v_Data['formulario']);

		if(!$v_Eventos && array_key_exists('possui_espacos_eventos', $v_Data['formulario'])){
			Session::put('EventServiceBasicFields', $v_Attraction->getEventBasicFields());
			return redirect(url('/admin/inventario/servicos-eventos/editar?link=1'))->with('message', 'Opera��o realizada com sucesso. Cadastre agora o espa�o para eventos');
		}
		else
			return redirect(url('/admin/inventario/atrativos-culturais'))->with('message', 'Opera��o realizada com sucesso.');
	}

	public function getCulturalAttractionHistory($p_Id)
	{
		$v_Attraction = CulturalAttraction::findOrFail($p_Id);
		$v_Title = 'C2 - Atrativos culturais - Hist�rico - ' . City::find($v_Attraction->city_id)->name;
		$v_Subtitle = 'Nome: ' . $v_Attraction->nome_oficial;
		return view('admin.util.history')->with(['p_Title' => $v_Title,
		                                         'p_Subtitle' => $v_Subtitle,
		                                         'p_History' => $v_Attraction->revisionHistory]);
	}

	public function deleteCulturalAttraction($p_Id)
	{
		CulturalAttraction::deleteCulturalAttraction($p_Id);
		return redirect()->back()->with('message', 'Opera��o realizada com sucesso');
	}

	/*
	|--------------------------------------------------------------------------
	| C3 - EconomicActivityAttraction
	|--------------------------------------------------------------------------
	*/

	public function getEconomicActivityAttractions()
	{
		return view('admin.moduleC.economicActivityAttraction.list')->with(['p_Status' => RevisionStatus::getList()]);
	}

	public function getDTEconomicActivityAttractions()
	{
		$v_Columns = Input::get('columns');
		$v_Name = $v_Columns[0]['search']['value'];
		$v_City = $v_Columns[1]['search']['value'];
		$v_CreatedAt = $v_Columns[2]['search']['value'];
		$v_UpdatedAt = $v_Columns[3]['search']['value'];
		$v_Status = $v_Columns[4]['search']['value'];
		$v_Order = Input::get('order')[0];
		$v_Start = Input::get('start');
		$v_Length = Input::get('length');
		$v_Draw = Input::get('draw');
		return EconomicActivityAttraction::getDT($v_Name, $v_City, $v_CreatedAt, $v_UpdatedAt, $v_Status, $v_Order, $v_Start, $v_Length, $v_Draw);
	}

	public function editEconomicActivityAttraction($p_Id = null)
	{
		if ($p_Id != null)
		{
			$v_Query = EconomicActivityAttraction::where('id',$p_Id);
			if(UserType::isMunicipio())
				$v_Query->where('city_id', Auth::user()->city_id);
			else if(UserType::isCircuito())
				$v_Query->whereIn('city_id', TouristicCircuitCities::getUserCircuitCities());
			$v_Attraction = $v_Query->firstOrFail();
			$v_CoverPhoto = InventoryPhoto::getCoverPhoto('C3', $p_Id);
			$v_PhotoGallery = InventoryPhoto::getPhotoGallery('C3', $p_Id);
		}
		else
		{
			$v_Attraction = null;
			$v_CoverPhoto = null;
			$v_PhotoGallery = [];
		}

		if(UserType::isMunicipio())
			$v_Cities = City::where('id',Auth::user()->city_id)->lists('name', 'id')->toArray();
		else if(UserType::isCircuito())
			$v_Cities = City::whereIn('id',TouristicCircuitCities::getUserCircuitCities())->lists('name', 'id')->toArray();
		else
			$v_Cities = City::getList();

		return view('admin.moduleC.economicActivityAttraction.edit')->with(['p_Cities' => $v_Cities,
		                                                                    'p_Attraction' => $v_Attraction,
		                                                                    'p_Types' => Type::getInventoryTypeListByName('C3'),
		                                                                    'p_Countries' => Country::getList(),
		                                                                    'p_CoverPhoto' => $v_CoverPhoto,
		                                                                    'p_PhotoGallery' => $v_PhotoGallery,
		                                                                    'p_PaymentMethods' => PaymentMethod::getList(),
		                                                                    'p_LanguageNames' => Language::getNameList(),
		                                                                    'p_Languages' => Language::getList(),
		                                                                    'p_TripTypes' => TripType::getList()]);
	}

	public function postEconomicActivityAttraction()
	{
		$v_PhotoFiles = Input::file('photo');
		$v_Photos = [];
		foreach($v_PhotoFiles['file'] as $c_Image)
		{
			if($c_Image != null && $c_Image->isValid())
				array_push($v_Photos, $c_Image);
			else
				array_push($v_Photos, null);
		}
		$v_PhotoInfo = Input::get('photo');
		$v_DeletedPhotos = Input::has('delete_photo') ? Input::get('delete_photo') : [];

		$v_Id = Input::get('id');
		$v_Eventos = false;
		if($v_Id != null){
			$v_Attraction = EconomicActivityAttraction::find($v_Id);
			$v_Eventos = $v_Attraction->possui_espacos_eventos;
		}
		$v_Data = Input::all();
		$v_Attraction = EconomicActivityAttraction::post($v_Id, $v_Photos, $v_PhotoInfo, $v_DeletedPhotos, $v_Data['formulario']);

		if(!$v_Eventos && array_key_exists('possui_espacos_eventos', $v_Data['formulario'])){
			Session::put('EventServiceBasicFields', $v_Attraction->getEventBasicFields());
			return redirect(url('/admin/inventario/servicos-eventos/editar?link=1'))->with('message', 'Opera��o realizada com sucesso. Cadastre agora o espa�o para eventos');
		}
		else
			return redirect(url('/admin/inventario/atrativos-atividades-economicas'))->with('message', 'Opera��o realizada com sucesso.');
	}

	public function getEconomicActivityAttractionHistory($p_Id)
	{
		$v_Attraction = EconomicActivityAttraction::findOrFail($p_Id);
		$v_Title = 'C3 - Atividades econ�micas/produ��o associada ao turismo - Hist�rico - ' . City::find($v_Attraction->city_id)->name;
		$v_Subtitle = 'Nome: ' . $v_Attraction->nome_oficial;
		return view('admin.util.history')->with(['p_Title' => $v_Title,
		                                         'p_Subtitle' => $v_Subtitle,
		                                         'p_History' => $v_Attraction->revisionHistory]);
	}

	public function deleteEconomicActivityAttraction($p_Id)
	{
		EconomicActivityAttraction::deleteEconomicActivityAttraction($p_Id);
		return redirect()->back()->with('message', 'Opera��o realizada com sucesso');
	}

	/*
	|--------------------------------------------------------------------------
	| C4 - ContemporaryRealization
	|--------------------------------------------------------------------------
	*/

	public function getContemporaryRealizations()
	{
		return view('admin.moduleC.contemporaryRealization.list')->with(['p_Status' => RevisionStatus::getList()]);
	}

	public function getDTContemporaryRealizations()
	{
		$v_Columns = Input::get('columns');
		$v_Name = $v_Columns[0]['search']['value'];
		$v_City = $v_Columns[1]['search']['value'];
		$v_CreatedAt = $v_Columns[2]['search']['value'];
		$v_UpdatedAt = $v_Columns[3]['search']['value'];
		$v_Status = $v_Columns[4]['search']['value'];
		$v_Order = Input::get('order')[0];
		$v_Start = Input::get('start');
		$v_Length = Input::get('length');
		$v_Draw = Input::get('draw');
		return ContemporaryRealization::getDT($v_Name, $v_City, $v_CreatedAt, $v_UpdatedAt, $v_Status, $v_Order, $v_Start, $v_Length, $v_Draw);
	}

	public function editContemporaryRealization($p_Id = null)
	{
		if ($p_Id != null)
		{
			$v_Query = ContemporaryRealization::where('id',$p_Id);
			if(UserType::isMunicipio())
				$v_Query->where('city_id', Auth::user()->city_id);
			else if(UserType::isCircuito())
				$v_Query->whereIn('city_id', TouristicCircuitCities::getUserCircuitCities());
			$v_Attraction = $v_Query->firstOrFail();
			$v_CoverPhoto = InventoryPhoto::getCoverPhoto('C4', $p_Id);
			$v_PhotoGallery = InventoryPhoto::getPhotoGallery('C4', $p_Id);
		}
		else
		{
			$v_Attraction = null;
			$v_CoverPhoto = null;
			$v_PhotoGallery = [];
		}

		if(UserType::isMunicipio())
			$v_Cities = City::where('id',Auth::user()->city_id)->lists('name', 'id')->toArray();
		else if(UserType::isCircuito())
			$v_Cities = City::whereIn('id',TouristicCircuitCities::getUserCircuitCities())->lists('name', 'id')->toArray();
		else
			$v_Cities = City::getList();

		return view('admin.moduleC.contemporaryRealization.edit')->with(['p_Cities' => $v_Cities,
		                                                                 'p_Attraction' => $v_Attraction,
		                                                                 'p_Types' => Type::getInventoryTypeListByName('C4'),
		                                                                 'p_Languages' => Language::getList(),
		                                                                 'p_Countries' => Country::getList(),
		                                                                 'p_PaymentMethods' => PaymentMethod::getList(),
		                                                                 'p_CoverPhoto' => $v_CoverPhoto,
	                                                                     'p_PhotoGallery' => $v_PhotoGallery,
	                                                                     'p_TripTypes' => TripType::getList()]);
	}

	public function postContemporaryRealization()
	{
		$v_PhotoFiles = Input::file('photo');
		$v_Photos = [];
		foreach($v_PhotoFiles['file'] as $c_Image)
		{
			if($c_Image != null && $c_Image->isValid())
				array_push($v_Photos, $c_Image);
			else
				array_push($v_Photos, null);
		}
		$v_PhotoInfo = Input::get('photo');
		$v_DeletedPhotos = Input::has('delete_photo') ? Input::get('delete_photo') : [];

		$v_Id = Input::get('id');
		$v_Eventos = false;
		if($v_Id != null){
			$v_Attraction = ContemporaryRealization::find($v_Id);
			$v_Eventos = $v_Attraction->possui_espacos_eventos;
		}
		$v_Data = Input::all();
		$v_Attraction = ContemporaryRealization::post($v_Id, $v_Photos, $v_PhotoInfo, $v_DeletedPhotos, $v_Data['formulario']);

		if(!$v_Eventos && array_key_exists('possui_espacos_eventos', $v_Data['formulario'])){
			Session::put('EventServiceBasicFields', $v_Attraction->getEventBasicFields());
			return redirect(url('/admin/inventario/servicos-eventos/editar?link=1'))->with('message', 'Opera��o realizada com sucesso. Cadastre agora o espa�o para eventos');
		}
		else
			return redirect(url('/admin/inventario/realizacoes-contemporaneas'))->with('message', 'Opera��o realizada com sucesso.');
	}

	public function getContemporaryRealizationHistory($p_Id)
	{
		$v_Attraction = ContemporaryRealization::findOrFail($p_Id);
		$v_Title = 'C4 - Realiza��es t�cnicas e cient�ficas contempor�neas - Hist�rico - ' . City::find($v_Attraction->city_id)->name;
		$v_Subtitle = 'Nome: ' . $v_Attraction->nome_oficial;
		return view('admin.util.history')->with(['p_Title' => $v_Title,
		                                         'p_Subtitle' => $v_Subtitle,
		                                         'p_History' => $v_Attraction->revisionHistory]);
	}

	public function deleteContemporaryRealization($p_Id)
	{
		ContemporaryRealization::deleteContemporaryRealization($p_Id);
		return redirect()->back()->with('message', 'Opera��o realizada com sucesso');
	}

	/*
	|--------------------------------------------------------------------------
	| C6.1 - GastronomicPrimaryProduct
	|--------------------------------------------------------------------------
	*/

	public function getGastronomicPrimaryProducts()
	{
		return view('admin.moduleC.gastronomicPrimaryProduct.list')->with(['p_Status' => RevisionStatus::getList()]);
	}

	public function getDTGastronomicPrimaryProducts()
	{
		$v_Columns = Input::get('columns');
		$v_Name = $v_Columns[0]['search']['value'];
		$v_City = $v_Columns[1]['search']['value'];
		$v_CreatedAt = $v_Columns[2]['search']['value'];
		$v_UpdatedAt = $v_Columns[3]['search']['value'];
		$v_Status = $v_Columns[4]['search']['value'];
		$v_Order = Input::get('order')[0];
		$v_Start = Input::get('start');
		$v_Length = Input::get('length');
		$v_Draw = Input::get('draw');
		return GastronomicPrimaryProduct::getDT($v_Name, $v_City, $v_CreatedAt, $v_UpdatedAt, $v_Status, $v_Order, $v_Start, $v_Length, $v_Draw);
	}

	public function editGastronomicPrimaryProduct($p_Id = null)
	{
		if ($p_Id != null)
		{
			$v_Query = GastronomicPrimaryProduct::where('id',$p_Id);
			if(UserType::isMunicipio())
				$v_Query->where('city_id', Auth::user()->city_id);
			else if(UserType::isCircuito())
				$v_Query->whereIn('city_id', TouristicCircuitCities::getUserCircuitCities());
			$v_Attraction = $v_Query->firstOrFail();
			$v_CoverPhoto = InventoryPhoto::getCoverPhoto('C6.1', $p_Id);
			$v_PhotoGallery = InventoryPhoto::getPhotoGallery('C6.1', $p_Id);
		}
		else
		{
			$v_Attraction = null;
			$v_CoverPhoto = null;
			$v_PhotoGallery = [];
		}

		if(UserType::isMunicipio())
			$v_Cities = City::where('id',Auth::user()->city_id)->lists('name', 'id')->toArray();
		else if(UserType::isCircuito())
			$v_Cities = City::whereIn('id',TouristicCircuitCities::getUserCircuitCities())->lists('name', 'id')->toArray();
		else
			$v_Cities = City::getList();

		return view('admin.moduleC.gastronomicPrimaryProduct.edit')->with(['p_Cities' => $v_Cities,
		                                                                   'p_CoverPhoto' => $v_CoverPhoto,
		                                                                   'p_PhotoGallery' => $v_PhotoGallery,
		                                                                   'p_Attraction' => $v_Attraction,
		                                                                   'p_TripTypes' => TripType::getList()]);
	}

	public function postGastronomicPrimaryProduct()
	{
		$v_PhotoFiles = Input::file('photo');
		$v_Photos = [];
		foreach($v_PhotoFiles['file'] as $c_Image)
		{
			if($c_Image != null && $c_Image->isValid())
				array_push($v_Photos, $c_Image);
			else
				array_push($v_Photos, null);
		}
		$v_PhotoInfo = Input::get('photo');
		$v_DeletedPhotos = Input::has('delete_photo') ? Input::get('delete_photo') : [];

		GastronomicPrimaryProduct::post(Input::get('id'), $v_Photos, $v_PhotoInfo, $v_DeletedPhotos, Input::all());
		if(Input::get('existe_c3')){
			return redirect(url('/admin/inventario/atrativos-atividades-economicas/editar'))->with('message', 'Opera��o realizada com sucesso.');
		}
		else{
			return redirect(url('/admin/inventario/produto-primario'))->with('message', 'Opera��o realizada com sucesso.');
		}
	}

	public function getGastronomicPrimaryProductHistory($p_Id)
	{
		$v_Attraction = GastronomicPrimaryProduct::findOrFail($p_Id);
		$v_Title = 'C6.1 - Gastronomia - Produto prim�rio - Hist�rico - ' . City::find($v_Attraction->city_id)->name;
		return view('admin.util.history')->with(['p_Title' => $v_Title,
		                                         'p_History' => $v_Attraction->revisionHistory]);
	}

	public function deleteGastronomicPrimaryProduct($p_Id)
	{
		GastronomicPrimaryProduct::deleteGastronomicPrimaryProduct($p_Id);
		return redirect()->back()->with('message', 'Opera��o realizada com sucesso');
	}

	/*
	|--------------------------------------------------------------------------
	| C6.2 - GastronomicTransformedProduct
	|--------------------------------------------------------------------------
	*/

	public function getGastronomicTransformedProducts()
	{
		return view('admin.moduleC.gastronomicTransformedProduct.list')->with(['p_Status' => RevisionStatus::getList()]);
	}

	public function getDTGastronomicTransformedProducts()
	{
		$v_Columns = Input::get('columns');
		$v_Name = $v_Columns[0]['search']['value'];
		$v_City = $v_Columns[1]['search']['value'];
		$v_CreatedAt = $v_Columns[2]['search']['value'];
		$v_UpdatedAt = $v_Columns[3]['search']['value'];
		$v_Status = $v_Columns[4]['search']['value'];
		$v_Order = Input::get('order')[0];
		$v_Start = Input::get('start');
		$v_Length = Input::get('length');
		$v_Draw = Input::get('draw');
		return GastronomicTransformedProduct::getDT($v_Name, $v_City, $v_CreatedAt, $v_UpdatedAt, $v_Status, $v_Order, $v_Start, $v_Length, $v_Draw);
	}

	public function editGastronomicTransformedProduct($p_Id = null)
	{
		if ($p_Id != null)
		{
			$v_Query = GastronomicTransformedProduct::where('id',$p_Id);
			if(UserType::isMunicipio())
				$v_Query->where('city_id', Auth::user()->city_id);
			else if(UserType::isCircuito())
				$v_Query->whereIn('city_id', TouristicCircuitCities::getUserCircuitCities());
			$v_Attraction = $v_Query->firstOrFail();
			$v_CoverPhoto = InventoryPhoto::getCoverPhoto('C6.2', $p_Id);
			$v_PhotoGallery = InventoryPhoto::getPhotoGallery('C6.2', $p_Id);
		}
		else
		{
			$v_Attraction = null;
			$v_CoverPhoto = null;
			$v_PhotoGallery = [];
		}

		if(UserType::isMunicipio())
			$v_Cities = City::where('id',Auth::user()->city_id)->lists('name', 'id')->toArray();
		else if(UserType::isCircuito())
			$v_Cities = City::whereIn('id',TouristicCircuitCities::getUserCircuitCities())->lists('name', 'id')->toArray();
		else
			$v_Cities = City::getList();

		return view('admin.moduleC.gastronomicTransformedProduct.edit')->with(['p_Cities' => $v_Cities,
		                                                                       'p_CoverPhoto' => $v_CoverPhoto,
		                                                                       'p_PhotoGallery' => $v_PhotoGallery,
		                                                                       'p_Attraction' => $v_Attraction,
		                                                                       'p_TripTypes' => TripType::getList()]);
	}

	public function postGastronomicTransformedProduct()
	{
		$v_PhotoFiles = Input::file('photo');
		$v_Photos = [];
		foreach($v_PhotoFiles['file'] as $c_Image)
		{
			if($c_Image != null && $c_Image->isValid())
				array_push($v_Photos, $c_Image);
			else
				array_push($v_Photos, null);
		}
		$v_PhotoInfo = Input::get('photo');
		$v_DeletedPhotos = Input::has('delete_photo') ? Input::get('delete_photo') : [];

		GastronomicTransformedProduct::post(Input::get('id'), $v_Photos, $v_PhotoInfo, $v_DeletedPhotos, Input::all());
		if(Input::get('existe_c3')){
			return redirect(url('/admin/inventario/atrativos-atividades-economicas/editar'))->with('message', 'Opera��o realizada com sucesso.');
		}
		else{
			return redirect(url('/admin/inventario/produto-transformado'))->with('message', 'Opera��o realizada com sucesso.');
		}
	}

	public function getGastronomicTransformedProductHistory($p_Id)
	{
		$v_Attraction = GastronomicTransformedProduct::findOrFail($p_Id);
		$v_Title = 'C6.2 - Gastronomia - Produto transformado - Hist�rico - ' . City::find($v_Attraction->city_id)->name;
		return view('admin.util.history')->with(['p_Title' => $v_Title,
		                                         'p_History' => $v_Attraction->revisionHistory]);
	}

	public function deleteGastronomicTransformedProduct($p_Id)
	{
		GastronomicTransformedProduct::deleteGastronomicTransformedProduct($p_Id);
		return redirect()->back()->with('message', 'Opera��o realizada com sucesso');
	}

	/*
	|--------------------------------------------------------------------------
	| C6.3 - GastronomicTypicalDish
	|--------------------------------------------------------------------------
	*/

	public function getGastronomicTypicalDishes()
	{
		return view('admin.moduleC.gastronomicTypicalDish.list')->with(['p_Status' => RevisionStatus::getList()]);
	}

	public function getDTGastronomicTypicalDishes()
	{
		$v_Columns = Input::get('columns');
		$v_Name = $v_Columns[0]['search']['value'];
		$v_City = $v_Columns[1]['search']['value'];
		$v_CreatedAt = $v_Columns[2]['search']['value'];
		$v_UpdatedAt = $v_Columns[3]['search']['value'];
		$v_Status = $v_Columns[4]['search']['value'];
		$v_Order = Input::get('order')[0];
		$v_Start = Input::get('start');
		$v_Length = Input::get('length');
		$v_Draw = Input::get('draw');
		return GastronomicTypicalDish::getDT($v_Name, $v_City, $v_CreatedAt, $v_UpdatedAt, $v_Status, $v_Order, $v_Start, $v_Length, $v_Draw);
	}

	public function editGastronomicTypicalDish($p_Id = null)
	{
		if ($p_Id != null)
		{
			$v_Query = GastronomicTypicalDish::where('id',$p_Id);
			if(UserType::isMunicipio())
				$v_Query->where('city_id', Auth::user()->city_id);
			else if(UserType::isCircuito())
				$v_Query->whereIn('city_id', TouristicCircuitCities::getUserCircuitCities());
			$v_Attraction = $v_Query->firstOrFail();
			$v_CoverPhoto = InventoryPhoto::getCoverPhoto('C6.3', $p_Id);
			$v_PhotoGallery = InventoryPhoto::getPhotoGallery('C6.3', $p_Id);
		}
		else
		{
			$v_Attraction = null;
			$v_CoverPhoto = null;
			$v_PhotoGallery = [];
		}

		if(UserType::isMunicipio())
			$v_Cities = City::where('id',Auth::user()->city_id)->lists('name', 'id')->toArray();
		else if(UserType::isCircuito())
			$v_Cities = City::whereIn('id',TouristicCircuitCities::getUserCircuitCities())->lists('name', 'id')->toArray();
		else
			$v_Cities = City::getList();

		return view('admin.moduleC.gastronomicTypicalDish.edit')->with(['p_Cities' => $v_Cities,
		                                                                'p_CoverPhoto' => $v_CoverPhoto,
		                                                                'p_PhotoGallery' => $v_PhotoGallery,
		                                                                'p_Attraction' => $v_Attraction,
		                                                                'p_TripTypes' => TripType::getList()]);
	}

	public function postGastronomicTypicalDish()
	{
		$v_PhotoFiles = Input::file('photo');
		$v_Photos = [];
		foreach($v_PhotoFiles['file'] as $c_Image)
		{
			if($c_Image != null && $c_Image->isValid())
				array_push($v_Photos, $c_Image);
			else
				array_push($v_Photos, null);
		}
		$v_PhotoInfo = Input::get('photo');
		$v_DeletedPhotos = Input::has('delete_photo') ? Input::get('delete_photo') : [];

		GastronomicTypicalDish::post(Input::get('id'), $v_Photos, $v_PhotoInfo, $v_DeletedPhotos, Input::all());
		if(Input::get('existe_c3')){
			return redirect(url('/admin/inventario/atrativos-atividades-economicas/editar'))->with('message', 'Opera��o realizada com sucesso.');
		}
		else{
			return redirect(url('/admin/inventario/prato-tipico'))->with('message', 'Opera��o realizada com sucesso.');
		}
	}

	public function getGastronomicTypicalDishHistory($p_Id)
	{
		$v_Attraction = GastronomicTypicalDish::findOrFail($p_Id);
		$v_Title = 'C6.3 - Gastronomia - Prato t�pico - Hist�rico - ' . City::find($v_Attraction->city_id)->name;
		return view('admin.util.history')->with(['p_Title' => $v_Title,
		                                         'p_History' => $v_Attraction->revisionHistory]);
	}

	public function deleteGastronomicTypicalDish($p_Id)
	{
		GastronomicTypicalDish::deleteGastronomicTypicalDish($p_Id);
		return redirect()->back()->with('message', 'Opera��o realizada com sucesso');
	}
}