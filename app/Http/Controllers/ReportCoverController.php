<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\ReportCover;
use \Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;


class ReportCoverController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ReportCover $reportcover)
    {
        $reportcovers = $reportcover->all(); 
        return view('admin.properties.reportCovers.list', compact('reportcovers'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('admin.properties.reportCovers.edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ReportCover $reportcover, Request $request)
    {

       
        $form = $request->except('_token');
        
        $v_Path = public_path() . '/imagens/';
        if(!\File::exists($v_Path))
            \File::makeDirectory($v_Path);
        $v_Path .=  '/calendario-eventos/';
        if(!\File::exists($v_Path))
            \File::makeDirectory($v_Path);

        $v_Data = [];
/*
        $v_DeletedPhotos = Input::has('delete_photo') ? Input::get('delete_photo') : [];
        $v_DeleteCover = in_array('capa',$v_DeletedPhotos);
        $v_DeleteHeader = in_array('cabecalho',$v_DeletedPhotos);
*/
        $v_Cover = Input::file('capa');
        /*if( ($v_Cover != null && $v_Cover->isValid()) || $v_DeleteCover){

            $v_OldFileName = explode('/', $reportcover->pcover);
            $v_OldFileName = array_pop($v_OldFileName);
            \File::delete($v_Path . $v_OldFileName);

            if($v_DeleteCover)
                $v_Data['pcover'] = null;
        }*/
        if($v_Cover != null && $v_Cover->isValid()){
            $v_Photo = Image::make($v_Cover);

            $v_PhotoName =  time() . str_random(10) . '.jpg';
            $v_Photo->widen(1920, function ($constraint){
                $constraint->upsize();
            });
            $v_Photo->save($v_Path . $v_PhotoName);
            $v_Data['pcover'] = $v_PhotoName;
        }
        
        $v_Header = Input::file('cabecalho');
        /*if( ($v_Header != null && $v_Header->isValid()) || $v_DeleteHeader){

            $v_OldFileName = explode('/', $reportcover->pheader);
            $v_OldFileName = array_pop($v_OldFileName);
            \File::delete($v_Path . $v_OldFileName);

            if($v_DeleteHeader)
                $v_Data['pheader'] = null;
        }*/
        if($v_Header != null && $v_Header->isValid()){
            $v_Photo = Image::make($v_Header);

            $v_PhotoName =  time() . str_random(10) . '.jpg';
            $v_Photo->widen(1920, function ($constraint){
                $constraint->upsize();
            });
            $v_Photo->save($v_Path . $v_PhotoName);
            $v_Data['pheader'] = $v_PhotoName;
        }        
        
        
   
        $v_Data['label'] = $form['label'];
        $v_Data['message'] = $form['event_calendar_last_page'];
        
        
       $status = $reportcover->create($v_Data); 
       
       if ($status) {
          return redirect(url('admin/capas-relatorios'))->with('message', 'Operação realizada com sucesso.');
       } else {
          return redirect(url('admin/capas-relatorios'));           
       }
      
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(ReportCover $reportcover, $id)
    {
        $r_reportcover = $reportcover->find($id);
        
        //dd($r_reportcover);
        
        return view('admin.properties.reportCovers.edit', compact('r_reportcover'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ReportCover $reportcover, Request $request, $id)
    {

        $datatable = $reportcover->find($id);

        $form = $request->except('_token');
        
        $v_Path = public_path() . '/imagens/';
        if(!\File::exists($v_Path))
            \File::makeDirectory($v_Path);
        $v_Path .=  '/calendario-eventos/';
        if(!\File::exists($v_Path))
            \File::makeDirectory($v_Path);

        $v_Data = [];

        $v_DeletedPhotos = Input::has('delete_photo') ? Input::get('delete_photo') : [];
        $v_DeleteCover = in_array('capa',$v_DeletedPhotos);
        $v_DeleteHeader = in_array('cabecalho',$v_DeletedPhotos);

        $v_Cover = Input::file('capa');
        if( ($v_Cover != null && $v_Cover->isValid()) || $v_DeleteCover){

            $v_OldFileName = explode('/', $datatable->pcover);
            $v_OldFileName = array_pop($v_OldFileName);
            \File::delete($v_Path . $v_OldFileName);

            if($v_DeleteCover)
                $v_Data['pcover'] = null;
        }
        if($v_Cover != null && $v_Cover->isValid()){
            $v_Photo = Image::make($v_Cover);

            $v_PhotoName =  time() . str_random(10) . '.jpg';
            $v_Photo->widen(1920, function ($constraint){
                $constraint->upsize();
            });
            $v_Photo->save($v_Path . $v_PhotoName);
            $v_Data['pcover'] = $v_PhotoName;
        }
        
        $v_Header = Input::file('cabecalho');
        if( ($v_Header != null && $v_Header->isValid()) || $v_DeleteHeader){

            $v_OldFileName = explode('/', $datatable->pheader);
            $v_OldFileName = array_pop($v_OldFileName);
            \File::delete($v_Path . $v_OldFileName);

            if($v_DeleteHeader)
                $v_Data['pheader'] = null;
        }
        if($v_Header != null && $v_Header->isValid()){
            $v_Photo = Image::make($v_Header);

            $v_PhotoName =  time() . str_random(10) . '.jpg';
            $v_Photo->widen(1920, function ($constraint){
                $constraint->upsize();
            });
            $v_Photo->save($v_Path . $v_PhotoName);
            $v_Data['pheader'] = $v_PhotoName;
        }        
        
        
        $v_Data['id'] = $id;
        $v_Data['label'] = $form['label'];
        $v_Data['message'] = $form['event_calendar_last_page'];
        
        
       $status = $datatable->update($v_Data); 
       
       if ($status) {
          return redirect(url('admin/capas-relatorios'))->with('message', 'Operação realizada com sucesso.');
       } else {
          return redirect(url('admin/capas-relatorios'));           
       }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(ReportCover $reportcover, $id)
    {
       $p_reportcover = $reportcover->find($id);
       $status = $p_reportcover->delete();
       if ($status) {
          return redirect(url('admin/capas-relatorios'))->with('message', 'Operação realizada com sucesso.'); 
       }
    }
}
