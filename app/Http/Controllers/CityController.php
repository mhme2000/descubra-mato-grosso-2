<?php

/* lida com as requisições de edição, criação e listagem de municípios e distritos. */

namespace App\Http\Controllers;

use App\City;
use App\Destination;
use App\District;
use \Illuminate\Support\Facades\Input;
use Validator;

class CityController extends BaseController
{
	/*
	|--------------------------------------------------------------------------
	| City
	|--------------------------------------------------------------------------
	*/

    public function getCities()
    {
        return view('admin.properties.city.list');
    }

    public function getDTCities()
    {
        $v_Columns = Input::get('columns');
        $v_Name = $v_Columns[0]['search']['value'];
        $v_Order = Input::get('order')[0];
        $v_Start = Input::get('start');
        $v_Length = Input::get('length');
        $v_Draw = Input::get('draw');
        return City::getDTCities($v_Name, $v_Order, $v_Start, $v_Length, $v_Draw);
    }

    public function editCity($p_Id = null)
    {
	    if ($p_Id != null)
		    $v_City = City::findOrFail($p_Id);
	    else
		    $v_City = null;

	    return view('admin.properties.city.edit')->with(['p_City' => $v_City]);
    }

	public function postCity()
	{
		$v_Id = Input::get('id');
		$v_Validator = Validator::make(Input::all(), $v_Id == null ? City::$m_Rules : City::$m_RulesEdit);
		if($v_Validator->fails())
			return redirect()->back()->withInput()->withErrors($v_Validator);

		City::post($v_Id, Input::get('nome'));
		return redirect(url('/admin/municipios'))->with('message', 'Operação realizada com sucesso.');
	}

	public function getCityTouristicCircuit()
	{
		return City::getCityTouristicCircuit(Input::get('city_id'));
	}

	public function getCityRegion()
	{
		return Destination::getCityRegion(Input::get('city_id'));
	}

	/*
	|--------------------------------------------------------------------------
	| District
	|--------------------------------------------------------------------------
	*/

    public function getAvailableCityDistricts()
    {
	    return District::getAvailableCityDistricts(Input::get('city_id'));
    }

    public function getCityDistricts()
    {
	    return District::getCityDistricts(Input::get('city_id'));
    }

    public function getCitiesDistricts()
    {
	    return District::getCitiesDistricts(Input::get('city_ids'));
    }

    public function getDistricts($p_CityId)
    {
	    $v_City = City::findOrFail($p_CityId);
	    $v_Districts = District::where('city_id', $p_CityId)->get();
	    return view('admin.properties.district.list')->with(['p_City' => $v_City,
	                                              'p_Districts' => $v_Districts]);
    }

    public function editDistrict($p_CityId, $p_Id = null)
    {
	    $v_City = City::findOrFail($p_CityId);
	    if ($p_Id != null)
		    $v_District = District::findOrFail($p_Id);
	    else
		    $v_District = null;

	    return view('admin.properties.district.edit')->with(['p_City' => $v_City,
	                                              'p_District' => $v_District]);
    }

	public function postDistrict($p_CityId)
	{
		$v_Id = Input::get('id');
		$v_Validator = Validator::make(Input::all(), $v_Id == null ? District::$m_Rules : District::$m_RulesEdit);
		if($v_Validator->fails())
			return redirect()->back()->withInput()->withErrors($v_Validator);

		District::post($v_Id, $p_CityId, Input::get('nome'));
		return redirect(url('/admin/municipios/' . $p_CityId . '/distritos'))->with('message', 'Operação realizada com sucesso.');
	}
}