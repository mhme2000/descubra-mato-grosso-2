<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReportCover extends Model
{
    protected $table = 'report_cover';
    public $timestamps = false;
    
    protected $fillable = [
        'label',
        'pcover',
        'pheader',
        'message',
    ];   
    
    
}
