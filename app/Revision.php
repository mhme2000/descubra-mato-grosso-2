<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//A tabela desta model é gerenciada pelo componente venturecraft. A inserção manual
//de registro pode causar falha na leitura dos dados.
class Revision extends Model
{
protected $table = 'revisions';    
   //Indica campos que podem ser salvos pelo função create/update
protected $fillable = [
    'revisionable_type',
    'revisionable_id',
    'user_id',
    'key',
    'old_value',
    'new_value',
];
/**
 * Função de filtro de Logs, usada na rotina que realiza atualização altomática
 * do cadastur.
 * @param array $data
 * @return type
 */
public function filterAtualizaCadastur(Array $data) {
    return $this->where(function($query) use($data){
        if (isset($data['id'])){
            $query->where('id', $data['id']);
        }

        if (isset($data['revisionable_type'])){
            $query->where('revisionable_type', 'like', "%$data[revisionable_type]");
        }
        
        if (isset($data['revisionable_id'])){
            $query->where('revisionable_id', $data['revisionable_id']);
        }
        
        if (isset($data['key'])){
            $query->where('key', $data['key']);
        }
        
        if (isset($data['created_at'])){
            $query->where('created_at','>', $data['created_at']);
        }

  
    })->get();
}

}
