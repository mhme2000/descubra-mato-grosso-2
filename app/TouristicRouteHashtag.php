<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class TouristicRouteHashtag extends Model
{
    public $timestamps = false;
    protected $table = 'touristic_route_hashtag';

    public static function deleteTouristicRouteHashtags($p_TouristicRouteId)
    {
        TouristicRouteHashtag::where('touristic_route_id', $p_TouristicRouteId)->delete();
    }

    public static function addTouristicRouteHashtag($p_TouristicRouteId, $p_HashtagId)
    {
        $v_TouristicRouteHashtag = new TouristicRouteHashtag();
        $v_TouristicRouteHashtag->touristic_route_id = $p_TouristicRouteId;
        $v_TouristicRouteHashtag->hashtag_id = $p_HashtagId;
        $v_TouristicRouteHashtag->save();
    }

    public static function getList($p_TouristicRouteId)
    {
        return TouristicRouteHashtag::join('hashtag', 'hashtag.id', '=', 'touristic_route_hashtag.hashtag_id')
            ->where('touristic_route_id', $p_TouristicRouteId)->lists('hashtag.name', 'hashtag.name')->toArray();
    }

    public static function getHashtagTouristicRoutes($p_HashtagIds, $p_Quantity)
    {
        $v_Query = TouristicRouteHashtag::join('touristic_route', 'touristic_route.id', '=', 'touristic_route_hashtag.touristic_route_id')
            ->join('touristic_route_photo', 'touristic_route_photo.touristic_route_id', '=', 'touristic_route.id')
            ->whereIn('touristic_route_hashtag.hashtag_id', $p_HashtagIds)
            ->where('touristic_route_photo.is_cover', 1)
            ->select(['touristic_route.id', 'touristic_route.nome', 'touristic_route.descricao_curta', 'touristic_route.slug', 'touristic_route_photo.url']);
        if(!empty($p_HashtagIds)){
            $v_Query->orderByRaw('FIELD(touristic_route_hashtag.hashtag_id, ' . implode(',', $p_HashtagIds) . ')');
        }
        return $v_Query->orderBy('touristic_route.destaque', 'desc')
            ->get()
            ->unique('id')
            ->take($p_Quantity);
    }
}
