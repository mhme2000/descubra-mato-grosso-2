<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class CuisineNationality extends Model
{
    public $timestamps = false;
    protected $table = 'cuisine_nationality';

    public static $m_Rules = array
    (
        'nome' => 'required|min:1|max:200'
    );

    public static $m_RulesEdit = array
    (
        'id' => 'required|numeric|min:1',
        'nome' => 'required|min:1|max:200'
    );

    public static function post($p_Id, $p_Name)
    {
        $v_CuisineNationality = CuisineNationality::findOrNew($p_Id);
        $v_CuisineNationality->nome_pt = $p_Name;
        $v_CuisineNationality->save();
    }

    public static function getList()
    {
        return CuisineNationality::orderBy('nome_pt')->lists('nome_pt', 'id')->toArray();
    }

    public static function getLanguageList($p_Lang)
    {
        return CuisineNationality::orderBy('nome_' . $p_Lang)->lists('nome_' . $p_Lang, 'id')->toArray();
    }
}