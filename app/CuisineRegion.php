<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class CuisineRegion extends Model
{
    public $timestamps = false;
    protected $table = 'cuisine_region';

    public static $m_Rules = array
    (
        'nome' => 'required|min:1|max:200'
    );

    public static $m_RulesEdit = array
    (
        'id' => 'required|numeric|min:1',
        'nome' => 'required|min:1|max:200'
    );

    public static function post($p_Id, $p_Name)
    {
        $v_CuisineRegion = CuisineRegion::findOrNew($p_Id);
        $v_CuisineRegion->nome_pt = $p_Name;
        $v_CuisineRegion->save();
    }

    public static function getList()
    {
        return CuisineRegion::orderBy('nome_pt')->lists('nome_pt', 'id')->toArray();
    }

    public static function getLanguageList($p_Lang)
    {
        return CuisineRegion::orderBy('nome_' . $p_Lang)->lists('nome_' . $p_Lang, 'id')->toArray();
    }
}