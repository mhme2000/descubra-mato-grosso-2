<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class SupplyType extends Model
{
    public $timestamps = false;
    protected $table = 'supply_type';

    public static $m_Rules = array
    (
        'nome' => 'required|min:1|max:250'
    );

    public static $m_RulesEdit = array
    (
        'id' => 'required|numeric|min:1',
        'nome' => 'required|min:1|max:250'
    );

    public static function post($p_Id, $p_Type, $p_Name)
    {
        $v_SupplyType = SupplyType::findOrNew($p_Id);
        $v_SupplyType->tipo = $p_Type;
        $v_SupplyType->nome = $p_Name;
        $v_SupplyType->save();
    }

    public static function getList($p_Type)
    {
        return SupplyType::where('tipo', $p_Type)->orderBy('nome')->lists('nome', 'id')->toArray();
    }
}