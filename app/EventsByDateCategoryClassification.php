<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventsByDateCategoryClassification extends Model
{
    protected $table = 'events_by_date_category_classification';

    //Relacionamento de 1 para 1
    public function Event(){
        return $this->hasOne(Event::class, 'id', 'event_id');
    }

    public function filter(Array $data, $paginate = 10) {
        return $this->where(function($query) use ($data) {
                            if (isset($data['event_id']))
                                $query->where('event_id', '=', $data['event_id']);
                            
                            if (isset($data['event']))
                                $query->where('name', 'like', "%$data[event]%");

                            if (isset($data['city']) && $data['city'] != 0)
                                $query->where('city_id', $data['city']);


                            if (isset($data['initialDate']) && isset($data['finalDate'])){
                                $query->where('start', '>=', $data['initialDate'].' 00:00:01');
                                $query->where('end', '<=', $data['finalDate'].' 23:59:59');

                            } elseif(isset($data['initialDate'])){
                                $query->where('start', '>=', $data['initialDate'].' 00:00:01');
                            }

                            if (isset($data['category_id']) && $data['category_id'] > 0){
                                $query->where('event_category_id', '=', $data['category_id']);   
                            }

                            if (isset($data['status'])){
                                $query->where('revision_status_id', $data['status']);
                            }
                            
                        })->groupby('event_id')
                        ->distinct('event_id')
                        ->orderBy('start', 'asc')
                        ->paginate($paginate);
    }
}
