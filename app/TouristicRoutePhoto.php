<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Intervention\Image\Facades\Image;

class TouristicRoutePhoto extends Model
{
    public $timestamps = false;
    protected $table = 'touristic_route_photo';

    public static function updatePhotos($p_RouteId, $p_Photos, $p_PhotoInfo, $p_DeletedPhotos)
    {
        $v_Path = public_path() . '/imagens/';
        if(!\File::exists($v_Path))
            \File::makeDirectory($v_Path);
        $v_Path .=  '/roteiros/';
        if(!\File::exists($v_Path))
            \File::makeDirectory($v_Path);

        foreach($p_DeletedPhotos as $c_PhotoId)
        {
            $v_Photo = TouristicRoutePhoto::find($c_PhotoId);
            $v_OldFileName = explode('/', $v_Photo->url);
            $v_OldFileName = array_pop($v_OldFileName);
            $v_Photo->delete();
            \File::delete($v_Path . $v_OldFileName);
        }
        foreach($p_Photos as $c_Index => $c_File)
        {
            if($c_File != null)
            {
                $v_Photo = TouristicRoutePhoto::findOrNew($p_PhotoInfo['id'][$c_Index]);
                $v_PhotoExtension = 'jpg';
                if($p_PhotoInfo['type'][$c_Index] == 'cover')
                {
                    $v_Photo->is_cover = 1;
                    $v_Photo->is_map = 0;
                    $v_Photo->is_testimonial = 0;
                }
                else if($p_PhotoInfo['type'][$c_Index] == 'map')
                {
                    $v_Photo->is_cover = 0;
                    $v_Photo->is_map = 1;
                    $v_Photo->is_testimonial = 0;
                    $v_PhotoExtension = 'png';
                }
                else if($p_PhotoInfo['type'][$c_Index] == 'testimonial')
                {
                    $v_Photo->is_cover = 0;
                    $v_Photo->is_map = 0;
                    $v_Photo->is_testimonial = 1;
                }
                else
                {
                    $v_Photo->is_cover = 0;
                    $v_Photo->is_map = 0;
                    $v_Photo->is_testimonial = 0;
                }

                $v_PhotoName =  time() . str_random(10) . '.' . $v_PhotoExtension;
                $v_Image = Image::make($c_File);
                $v_Image->widen(1920, function ($constraint){
                    $constraint->upsize();
                });
                if($v_PhotoExtension == 'jpg')
                    $v_Image->encode('jpg');
                $v_Image->save($v_Path . $v_PhotoName);
                if ($v_Photo->url != null)
                {
                    $v_OldFileName = explode('/', $v_Photo->url);
                    $v_OldFileName = array_pop($v_OldFileName);
                    \File::delete($v_Path . $v_OldFileName);
                }
                $v_Photo->url = url('/imagens/roteiros/' . $v_PhotoName);
                $v_Photo->touristic_route_id = $p_RouteId;
                $v_Photo->save();
            }
        }
    }

    public static function getCoverPhoto($p_RouteId)
    {
        return TouristicRoutePhoto::where('is_cover', 1)->where('touristic_route_id', $p_RouteId)->first();
    }

    public static function getMapPhoto($p_RouteId)
    {
        return TouristicRoutePhoto::where('is_map', 1)->where('touristic_route_id', $p_RouteId)->first();
    }

    public static function getTestimonialPhoto($p_RouteId)
    {
        return TouristicRoutePhoto::where('is_testimonial', 1)->where('touristic_route_id', $p_RouteId)->first();
    }

    public static function getPhotoGallery($p_RouteId)
    {
        return TouristicRoutePhoto::where('is_cover', 0)->where('is_map', 0)->where('is_testimonial', 0)
                               ->where('touristic_route_id', $p_RouteId)->get();
    }
}