<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class TouristicRouteDestinations extends Model
{
    public $timestamps = false;
    protected $table = 'touristic_route_destinations';

    public static function updateTouristicRouteDestinations($p_TouristicRouteId, $p_Destinations)
    {
        TouristicRouteDestinations::where('touristic_route_id', $p_TouristicRouteId)->delete();
        foreach($p_Destinations as $c_Destination)
        {
            $v_TouristicRouteCity = new TouristicRouteDestinations();
            $v_TouristicRouteCity->touristic_route_id = $p_TouristicRouteId;
            $v_TouristicRouteCity->destination_id = $c_Destination;
            $v_TouristicRouteCity->save();
        }
    }

    public static function getSelectedDestinations($p_TouristicRouteId)
    {
        return TouristicRouteDestinations::where('touristic_route_id', $p_TouristicRouteId)->lists('destination_id')->toArray();
    }

    public static function getTouristicRouteDestinations($p_TouristicRouteId)
    {
        return TouristicRouteDestinations::join('destination', 'destination.id', '=', 'touristic_route_destinations.destination_id')
            ->join('destination_photo', 'destination_photo.destination_id', '=', 'destination.id')
            ->where('touristic_route_id', $p_TouristicRouteId)
            ->where('destination.publico', 1)
            ->where('destination_photo.is_cover', 1)
            ->select(['destination.city_id', 'destination.nome', 'destination.slug', 'destination.descricao_curta', 'destination_photo.url'])
            ->orderBy('destination.destaque', 'desc')
            ->get();
    }

    public static function getTouristicRouteDestinationsIdList($p_TouristicRouteId)
    {
        return TouristicRouteDestinations::join('destination', 'destination.id', '=', 'touristic_route_destinations.destination_id')
            ->where('touristic_route_id', $p_TouristicRouteId)
            ->where('destination.publico', 1)
            ->lists('touristic_route_id', 'destination.id')->toArray();
    }
}