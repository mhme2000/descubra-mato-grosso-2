<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use \Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class BlogArticle extends Model
{
    protected $table = 'blog_article';
    protected $guarded = [];

    public static $m_Rules = array
    (
        'foto_capa' => 'required|image'
    );

    public static $m_RulesEdit = array
    (
        'id' => 'required|numeric|min:1',
        'foto_capa' => 'image'
    );

    public static function getDT($p_CreatedAt, $p_UpdatedAt, $p_Title, $p_Status, $p_Order, $p_Start, $p_Length, $p_Draw)
    {
        $v_Query = BlogArticle::select(DB::raw('SQL_CALC_FOUND_ROWS blog_article.id, blog_article.created_at, blog_article.updated_at, blog_article.titulo_pt, blog_article.active'));

        if($p_CreatedAt != '')
        {
            $v_StartDate = Carbon::createFromFormat('d/m/Y', substr($p_CreatedAt, 0, 10));
            $v_Query->where('blog_article.created_at', '>=', $v_StartDate->startOfDay()->format('Y-m-d H:i:s'));
            $v_EndDate = Carbon::createFromFormat('d/m/Y', substr($p_CreatedAt, 13, 23));
            $v_Query->where('blog_article.created_at', '<=', $v_EndDate->endOfDay()->format('Y-m-d H:i:s'));
        }
        if($p_UpdatedAt != '')
        {
            $v_StartDate = Carbon::createFromFormat('d/m/Y', substr($p_UpdatedAt, 0, 10));
            $v_Query->where('blog_article.updated_at', '>=', $v_StartDate->startOfDay()->format('Y-m-d H:i:s'));
            $v_EndDate = Carbon::createFromFormat('d/m/Y', substr($p_UpdatedAt, 13, 23));
            $v_Query->where('blog_article.updated_at', '<=', $v_EndDate->endOfDay()->format('Y-m-d H:i:s'));
        }

        if($p_Title != '')
            $v_Query->where('blog_article.titulo_pt', 'LIKE', '%' . $p_Title . '%');

        if($p_Status != '')
            $v_Query->where('blog_article.active',  $p_Status);

        if($p_Order != null)
        {
            if($p_Order["column"] == 0)
                $v_Query->orderBy('blog_article.created_at', $p_Order["dir"]);
            if($p_Order["column"] == 1)
                $v_Query->orderBy('blog_article.updated_at', $p_Order["dir"]);
            if($p_Order["column"] == 2)
                $v_Query->orderBy('blog_article.titulo_pt', $p_Order["dir"]);
            if($p_Order["column"] == 3)
                $v_Query->orderBy('blog_article.active', $p_Order["dir"]);
        }

        if($p_Length != -1)
            $v_Query->take($p_Length)->skip($p_Start);

        $v_QueryRes = $v_Query->get()->toArray();
        $v_Data = [];
        for($c_Index = 0 ; $c_Index < sizeof($v_QueryRes) ; $c_Index++)
        {
            array_push($v_Data, [
                Carbon::createFromFormat('Y-m-d H:i:s', $v_QueryRes[$c_Index]['created_at'])->format('d/m/Y'),
                Carbon::createFromFormat('Y-m-d H:i:s', $v_QueryRes[$c_Index]['updated_at'])->format('d/m/Y H:i'),
                $v_QueryRes[$c_Index]['titulo_pt'],
                $v_QueryRes[$c_Index]['active']==1? 'Ativo' : 'Desativado',
                '<div class="actions-div">' .
                    '<a href="' . url('admin/blog/editar/' . $v_QueryRes[$c_Index]['id']) . '" title="Editar" type="button" class="btn btn-success"><i class="fa fa-edit"></i></a>' .
                    '<a href="' . url('admin/blog/desativar/' . $v_QueryRes[$c_Index]['id']) . '" title="' . ($v_QueryRes[$c_Index]['active']==1? 'Desativar' : 'Ativar') . '" type="button" class="btn btn-success"><i class="fa fa-' . ($v_QueryRes[$c_Index]['active']==1? 'times' : 'check') . '"></i></a>' .
                    '<a href="' . url('admin/blog/excluir/' . $v_QueryRes[$c_Index]['id']) . '" title="Excluir" type="button" class="btn btn-success delete-btn"><i class="fa fa-trash-o"></i></a>' .
                '</div>'
            ]);
        }

        $v_DataTableAjax = new \stdClass();
        $v_DataTableAjax->draw = $p_Draw;
        $v_DataTableAjax->recordsFiltered = BlogArticle::getTotalRows();
        $v_DataTableAjax->recordsTotal = BlogArticle::count();
        $v_DataTableAjax->data = $v_Data;
        return json_encode($v_DataTableAjax);
    }

    public static function getTotalRows()
    {
        return DB::select(DB::raw("SELECT FOUND_ROWS() AS total_rows"))[0]->total_rows;
    }

    public static function post($p_Id, $p_CoverPhoto, $p_Data)
    {
        $v_Path = public_path() . '/imagens/';
        if(!\File::exists($v_Path))
            \File::makeDirectory($v_Path);
        $v_Path .=  '/blog/';
        if(!\File::exists($v_Path))
            \File::makeDirectory($v_Path);

        $v_ArticleData = $p_Data['formulario'];
        $v_PhotoUrl = null;
        if($p_CoverPhoto != null)
        {
            $v_PhotoName =  time() . str_random(10) . '.jpg';
            $p_CoverPhoto->widen(1920, function ($constraint){
                $constraint->upsize();
            });
            $p_CoverPhoto->encode('jpg')->save($v_Path . $v_PhotoName);
            $v_PhotoUrl = url('/imagens/blog/' . $v_PhotoName);
        }

        $v_Article = BlogArticle::findOrNew($p_Id);
        if($v_PhotoUrl != null)
        {
            if($v_Article->foto_capa_url != null)
            {
                $v_OldFileName = explode('/', $v_Article->foto_capa_url);
                $v_OldFileName = array_pop($v_OldFileName);
                \File::delete($v_Path . $v_OldFileName);
            }
            $v_ArticleData['foto_capa_url'] = $v_PhotoUrl;
        }

        if($p_Id == null)
            $v_ArticleData['slug'] = BlogArticle::generateSlug($v_ArticleData['titulo_pt']);
        $v_Article = BlogArticle::updateOrCreate(['id' => $p_Id], $v_ArticleData);
        Hashtag::updateHashtag('blog', $v_Article->id, $p_Data['hashtags']);
    }

    public static function generateSlug($p_String)
    {
        $v_Slug = Str::slug($p_String);
        if(BlogArticle::where('slug', $v_Slug)->count() == 0)
            return $v_Slug;

        $v_UniqueSlug = false;
        $v_Index = 0;
        while(!$v_UniqueSlug){
            if(BlogArticle::where('slug', $v_Slug . '-' . $v_Index)->count() == 0)
                $v_UniqueSlug = true;
            else
                $v_Index++;
        }

        return $v_Slug . '-' . $v_Index;
    }

    public static function getArticles($p_Quantity)
    {
        return BlogArticle::where('active', 1)
                          ->select(['id', 'created_at', 'foto_capa_url', 'slug', 'titulo', 'descricao_curta'])
                          ->orderBy('created_at', 'desc')
                          ->take($p_Quantity)
                          ->get();
    }

    public static function getArticlesPagination($p_Quantity)
    {
        return BlogArticle::where('active', 1)
                          ->select(['id', 'created_at', 'foto_capa_url', 'slug', 'titulo', 'descricao_curta'])
                          ->orderBy('created_at', 'desc')
                          ->paginate($p_Quantity);
    }

    public static function getArticlesFilter($p_HashtagIds, $p_Quantity, $p_ExceptId = null)
    {
        $v_Query = BlogArticle::join('blog_article_hashtag', 'blog_article_hashtag.blog_article_id', '=', 'blog_article.id')
                              ->whereIn('blog_article_hashtag.hashtag_id', $p_HashtagIds);
        if($p_ExceptId != null)
            $v_Query->where('blog_article.id', '!=', $p_ExceptId);

        if(!empty($p_HashtagIds)){
            $v_Query->orderByRaw('FIELD(blog_article_hashtag.hashtag_id, ' . implode(',', $p_HashtagIds) . ')');
        }

        return $v_Query->select(['blog_article.id', 'blog_article.created_at', 'blog_article.foto_capa_url',
                'blog_article.slug', 'blog_article.titulo', 'blog_article.descricao_curta'])
                       ->orderBy('blog_article.created_at', 'desc')
                       ->get()
                       ->unique('id')
                       ->take($p_Quantity);
    }

    public static function getArticlesArchive($p_Month, $p_Quantity)
    {
        return BlogArticle::where('blog_article.created_at', 'LIKE', $p_Month . '%')
                          ->select(['blog_article.id', 'blog_article.created_at', 'blog_article.foto_capa_url',
                              'blog_article.slug', 'blog_article.titulo', 'blog_article.descricao_curta'])
                          ->orderBy('blog_article.created_at', 'desc')
                          ->groupBy('blog_article.id')->take($p_Quantity)
                          ->get();
    }

    public static function getArticle($p_Slug)
    {
        return BlogArticle::where('blog_article.slug', $p_Slug)
                          ->where('blog_article.active', 1)
                          ->select(['blog_article.id', 'blog_article.created_at', 'blog_article.foto_capa_url',
                              'blog_article.slug', 'blog_article.titulo', 'blog_article.descricao_curta', 'blog_article.conteudo'])
                          ->firstOrFail();
    }

    public static function getArticleHashtags($p_Id)
    {
        return Hashtag::join('blog_article_hashtag', 'blog_article_hashtag.hashtag_id', '=', 'hashtag.id')
            ->where('blog_article_hashtag.blog_article_id', $p_Id)
            ->select(['hashtag.name', 'hashtag.id'])
            ->get();
    }

    public static function deleteArticle($p_Id)
    {
        $v_Article = BlogArticle::findOrFail($p_Id);
        if($v_Article->foto_capa_url != null)
        {
            $v_Path = public_path() . '/imagens/blog/';
            $v_FileName = explode('/', $v_Article->foto_capa_url);
            $v_FileName = array_pop($v_FileName);
            \File::delete($v_Path . $v_FileName);
        }
        $v_Article->delete();
    }

    public static function getArchiveMonths()
    {
        return BlogArticle::where('blog_article.active', 1)
                          ->select(DB::raw('DATE_FORMAT(created_at, "%m/%Y") as month_year, DATE_FORMAT(created_at, "%m") as month, DATE_FORMAT(created_at, "%Y") as year'))
                          ->groupBy('month_year')
                          ->orderBy('created_at')
                          ->get();
    }

    public static function getArchiveYears()
    {
        return BlogArticle::where('blog_article.active', 1)
                          ->select(DB::raw('DATE_FORMAT(created_at, "%Y") as year'))
                          ->groupBy('year')
                          ->orderBy('created_at', 'DESC')
                          ->lists('year')->toArray();
    }
}