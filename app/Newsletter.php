<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;

class Newsletter extends Model
{
    public $timestamps = false;
    protected $table = 'newsletter';

    public static $m_Rules = array
    (
        'email' => 'required|email|unique:newsletter,email'
    );

    public static $m_UnsubscribeRules = array
    (
        'email' => 'required|email'
    );

    public static function post($p_Email)
    {
        $v_Subscriber = new Newsletter();
        $v_Subscriber->email = $p_Email;
        $v_Subscriber->save();
    }

    public static function unsubscribe($p_Email)
    {
        Newsletter::where('email', $p_Email)->delete();
    }

    public static function sendEmail($p_Subject, $p_Message)
    {
        $v_Subscribers = Newsletter::all();
        foreach($v_Subscribers as $c_Subscriber)
        {
            Mail::send('emails.newsletter', ['p_Message' => $p_Message, 'p_Email' => $c_Subscriber->email], function ($message) use ($c_Subscriber, $p_Subject)
            {
                $message->to($c_Subscriber->email, '')->subject($p_Subject);
            });
        }
    }
}