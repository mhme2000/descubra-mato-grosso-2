<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class PoliticalParty extends Model
{
    public $timestamps = false;
    protected $table = 'political_party';

    public static $m_Rules = array
    (
        'nome' => 'required|min:1|max:200',
        'sigla' => 'required|min:1|max:10'
    );

    public static $m_RulesEdit = array
    (
        'id' => 'required|numeric|min:1',
        'nome' => 'required|min:1|max:200',
        'sigla' => 'required|min:1|max:10'
    );

    public static function post($p_Id, $p_Abbreviation, $p_Name)
    {
        $v_Party = PoliticalParty::findOrNew($p_Id);
        $v_Party->abbreviation = $p_Abbreviation;
        $v_Party->name = $p_Name;
        $v_Party->save();
    }

    public static function getList()
    {
        return [''=>''] +PoliticalParty::orderBy('abbreviation')->lists('abbreviation', 'id')->toArray();
    }
}