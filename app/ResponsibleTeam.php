<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use \Carbon\Carbon;

class ResponsibleTeam extends Model
{
    protected $table = 'responsible_team';

    protected $guarded = [];

    public static function getDT($v_Responsible, $v_Institution, $v_City, $p_UpdatedAt, $p_Order, $p_Start, $p_Length, $p_Draw)
    {
        $v_Query = ResponsibleTeam::join('city', 'responsible_team.city_id', '=', 'city.id')
                                  ->selectRaw('SQL_CALC_FOUND_ROWS responsible_team.id, responsible_team.responsavel, responsible_team.instituicao, city.name as city_name, responsible_team.updated_at');

        if(UserType::isMunicipio())
            $v_Query->where('city.id', Auth::user()->city_id);
        else if(UserType::isCircuito())
            $v_Query->whereIn('city.id', TouristicCircuitCities::getUserCircuitCities());

        if($v_Responsible != '')
            $v_Query->where('responsible_team.responsavel', 'LIKE', '%' . $v_Responsible . '%');

        if($v_Institution != '')
            $v_Query->where('responsible_team.instituicao', 'LIKE', '%' . $v_Institution . '%');

        if($v_City != '')
            $v_Query->where('city.name', 'LIKE', '%' . $v_City . '%');

        if($p_UpdatedAt != '')
        {
            $v_StartDate = Carbon::createFromFormat('d/m/Y', substr($p_UpdatedAt, 0, 10));
            $v_Query->where('responsible_team.updated_at', '>=', $v_StartDate->startOfDay()->format('Y-m-d H:i:s'));
            $v_EndDate = Carbon::createFromFormat('d/m/Y', substr($p_UpdatedAt, 13, 23));
            $v_Query->where('responsible_team.updated_at', '<=', $v_EndDate->endOfDay()->format('Y-m-d H:i:s'));
        }

        if($p_Order != null)
        {
            if($p_Order["column"] == 0)
                $v_Query->orderBy('responsible_team.responsavel', $p_Order["dir"]);
            if($p_Order["column"] == 1)
                $v_Query->orderBy('responsible_team.instituicao', $p_Order["dir"]);
            if($p_Order["column"] == 2)
                $v_Query->orderBy('city.name', $p_Order["dir"]);
            if($p_Order["column"] == 3)
                $v_Query->orderBy('responsible_team.updated_at', $p_Order["dir"]);
        }

        if($p_Length != -1)
            $v_Query->take($p_Length)->skip($p_Start);

        $v_QueryRes = $v_Query->get()->toArray();
        $v_Data = [];

        for($c_Index = 0 ; $c_Index < sizeof($v_QueryRes) ; $c_Index++)
        {
            array_push($v_Data, [
                $v_QueryRes[$c_Index]['responsavel'],
                $v_QueryRes[$c_Index]['instituicao'],
                $v_QueryRes[$c_Index]['city_name'],
                Carbon::createFromFormat('Y-m-d H:i:s', $v_QueryRes[$c_Index]['updated_at'])->format('d/m/Y - H:i'),
                '<div class="actions-div">' .
                    '<a href="' . url('admin/equipes-responsaveis/editar/' . $v_QueryRes[$c_Index]['id']) . '" title="Editar" type="button" class="btn btn-success"><i class="fa fa-edit"></i></a>' .
                    '<a href="' . url('admin/equipes-responsaveis/excluir/' . $v_QueryRes[$c_Index]['id']) . '" title="Excluir" type="button" class="btn btn-success delete-btn"><i class="fa fa-trash-o"></i></a>' .
                '</div>'
            ]);
        }

        $v_DataTableAjax = new \stdClass();
        $v_DataTableAjax->draw = $p_Draw;
        $v_DataTableAjax->recordsFiltered = ResponsibleTeam::getTotalRows();

        $v_DataTableAjax->recordsTotal = ResponsibleTeam::count();

        $v_DataTableAjax->data = $v_Data;
        return json_encode($v_DataTableAjax);
    }

    public static function getTotalRows()
    {
        return DB::select(DB::raw("SELECT FOUND_ROWS() AS total_rows"))[0]->total_rows;
    }

    public static function post($p_Id, $p_Data)
    {
        ResponsibleTeam::updateOrCreate(['id' => $p_Id], $p_Data['responsible_team']);
    }

    public static function getTeamsData()
    {
        $v_Query = ResponsibleTeam::join('city', 'city.id', '=', 'responsible_team.city_id')
                                  ->selectRaw('responsible_team.*, city.name as cidade');
        if(UserType::isCircuito())
            $v_Query->whereIn('city_id', TouristicCircuitCities::getUserCircuitCities());
        else if(UserType::isMunicipio())
            $v_Query->where('city_id', Auth::user()->city_id);

        return $v_Query->get();
    }
}