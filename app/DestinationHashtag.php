<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class DestinationHashtag extends Model
{
    public $timestamps = false;
    protected $table = 'destination_hashtag';

    public static function deleteDestinationHashtags($p_DestinationId)
    {
        DestinationHashtag::where('destination_id', $p_DestinationId)->delete();
    }

    public static function addDestinationHashtag($p_DestinationId, $p_HashtagId)
    {
        $v_DestinationHashtag = new DestinationHashtag();
        $v_DestinationHashtag->destination_id = $p_DestinationId;
        $v_DestinationHashtag->hashtag_id = $p_HashtagId;
        $v_DestinationHashtag->save();
    }

    public static function getList($p_DestinationId)
    {
        return DestinationHashtag::join('hashtag', 'hashtag.id', '=', 'destination_hashtag.hashtag_id')
            ->where('destination_id', $p_DestinationId)->lists('hashtag.name', 'hashtag.name')->toArray();
    }

    public static function getHashtagDestinations($p_HashtagIds, $p_Quantity)
    {
        $v_Query = DestinationHashtag::join('destination', 'destination.id', '=', 'destination_hashtag.destination_id')
            ->join('destination_photo', 'destination_photo.destination_id', '=', 'destination.id')
            ->whereIn('destination_hashtag.hashtag_id', $p_HashtagIds)
            ->where('destination.publico', 1)
            ->where('destination_photo.is_cover', 1)
            ->select(['destination.id', 'destination.nome', 'destination.slug', 'destination.descricao_curta',
                'destination_photo.url']);
        if(!empty($p_HashtagIds)){
            $v_Query->orderByRaw('FIELD(destination_hashtag.hashtag_id, ' . implode(',', $p_HashtagIds) . ')');
        }
        return $v_Query->orderBy('destination.destaque', 'desc')
            ->get()
            ->unique('id')
            ->take($p_Quantity);
    }

    public static function getHashtagCityIds($p_HashtagIds)
    {
        return DestinationHashtag::join('destination', 'destination.id', '=', 'destination_hashtag.destination_id')
            ->whereIn('destination_hashtag.hashtag_id', $p_HashtagIds)
            ->groupBy('destination.id')
            ->lists('destination.city_id')->toArray();
    }
}
