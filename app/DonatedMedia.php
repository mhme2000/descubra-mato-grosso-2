<?php
namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request;

class DonatedMedia extends Model
{
    protected $table = 'donated_media';
    protected $guarded = [];

    public static $m_Rules = array
    (
        'nome' => 'required|min:1|max:250',
        'email' => 'required|email',
        'documento' => 'required|min:14|max:18',
        'telefone' => 'required',
        'permite_compartilhar' => 'required|numeric',
        'permite_uso_comercial' => 'required|numeric',
        'city_id' => 'required|numeric|min:1',
        'descricao' => 'required|min:1|max:250',
        'tipo_midia' => 'required'
    );

    public static function getDT($p_Type, $p_CreatedAt, $p_City, $p_Description, $p_Name, $p_Phone, $p_Order, $p_Start, $p_Length, $p_Draw)
    {
        $v_Query = DonatedMedia::leftJoin('city', 'city.id', '=', 'donated_media.city_id')->selectRaw('SQL_CALC_FOUND_ROWS donated_media.*, city.name as cidade');

        if($p_Type != '')
            $v_Query->where('tipo_midia', $p_Type);

        if($p_CreatedAt != '')
        {
            $v_StartDate = Carbon::createFromFormat('d/m/Y', substr($p_CreatedAt, 0, 10));
            $v_Query->where('created_at', '>=', $v_StartDate->startOfDay()->format('Y-m-d H:i:s'));
            $v_EndDate = Carbon::createFromFormat('d/m/Y', substr($p_CreatedAt, 13, 23));
            $v_Query->where('created_at', '<=', $v_EndDate->endOfDay()->format('Y-m-d H:i:s'));
        }

        if($p_City != '')
            $v_Query->where('city.name', 'LIKE', '%' . $p_City . '%');

        if($p_Description != '')
            $v_Query->where('descricao', 'LIKE', '%' . $p_Description . '%');

        if($p_Name != '')
            $v_Query->where('nome', 'LIKE', '%' . $p_Name . '%');

        if($p_Phone != '')
            $v_Query->where('telefone', 'LIKE', '%' . $p_Phone . '%');

        if($p_Order != null)
        {
            if($p_Order["column"] == 0)
                $v_Query->orderBy('tipo_midia', $p_Order["dir"]);
            if($p_Order["column"] == 1)
                $v_Query->orderBy('created_at', $p_Order["dir"]);
            if($p_Order["column"] == 2)
                $v_Query->orderBy('city.name', $p_Order["dir"]);
            if($p_Order["column"] == 3)
                $v_Query->orderBy('descricao', $p_Order["dir"]);
            if($p_Order["column"] == 4)
                $v_Query->orderBy('nome', $p_Order["dir"]);
            if($p_Order["column"] == 5)
                $v_Query->orderBy('telefone', $p_Order["dir"]);
        }

        if($p_Length != -1)
            $v_Query->take($p_Length)->skip($p_Start);

        $v_QueryRes = $v_Query->get()->toArray();
        $v_Data = [];
        for($c_Index = 0 ; $c_Index < sizeof($v_QueryRes) ; $c_Index++)
        {
            array_push($v_Data, [
                $v_QueryRes[$c_Index]['tipo_midia'],
                Carbon::createFromFormat('Y-m-d H:i:s', $v_QueryRes[$c_Index]['created_at'])->format('d/m/y'),
                $v_QueryRes[$c_Index]['cidade'],
                $v_QueryRes[$c_Index]['descricao'],
                $v_QueryRes[$c_Index]['nome'],
                $v_QueryRes[$c_Index]['telefone'],
                '<div class="actions-div">' .
                    '<a href="' . $v_QueryRes[$c_Index]['url'] . '" target="_blank" title="Baixar" download type="button" class="btn btn-success"><i class="fa fa-download"></i></a>' .
                    '<a href="' . url('admin/doacoes/termo/' . $v_QueryRes[$c_Index]['id']) . '" target="_blank" title="Termo de doação" type="button" class="btn btn-success"><i class="fa fa-file-text-o"></i></a>' .
                    '<a href="' . url('admin/doacoes/excluir/' . $v_QueryRes[$c_Index]['id']) . '" title="Excluir" type="button" class="btn btn-success delete-btn"><i class="fa fa-trash-o"></i></a>' .
                '</div>'
            ]);
        }

        $v_DataTableAjax = new \stdClass();
        $v_DataTableAjax->draw = $p_Draw;
        $v_DataTableAjax->recordsFiltered = DonatedMedia::getTotalRows();
        $v_DataTableAjax->recordsTotal = DonatedMedia::count();
        $v_DataTableAjax->data = $v_Data;
        return json_encode($v_DataTableAjax);
    }

    public static function getTotalRows()
    {
        return DB::select(DB::raw("SELECT FOUND_ROWS() AS total_rows"))[0]->total_rows;
    }

    public static function post($p_Data, $p_File)
    {
        $v_Path = public_path() . '/imagens/';
        if(!\File::exists($v_Path))
            \File::makeDirectory($v_Path);
        $v_Path .=  '/doacoes/';
        if(!\File::exists($v_Path))
            \File::makeDirectory($v_Path);

        if ($p_File != null && $p_File->isValid())
        {
            $p_Data['ip'] = Request::getClientIp();
            $p_Data['nome_original_arquivo'] = $p_File->getClientOriginalName();

            $v_Extension = $p_File->getClientOriginalExtension();
            $v_FileName = time() . str_random(10) . '.' . $v_Extension;
            $p_File->move($v_Path, $v_FileName);

            $p_Data['url'] = url('/imagens/doacoes/' . $v_FileName);
            DonatedMedia::create($p_Data);
        }
    }

    public static function deleteMedia($p_Id)
    {
        $v_Path = public_path() . '/imagens/';
        if(!\File::exists($v_Path))
            \File::makeDirectory($v_Path);
        $v_Path .=  '/doacoes/';
        if(!\File::exists($v_Path))
            \File::makeDirectory($v_Path);

        $v_Donation = DonatedMedia::findOrFail($p_Id);
        $v_FileName = explode('/', $v_Donation->url);
        $v_FileName = array_pop($v_FileName);
        $v_Donation->delete();
        \File::delete($v_Path . $v_FileName);
    }

    public static function getReport($p_Data)
    {
        $v_Type = $p_Data['tipo_midia'];
        $v_CreatedAt = $p_Data['created_at'];
        $v_City = $p_Data['municipio'];
        $v_Description = $p_Data['descricao'];
        $v_Name = $p_Data['nome'];
        $v_Phone = $p_Data['telefone'];

        $v_Query = DonatedMedia::leftJoin('city', 'city.id', '=', 'donated_media.city_id')->selectRaw('donated_media.*, city.name as cidade');

        if($v_Type != '')
            $v_Query->where('tipo_midia', $v_Type);

        if($v_CreatedAt != '')
        {
            $v_StartDate = Carbon::createFromFormat('d/m/Y', substr($v_CreatedAt, 0, 10));
            $v_Query->where('created_at', '>=', $v_StartDate->startOfDay()->format('Y-m-d H:i:s'));
            $v_EndDate = Carbon::createFromFormat('d/m/Y', substr($v_CreatedAt, 13, 23));
            $v_Query->where('created_at', '<=', $v_EndDate->endOfDay()->format('Y-m-d H:i:s'));
        }

        if($v_City != '')
            $v_Query->where('city.name', 'LIKE', '%' . $v_City . '%');

        if($v_Description != '')
            $v_Query->where('descricao', 'LIKE', '%' . $v_Description . '%');

        if($v_Name != '')
            $v_Query->where('nome', 'LIKE', '%' . $v_Name . '%');

        if($v_Phone != '')
            $v_Query->where('telefone', 'LIKE', '%' . $v_Phone . '%');

        $v_AllowSharingOptions = [0 => 'Não',
                                  1 => 'Sim',
                                  2 => 'Sim, desde que outros compartilhem igual'];

        $v_Results = $v_Query->get();

        $v_TemplateFile = public_path() . '/templates-relatorios/RelatorioDoacaoMidias.xlsx';
        \Excel::load($v_TemplateFile, function($v_Template) use($v_Results, $v_AllowSharingOptions){
            $v_Template->sheet('Relatorio', function($sheet) use($v_Results, $v_AllowSharingOptions) {
                $v_CurrentRow = 2;
                foreach($v_Results as $c_Result)
                {
                    $v_Data = [];
                    array_push($v_Data, $c_Result->tipo_midia);
                    array_push($v_Data, $v_AllowSharingOptions[$c_Result->permite_compartilhar]);
                    array_push($v_Data, $c_Result->permite_uso_comercial == 1 ? 'Sim' : 'Não');
                    array_push($v_Data, Carbon::createFromFormat('Y-m-d H:i:s', $c_Result->created_at)->format('d/m/Y'));
                    array_push($v_Data, $c_Result->cidade);
                    array_push($v_Data, $c_Result->descricao);
                    array_push($v_Data, $c_Result->pessoa_juridica == 1 ? 'CNPJ' : 'CPF');
                    array_push($v_Data, $c_Result->documento);
                    array_push($v_Data, $c_Result->nome);
                    array_push($v_Data, $c_Result->email);
                    array_push($v_Data, $c_Result->telefone);
                    array_push($v_Data, $c_Result->url);

                    $sheet->row($v_CurrentRow,$v_Data);
                    $v_CurrentRow++;
                }
                $sheet->setBorder('A2:L'.(count($v_Results)+1), 'thin');
            });
        })->download('xlsx');
    }
}