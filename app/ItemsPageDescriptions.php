<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemsPageDescriptions extends Model
{
    public $timestamps = false;
    protected $table = 'items_page_descriptions';

    public static function post($p_Data)
    {
        foreach($p_Data['language'] as $c_Index => $p_Language)
        {
            $v_ItemsPageDescriptions = ItemsPageDescriptions::findOrNew($p_Data['id'][$c_Index]);
            $v_ItemsPageDescriptions->language = $p_Language;
            $v_ItemsPageDescriptions->pagina = $p_Data['pagina'][$c_Index];
            $v_ItemsPageDescriptions->descricao = $p_Data['descricao'][$c_Index];
            $v_ItemsPageDescriptions->descricao_outra = $p_Data['descricao_outra'][$c_Index];
            $v_ItemsPageDescriptions->save();
        }
    }

    public static function getByLanguage($p_Language, $p_Page)
    {
        return ItemsPageDescriptions::where('language', $p_Language)->where('pagina', $p_Page)->first();
    }
}