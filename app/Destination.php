<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use \Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class Destination extends Model
{
    protected $table = 'destination';
    protected $guarded = [];


    //Relacionamento de 1 para 1
    public function City(){
        return $this->hasOne(City::class, 'id', 'city_id');
    }

    //Relacionamento de 1 para muitos
    public function DestinationPhoto(){
        return $this->hasMany(DestinationPhoto::class, 'destination_id', 'id');
    }


    public static function getDT($p_Type, $p_Circuit, $p_City, $p_District, $p_UpdatedAt, $p_Status, $p_Order, $p_Start, $p_Length, $p_Draw)
    {
        $v_Query = Destination::join('city', 'city.id', '=', 'destination.city_id')
            ->leftJoin('district', 'district.id', '=', 'destination.district_id')
            ->leftJoin('touristic_circuit_cities', 'touristic_circuit_cities.city_id', '=', 'destination.city_id')
            ->leftJoin('touristic_circuit', 'touristic_circuit_cities.city_id', '=', 'touristic_circuit.id')
            ->selectRaw('SQL_CALC_FOUND_ROWS  touristic_circuit.nome as circuito, district.name as distrito, 
            city.name as municipio, destination.id, destination.nome, destination.district_id, 
            destination.updated_at, destination.publico');

        if($p_Type != '')
        {
            if($p_Type == 'Distrito')
                $v_Query->whereNotNull('destination.district_id');
            else
                $v_Query->whereNull('destination.district_id');
        }

        if($p_City != '')
            $v_Query->where('city.name', 'LIKE', '%' . $p_City . '%');

        if($p_District != '')
            $v_Query->where('district.name', 'LIKE', '%' . $p_District . '%');

        if($p_Circuit != '')
            $v_Query->where('touristic_circuit.nome', 'LIKE', '%' . $p_Circuit . '%');            

        if($p_UpdatedAt != '')
        {
            $v_StartDate = Carbon::createFromFormat('d/m/Y', substr($p_UpdatedAt, 0, 10));
            $v_Query->where('destination.updated_at', '>=', $v_StartDate->startOfDay()->format('Y-m-d H:i:s'));
            $v_EndDate = Carbon::createFromFormat('d/m/Y', substr($p_UpdatedAt, 13, 23));
            $v_Query->where('destination.updated_at', '<=', $v_EndDate->endOfDay()->format('Y-m-d H:i:s'));
        }

        if($p_Status != '')
            $v_Query->where('destination.publico',  $p_Status);

        if($p_Order != null)
        {
            if($p_Order["column"] == 1)
                $v_Query->orderBy('city.name', $p_Order["dir"]);
            if($p_Order["column"] == 2)
                $v_Query->orderBy('district.name', $p_Order["dir"]);
            if($p_Order["column"] == 3)
                $v_Query->orderBy('destination.updated_at', $p_Order["dir"]);
            if($p_Order["column"] == 4)
                $v_Query->orderBy('destination.publico', $p_Order["dir"]);
        }

        if($p_Length != -1)
            $v_Query->take($p_Length)->skip($p_Start);

        $v_QueryRes = $v_Query->get()->toArray();
        $v_Data = [];
        for($c_Index = 0 ; $c_Index < sizeof($v_QueryRes) ; $c_Index++)
        {
            array_push($v_Data, [
                $v_QueryRes[$c_Index]['district_id'] == null ? 'Município' : 'Distrito',
                $v_QueryRes[$c_Index]['circuito'],
                $v_QueryRes[$c_Index]['municipio'],
                $v_QueryRes[$c_Index]['distrito'],
                Carbon::createFromFormat('Y-m-d H:i:s', $v_QueryRes[$c_Index]['updated_at'])->format('d/m/Y H:i'),
                $v_QueryRes[$c_Index]['publico'] == 1 ? 'Ativo' : 'Desativado',
                '<div class="actions-div">' .
                    '<a href="' . url('admin/destinos/editar/' . ($v_QueryRes[$c_Index]['district_id'] == null ? 'municipio/' : 'distrito/') . $v_QueryRes[$c_Index]['id']) . '" title="Editar" type="button" class="btn btn-success"><i class="fa fa-edit"></i></a>' .
                    '<a href="' . url('admin/destinos/desativar/' . $v_QueryRes[$c_Index]['id']) . '" title="' . ($v_QueryRes[$c_Index]['publico']==1? 'Desativar' : 'Ativar') . '" type="button" class="btn btn-success"><i class="fa fa-' . ($v_QueryRes[$c_Index]['publico']==1? 'times' : 'check') . '"></i></a>' .
                '</div>'
            ]);
        }

        $v_DataTableAjax = new \stdClass();
        $v_DataTableAjax->draw = $p_Draw;
        $v_DataTableAjax->recordsFiltered = Destination::getTotalRows();
        $v_DataTableAjax->recordsTotal = Destination::count();
        $v_DataTableAjax->data = $v_Data;
        return json_encode($v_DataTableAjax);
    }

    public static function getTotalRows()
    {
        return DB::select(DB::raw("SELECT FOUND_ROWS() AS total_rows"))[0]->total_rows;
    }

    public static function getList()
    {
        return Destination::orderBy('nome')->lists('nome', 'id')->toArray();
    }
    public static function getPublicList()
    {
        return Destination::where('publico', 1)->orderBy('nome')->lists('nome', 'id')->toArray();
    }
    public static function getSlugList()
    {
        return Destination::where('publico', 1)->orderBy('nome')->lists('nome', 'slug')->toArray();
    }
    public static function getCompleteList()
    {
        return Destination::join('destination_photo', 'destination_photo.destination_id', '=', 'destination.id')
                          ->where('destination.publico', 1)
                          ->where('destination_photo.is_cover', 1)
                          ->select(['destination.nome', 'destination.descricao_curta', 'destination.id', 'destination.city_id','destination.latitude', 'destination.longitude',
                              'destination_photo.url', 'destination.slug'])
                          ->orderBy('nome')
                          ->groupBy('destination.id')
                          ->get();
    }
    public static function getCompleteNameList()
    {
        return Destination::join('destination_photo', 'destination_photo.destination_id', '=', 'destination.id')
                          ->where('destination.publico', 1)
                          ->where('destination_photo.is_cover', 1)
                          ->lists('destination.nome')->toArray();
    }

    public static function post($p_Photos, $p_PhotoInfo, $p_DeletedPhotos, $p_Data)
    {
        $v_DestinationData = $p_Data['destino'];
        if(!empty($v_DestinationData['district_id']))
            $v_Name = District::find($v_DestinationData['district_id'])->name;
        else
            $v_Name = City::find($v_DestinationData['city_id'])->name;

        $v_DestinationData['nome'] = $v_Name;
        $v_DestinationData['destaque'] = array_key_exists('destaque', $v_DestinationData) ? 1 : 0;
        $v_DestinationData['turismo_negocios_eventos'] = array_key_exists('turismo_negocios_eventos', $v_DestinationData) ? 1 : 0;
        $v_DestinationData['indutor_turismo_lazer'] = array_key_exists('indutor_turismo_lazer', $v_DestinationData) ? 1 : 0;

        if(array_key_exists('id', $v_DestinationData))
        {
            $v_Id = $v_DestinationData['id'];
            unset($v_DestinationData['id']);
        }
        else
            $v_Id =  null;

        array_walk($v_DestinationData, function (&$c_Item) {
            $c_Item = ($c_Item === '') ? null : $c_Item;
        });

        if($v_Id == null)
            $v_DestinationData['slug'] = Destination::generateSlug($v_DestinationData['nome']);

        $v_Destination = Destination::updateOrCreate(['id' => $v_Id], $v_DestinationData);
        DestinationPhoto::updatePhotos($v_Destination->id, $p_Photos, $p_PhotoInfo, $p_DeletedPhotos);
        if(!array_key_exists('hashtags', $p_Data))
            $p_Data['hashtags'] = [];
        Hashtag::updateHashtag('destination', $v_Destination->id, $p_Data['hashtags']);
        if(!array_key_exists('tipos_viagem', $p_Data))
            $p_Data['tipos_viagem'] = [];
        TripDestinations::updateTripTypes($v_Destination->id, $p_Data['tipos_viagem']);
    }

    public static function generateSlug($p_String)
    {
        $v_Slug = Str::slug($p_String);
        if(Destination::where('slug', $v_Slug)->count() == 0)
            return $v_Slug;

        $v_UniqueSlug = false;
        $v_Index = 0;
        while(!$v_UniqueSlug){
            if(Destination::where('slug', $v_Slug . '-' . $v_Index)->count() == 0)
                $v_UniqueSlug = true;
            else
                $v_Index++;
        }

        return $v_Slug . '-' . $v_Index;
    }

    public static function getCityDestinationsIds()
    {
        return Destination::whereNull('district_id')->lists('city_id')->toArray();
    }

    public static function getDistrictDestinationsIds()
    {
        return Destination::whereNotNull('district_id')->lists('district_id')->toArray();
    }

    public static function getHeaderDestinations($p_Quantity)
    {
        return Destination::join('destination_photo', 'destination_photo.destination_id', '=', 'destination.id')
            ->where('destination.publico', 1)
            ->where('destination_photo.is_cover', 1)
            ->select(['destination.nome', 'destination.slug', 'destination_photo.url'])
            ->orderBy('destination.destaque', 'desc')
            ->orderByRaw('RAND()')->take($p_Quantity)
            ->get();
    }

    public static function getDestinations($p_Quantity)
    {
        return Destination::join('destination_photo', 'destination_photo.destination_id', '=', 'destination.id')
            ->where('destination.publico', 1)
            ->where('destination_photo.is_cover', 1)
            ->select(['destination.nome', 'destination.slug', 'destination.id', 'destination.descricao_curta',
                'destination_photo.url'])
            ->orderBy('destination.destaque', 'desc')
            ->orderByRaw('RAND()')
            ->groupBy('destination.id')->take($p_Quantity)
            ->get();
    }

    public static function getDestination($p_Slug)
    {
        return Destination::join('destination_photo', 'destination_photo.destination_id', '=', 'destination.id')
            ->where('destination.publico', 1)
            ->where('destination.slug', $p_Slug)
            ->where('destination_photo.is_cover', 1)
            ->select(['destination.*', 'destination.depoimento', 'destination.depoimento_autor',
                'destination.descricao_curta', 'destination.titulo', 'destination.descricao',
                'destination_photo.url'])
            ->firstOrFail();
    }

    public static function getMapDestinations()
    {
        return Destination::join('destination_photo', 'destination_photo.destination_id', '=', 'destination.id')
                          ->whereNotNull('destination.latitude')->whereNotNull('destination.longitude')
                          ->where('destination.publico', 1)
                          ->where('destination_photo.is_cover', 1)
                          ->select(['destination.nome', 'destination.slug', 'destination.id', 'destination_photo.url',
                              'destination.latitude', 'destination.longitude'])
                          ->groupBy('destination.id')
                          ->get();
    }

    public static function getCityId($p_DestinationId)
    {
        return Destination::find($p_DestinationId)->city_id;
    }

    public static function getDestinationByCityId($p_CityId)
    {
        return Destination::where('city_id', $p_CityId)->whereNull('district_id')->select(['slug', 'nome'])->firstOrFail();
    }


    public static function getCityRegion($p_CityId)
    {
        $v_Response['error'] = 'ok';
        $v_Destination = Destination::join('region', 'region.id', '=', 'destination.region_id')
                                    ->where('destination.city_id', $p_CityId)->select('region.name')->groupBy('region.id')->first();
        $v_Response['data'] = $v_Destination == null ? '' : $v_Destination->name;
        return $v_Response;
    }

    public static function getReport($p_Data)
    {
        $v_CircuitId = $p_Data['touristic_circuit_id'];
        $v_RegionId = $p_Data['region_id'];

        $v_Query = Destination::leftJoin('city', 'city.id', '=', 'destination.city_id')
            ->leftJoin('region', 'destination.region_id', '=', 'region.id')
            ->leftJoin('touristic_circuit_cities', 'destination.city_id', '=', 'touristic_circuit_cities.city_id')
            ->leftJoin('touristic_circuit', 'touristic_circuit_cities.touristic_circuit_id', '=', 'touristic_circuit.id')
            ->leftJoin(DB::raw('(SELECT * FROM `destination_photo` WHERE is_cover = 1) cover_photo'), function($join) {
                $join->on('destination.id', '=', 'cover_photo.destination_id');
            })
            ->select('destination.*', 'city.name as cidade', 'touristic_circuit.nome as circuito', 'cover_photo.is_cover as foto_capa', 'region.name as regiao')
            ->groupBy('destination.id');

        if(array_key_exists('id', $p_Data))
            $v_Query->whereIn('destination.city_id', $p_Data['id']);

        if($v_CircuitId != '')
            $v_Query->where('touristic_circuit.id', $v_CircuitId);

        if($v_RegionId != '')
            $v_Query->where('region_id', $v_RegionId);

        $v_Results = $v_Query->get();

        $v_TemplateFile = public_path() . '/templates-relatorios/RelatorioDestinos.xlsx';
        \Excel::load($v_TemplateFile, function($v_Template) use($v_Results){
            $v_Template->sheet('Relatorio', function($sheet) use($v_Results) {
                $v_CurrentRow = 2;
                foreach($v_Results as $c_Result)
                {
                    $v_Data = [];
                    try {
                        array_push($v_Data, $c_Result->nome);
                        array_push($v_Data, $c_Result->cidade);
                        array_push($v_Data, $c_Result->regiao);
                        array_push($v_Data, $c_Result->circuito);
                        array_push($v_Data, $c_Result->destaque == 1 ? 'Sim' : 'Não');
                        array_push($v_Data, $c_Result->turismo_negocios_eventos == 1 ? 'Sim' : 'Não');
                        array_push($v_Data, $c_Result->indutor_turismo_lazer == 1 ? 'Sim' : 'Não');
                        array_push($v_Data, $c_Result->foto_capa == 1 ? 'Sim' : 'Não');
                        array_push($v_Data, $c_Result->publico == 1 ? 'Sim' : 'Não');
                        $v_ShortDescription = json_decode($c_Result->descricao_curta,1);
                        $v_Description = json_decode($c_Result->descricao,1);
                        array_push($v_Data, $v_ShortDescription['pt']);
                        array_push($v_Data, $v_Description['pt']);
                        array_push($v_Data, $v_ShortDescription['en']);
                        array_push($v_Data, $v_Description['en']);
                        array_push($v_Data, $v_ShortDescription['es']);
                        array_push($v_Data, $v_Description['es']);
                        array_push($v_Data, $v_ShortDescription['fr']);
                        array_push($v_Data, $v_Description['fr']);

                        $sheet->row($v_CurrentRow,$v_Data);
                        $v_CurrentRow++;
                    } catch(\Exception $e){
                    }
                    $c_Result = null;
                }
                $sheet->setBorder('A2:Q'.($v_CurrentRow-1), 'thin');
            });
        })->download('xlsx');
    }

    public static function get360Destinations($p_Filter)
    {
        $v_Query = Destination::with('tripCategories')
                              ->join('destination_photo', 'destination_photo.destination_id', '=', 'destination.id')
                              ->where('destination.publico', 1)
                              ->where('destination_photo.is_cover', 1)
                              ->whereNotNull('destination.minas_360')
                              ->select(['destination.id', 'destination.nome', 'destination.minas_360', 'destination_photo.url']);

        if($p_Filter != null){
            $v_Query->leftJoin('trip_destinations', 'trip_destinations.destination_id', '=', 'destination.id')
                    ->whereIn('trip_destinations.trip_type_id', $p_Filter);
        }

        return $v_Query->groupBy('destination.id')->get();
    }

    public function tripCategories()
    {
        return $this->hasMany('App\TripDestinations')
                    ->join('trip_type', 'trip_type.id', '=', 'trip_destinations.trip_type_id')
                    ->join('trip_category', 'trip_category.id', '=', 'trip_type.trip_category_id')
                    ->where('trip_type.publico', 1)
                    ->select('trip_destinations.destination_id', 'trip_category.nome')
                    ->groupBy('trip_category.id');
    }
}