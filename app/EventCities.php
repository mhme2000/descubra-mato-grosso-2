<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class EventCities extends Model
{
    public $timestamps = false;
    protected $table = 'event_cities';

    public static function updateEventCities($p_EventId, $p_Cities)
    {
        EventCities::where('event_id', $p_EventId)->delete();
        foreach($p_Cities as $c_City)
        {
            $v_EventCity = new EventCities();
            $v_EventCity->event_id = $p_EventId;
            $v_EventCity->city_id = $c_City;
            $v_EventCity->save();
        }
    }

    public static function getSelectedCities($p_EventId)
    {
        return EventCities::where('event_id', $p_EventId)->lists('city_id')->toArray();
    }
}