<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class AttractionPhoto extends Model
{
    public $timestamps = false;
    protected $table = 'attraction_photo';
    protected $guarded = [];

    public static function updatePhotos($p_Photos, $p_AttractionId)
    {
        $v_Path = public_path() . '/imagens/';
        if(!\File::exists($v_Path))
            \File::makeDirectory($v_Path);
        $v_Path .=  '/atracoes/';
        if(!\File::exists($v_Path))
            \File::makeDirectory($v_Path);

        $v_OldPhotos = AttractionPhoto::where('attraction_id', $p_AttractionId)->get();
        foreach($v_OldPhotos as $c_Photo)
        {
            $v_OldFileName = explode('/', $c_Photo->url);
            $v_OldFileName = array_pop($v_OldFileName);
            $c_Photo->delete();
            \File::delete($v_Path . $v_OldFileName);
        }

        $v_InventoryPath = public_path() . '/imagens/inventario/';
        foreach($p_Photos as $c_Photo)
        {
            $v_FileName = explode('/', $c_Photo->url);
            $v_FileName = array_pop($v_FileName);
            if(\File::exists($v_InventoryPath . $v_FileName)) {
                \File::copy($v_InventoryPath . $v_FileName, $v_Path . $v_FileName);

                AttractionPhoto::create(['is_cover' => $c_Photo->is_cover,
                                         'url' => url('/imagens/atracoes/' . $v_FileName),
                                         'attraction_id' => $p_AttractionId]);
            }
        }
    }

    public static function getCoverPhoto($p_AttractionId)
    {
        return AttractionPhoto::where('is_cover', 1)->where('attraction_id', $p_AttractionId)->first();
    }

    public static function getPhotoGallery($p_AttractionId, $p_Quantity = null)
    {
        $v_Query = AttractionPhoto::where('is_cover', 0)->where('attraction_id', $p_AttractionId);
        if($p_Quantity != null)
            $v_Query->orderByRaw('RAND()')->take($p_Quantity);

        return $v_Query->get();
    }
}