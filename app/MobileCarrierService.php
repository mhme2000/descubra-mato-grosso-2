<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class MobileCarrierService extends Model
{
    public $timestamps = false;
    protected $table = 'mobile_carrier_service';

    public static $m_Rules = array
    (
        'nome' => 'required|min:1|max:50'
    );

    public static $m_RulesEdit = array
    (
        'id' => 'required|numeric|min:1',
        'nome' => 'required|min:1|max:50'
    );

    public static function post($p_Id, $p_Name)
    {
        $v_Service = MobileCarrierService::findOrNew($p_Id);
        $v_Service->name = $p_Name;
        $v_Service->save();
    }

    public static function getList()
    {
        $v_Carriers = MobileCarrier::orderBy('name')->get();
        $v_Services = MobileCarrierService::orderBy('name')->get();
        $v_List = [];
        foreach($v_Carriers as $c_Carrier)
        {
            foreach($v_Services as $c_Service)
            {
                $v_Item = $c_Carrier->name . ': ' . $c_Service->name;
                $v_List += [$v_Item => $v_Item];
            }
        }

        return $v_List;
    }
}