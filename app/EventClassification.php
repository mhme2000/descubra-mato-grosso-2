<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventClassification extends Model
{
    protected $table = 'event_classification';
    public $timestamps = false;

    protected $fillable = [
        'nome',
        'ativo',
        'initial_date',
        'final_date',
        'public'

    ];

    public function EventSelectedClassification(){
        return $this->hasMany(EventSelectedClassification::class, 'event_classification_id', 'id');
    }
    
    public static function getList()
    {
        return EventClassification::orderBy('nome')->lists('nome', 'id')->toArray();
    }
}
