<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    public $timestamps = false;

    protected $table = 'user';

    protected $hidden = ['password', 'remember_token'];
	protected $guarded = [];

    public static $m_Rules = array
    (
        'email'=>'required|email|unique:user',
        'senha' => 'required|min:6|confirmed'
    );

    public static $m_RulesEdit = array
    (
        'id' => 'required|numeric',
        'email' => 'required|email|unique:user,email,{id}',
        'senha' => 'min:6|confirmed'
    );

    public static function getEditRules($p_Id)
    {
        $v_Rules = [];
        foreach(User::$m_RulesEdit as $c_RuleId => $c_Rule)
            $v_Rules[$c_RuleId] = str_replace('{id}', $p_Id, $c_Rule);
        return $v_Rules;
    }

    public static $m_RulesTradeUser = array
    (
        'email'=>'required|email|unique:user',
        'senha' => 'min:6|confirmed'
    );

    public static function getDT($p_Circuit, $p_City, $p_Name, $p_Email, $p_TypeId, $p_Status, $p_Order, $p_Start, $p_Length, $p_Draw)
    {
        $v_Query = User::join('user_type', 'user_type.id', '=', 'user.user_type_id')
            ->leftJoin('city', 'city.id', '=', 'user.city_id')
            ->leftJoin('touristic_circuit', 'touristic_circuit.id', '=', 'user.touristic_circuit_id')
                       ->select(DB::raw('SQL_CALC_FOUND_ROWS user.id, user.name, user.email, user.user_type_id, user.active, user_type.name as user_type, touristic_circuit.nome as circuito, city.name as cidade'));

        if($p_Circuit != '')
            $v_Query->having('circuito', 'LIKE', '%' . $p_Circuit . '%');

        if($p_City != '')
            $v_Query->having('cidade', 'LIKE', '%' . $p_City . '%');

        if($p_Name != '')
            $v_Query->where('user.name', 'LIKE', '%' . $p_Name . '%');

        if($p_Email != '')
            $v_Query->where('user.email', 'LIKE', '%' . $p_Email . '%');

        if($p_TypeId != '')
            $v_Query->where('user.user_type_id', $p_TypeId);

        if($p_Status != '')
            $v_Query->where('user.active', $p_Status);

        if($p_Order != null)
        {
            if($p_Order["column"] == 0)
                $v_Query->orderBy('circuito', $p_Order["dir"]);
            if($p_Order["column"] == 1)
                $v_Query->orderBy('cidade', $p_Order["dir"]);
            if($p_Order["column"] == 2)
                $v_Query->orderBy('user.name', $p_Order["dir"]);
            if($p_Order["column"] == 3)
                $v_Query->orderBy('user.email', $p_Order["dir"]);
            if($p_Order["column"] == 4)
                $v_Query->orderBy('user_type.name', $p_Order["dir"]);
            if($p_Order["column"] == 5)
                $v_Query->orderBy('user.active', $p_Order["dir"]);
        }

        if($p_Length != -1)
            $v_Query->take($p_Length)->skip($p_Start);

        $v_QueryRes = $v_Query->get()->toArray();
        $v_Data = [];
        for($c_Index = 0 ; $c_Index < sizeof($v_QueryRes) ; $c_Index++)
        {
            array_push($v_Data, [
                $v_QueryRes[$c_Index]['circuito'],
                $v_QueryRes[$c_Index]['cidade'],
                $v_QueryRes[$c_Index]['name'],
                $v_QueryRes[$c_Index]['email'],
                $v_QueryRes[$c_Index]['user_type'],
                $v_QueryRes[$c_Index]['active']==1? 'Ativo' : 'Desativado',
                '<div class="actions-div">' .
                    '<a href="' . url('admin/usuarios/editar/' . $v_QueryRes[$c_Index]['id']) . '" title="Editar" type="button" class="btn btn-success"><i class="fa fa-edit"></i></a>' .
                    '<a href="' . url('admin/usuarios/desativar/' . $v_QueryRes[$c_Index]['id']) . '" title="' . ($v_QueryRes[$c_Index]['active']==1? 'Desativar' : 'Ativar') . '" type="button" class="btn btn-success"><i class="fa fa-' . ($v_QueryRes[$c_Index]['active']==1? 'times' : 'check') . '"></i></a>' .
//                '<a href="' . url('admin/eventos/excluir/' . $v_QueryRes[$c_Index]['id']) . '" title="Excluir" type="button" class="btn btn-success delete-btn"><i class="fa fa-trash-o"></i></a>' .
                '</div>'
            ]);
        }

        $v_DataTableAjax = new \stdClass();
        $v_DataTableAjax->draw = $p_Draw;
        $v_DataTableAjax->recordsFiltered = User::getTotalRows();
        $v_DataTableAjax->recordsTotal = User::count();
        $v_DataTableAjax->data = $v_Data;
        return json_encode($v_DataTableAjax);
    }

    public static function getTotalRows()
    {
        return DB::select(DB::raw("SELECT FOUND_ROWS() AS total_rows"))[0]->total_rows;
    }

    public static function resetPassword($p_Email)
    {
        $v_User = User::where('email', $p_Email)->first();
        if($v_User == null)
            return redirect()->back()->with(['error_message' => 'Usuário não encontrado!']);
        else
        {
            $v_NewPassword = str_random(3) . time() . str_random(12);
            $v_User->password = Hash::make($v_NewPassword);
            $v_User->save();
            Mail::send('emails.passwordReset', ['p_NewPassword' => $v_NewPassword],
                function ($message) use ($v_User)
                {
                    $message->to($v_User->email, $v_User->name)->subject('Descubra Mato Grosso - Senha resetada');
                });
            return redirect(url('admin/login'))->with('message', 'Uma nova senha foi enviada para o seu e-mail!');
        }
    }

    public static function post($p_Id, $p_Data, $p_Pwd)
    {
        if($p_Pwd != null)
	        $p_Data['password'] = \Hash::make($p_Pwd);

        array_walk($p_Data, function (&$c_Item) {
            $c_Item = ($c_Item === '') ? null : $c_Item;
        });

	    User::updateOrCreate(['id' => $p_Id], $p_Data);
        return redirect(url('/admin/usuarios'))->with('message', 'Usuário alterado com sucesso!');
    }

    public static function postRegisterTradeUser($p_Name, $p_Email, $p_Pwd)
    {
        $p_Data = [
            'name' => $p_Name,
            'email' => $p_Email,
            'password' => \Hash::make($p_Pwd),
            'user_type_id' => 6
        ];

	    $v_User = User::create($p_Data);
        Auth::login($v_User);
        return redirect(url('/admin/painel-geral'))->with('message', 'Cadastro realizado com sucesso!');
    }

    public static function postProfile($p_Id, $p_Data, $p_Pwd)
    {
        if($p_Pwd != null)
	        $p_Data['password'] = \Hash::make($p_Pwd);

	    User::updateOrCreate(['id' => $p_Id], $p_Data);
        return redirect(url('/admin/editar-perfil'))->with('message', 'Perfil alterado com sucesso!');
    }

    public static function toggleActive($p_Id)
    {
        $v_User = User::find($p_Id);
        $v_User->active = !$v_User->active;
        $v_User->save();
    }

    public static function isActive($p_Email)
    {
        $v_User = User::where('email', $p_Email)->first();
        if($v_User == null)
            return null;
        else if ($v_User->active == 0)
            return false;
        return true;
    }

    public static function getList()
    {
        return User::orderBy('name')->lists('name', 'id')->toArray();
    }

    public static function getItemUsers($p_CityId, $p_Type, $p_ItemId)
    {
        $v_CircuitId = TouristicCircuitCities::getCityCircuit($p_CityId);
        return User::where('active', 1)
                   ->where(function($q) use($p_CityId, $p_Type, $p_ItemId, $v_CircuitId) {
                       $q->whereIn('id', UserTradeItem::itemApprovedUsers($p_Type, $p_ItemId))
                         ->orWhere('city_id', $p_CityId)
                         ->orWhereIn('touristic_circuit_id', $v_CircuitId);
                   })
                   ->get();
    }
}
