<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class InventoryItem extends Model
{
    public $timestamps = false;
    protected $table = 'inventory_item';

    public static $m_Rules = array
    (
        'nome' => 'required|min:1|max:300'
    );

    public static $m_RulesEdit = array
    (
        'id' => 'required|numeric|min:1',
        'nome' => 'required|min:1|max:300'
    );

    public static function getList()
    {
        return InventoryItem::lists('name', 'id')->toArray();
    }
}