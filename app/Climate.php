<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Climate extends Model
{
    public $timestamps = false;
    protected $table = 'climate';

    public static $m_Rules = array
    (
        'nome' => 'required|min:1|max:300'
    );

    public static $m_RulesEdit = array
    (
        'id' => 'required|numeric|min:1',
        'nome' => 'required|min:1|max:300'
    );

    public static function post($p_Id, $p_Name)
    {
        $v_Climate = Climate::findOrNew($p_Id);
        $v_Climate->name = $p_Name;
        $v_Climate->save();
    }

    public static function getList()
    {
        return Climate::orderBy('name')->lists('name', 'id')->toArray();
    }
}