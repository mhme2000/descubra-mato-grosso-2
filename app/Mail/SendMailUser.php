<?php

  class SendMailUser {
    public function build() {
      return $this->from('pitmt@sedec.mt.gov.br')
                  ->view('emails.passwordReset');
    }
  }
?>