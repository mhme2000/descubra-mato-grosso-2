<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use \Carbon\Carbon;

class TouristicRoute extends Model
{
    protected $table = 'touristic_route';
	protected $guarded = [];

    public static function getDT($p_CreatedAt, $p_UpdatedAt, $p_Name, $p_Duration, $p_Status, $p_Order, $p_Start, $p_Length, $p_Draw)
    {
        $v_Query = TouristicRoute::select(DB::raw('SQL_CALC_FOUND_ROWS id, created_at, updated_at, nome_pt, dias_duracao, active'));

        if($p_CreatedAt != '')
        {
            $v_StartDate = Carbon::createFromFormat('d/m/Y', substr($p_CreatedAt, 0, 10));
            $v_Query->where('created_at', '>=', $v_StartDate->startOfDay()->format('Y-m-d H:i:s'));
            $v_EndDate = Carbon::createFromFormat('d/m/Y', substr($p_CreatedAt, 13, 23));
            $v_Query->where('created_at', '<=', $v_EndDate->endOfDay()->format('Y-m-d H:i:s'));
        }
        if($p_UpdatedAt != '')
        {
            $v_StartDate = Carbon::createFromFormat('d/m/Y', substr($p_UpdatedAt, 0, 10));
            $v_Query->where('updated_at', '>=', $v_StartDate->startOfDay()->format('Y-m-d H:i:s'));
            $v_EndDate = Carbon::createFromFormat('d/m/Y', substr($p_UpdatedAt, 13, 23));
            $v_Query->where('updated_at', '<=', $v_EndDate->endOfDay()->format('Y-m-d H:i:s'));
        }

        if($p_Name != '')
            $v_Query->where('nome_pt', 'LIKE', '%' . $p_Name . '%');

        if($p_Duration != '')
            $v_Query->where('dias_duracao', $p_Duration);

        if($p_Status != '')
            $v_Query->where('active',  $p_Status);

        if($p_Order != null)
        {
            if($p_Order["column"] == 0)
                $v_Query->orderBy('created_at', $p_Order["dir"]);
            if($p_Order["column"] == 1)
                $v_Query->orderBy('updated_at', $p_Order["dir"]);
            if($p_Order["column"] == 2)
                $v_Query->orderBy('nome_pt', $p_Order["dir"]);
            if($p_Order["column"] == 3)
                $v_Query->orderBy('duration', $p_Order["dir"]);
            if($p_Order["column"] == 4)
                $v_Query->orderBy('active', $p_Order["dir"]);
        }

        if($p_Length != -1)
            $v_Query->take($p_Length)->skip($p_Start);

        $v_QueryRes = $v_Query->get()->toArray();
        $v_Data = [];
        $v_IsParceiro = UserType::isParceiro();
        for($c_Index = 0 ; $c_Index < sizeof($v_QueryRes) ; $c_Index++)
        {
            array_push($v_Data, [
	            Carbon::createFromFormat('Y-m-d H:i:s', $v_QueryRes[$c_Index]['created_at'])->format('d/m/Y'),
	            Carbon::createFromFormat('Y-m-d H:i:s', $v_QueryRes[$c_Index]['updated_at'])->format('d/m/Y H:i'),
	            $v_QueryRes[$c_Index]['nome_pt'],
	            $v_QueryRes[$c_Index]['dias_duracao'],
	            $v_QueryRes[$c_Index]['active']==1? 'Ativo' : 'Desativado',
                '<div class="actions-div">' .
                    '<a href="' . url('admin/roteiros/editar/' . $v_QueryRes[$c_Index]['id']) . '" title="Editar" type="button" class="btn btn-success"><i class="fa fa-edit"></i></a>' .
                    ($v_IsParceiro ? '' : '<a href="' . url('admin/roteiros/desativar/' . $v_QueryRes[$c_Index]['id']) . '" title="' . ($v_QueryRes[$c_Index]['active']==1? 'Desativar' : 'Ativar') . '" type="button" class="btn btn-success"><i class="fa fa-' . ($v_QueryRes[$c_Index]['active']==1? 'times' : 'check') . '"></i></a>') .
                    ($v_IsParceiro ? '' : '<a href="' . url('admin/roteiros/excluir/' . $v_QueryRes[$c_Index]['id']) . '" title="Excluir" type="button" class="btn btn-success delete-btn"><i class="fa fa-trash-o"></i></a>') .
                '</div>'
            ]);
        }

        $v_DataTableAjax = new \stdClass();
        $v_DataTableAjax->draw = $p_Draw;
        $v_DataTableAjax->recordsFiltered = TouristicRoute::getTotalRows();
        $v_DataTableAjax->recordsTotal = TouristicRoute::count();
        $v_DataTableAjax->data = $v_Data;
        return json_encode($v_DataTableAjax);
    }

    public static function getTotalRows()
    {
        return DB::select(DB::raw("SELECT FOUND_ROWS() AS total_rows"))[0]->total_rows;
    }

    public static function post($p_Id, $p_Photos, $p_PhotoInfo, $p_DeletedPhotos, $p_Data)
    {
        $p_RouteData = $p_Data['roteiro'];
        $p_RouteData['destaque'] = array_key_exists('destaque', $p_RouteData) ? 1 : 0;

        if(array_key_exists('tipo_roteiro', $p_Data)) {
            $v_RouteTypes = $p_Data['tipo_roteiro'];
            $v_Types = '';
            foreach($v_RouteTypes as $c_Type)
                $v_Types .= $c_Type . ';';
            $p_RouteData['tipo_roteiro'] = $v_Types;
        }
        else
            $p_RouteData['tipo_roteiro'] = null;

        array_walk($p_RouteData, function (&$c_Item) {
            $c_Item = ($c_Item === '') ? null : $c_Item;
        });

        if($p_Id == null)
            $p_RouteData['slug'] = TouristicRoute::generateSlug($p_RouteData['nome_pt']);
        $v_TouristicRoute = TouristicRoute::updateOrCreate(['id' => $p_Id], $p_RouteData);

        TouristicRouteDestinations::updateTouristicRouteDestinations($v_TouristicRoute->id, $p_Data['destinos_participantes']);
        TouristicRouteDay::post($v_TouristicRoute->id, $p_RouteData['dias_duracao'], $p_Data['dias']);
        Hashtag::updateHashtag('touristicRoute', $v_TouristicRoute->id, $p_Data['hashtags']);
        TouristicRoutePhoto::updatePhotos($v_TouristicRoute->id, $p_Photos, $p_PhotoInfo, $p_DeletedPhotos);
        if(!array_key_exists('tipos_viagem', $p_Data))
            $p_Data['tipos_viagem'] = [];
        TripTouristicRoutes::updateTripTypes($v_TouristicRoute->id, $p_Data['tipos_viagem']);
    }

    public static function generateSlug($p_String)
    {
        $v_Slug = Str::slug($p_String);
        if(TouristicRoute::where('slug', $v_Slug)->count() == 0)
            return $v_Slug;

        $v_UniqueSlug = false;
        $v_Index = 0;
        while(!$v_UniqueSlug){
            if(TouristicRoute::where('slug', $v_Slug . '-' . $v_Index)->count() == 0)
                $v_UniqueSlug = true;
            else
                $v_Index++;
        }

        return $v_Slug . '-' . $v_Index;
    }

    public static function getRoutes($p_Quantity)
    {
        return TouristicRoute::join('touristic_route_photo', 'touristic_route_photo.touristic_route_id', '=', 'touristic_route.id')
            ->where('touristic_route.active', 1)
            ->where('touristic_route_photo.is_cover', 1)
            ->select(['touristic_route.nome', 'touristic_route.slug', 'touristic_route.descricao_curta', 'touristic_route_photo.url'])
            ->orderBy('touristic_route.destaque', 'desc')
            ->orderByRaw('RAND()')
            ->groupBy('touristic_route.id')->take($p_Quantity)
            ->get();
    }

    public static function getRoutesByDuration($p_Duration, $p_Quantity)
    {
        $v_Query = TouristicRoute::join('touristic_route_photo', 'touristic_route_photo.touristic_route_id', '=', 'touristic_route.id')
            ->where('touristic_route.active', 1)
            ->where('touristic_route_photo.is_cover', 1);

        if($p_Duration == 1)
            $v_Query->where('touristic_route.dias_duracao', '<=', 3);
        else if($p_Duration == 2)
            $v_Query->where('touristic_route.dias_duracao', '>', 3)->where('touristic_route.dias_duracao', '<=', 7);
        else
            $v_Query->where('touristic_route.dias_duracao', '>', 7);
        return $v_Query->select(['touristic_route.nome', 'touristic_route.slug', 'touristic_route.descricao_curta', 'touristic_route_photo.url'])
            ->orderBy('touristic_route.destaque', 'desc')
            ->orderByRaw('RAND()')
            ->groupBy('touristic_route.id')->take($p_Quantity)
            ->get();
    }

    public static function getRoute($p_Slug)
    {
        return TouristicRoute::join('touristic_route_photo', 'touristic_route_photo.touristic_route_id', '=', 'touristic_route.id')
            ->where('touristic_route.slug', $p_Slug)
            ->where('touristic_route.active', 1)
            ->where('touristic_route_photo.is_cover', 1)
            ->select(['touristic_route.*', 'touristic_route_photo.url'])
            ->firstOrFail();
    }

    public static function getMapRoutes()
    {
        return TouristicRoute::join('touristic_route_photo', 'touristic_route_photo.touristic_route_id', '=', 'touristic_route.id')
            ->where('touristic_route.active', 1)
            ->where('touristic_route_photo.is_cover', 1)
            ->select(['touristic_route.nome', 'touristic_route.slug', 'touristic_route.id', 'touristic_route_photo.url'])
            ->groupBy('touristic_route.id')
            ->get();
    }

	public static function deleteTouristicRoute($p_Id)
	{
		$v_Photos = TouristicRoutePhoto::where('touristic_route_id', $p_Id)->get();
		foreach($v_Photos as $c_Photo)
		{
			$v_Path = public_path() . '/imagens/roteiros/';
			$v_FileName = explode('/', $c_Photo->url);
			$v_FileName = array_pop($v_FileName);
			\File::delete($v_Path . $v_FileName);
		}
		TouristicRoute::where('id', $p_Id)->delete();
	}

    public static function get360Routes($p_Filter)
    {
        $v_Query = TouristicRoute::with('tripCategories')
                                 ->join('touristic_route_photo', 'touristic_route_photo.touristic_route_id', '=', 'touristic_route.id')
                                 ->where('touristic_route.active', 1)
                                 ->where('touristic_route_photo.is_cover', 1)
                                 ->whereNotNull('touristic_route.minas_360')
                                 ->select(['touristic_route.id', 'touristic_route.nome', 'touristic_route.minas_360', 'touristic_route_photo.url']);

        if($p_Filter != null){
            $v_Query->leftJoin('trip_touristic_routes', 'trip_touristic_routes.touristic_route_id', '=', 'touristic_route.id')
                    ->whereIn('trip_touristic_routes.trip_type_id', $p_Filter);
        }

        return $v_Query->groupBy('touristic_route.id')->get();
    }

    public function tripCategories()
    {
        return $this->hasMany('App\TripTouristicRoutes')
                    ->join('trip_type', 'trip_type.id', '=', 'trip_touristic_routes.trip_type_id')
                    ->join('trip_category', 'trip_category.id', '=', 'trip_type.trip_category_id')
                    ->where('trip_type.publico', 1)
                    ->select('trip_touristic_routes.touristic_route_id', 'trip_category.nome')
                    ->groupBy('trip_category.id');
    }
}