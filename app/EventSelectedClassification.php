<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventSelectedClassification extends Model
{
    protected $table = 'event_selected_classification';
    public $timestamps = false;
    
    protected $fillable = [
        'event_classification_id',
        'event_id',
    ];

    public function Event(){
        return $this->belongsTo(Event::class, 'event_id', 'id');
    }

    public function EventClassification(){
        return $this->belongsTo(EventClassification::class, 'event_classification_id', 'id');
    }

}
