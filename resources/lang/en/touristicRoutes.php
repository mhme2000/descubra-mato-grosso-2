<?php

return [

	'find_perfect_route'  =>  'Find the perfect road trip',
	'route_how_many_days'  =>  'How long do you want to travel?',
	'where_will_you_pass'  =>  'Where are you heading to?',
	'how_split_your_trip'  =>  'How to divide your trip',
	'attractions_to_visit'  =>  'Attractions you can visit',
	'day' => 'Day ',
	'days'  =>  'Day | days',
	'duration'  =>  'Road trip lasting',
	'what_will_you_see'  =>  'What are you going to see?',
	'choose_duration'  =>  'Choose the duration',
	'duration_1'  =>  'Up to 3 days',
	'duration_2'  =>  'From 3 to 7 days',
	'duration_3'  =>  'More than 7 days',

];
