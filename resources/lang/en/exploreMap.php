<?php

return [

	'mark_attractions_to_show_or_select_destination'  =>  'Sign the attractions to show or filter by destination',
	'nature'  =>  'Nature',
	'waterfalls'  =>  'Waterfalls',
	'lakes'  =>  'Lakes',
	'caves'  =>  'Caves',
	'mountains'  =>  'Mountains',
	'hidromineral'  =>  'Hydro-mineral ranches',
	'culture'  =>  'Culture',
	'historic_centers'  =>  'Historical centers',
	'buildings_monuments'  =>  'Buildings & Monuments',
	'churches'  =>  'Churches',
	'museums'  =>  'Museums, Cultural Institutions & Theaters',
	'zoos'  =>  'Zoo & Botanic Garden',
	'markets'  =>  'Fairs & Markets',
	'select_destination'  =>  'Select a destination to see its attractions',


];
