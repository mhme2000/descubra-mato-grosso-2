<?php

return [

	'visit_gallery' => 'VISITE A GALERIA',
	'read_more' => 'Leia mais',
	'read_more_posts' => 'Leia mais postagens',
	'slideshow' => 'Slideshow',
	'previous' => 'Anterior',
	'next' => 'Próximo',

];
