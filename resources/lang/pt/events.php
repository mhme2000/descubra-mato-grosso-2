<?php

return [

	'find_event' => 'Encontre o evento que você procura',
	'event_date' => 'Data do evento',
	'fill_form_message' => 'Preencha os dados do filtro para pesquisar',

];
