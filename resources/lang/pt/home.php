<?php

return [

    'more_activities' => 'Descubra o que fazer por aqui',
    'scroll_down_more' => 'Role para baixo e veja mais',
    'send_photos' => 'Envie suas fotos',

];
