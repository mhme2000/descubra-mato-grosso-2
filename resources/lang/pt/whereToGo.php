<?php

return [

    'discover_routes' => 'Descubra os roteiros de Mato Grosso',
    'discover_destinations' => 'Descubra os destinos de Mato Grosso',
    'find_destination' => 'Encontre o seu destino em Mato Grosso',
    'what_do_you_expect' => 'O que você deseja?',

    'choose_trip_category' => 'Escolha um tipo de viagem',
    'choose_trip_type' => 'Escolha um tema específico',
    'search' => 'Buscar',

    'destinations_a_z' => 'Destinos de A a Z',
    'find_perfect_park' => 'Encontre o parque perfeito',



];