<?php

return [
    'page_not_found' => 'Página não encontrada',
    'subject' => 'Assunto',
    'state' => 'Estado onde você reside',
    'city' => 'Cidade',
    'city_live' => 'Cidade onde você reside',
    'category' => 'Categoria',
    'share' => 'Compartilhe',

    'january' => 'Janeiro',
    'february' => 'Fevereiro',
    'march' => 'Março',
    'april' => 'Abril',
    'may' => 'Maio',
    'june' => 'Junho',
    'july' => 'Julho',
    'august' => 'Agosto',
    'september' => 'Setembro',
    'october' => 'Outubro',
    'november' => 'Novembro',
    'december' => 'Dezembro',

    'sunday_abbreviation' => 'D',
    'monday_abbreviation' => 'S',
    'tuesday_abbreviation' => 'T',
    'wednesday_abbreviation' => 'Q',
    'thursday_abbreviation' => 'Q',
    'friday_abbreviation' => 'S',
    'saturday_abbreviation' => 'S',

    'sunday' => 'Domingo',
    'monday' => 'Segunda',
    'tuesday' => 'Terça',
    'wednesday' => 'Quarta',
    'thursday' => 'Quinta',
    'friday' => 'Sexta',
    'saturday' => 'Sábado',
    'holiday' => 'Feriado',

    'closed' => 'Fechado',
    '24hours' => '24 horas',



];
