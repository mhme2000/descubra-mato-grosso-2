<?php

return [

	'visit_gallery'  =>  'VISITE LA GALERÍA',
	'read_more'  =>  'Lea más',
	'read_more_posts'  =>  'Lea más publicaciones',
	'slideshow'  =>  'Presentación de diapositivas',
	'previous'  =>  'Anterior',
	'next'  =>  'Próximo',


];
