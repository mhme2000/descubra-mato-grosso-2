<?php

return [

	'weather_forecast'  =>  'Pronóstico meteorológico',
	'directions'  =>  'Cómo llegar',
	'forecast_unavailable'  =>  'Pronóstico indisponible',
	'select_destination'  =>  'Seleccione su destino',
	'type_origin'  =>  'Digite su punto de partida',
	'tourism_guide'  =>  'Guía Oficial de Turismo de Minas Gerais',
	'pdf_file'  =>  'Archivo PDF',
	
];
