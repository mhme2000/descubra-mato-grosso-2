<?php

return [

	'mark_attractions_to_show_or_select_destination'  =>  'Marque los atractivos a exhibir o filtre por destino',
	'nature'  =>  'Naturaleza',
	'waterfalls'  =>  'Cascadas',
	'lakes'  =>  'Lagos',
	'caves'  =>  'Grutas',
	'mountains'  =>  'Montañas',
	'hidromineral'  =>  'Estancias Hidro-minerales',
	'culture'  =>  'Cultura',
	'historic_centers'  =>  'Centros Históricos',
	'buildings_monuments'  =>  'Edificios y Monumentos',
	'churches'  =>  'Iglesias',
	'museums'  =>  'Museos, Instituciones Culturales y Teatros',
	'zoos'  =>  'Zoológico y Jardín Botánico',
	'markets'  =>  'Ferias y Mercados',
	'select_destination'  =>  'Seleccione un destino para exhibir sus atractivos',


];
