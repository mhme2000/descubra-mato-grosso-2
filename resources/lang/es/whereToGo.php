<?php

return [

	'discover_routes'  =>  'Descubra los guías de viaje de Minas',
	'discover_destinations'  =>  'Descubra los destinos de Minas',
	'find_destination'  =>  'Encuentre su destino en Minas',
	'what_do_you_expect'  =>  '¿Qué desea?',
	'choose_trip_category'  =>  'Escoja un tipo de viaje',
	'choose_trip_type'  =>  'Escoja un tema específico',
	'search'  =>  'Buscar',
	'destinations_a_z'  =>  'Destinos desde A hasta Z',
	'find_perfect_park'  =>  'Encuentre el parque perfecto',

];
