<?php

return [

	'mark_attractions_to_show_or_select_destination'  =>  'Marquez les attractions à être présentées ou filtrez par destination',
	'nature'  =>  'Nature',
	'waterfalls'  =>  'Cascades',
	'lakes'  =>  'Lacs',
	'caves'  =>  'Grottes',
	'mountains'  =>  'Montagnes',
	'hidromineral'  =>  'Villes thermales',
	'culture'  =>  'Culture',
	'historic_centers'  =>  'Centres Historiques',
	'buildings_monuments'  =>  'Immeubles et Monuments',
	'churches'  =>  'Églises',
	'museums'  =>  'Musées, Institutions Culturelles et Théatres',
	'zoos'  =>  'Zoo et Jardin Botanique',
	'markets'  =>  'Foires et Marchés',
	'select_destination'  =>  'Sélectionnez une destination pour présenter vos attractions',

];
