<?php

return [

	'visit_gallery'  =>  'VISITEZ LA GALERIE',
	'read_more'  =>  'Lisez plus',
	'read_more_posts'  =>  'Lisez plus de posts',
	'slideshow'  =>  'Slideshow',
	'previous'  =>  'Antérieur',
	'next'  =>  'Prochain',
	
];
