<?php

return [

	'search_results_for'  =>  'Résultats de la recherche par:',
	'show_more_results'  =>  'Présentez plus de  résultats',
	'occurrences_in_destinations'  =>  'Évènements en "Destinations"|Évènements en "Destinations"',
	'occurrences_in_routes'  =>  'Évènements en "Itinéraires"|Évènements en "Itinéraires"',
	'occurrences_in_attractions'  =>  'Évènements en "Attractions"|Évènements en "Attractions"',
	'occurrences_in_accomodation' => 'Évènements en "Hébergement"|Évènements en "Hébergement"',
	'occurrences_in_feeding' => 'Évènements en "Alimentation"|Évènements en "Alimentation"',
	'occurrences_in_services' => 'Évènements en "Services"|Évènements en "Services"',
	'occurrences_in_recreation' => 'Évènements en "Loisir"|Évènements en "Loisir"',
	'occurrences_in_events' => 'Évènements en "Évènements"|Évènements en "Évènements"',
	'occurrences'  =>  'Résultat|Résultats',
	'attractions_in'  =>  'Attractions sur',
	
];
