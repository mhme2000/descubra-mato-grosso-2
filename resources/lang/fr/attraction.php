'<?php

return [

	'useful_information'  =>  'Informations utiles',
	'location'  =>  'Localisation',
	'location_urban'  =>  'Urbaine',
	'location_rururban'  =>  'Rurbaine',
	'location_rural'  =>  'Rurale',
	'surroundings_description'  =>  'Description des environs et distance des principaux points',
	'reference_points'  =>  'Points de référence',
	'historic_period'  =>  'Période historique',
	'functioning_period'  =>  'Horaire de fonctionnement',
	'functioning_period_2'  =>  'Horaire de fonctionnement',
	'functioning_period_observation'  =>  'Observation concernant le fonctionnement',
	'visitation_type'  =>  'Type de visite',
	'visitation_type_not_guided'  =>  'Non guidée',
	'visitation_type_self_guided'  =>  'Auto-guidée',
	'visitation_type_guided'  =>  'Guidée',
	'entrance_type'  =>  'Entrée',
	'entrance_type_free'  =>  'Gratuite',
	'entrance_type_paid'  =>  'Payante',
	'value'  =>  'Valeur',
	'activities'  =>  'Activités réalisées',
	'requires_authorization'  =>  'Autorisation préalable nécessaire',
	'payment_methods'  =>  'Méthodes de paiement',
	'complementary_information'  =>  'Informations complémentaires',
	'accepts_animals'  =>  'Animaux admis',
	'gay_friendly'  =>  'Gay friendly',
	'doesnt_accept_kids'  =>  'Enfants non admis',
	'facilities'  =>  'Facilités',
	'cable_tv'  =>  'TV par câble',
	'voltage'  =>  'Voltage',
	'daily_rate_type'  =>  'Type de séjour',
	'daily_rate_type_no_breakfest'  =>  'Sans petit-déjeuner',
	'daily_rate_type_breakfest'  =>  'Avec petit-déjeuner',
	'daily_rate_type_half_pension'  =>  'Demi-pension',
	'daily_rate_type_full_pension'  =>  'Pension complète',
	'recreation'  =>  'Récréation et loisir',
	'pool'  =>  'Piscine',
	'adapted_pool'  =>  'Piscine adaptée',
	'heated_pool'  =>  'Piscine chauffée',
	'heated_adapted_pool'  =>  'Piscine chauffée adaptée',
	'dry_sauna'  =>  'Sauna sec',
	'adapted_dry_sauna'  =>  'Sauna sec adapté',
	'sauna'  =>  'Sauna à vapeur',
	'adapted_sauna'  =>  'Sauna à vapeur adapté',
	'court'  =>  'Terrain de sports',
	'adapted_court'  =>  'Terrain de sports adapté',
	'playground'  =>  'Playground',
	'adapted_playground'  =>  'Playground adapté',
	'hottub'  =>  'Hydromassage',
	'spa' => 'Spa',
	'day_use' => 'Utiliser Day',
	'touristic_guide' => 'Guide touristique',
	'fishing_guide' => 'Guide de pêche',
	'recreation_monitor' => 'loisirs moniteur/visites',
	'game_room'  =>  'Salle de jeux',
	'mounting_animals'  =>  "Animaux d'équitation",
	'soccer_court'  =>  'Terrain de football',
	'bowling'  =>  'Boliche',
	'boats'  =>  'Bateaux/Embarcations',
	'fishing'  =>  'Équipements de pêche',
	'cheering_service'  =>  "Service d'animation",
	'cooper_track'  =>  'Piste de cooper',
	'carriages'  =>  'Charrettes',
	'golf'  =>  'Golf',
	'diving_equipment'  =>  'Équipement de plongée',
	'ping_pong'  =>  'Ping-pong',
	'eletronic_games'  =>  'Jeux électroniques',
	'bar_restaurant'  =>  'Bar/Café/Restaurant',
	'services_equipments'  =>  'Services et équipements',
	'event_place'  =>  'Lieu pour les  évènements',
	'capacity'  =>  'Capacité',
	'live_music'  =>  'Musique live',
	'mechanic_music'  =>  'Musique mécanique',
	'parking_lot'  =>  'Stationnement',
	'air_conditioning'  =>  'Air réfrigéré',
	'recreation_area'  =>  'Récréation/Aire de loisir',
	'smoking_lounge'  =>  'Espace pour fumeurs',
	'adapted_toilets'  =>  'Toilettes adaptées',
	'braille_menu'  =>  'Menu en braille',
	'vallets'  =>  'Voituriers',
	'internet'  =>  'Internet',
	'cuisine_nationality'  =>  'Cuisine - Nationalité',
	'cuisine_service'  =>  'Cuisine - Service',
	'cuisine_theme'  =>  'Cuisine - Thème',
	'cuisine_region'  =>  'Cuisine - Région',
	'restrictions'  =>  'Restrictions',
	'tourism_specialization'  =>  'Secteurs ou types de tourisme dans lequel il est spécialisé',
	'specialization_adventure'  =>  'Aventure',
	'specialization_ecoturism'  =>  'Écotourisme',
	'specialization_rural'  =>  'Rural',
	'specialization_sun_beach'  =>  'Soleil et plage',
	'specialization_studies'  =>  'Études et échange',
	'specialization_business'  =>  'Affaires et évènements',
	'specialization_cultural'  =>  'Culturel',
	'specialization_nautic'  =>  'Nautique',
	'specialization_health'  =>  'Santé (bien-être et médical)',
	'specialization_fishing'  =>  'Pêche',
	'service_types'  =>  'Types de service',
	'service_type_excursion'  =>  'Excursion',
	'service_type_local'  =>  'Promendade locale',
	'service_type_special'  =>  'Spécial',
	'service_type_transfer'  =>  'Transfert',
	'adapted_vehicles_quantity'  =>  'Quantité de véhicules adaptés',
	'support_services_equipments'  =>  'Services et équipements de soutien',
	'security_service'  =>  'Services de sûreté',
	'medical_services'  =>  'Services Médicaux',
	'ambulatorial_services'  =>  'Services de premiers soins',
	'buffet'  =>  'Service de café/buffet',
	'touristic_info'  =>  'Informations touristiques',
	'stores'  =>  'Magasins',
	'bank24hours'  =>  'Banque 24 heures',
	'parking_lot_spots'  =>  'Places',
	'main_activities'  =>  'Activités principales',
	'accessibility'  =>  'Accessibilité',
	'accessibility_accessible_external_route'  =>  'Itinéraire extérieur accessible',
	'accessibility_accessible_external_route_others'  =>  'Itinéraire extérieur accessible - Autres',
	'accessibility_boarding_area'  =>  "Lieu d'embarquement et de débarquement",
	'accessibility_parking_spot'  =>  'Place sur le stationnement',
	'accessibility_access_area'  =>  'Domaine de circulation/accès interne pour les fauteuils roulants',
	'accessibility_stairs'  =>  'Escalier',
	'accessibility_elevator'  =>  'Ascenseur',
	'accessibility_communication'  =>  'Communication',
	'acessibility_special_seat_obese' => 'Siège spécial obèses',
	'accessibility_toilet'  =>  'Toilette',
	'accessibility_low_sidewalk'  =>  'Trottoir rabaissé',
	'accessibility_walking_stripes'  =>  'Passage à piéton',
	'accessibility_ramp'  =>  'Rampe',
	'accessibility_sound_semaphore'  =>  'Feux de signalisation sonores',
	'accessibility_floor_tactile_alert'  =>  "Surface tactile d'alerte",
	'accessibility_regular_floor'  =>  'Sol régulier/antidérapant',
	'accessibility_obstacle_free'  =>  'Sans  obstacles',
	'accessibility_signalized'  =>  'Signalé',
	'accessibility_leveled_access'  =>  'Avec un accès à niveau',
	'accessibility_signalized_a'  =>  'Signalée',
	'accessibility_large_wheelchair'  =>  'Alargada pour fauteuil roulant',
	'accessibility_sidewalk_ramp'  =>  "Rampe d'accès au trottoir",
	'accessibility_elevator_platform'  =>  'Plateforme élévatrice',
	'accessibility_circulation_furniture'  =>  'Avec une circulation entre les meubles',
	'accessibility_large_door'  =>  'Porte large',
	'accessibility_handrail'  =>  'Rambarde',
	'accessibility_resting_platform'  =>  'Palier pour le repos',
	'accessibility_tactile_alert_signaling'  =>  "Signalisation tactile d'alerte",
	'accessibility_non_skid_floor'  =>  'Sol antidérapant',
	'accessibility_braille_signaling'  =>  'Signalé en Braille',
	'accessibility_sound_mechanism'  =>  'Dispositif sonore',
	'accessibility_light_mechanism'  =>  'Dispositif lumineux',
	'accessibility_eletronic_sensor'  =>  'Capteur électronique (porte)',
	'accessibility_braille_informative_text'  =>  'Texte informatif en Braille',
	'accessibility_large_informative_text'  =>  'Texte informatif avec une police plus grande',
	'accessibility_signaling_interpreter'  =>  'Interprète en Libras (langue brésilienne de signaux)',
	'accessibility_support_bar'  =>  'Barre de soutien',
	'accessibility_large_door_wheelchair'  =>  "Porte suffisamment large pour l'entrée de fauteuil roulant",
	'accessibility_wheelchair_spin'  =>  'Plaque tournante pour fauteuil roulant',
	'accessibility_wheelchair_access'  =>  'Accès pour fauteuil roulant',
	'accessibility_low_sink'  =>  'Évier rabaissé',
	'accessibility_low_mirror'  =>  "Miroir rabaissé ou avec un angle d'accès visible",
	'accessibility_adapted_bath'  =>  'Box ou baignore adaptée',
	'accessibility_agency_structure'  =>  'Acessibilité dans la structure des agences de tourisme',
	'accessibility_adapted_routes'  =>  'Itinéraires Adaptés',
	'accessibility_accomodation'  =>  "Moyens d'hébergement",
	'accessibility_transport'  =>  'Transports',
	'accessibility_prepared_guides'  =>  'Guides préparés',
	'accessibility_attractions'  =>  'Attractions',
	'accessibility_accessible_material'  =>  'Matériel accessible',
	'accessibility_special_parking_spots'  =>  'Places de stationnement spéciales',
	'accessibility_access_ramps'  =>  "Rampes d'accès",
	'accessibility_capacitated_workers'  =>  'Employés qualifiés',
	'accessibility_tactile_signaling'  =>  'Signaux tactiles (handicap visuel)',
	'accessibility_accessible_toilet'  =>  'Toilette accessible',
	'accessibility_displacement_space'  =>  'Espace pour le déplacement (handicapés)',
	'accessibility_physical'  =>  'Physique ou motrice',
	'accessibility_visual'  =>  'Visuel',
	'accessibility_hearing'  =>  'Auditive',
	'accessibility_mental'  =>  'Mentale',
	'accessibility_reduced_mobility'  =>  "Mobilité réduite (personnes âgées, femmes enceintes, obèses parmi d'autres)",
	'accessibility_large_door_obese' => 'Porte assez large pour entrer obèses',
	'accessibility_toilet_seat_obese' => 'Santé avec siège spécial pour obèses',

	'place_closed' => 'Local fermé.',
	'place_closed_reason' => 'Raison: ',
	'place_closed_works' => 'Fermé pour rénovation / construction',
	'place_closed_restricted' => "Restreint par l'organisme de réglementation",
	'place_closed_economic_conjuncture' => 'La conjoncture économique',
	'place_closed_maintenance' => "Problèmes d'entretien",
	'place_closed_meteorological_phenomena' => "Phénomène météorologique",
	'place_closed_climatic_phenomena' => 'Phénomène climatologique',
	'place_closed_access_infrastructure_damaged' => "Infrastructure d'accès endommagé",
	'place_closed_other' => 'Autre',

];
