<?php

return [

	'weather_forecast'  =>  'Prévision du Temps',
	'directions'  =>  'Comment arriver',
	'forecast_unavailable'  =>  'Prévision non  disponible',
	'select_destination'  =>  'Sélectionnez votre destination',
	'type_origin'  =>  'Saisissez votre point de départ',
	'tourism_guide'  =>  'Guide Officiel du Tourisme de Minas Gerais',
	'pdf_file'  =>  'Fichier PDF',
	
];
