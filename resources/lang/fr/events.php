<?php

return [

	'find_event'  =>  "Trouvez l'évènement que vous recherchez",
	'event_date'  =>  "Date de l'évènement",
	'fill_form_message'  =>  'Remplissez les données du filtre pour enquêter',

];
