<?php

return [

	'search_content'  =>  'Recherche par contenus',
	'back'  =>  'Retour',
	'related_content'  =>  'Contenu Associé',
	'archive'  =>  'Fichier',

];
