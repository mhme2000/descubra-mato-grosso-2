<?php

return [

	'name'  =>  'Nom',
	'message'  =>  'Message',
	'send'  =>  'Envoyer',
	'message_success'  =>  'Message envoyé avec succès!',
	'message_many_trials_same_ip'  =>  "Trop d'essais enregistrés sur le même IP. Réessayez plus tard",
	'confirm_you_are_not_a_robot'  =>  "Vousdevez confirmer que vous n'êtes pas un robot!",

];
