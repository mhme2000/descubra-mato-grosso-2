@extends('emails.base')
@section('mail-title')
	Senha resetada
@stop
@section('mail-content')
	<p style="font-size: 16px;line-height: 24px;padding: 25px 0;">Sua senha do <a href="{{url('/admin')}}">Descubra Mato Grosso</a> foi resetada.</p>
	<p style="font-size: 16px;line-height: 24px;padding: 25px 0;">A nova senha é <strong>{{ $p_NewPassword }}</strong></p>
	<p style="font-size: 16px;line-height: 24px;padding: 25px 0;">Para sua segurança, altere-a assim que possível.</p>
@stop
