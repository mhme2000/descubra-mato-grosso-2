@extends('emails.base')
@section('mail-title')
	{{ $p_Title }}
@stop
@section('mail-content')
	{!! $p_Msg !!}
@stop