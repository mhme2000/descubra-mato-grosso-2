@extends('emails.base')
@section('mail-title')
	Fale conosco
@stop
@section('mail-content')
	<p style="font-size: 16px;line-height: 24px;padding: 25px 0;">Nome: {{ $p_Name }}</p>
	<p style="font-size: 16px;line-height: 24px;padding: 25px 0;">E-mail: {{ $p_Email }}</p>
	<p style="font-size: 16px;line-height: 24px;padding: 25px 0;">Assunto: {{ $p_Subject }}</p>
	<p style="font-size: 16px;line-height: 24px;padding: 25px 0;">Estado: {{ $p_City->state }}</p>
	<p style="font-size: 16px;line-height: 24px;padding: 25px 0;">Cidade: {{ $p_City->name }}</p>
	<p style="font-size: 16px;line-height: 24px;padding: 25px 0;">Mensagem: {{ $p_Message }}</p>
@stop
