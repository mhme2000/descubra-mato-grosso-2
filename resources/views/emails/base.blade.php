<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Descubra Mato Grosso</title>
	<style type="text/css">
		h1,h2,h3,h4,h5,h6 {margin-bottom:15px; color:#000;}
	</style>
</head>
<body bgcolor="#FFF" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" style="-webkit-font-smoothing:antialiased;-webkit-text-size-adjust:none;width: 100%!important;height: 100%;font-family: 'Arial', sans-serif;">
<table width="600" align="center" cellpadding="0" cellspacing="0" bgcolor="#fff">
	<tr>
		<td>
			<table id="main" width="600" align="center" cellpadding="0" cellspacing="0" bgcolor="#fff">
				<tr>
					<td>
						<table id="header" cellpadding="0" cellspacing="0" align="center" bgcolor="#fff">
							<tr height="80">
								<td width="600" bgcolor="" align="center"><img src="{{url('/portal/assets/imgs/descubraMatoGrosso')}}" alt="Descubra Mato Grosso"></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<table id="content-1" cellpadding="0" cellspacing="45" align="left">
							<tr>
								<td>
									<h1 style="color:#f26522;font-weight:bold;margin:0;padding:0.25rem 0;font-size:24px;">@yield('mail-title')</h1>

									@yield('mail-content')

									{{--<p style="font-size: 16px;line-height: 24px;padding: 25px 0;">--}}
										{{--Quisque vehicula metus eu tincidunt tempus. Sed sit amet auctor nulla. Ut mollis blandit est, id tincidunt dolor cursus in. Donec posuere, arcu eget ornare tempus, risus mauris tristique sem, vel imperdiet massa metus sit amet felis. Mauris dapibus varius sem ut pharetra. In porta eros sed auctor semper. Fusce non neque pulvinar, pretium nisl a, congue quam. Ut interdum nulla ac vehicula facilisis. In maximus nunc quis tortor eleifend, a viverra est suscipit.--}}
									{{--</p>--}}
									{{--<p id="call" style="font-size: 16px;line-height: 24px;color: #f26522;text-align: center;padding: 0;">--}}
										{{--Exemplo de inserção de <a href="#" style="color: #f26522;text-decoration: underline;">call to action</a>--}}
									{{--</p>--}}
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<table id="header" cellpadding="0" cellspacing="0" align="center" bgcolor="#ebebeb">
							<tr height="90">
								<td width="600" bgcolor="" align="center"><img src="{{url('/portal/assets/imgs/sedec.png')}}" alt="SEDEC"></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</body>
</html>