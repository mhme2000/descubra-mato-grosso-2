@extends('emails.base')
@section('mail-title')
    Erro no sistema
@stop
@section('mail-content')
    <p>{!! $p_Error !!}</p>
@stop