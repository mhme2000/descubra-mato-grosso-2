<!DOCTYPE html>
<html lang="pt-br">

<head>
    <!-- Analytics Interno-->


@if (url()=="http://201.49.164.168" || url()=="http://201.49.164.168")
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-125337813-1"></script>
    
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'REPLACEME');
    </script>

@endif


    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="no-cache" http-equiv="pragma">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta property="og:type" content="website">
    <meta property="og:url" content="">
    <meta property="og:title" content="">
    <meta property="og:description" content="">
    <meta property="og:image" content="">
    <meta name="description" content="Descubra Mato Grosso">
    <meta name="author" content="Descubra Mato Grosso">
    <link rel="icon" href="{{url('/favicon.ico')}}" />
    <title>@if(isset($title)){{$title}}@else{{'Descubra Mato Grosso'}}@endif</title>
    <link rel="stylesheet" type="text/css" href="{{url('/portal/assets/libs/bootstrap-4.1.3/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('/portal/assets/libs/bootstrap-4.1.3/bootstrap-reboot.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('/portal/assets/libs/bootstrap-4.1.3/bootstrap-grid.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('/portal/assets/libs/open-iconic/css/open-iconic-bootstrap.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('/portal/assets/libs/daterangepicker-master/daterangepicker.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('/portal/assets/libs/slick/slick-slide.css')}}">
    <script type="text/javascript" src="{{url('/portal/assets/libs/jquery/jquery-3.3.1.min.js')}}"></script>
    <script type="text/javascript" src="{{url('/portal/assets/libs/slick/slick.js')}}"></script>
    <style>
        @font-face {
            font-family: 'Signika';
            font-style: normal;
            font-weight: 400;
            src: url("{{url('/portal/assets/fonts/signika/signika-v8-latin-regular.eot')}}");
            /* IE9 Compat Modes */
            src: local('Signika Regular'),
            local('Signika-Regular'),
            url("{{url('/portal/assets/fonts/signika/signika-v8-latin-regular.eot?#iefix')}}") format('embedded-opentype'),
            /* IE6-IE8 */
            url("{{url('/portal/assets/fonts/signika/signika-v8-latin-regular.woff2')}}") format('woff2'),
            /* Super Modern Browsers */
            url("{{url('/portal/assets/fonts/signika/signika-v8-latin-regular.woff')}}") format('woff'),
            /* Modern Browsers */
            url("{{url('/portal/assets/fonts/signika/signika-v8-latin-regular.ttf')}}") format('truetype'),
            /* Safari, Android, iOS */
            url("{{url('/portal/assets/fonts/signika/signika-v8-latin-regular.svg#Signika')}}") format('svg');
            /* Legacy iOS */
        }
    </style>
    <link rel="stylesheet" type="text/css" href="{{url('/portal/assets/libs/portal/css/main.css')}}"> @yield('pageCSS')
</head>

<body>

    @yield('header') 
    @yield('main-content') 
    @yield('footer')


    <script type="text/javascript" src="{{url('/portal/assets/libs/popper/popper-2018.min.js')}}"></script>
    <script type="text/javascript" src="{{url('/portal/assets/libs/bootstrap-4.1.3/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{url('/portal/assets/libs/bootstrap-4.1.3/bootstrap.bundle.min.js')}}"></script>
   
    <script type="text/javascript" src="{{url('/portal/assets/libs/daterangepicker-master/moment.min.js')}}"></script>
    <script type="text/javascript" src="{{url('/portal/assets/libs/daterangepicker-master/daterangepicker.js')}}"></script>
    <script type="text/javascript" src="{{url('/portal/assets/libs/portal/js/main.js')}}"></script>
    @yield('pageScript')

<script>





$(document).ready(function() {

});

$(window).on('load', function() {
    $('.modal-onload').fadeOut(200, function(){
        $('.modal-onload').removeClass('modal-onload').addClass('unload-modal');
    });
});

$(document).on('unload', function() {
  //$('.unload-modal').removeClass('unload-modal').addClass('modal-onload');
  //alert('oi');
});
    
</script>    
</body>

</html>
