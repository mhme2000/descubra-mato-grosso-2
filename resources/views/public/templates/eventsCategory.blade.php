{{-- Recebe Array para organização de categoria de acordo com recomendações --}}
<script>
recCategoria=NULL;
</script>
{{-- Fim de Processo --}}

<div class="container mt-2 fadeInOnLoad" style="display: none;">
        <h1 class="text-center text-black-10 display-4">CATEGORIAS</h1>
        <section class="customer-logos slider overlap-category-title">

                <div class="slide text-center btnSearchCategory"  category-id="8">
                                <div class="rounded-circle bg-page @if (isset($filter) && isset($filter['category_id']) && $filter['category_id']==8) selected-category @endif shadow-sm-3 w-75 p-3 p-sm-3 p-md-3 mt-2 mb-2 mr-auto ml-auto">
                                        <img class="fade-image w-100 mr-auto ml-auto" src="{{url('/image/60/60/false/false/portal/assets/libs/imgs/categories_of_events/pictograma/gastronomia.png')}}">
                                </div>
                                <span class="text-category-span mt-auto mb-auto"><small><b>GASTRONÔMICOS</b></small></span>
                </div>
                <div class="slide text-center btnSearchCategory"  category-id="9">
                                <div class="rounded-circle bg-page @if (isset($filter) && isset($filter['category_id']) && $filter['category_id']==9) selected-category @endif shadow-sm-3 w-75 p-3 p-sm-3 p-md-3 mt-2 mb-2 mr-auto ml-auto">
                                        <img class="fade-image w-100 mr-auto ml-auto" src="{{url('/image/60/60/false/false/portal/assets/libs/imgs/categories_of_events/pictograma/popular.png')}}">
                                </div>
                                <span class="text-category-span mt-auto mb-auto"><small><b>POPULAR</b></small></span>
                </div>
                <div class="slide text-center btnSearchCategory"  category-id="4">
                                <div class="rounded-circle bg-page @if (isset($filter) && isset($filter['category_id']) && $filter['category_id']==4) selected-category @endif shadow-sm-3 w-75 p-3 p-sm-3 p-md-3 mt-2 mb-2 mr-auto ml-auto">
                                        <img class="fade-image w-100 mr-auto ml-auto" src="{{url('/image/60/60/false/false/portal/assets/libs/imgs/categories_of_events/pictograma/shows.png')}}">
                                </div>
                                <span class="text-category-span mt-auto mb-auto"><small><b>SHOWS</b></small></span>
                </div>
                <div class="slide text-center btnSearchCategory"  category-id="10">
                                <div class="rounded-circle bg-page @if (isset($filter) && isset($filter['category_id']) && $filter['category_id']==10) selected-category @endif shadow-sm-3 w-75 p-3 p-sm-3 p-md-3 mt-2 mb-2 mr-auto ml-auto">
                                        <img class="fade-image w-100 mr-auto ml-auto" src="{{url('/image/60/60/false/false/portal/assets/libs/imgs/categories_of_events/pictograma/religioso.png')}}">
                                </div>
                                <span class="text-category-span mt-auto mb-auto"><small><b>RELIGIOSOS</b></small></span>
                </div>
                <div class="slide text-center btnSearchCategory" category-id="6">
                                <div class="rounded-circle bg-page @if (isset($filter) && isset($filter['category_id']) && $filter['category_id']==6) selected-category @endif shadow-sm-3 w-75 p-3 p-sm-3 p-md-3 mt-2 mb-2 mr-auto ml-auto">
                                        <img class="fade-image w-100 mr-auto ml-auto" src="{{url('/image/60/60/false/false/portal/assets/libs/imgs/categories_of_events/pictograma/culturais.png')}}">
                                </div>
                                <span class="text-category-span mt-auto mb-auto"><small><b>CULTURAIS</b></small></span>
                </div>
                <div class="slide text-center btnSearchCategory"  category-id="1">
                                <div class="rounded-circle bg-page @if (isset($filter) && isset($filter['category_id']) && $filter['category_id']==1) selected-category @endif shadow-sm-3 w-75 p-3 p-sm-3 p-md-3 mt-2 mb-2 mr-auto ml-auto">
                                        <img class="fade-image w-100 mr-auto ml-auto" src="{{url('/image/60/60/false/false/portal/assets/libs/imgs/categories_of_events/pictograma/feira.png')}}">
                                </div>
                                <span class="text-category-span mt-auto mb-auto"><small><b>FEIRAS</b></small></span>
                </div>
                <div class="slide text-center btnSearchCategory" category-id="7">
                                <div class="rounded-circle bg-page @if (isset($filter) && isset($filter['category_id']) && $filter['category_id']==7) selected-category @endif shadow-sm-3 w-75 p-3 p-sm-3 p-md-3 mt-2 mb-2 mr-auto ml-auto">
                                        <img class="fade-image w-100 mr-auto ml-auto" src="{{url('/image/60/60/false/false/portal/assets/libs/imgs/categories_of_events/pictograma/esportivo.png')}}">
                                </div>
                                <span class="text-category-span mt-auto mb-auto"><small><b>ESPORTIVOS</b></small></span>
                </div>
                <div class="slide text-center btnSearchCategory" category-id="5">
                        <div class="rounded-circle bg-page @if (isset($filter) && isset($filter['category_id']) && $filter['category_id']==5) selected-category @endif shadow-sm-3 w-75 p-3 p-sm-3 p-md-3 mt-2 mb-2 mr-auto ml-auto">
                                <img class="fade-image w-100 mr-auto ml-auto" src="{{url('/image/60/60/false/false/portal/assets/libs/imgs/categories_of_events/pictograma/civico.png')}}">
                        </div>
                        <span class="text-category-span"><small><b>CÍVICOS</b></small></span>
                </div>
                <div class="slide text-center btnSearchCategory" category-id="2">
                        <div class="rounded-circle bg-page @if (isset($filter) && isset($filter['category_id']) && $filter['category_id']==2) selected-category @endif shadow-sm-3 w-75 p-3 p-sm-3 p-md-3 mt-2 mb-2 mr-auto ml-auto">
                                <img class="fade-image w-100 mr-auto ml-auto" src="{{url('/image/60/60/false/false/portal/assets/libs/imgs/categories_of_events/pictograma/congresso.png')}}">
                        </div>
                        <span class="text-category-span mt-auto mb-auto"><small><b>CONGRESSOS</b></small></span>
                </div>
                <div class="slide text-center btnSearchCategory"  category-id="3">
                        <div class="rounded-circle bg-page @if (isset($filter) && isset($filter['category_id']) && $filter['category_id']==3) selected-category @endif shadow-sm-3 w-75 p-3 p-sm-3 p-md-3 mt-2 mb-2 mr-auto ml-auto">
                                <img class="fade-image w-100 mr-auto ml-auto" src="{{url('/image/60/60/false/false/portal/assets/libs/imgs/categories_of_events/pictograma/outros.png')}}">
                        </div>
                        <span class="text-category-span mt-auto mb-auto"><small><b>OUTROS</b></small></span>
                </div>
        </section>
</div>

