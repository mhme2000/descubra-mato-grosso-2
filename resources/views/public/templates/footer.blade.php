<footer class="text-muted">
    <div class="row-fluid line-footer">
        <img class="img-fluid py-0" src="{{url('/portal/assets/imgs/_ondas.png')}}" alt="">
    </div>
    <div class="container">
        <div class="row">
            <!--<div class="col-12 col-sm-6 col-md-3 text-justify" style="width: 18rem;">                     
                <h6 style="font-size: 16px">CONTATOS:</h6>
                <p style="font-size: 14px">+55 65 3613-9300 / 3613-9313</p>
                <h6 style="font-size: 16px">E-MAIL:</h6>
                <p style="font-size: 14px">marketing@sedec.mt.gov.br</p>
                <p style="font-size: 14px">Rua Voluntários da Pátria, 118 Centro Norte - Cuiabá - MT CEP: 780005-000</p>                
            </div>-->
            <div class="col-12 col-sm-6 col-md-3 py-5 text-left pl-5">            
                
                    
                
                <a class="nav-link" href="{{url($p_Language . '/fale-conosco')}}" style="color: white; font-family: Trebuchet MS;"><strong>Fale conosco</strong></a>
                <a class="nav-link" href="{{url($p_Language . '/conheca/galeria')}}" style="color: white; font-family: Trebuchet MS;"><strong>Galeria</strong></a>
                <a href="{{url($p_Language . '/doacao-de-midias')}}" class="nav-link" style="color: white; font-family: Trebuchet MS;"><strong>Doe suas fotos/vídeos</strong></a>   
                <a class="nav-link" href="{{url('/admin/login')}}" target="_blank" ><h2 style="font-size: 19px; color: orange; font-family: Trebuchet MS;">Área do Prestador</h2></a>
            </div>
            <div class="col-12 col-sm-6 col-md-3 py-5 text-center" style="width: 18rem;">
                <a href="#" role="button">
                    <img class="img-fluid" src="{{url('/portal/assets/imgs/descubraMatoGrosso.png')}}" alt="Descubra Mato Grosso">
                </a>                                             
            </div>
            <div class="col-12 col-sm-6 col-md-3 py-5 text-center" style="width: 18rem;">
                <a href="http://www.sedec.mt.gov.br/" target="_blank" role="button">
                    <img class="img-fluid" src="{{url('/portal/assets/imgs/sedec.png')}}" alt="SEDEC">
                </a>                                               
            </div>
            <div class="col-12 col-sm-6 col-md-3 py-5 text-center" style="width: 18rem;">
                <a href="https://cadastur.turismo.gov.br/hotsite/" target="_blank" role="button">
                    <img class="img-fluid" src="{{url('/portal/assets/imgs/logoCadastur.png')}}" alt="cadastur">
                </a>                                               
            </div>
        </div>
    </div>
    <div class="text-center">
        <div class="container">                  
            <h2 style="font-size: 15px; color:white; margin-bottom:0px;">Secretaria Adjunta de Turismo</h2>
            <p style="font-size: 14px; margin-bottom:0px;">Rua Voluntários da Pátria, 118 - Centro Norte - Cuiabá/MT - CEP 78.005-180</p>                
            <p style="font-size: 14px; ">+55 (65) 3613-9320 / +55 (65) 3613-9300</p>
            <p style="font-size: 12px">Cedido gratuitamente pela Secretaria de Estado de Turismo de Minas Gerais e desenvolvido pela empresa de software Oro Digital LTDA. </p>
        </div>
    </div>
</footer>
<style>
footer {
    background-color: #009DC7;
}
</style>