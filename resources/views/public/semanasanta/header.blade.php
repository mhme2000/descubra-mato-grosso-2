<?php  $today = date("Y-m-d H:i:s");  $date = get_limite_data_especial();  ?>
<style>

.nav-link-white, .navbar-light .navbar-nav .nav-link{
    color:white;
}

.nav-link:hover{
    color:red;
}


.calendar-day-single, .calendar-month-single {
    background-color:#6d4e8a;
}
.btn-primary{
    background-color:#6d4e8a;
}
.btn-primary:hover {
    background-color:black;
}
@media (min-width: 992px)  {
    h1{
        font-size:4em;
    }
    h2{
        font-size:2em;

    }
    h3{
        font-size:2em;
        line-height:2em;
    }      
    header{
    background-image:url('/assets/img/semanasanta/banner.jpg'); 
    color:white;
    
}    
    
}

@media (max-width: 991px)  {
    h1{
        font-size:1.7em;
    }
    h2{
        font-size:1em;
        line-height:2em;
    }  
    h3{
        font-size:1em;
        line-height:2em;
    }         
    header{
    background-color:#6d4e8a;
    color:white;
    
}     
}
.custom-select, .form-control {
    border:1px solid #6d4e8a;
}

.link-no-hover{
    color:black;
}
  
</style>

<header class=" container-fluid  shadow-sm-1">
    <nav class="navbar navbar-expand-md flex-nowrap  navbar-light">
        <div class="col-7 col-sm-3 col-md-3 col-lg-3 col-xl-3">
            <a class="navbar-brand mr-0 mr-md-2" href="{{url($p_Language)}}">
                <img class="img-fluid" src="{{url('/assets/img/semanasanta/minasgerais.png')}}" alt="Minas Gerais" title="Minas Gerais">
            </a>
        </div>
 

        <ul class="navbar-nav flex-row small mr-auto ml-auto d-none d-md-flex">
            <li class="nav-item">
                <a class="nav-link px-1" href="https://www.facebook.com/VisiteMinasGerais/" target="_blank">
                    <img src="{{url('/assets/img/semanasanta/face.banner.png')}}" alt="Facebook" title="Facebook">
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link px-1" href="https://instagram.com/visiteminasgerais" target="_blank">
                    <img src="{{url('/assets/img/semanasanta/instagram.png')}}" alt="Instagram" title="Instagram">
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link strings px-1" href="{{url($p_Language . '/minas360')}}" title="Minas em 360">Minas em 360°</a>
            </li>
            <li class="nav-item">
                    <a class="nav-link strings px-1" href="{{url($p_Language . '/blog')}}">Blog</a>
            </li>
            <li class="nav-item">
                <a class="nav-link strings px-1" href="{{url($p_Language . '/fale-conosco')}}">{{trans('menu.contact')}}</a>
            </li>
            <li class="nav-item">
                <a class="nav-link strings px-1" href="{{url('/admin/login')}}" target="_blank">{{trans('menu.register_your_business', [],'messages', $p_Language)}}</a>
            </li>
            <li class="nav-item">
                <div class="dropdown">
                    <button class="btn btn-light dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <img class="lang-flag" src="{{url('/portal/assets/imgs/flag-' . $p_Language . '.png')}}">
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                        <a class="dropdown-item" href="{{url('/pt')}}"><img class="lang-flag" src="{{url('/portal/assets/imgs/flag-' . "pt" . '.png')}}"> Português</a>
                        <!--
                        <a class="dropdown-item disabled" href="{{url('/fr')}}"><img class="lang-flag" src="{{url('/portal/assets/imgs/flag-' . "fr" . '.png')}}"> Français</a>
                        <a class="dropdown-item disabled" href="{{url('/en')}}"><img class="lang-flag" src="{{url('/portal/assets/imgs/flag-' . "en" . '.png')}}"> English</a>
                        <a class="dropdown-item disabled" href="{{url('/es')}}"><img class="lang-flag" src="{{url('/portal/assets/imgs/flag-' . "es" . '.png')}}"> Español</a>
                        -->
                    </div>
                </div>
            </li>
        </ul>
        <button class="navbar-toggler ml-auto" type="button" data-toggle="collapse" data-target="#navbar2">
            <span class="navbar-toggler-icon"></span>
        </button>
    </nav>
    <nav class="navbar navbar-expand-md navbar-light">
        <div class="navbar-collapse collapse pt-2 pt-md-0 flex-nowrap" id="navbar2">
            <div class="row container-fluid">
                <div class="col-12 col-md-12">
                    <ul class="navbar-nav">
                        <li class="nav-item text-alert-lighter">
                            <a class="nav-link nav-link-white " href="{{url($p_Language . '/o-que-fazer')}}"><strong>{{trans('menu.what_to_do')}}</strong></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link nav-link-white " href="{{url($p_Language . '/roteiros')}}"><strong>{{trans('menu.where_to_go')}}</strong></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link nav-link-white " href="{{url($p_Language . '/explore-o-mapa')}}"><strong>{{trans('menu.explore_the_map')}}</strong></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link nav-link-white " href="{{url($p_Language . '/roteiros-duracao')}}"><strong>{{trans('menu.plan_your_trip')}}</strong></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link nav-link-white " href="{{url($p_Language . '/eventos')}}"><strong>{{trans('menu.events')}}</strong></a>
                        </li>
                        <li class="nav-item pl-3 d-none d-md-flex w-25">
                                <form method="GET" action="{{url('/pt/busca')}}"  accept-charset="UTF-8" class="form-inline py-1">
                                    <div class="input-group">
                                        <input type="text" name="q" class="form-control" placeholder="{{trans('menu.search_in_site')}}" aria-label="" aria-describedby="btnGroupAddon2">
                                        <div class="input-group-append">
                                            <button type="submit" class="input-group-text" id="btnGroupAddon2"><span class="oi oi-magnifying-glass"></span></button>
                                        </div>
                                    </div>                                                                                        
                                </form>
                            </li>
                    </ul>       
                </div>

                <ul class="navbar-nav flex-row small mr-auto ml-auto d-flex d-md-none">
                    <li class="nav-item">
                        <a class="nav-link px-1" href="https://www.facebook.com/VisiteMinasGerais/" target="_blank">
                            <img src="{{url('/assets/img/semanasanta/face.banner.png')}}"  alt="Facebook" title="Facebook">
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link px-1" href="https://instagram.com/visiteminasgerais" target="_blank">
                            <img src="{{url('/assets/img/semanasanta/instagram.png')}}"  alt="Instagram" title="Instagram">
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link strings px-1" href="{{url($p_Language . '/minas360')}}" title="Minas em 360">Minas em 360°</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link strings px-1" href="{{url($p_Language . '/fale-conosco')}}">{{trans('menu.contact')}}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link strings px-1" href="{{url('/admin/login')}}" target="_blank">{{trans('menu.register_your_business', [],'messages', $p_Language)}}</a>
                    </li>
                </ul>

                <ul class="navbar-nav flex-row small mr-auto ml-auto d-flex d-md-none">
                        <li class="nav-item">
                                <div class="btn-group btn-group-xs" role="group" aria-label="">
                                        <button type="button" class="btn btn-secondary"><img class="lang-flag" src="{{url('/portal/assets/imgs/flag-' . "pt" . '.png')}}"></button>

                                </div>
                        </li>
                </ul>                

            </div>
        </div>
    </nav>
</header>

<div class="container mt-5" >
    <h1 style="font-family:santa_feregular; color:red;"> Venha viver a <a href="/pt/semana-santa" style="color:#6d4e8a;"> Semana Santa</a> em Minas Gerais</h1>
    </div>

@include('public.semanasanta.searchbar')