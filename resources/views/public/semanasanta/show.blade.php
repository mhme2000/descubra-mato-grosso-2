@extends('public.templates.base')
@section('pageCSS')
<link rel="stylesheet" type="text/css" href="{{url('/portal/assets/libs/portal/css/eventsShow.css')}}">
<link rel="stylesheet" type="text/css" href="{{url('/portal/assets/libs/portal/css/eventsSearchbar.css')}}">
<link rel="stylesheet" type="text/css" href="{{url('/assets/fonts/santafe/stylesheet.css')}}">
<link rel="stylesheet" type="text/css" href="{{url('/assets/fonts/mistral/stylesheet.css')}}">

@endsection

@section('header')
@include('public.semanasanta.header')
@endsection

@section('main-content')
<main class="container-fluid mt-4">


    <section class="container">

        <!-- START THE FEATURETTES -->

        @if (isset($event))

        <hr class="featurette-divider">

        <div class="row featurette">
            <div class="col-md-5 order-md-1">

                @if (count($event->EventPhotos) == 0)
                <img class="d-block w-100"
                    src="/image/430/322/true/true/http://www.minasgerais.com.br/imagens/eventos/1516886575Dh2B2LCh6f.jpg"
                    alt="First slide">
                @else

                {{--*/ $carouselActive=0 /*--}}

                <div id="carouselEvento" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        @foreach ($event->EventPhotos as $photos)
                        <div
                            class="carousel-item @if ($carouselActive == 0) {{'active'}} {{--*/ $carouselActive=1 /*--}} @endif">
                            <img class="d-block w-100" src="{{url("/image/430/322/true/true")}}/{{$photos->url}}"
                                alt="">
                        </div>
                        @endforeach

                    </div>
                    <a class="carousel-control-prev" href="#carouselEvento" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselEvento" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>


                @endif
                <div class="text-left pt-2">
                    @if (!$event->facebook == '' || !$event->instagram == '' || !$event->twitter == '' ||
                    !$event->flickr == '')
                    <small >Visite:</small>
                    @endif
                    @if (!$event->facebook == '')
                    <a href="{{$event->facebook}}" target="_blank"><img
                            src="{{url('/portal/assets/imgs/social/facebook_p.png')}}" alt="Facebook"></a>
                    @endif
                    @if (!$event->instagram == '')
                    <a href="{{$event->instagram}}" target="_blank"><img
                            src="{{url('portal/assets/imgs/social/instagram_p.png')}}" alt="Instagram"></a>
                    @endif
                    @if (!$event->twitter == '')
                    <a href="{{$event->twitter}}" target="_blank"><img
                            src="{{url('portal/assets/imgs/social/twitter_p.png')}}" alt="Twitter"></a>
                    @endif
                    @if (!$event->flickr == '')
                    <a href="{{$event->flickr}}" target="_blank"><img
                            src="{{url('portal/assets/imgs/social/flicker_p.png')}}" alt="Flickr"></a>
                    @endif
                </div>
            </div>
            <div class="col-md-7 order-md-2">
                <h2 style="font-family:mistralregular; color:purple;">
                  {{$event->nome}}
                </h2>
                <p><span class="oi oi-map-marker"></span>&nbsp;

                    <a href="https://www.google.com/maps/dir/Current+Location/{{$event->latitude}},{{$event->longitude}}">
                        <b> {{$event->City->name}} </b>

                    @if (!$event->local_evento == '') - {{$event->local_evento}}@endif<small> -
                        {{trim($event->logradouro)}}@if (trim($event->numero) != '' &&
                        !in_array(strtoupper(trim($event->numero)), array("0","S/N", "S/Nº", "S/N°", "SN", "SEM N°",
                        "SEM NUMERO", "SEM NÚMERO", "SEM NúMERO")) ), {{$event->numero}}@endif,
                        {{trim($event->bairro)}}</small>  </a> </p>

                <p><span class="oi oi-clock"></span>&nbsp;{{getLiteralDate($event->inicio, ($event->inicio != $event->fim ) ? $event->fim: null)}}</p>

                @if (!trim($event->site == ''))
                <p><span class="oi oi-home"></span>&nbsp;<a href="{{$event->site}}" target="_blank">{{$event->site}}</a> </p>
                @endif

                @if (!trim($event->telefone == ''))
                <p><span class="oi oi-phone"></span>&nbsp;{{$event->telefone}}</p>
                @endif
                @if (!trim($event->email == ''))
                <p><span class="oi oi-envelope-closed"></span>&nbsp;<a href="mailto:{{$event->email}}">{{$event->email}}</a></p>
                @endif
                @if (!trim(json_decode($event->dados_relevantes,true)[$p_Language][0]['dado']) == '')
                <p><span class="oi oi-clipboard"></span>&nbsp;<b>{{trim(json_decode($event->dados_relevantes,true)[$p_Language][0]['dado'])}}:</b> {{trim(json_decode($event->dados_relevantes,true)[$p_Language][0]['descricao'])}}</p>
                @endif
                @if (!trim(json_decode($event->dados_relevantes,true)[$p_Language][1]['dado']) == '')
                <p><span class="oi oi-clipboard"></span>&nbsp;<b>{{trim(json_decode($event->dados_relevantes,true)[$p_Language][1]['dado'])}}:</b> {{trim(json_decode($event->dados_relevantes,true)[$p_Language][1]['descricao'])}}</p>
                @endif
                @if (!trim(json_decode($event->dados_relevantes,true)[$p_Language][2]['dado']) == '')
                <p><span class="oi oi-clipboard"></span>&nbsp;<b>{{trim(json_decode($event->dados_relevantes,true)[$p_Language][2]['dado'])}}:</b> {{trim(json_decode($event->dados_relevantes,true)[$p_Language][2]['descricao'])}}</p>
                @endif
            </div>

        </div>

        <hr>

        <div class="row">
            <div class="col-md-7">
                @if (!trim(json_decode($event->sobre,true)[$p_Language]) == '')
                <h3  style="font-family:mistralregular; color:purple;"> SOBRE O EVENTO</h3>
                @if (!trim(json_decode($event->descricao_curta,true)[$p_Language]) == '')
                <p class="text-justify">{{json_decode($event->descricao_curta,true)[$p_Language]}}</p>
                @endif
                <p class="text-justify">{{json_decode($event->sobre,true)[$p_Language]}}</p>
                @endif

                @if (!trim(json_decode($event->informacoes_uteis,true)[$p_Language]) == '')
                <h5 style="font-family:mistralregular; color:purple;">
                <img  src="{{url('/assets/img/semanasanta/informacao.png')}}" > 
                  &nbsp; MAIS INFORMAÇÕES</h5>
                <p class="text-justify">{{json_decode($event->informacoes_uteis,true)[$p_Language]}}</p>
                @endif
            </div>

            <div class="col-md-5">

                    <div class="col-md-12"> 
                        @if (isset($cityPhoto))       
                            <h3 class=" p-2 text-uppercase align-top text-center ">
                                <a class="link-hover"
                                    href="@if (isset($citySlug)){{url('/'.$p_Language.'/destinos/'.$citySlug)}}@else{{"#"}}@endif"
                                    style="font-family:mistralregular; color:purple;">
                                    {{$event->City->name}}
                                </a>
                            </h3>
                    </div>
                    <div class="col-md-12"> 
                        <a href="@if (isset($citySlug)){{url('/'.$p_Language.'/destinos/'.$citySlug)}}@else{{"#"}}@endif">
                            <img class="img-fluid mx-auto fade-image" src="{{url('/image/450/322/true/true/'.$cityPhoto)}}"
                                alt="">
                        </a>
                        @if (isset($citySlug))
                        <div class="text-center pt-2">
                            <a href="{{url('/'.$p_Language.'/apoio-destino/'.$citySlug.'?atrativos=')}}" class="link-hover">
                                <img class="img-fluid" style="height: 32px" src="/assets/img/semanasanta/atrativos.png"
                                    alt=""><span class="fnt-gradients"><b style="font-family:mistralregular; color:purple;font-size:1em;"> ATRATIVOS</b></span></a>
                            <a href="{{url('/'.$p_Language.'/apoio-destino/'.$citySlug.'?tipo_acomodacao=')}}"
                                class="link-hover"> <img class="img-fluid" style="height: 32px"
                                    src="/assets/img/semanasanta/hospedagem.png" alt=""> <span
                                    class="fnt-gradients"><b style="font-family:mistralregular; color:purple;font-size:1em;">HOSPEDAGEM</b></span></a>
                            <a href="{{url('/'.$p_Language.'/apoio-destino/'.$citySlug.'?tipo_estabelecimento=')}}"
                                class="link-hover"> <img class="img-fluid" style="height: 32px"
                                    src="/assets/img/semanasanta/alimentacao.png" alt=""> <span
                                    class="fnt-gradients"><b style="font-family:mistralregular; color:purple;font-size:1em;">ALIMENTAÇÃO</b></span></a>          
                        </div>
                        @endif
                        </div>
                    </div>
                </div>
            
            </div>

                @endif

            </div>
        </div>



        @endif

        <hr>

        <!-- /END THE FEATURETTES -->

    </section>
</main>
@include('public.semanasanta.menuMobile')
@endsection

@section('footer')
@include('public.semanasanta.footer')
@endsection

@section('pageScript')
<script type="text/javascript" src="{{url('/portal/assets/libs/portal/js/eventsShow.js')}}"></script>
<script type="text/javascript" src="{{url('/portal/assets/libs/portal/js/eventsSearchbar.js')}}"></script>
@endsection