@extends('public.templates.base')
<!DOCTYPE html>
<html lang="pt-br">

    <head>
        <script async
            src="http://analytics.turismo.mg.gov.br/analytics/default/b?url=http%3A%2F%2Fwww%7Cminasgerais%7Ccom%7Cbr%2Fpt%2Froteiros&ref=http%3A%2F%2Fwww%7Cminasgerais%7Ccom%7Cbr%2Fpt&c=GA1.3.1481861829.1583153592&t=Portal Minas Gerais&env=1">
        </script>

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-125337813-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-125337813-1');
        </script>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="no-cache" http-equiv="pragma">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
        <meta name="description" property="description" content="">
    

        <meta name="image" property="image" content="">
    

        <meta property="og:url" content="http://www.descubramatogrosso.com.br/pt/roteiros">
    <meta property="og:title" content="Portal Minas Gerais">
    <meta property="og:type" content="website">
    <meta property="og:site_name" content="Portal Minas Gerais">
    

        <meta name="twitter:card" content="Portal Minas Gerais">
    <meta name="twitter:site" content="Portal Minas Gerais">
    <meta name="twitter:title" content="Portal Minas Gerais">
    <meta name="twitter:creator" content="Portal Minas Gerais">
    <meta name="twitter:domain" content="www.descubramatogrosso.com.br">
    

                <meta name="image" property="image" content="">
    

                <meta name="author" content="Portal Minas Gerais">
        <link rel="icon" href="http://www.descubramatogrosso.com.br/favicon.ico" />
        <title>Descubra Mato Grosso</title>
        <link rel="stylesheet" type="text/css" href="http://www.descubramatogrosso.com.br/portal/assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="http://www.descubramatogrosso.com.br/portal/assets/css/main.css">
        <style>
            @font-face {
                font-family: 'FontAwesome';
                src: url('http://www.descubramatogrosso.com.br/assets/fonts/font-awesome/fontawesome-webfont78ce.eot?v=4.2.0');
                src: url('http://www.descubramatogrosso.com.br/assets/fonts/font-awesome/fontawesome-webfontd41d.eot?#iefix&amp;v=4.2.0') format('embedded-opentype'),
                url('http://www.descubramatogrosso.com.br/assets/fonts/font-awesome/fontawesome-webfont78ce.woff?v=4.2.0') format('woff'),
                url('http://www.descubramatogrosso.com.br/assets/fonts/font-awesome/fontawesome-webfont78ce.ttf?v=4.2.0') format('truetype'),
                url('http://www.descubramatogrosso.com.br/assets/fonts/font-awesome/fontawesome-webfont78ce.svg?v=4.2.0#fontawesomeregular') format('svg');
                font-weight: normal;
                font-style: normal;
            }
</style>

   
    <!-- MENU RESUMIDO -->
    
    <header class=" container-fluid bg shadow-sm-1" style="background-color:white;">
        <nav class="navbar navbar-expand-lg bg-white navbar-light"> 
            <a class="navbar-brand mb-0" href="http://descubramatogrosso.com.br/pt">
                <img class="img-fluid" src="http://descubramatogrosso.com.br/portal/assets/imgs/descubraMatoGrosso.png" alt="Descubra Mato Grosso" title="Descubra Mato Grosso">
            </a>
            <button class="navbar-toggler ml-auto" type="button" data-toggle="collapse" data-target="#navbarPortal">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarPortal" style="overflow:auto;">
                <ul class="navbar-nav small mr-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="{{url($p_Language . '/para-onde-ir')}}" style="font-size: 18px; color: #0000FF; font-family: Trebuchet MS;""><strong>Onde ir</strong></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{url($p_Language . '/o-que-fazer')}}" style="font-size: 18px; color: #0000FF; font-family: Trebuchet MS;""><strong>Atrações</strong></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{url($p_Language . '/eventos')}}" style="font-size: 18px; color: #0000FF; font-family: Trebuchet MS;""><strong>Eventos</strong></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{url($p_Language . '/explore-o-mapa')}}" style="font-size: 18px; color: #0000FF; font-family: Trebuchet MS;""><strong>Mapa</strong></a>
                    </li>
                </ul>
                <style>
                    a:hover {
                        text-transform:uppercase;
                        
                        
                    }
                </style>
                <ul class="navbar-nav">
                    <li class="nav-item pl-3 d-none d-md-flex w-100">
                        <form method="GET" action="http://descubramatogrosso.com.br/pt/busca"  accept-charset="UTF-8" class="form-inline py-1">
                            
                                <input type="text" name="q" class="form-control" placeholder="Buscar..." aria-label="" aria-describedby="btnGroupAddon2">
                                    
                                        <button type="submit" class="btn btn-outline-secondary" id="btnGroupAddon2"><span class="oi oi-magnifying-glass"></span></button>
                                    
                                                                                                                 
                        </form>
                    </li>
                </ul>
                <ul class="navbar-nav small my-2 my-lg-0 pl-5">
                    <li class="nav-item">
                        <a class="nav-link" href="https://www.facebook.com/descubraMatoGrosso/" target="_blank">
                            <img src="http://descubramatogrosso.com.br/portal/assets/imgs/facebookGray.png" alt="Facebook" title="Facebook">
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="https://www.instagram.com/descubramatogrosso/" target="_blank">
                            <img src="http://descubramatogrosso.com.br/portal/assets/imgs/instagramGray.png" alt="Instagram" title="Instagram">
                        </a>
                    </li>
                    <!--<li class="nav-item">
                        <a class="nav-link" href="http://descubramatogrosso.com.br/pt"><img src="http://descubramatogrosso.com.br/portal/assets/imgs/flag-pt.png"></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="http://descubramatogrosso.com.br/fr"><img class="lang-flag" src="http://descubramatogrosso.com.br/portal/assets/imgs/flag-fr.png"></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="http://descubramatogrosso.com.br/en"><img class="lang-flag" src="http://descubramatogrosso.com.br/portal/assets/imgs/flag-en.png"></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="http://descubramatogrosso.com.br/es"><img class="lang-flag" src="http://descubramatogrosso.com.br/portal/assets/imgs/flag-es.png"></a>
                    </li>-->
                </ul>
                <!-- Futuramente quando precisar
                    <li class="nav-item">
                        <a class="nav-link" href="http://descubramatogrosso.com.br/pt/roteiros-duracao">Planeje sua viagem</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="http://descubramatogrosso.com.br/pt/explore-o-mapa">Explore o mapa</a>
                    </li>
                    <li class="nav-item">
                            <a class="nav-link strings px-1" href="http://descubramatogrosso.com.br/pt/blog">Blog</a>
                    </li> 
                <ul class="navbar-nav ">
                    <li class="nav-item">
                        <div class="dropdown">
                            <button class="btn btn-light dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img class="lang-flag" src="http://descubramatogrosso.com.br/portal/assets/imgs/flag-pt.png">
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                <a class="dropdown-item" href="http://descubramatogrosso.com.br/pt"><img class="lang-flag" src="http://descubramatogrosso.com.br/portal/assets/imgs/flag-pt.png"> Português</a>
                                <a class="dropdown-item disabled" href="http://descubramatogrosso.com.br/fr"><img class="lang-flag" src="http://descubramatogrosso.com.br/portal/assets/imgs/flag-fr.png"> Français</a>
                                <a class="dropdown-item disabled" href="http://descubramatogrosso.com.br/en"><img class="lang-flag" src="http://descubramatogrosso.com.br/portal/assets/imgs/flag-en.png"> English</a>
                                <a class="dropdown-item disabled" href="http://descubramatogrosso.com.br/es"><img class="lang-flag" src="http://descubramatogrosso.com.br/portal/assets/imgs/flag-es.png"> Español</a>
                            </div>
                        </div>
                    </li>
                </ul>-->
            </div>
        </nav>
    </header> 
    

    <!-- MENU COMPLETO -->
   

<div class="row-fluid" id="destinos" style="padding:1% 0;">
<div class="container">
        <div class="col-lg-12" id="list-title">
            <h2 style="color:#0B9BC4; font-size:40px; font-weight: 700;" class="">{{trans('menu.where_to_go')}}</h2>
        </div>
    </div>

    <div class="container">

        <div class="col-lg-12">
            <div class="col-lg-8 col-lg-offset-2">
                <div class="col-lg-12" >
                    <p>{!! $p_Content == null ? '' : nl2br($p_Content->descricao) !!}</p>
                </div>
            </div>
        </div>

        <!-- FRAGMENTO ATUALIZADO -->
        <div class="col-lg-12 line no-padding">
            @foreach($p_Places as $c_Index => $c_Place)
                <?php
                    $v_Description = json_decode($c_Place['descricao_curta'],1)[$p_Language];
                    if($c_Place['tipo'] == 'destinos')
                        $v_Name = $c_Place['nome'];
                    else
                        $v_Name = json_decode($c_Place['nome'],1)[$p_Language];
                    ?>
                @if($c_Index < 3)
                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 list-thumbs">
                    <a href="{{url($p_Language . '/' . $c_Place['tipo'] . '/' . $c_Place['slug'])}}">
                        <div class="hoverzoom">
                            <div class="thumbs-mini-three">
                                <!--<img src="{{$c_Place['url']}}">-->
                                <div class="thumbs-mini-recorte" style="background: url('{{$c_Place['url']}}') no-repeat;"></div>
                            </div>
                            <div class="retina-hover">
                                <div class="col-lg-12 title">
                                    <p>{{$v_Name}}</p>
                                </div>
                                <div class="col-lg-12 no-padding">
                                    <hr>
                                </div>
                                <div class="col-lg-12 text">
                                    <p>{{$v_Description}}</p>
                                </div>
                            </div>
                            <div class="retina">
                                <p>{{$v_Name}}</p>
                            </div>
                        </div>
                    </a>
                </div>
                @endif
            @endforeach
        </div>
        <!-- FRAGMENTO ATUALIZADO -->

        <div class="col-lg-12" >
            <div class="col-lg-12 hospedagem">
                <h2 style="font-weight:500; color:#b4b4b4; margin-top:0; font-size: 2rem;" >
                    {{trans('whereToGo.find_destination')}}
                </h2>
                <p style="color:#4b4b4b;font-size:16px;font-family: Signika;">
                    {!! $p_Content == null ? '' : nl2br($p_Content->descricao_outra) !!}
                </p>
            </div>
        </div>
        <!-- FRAGMENTO ATUALIZADO -->
    </div>

    <!-- FRAGMENTO ATUALIZADO -->
    <div class="col-lg-12" id="tarja" style="background-color:">
        <div class="container no-padding">
            <div class="col-lg-12">
                <div class="row" id="options">
                    {!! Form::open(array('onsubmit' => 'return submitFilter()')) !!}
                        <div class="col-lg-3">
                            <p style="font-size:20px;">{{trans('whereToGo.what_do_you_expect')}}</p>
                        </div>

                        <div class="col-lg-3">
                            <div class="col-lg-12 selectoptions">
                                <?php
                                    $v_CategoriesArray = [];
                                    foreach($p_TripCategories as $c_CategoryIndex => $c_Value)
                                        $v_CategoriesArray += [$c_CategoryIndex => json_decode($c_Value,1)[$p_Language]];
                                ?>
                                {!! Form::select('categoria_viagem', [''=>trans('whereToGo.choose_trip_category')] + $v_CategoriesArray, null, ['id' => 'categoriaViagem']) !!}
                            </div>
                        </div>
                        
                        <div class="col-lg-3">
                            <div class="col-lg-12 selectoptions ">
                                {!! Form::select('tipo_viagem', [''=>trans('whereToGo.choose_trip_type')], null, ['id' => 'tipoViagem', 'required' => 'required']) !!}
                            </div>
                        </div>

                        <div class="col-lg-3">
                            <div class="col-lg-12 selectoptions no-padding">
                            <div id="formSearch">
                                <div class="input-group" style="width: 100%;display:block;">
                                    <div class="destination-select2-container">
                                        {!! Form::select('cidade', ["" => trans('planYourTrip.select_destination')] + $p_DestinationList, null, ['id' => 'cidade', 'class' => 'form-control select2', 'style' => 'width:100%']) !!}
                                    </div>
                                    <span class="input-group-btn" id="btn-search">
                                        <button class="btn btn-secondary btn-submit" type="submit" id="btn-submit"></button>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <!-- FRAGMENTO ATUALIZADO -->

    <div class="col-lg-12" id="destinos" style="margin-top:3%;">
        <div class="container">
            <div class="col-lg-12 line no-padding" id="filterResultsDiv">
                @foreach($p_Places as $c_Index => $c_Place)
                    <?php
                        $v_Description = json_decode($c_Place['descricao_curta'],1)[$p_Language];
                        if($c_Place['tipo'] == 'destinos')
                            $v_Name = $c_Place['nome'];
                        else
                            $v_Name = json_decode($c_Place['nome'],1)[$p_Language];
                    ?>
                    @if($c_Index >= 3)
                        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 list-thumbs" style="margin-bottom:25px;">
                            <a href="{{url($p_Language . '/' . $c_Place['tipo'] . '/' . $c_Place['slug'])}}">
                                <div class="hoverzoom">
                                    <div class="thumbs-mini-four">
                                        <img src="{{$c_Place['url']}}">
                                    </div>
                                    <div class="retina-hover">
                                        <div class="col-lg-12 title">
                                            <p>{{$v_Name}}</p>
                                        </div>
                                        <div class="col-lg-12 no-padding">
                                            <hr>
                                        </div>
                                        <div class="col-lg-12 text">
                                            <p>{{$v_Description}}</p>
                                        </div>
                                    </div>
                                    <div class="retina">
                                        <p>{{$v_Name}}</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>
    </div>
</div>

    <footer class="text-muted">
        <div class="row-fluid line-footer">
            <img class="img-fluid py-0" src="{{url('/portal/assets/imgs/_ondas.png')}}" alt="">
        </div>
        <div class="container">
            <div class="row">
                <!--<div class="col-12 col-sm-6 col-md-3 text-justify" style="width: 18rem;">                     
                    <h6 style="font-size: 16px">CONTATOS:</h6>
                    <p style="font-size: 14px">+55 65 3613-9300 / 3613-9313</p>
                    <h6 style="font-size: 16px">E-MAIL:</h6>
                    <p style="font-size: 14px">marketing@sedec.mt.gov.br</p>
                    <p style="font-size: 14px">Rua Voluntários da Pátria, 118 Centro Norte - Cuiabá - MT CEP: 780005-000</p>                
                </div>-->
                <div class="col-12 col-sm-6 col-md-3 py-5 text-left pl-5">            
                    
                        
                    
                    <a class="nav-link" href="{{url($p_Language . '/fale-conosco')}}" style="color: white; font-family: Trebuchet MS;"><strong>Fale conosco</strong></a>
                    <a class="nav-link" href="{{url($p_Language . '/conheca/galeria')}}" style="color: white; font-family: Trebuchet MS;"><strong>Galeria</strong></a>
                    <a href="{{url($p_Language . '/doacao-de-midias')}}" class="nav-link" style="color: white; font-family: Trebuchet MS;"><strong>Doe suas fotos/vídeos</strong></a>   
                    <a class="nav-link" href="{{url('/admin/login')}}" target="_blank" ><h2 style="font-size: 19px; color: orange; font-family: Trebuchet MS;">Área do Prestador</h2></a>
                </div>
                <div class="col-12 col-sm-6 col-md-3 py-5 text-center" style="width: 18rem;">
                    <a href="#" role="button">
                        <img class="img-fluid" src="{{url('/portal/assets/imgs/descubraMatoGrosso.png')}}" alt="Descubra Mato Grosso">
                    </a>                                             
                </div>
                <div class="col-12 col-sm-6 col-md-3 py-5 text-center" style="width: 18rem;">
                    <a href="http://www.sedec.mt.gov.br/" target="_blank" role="button">
                        <img class="img-fluid" src="{{url('/portal/assets/imgs/sedec.png')}}" alt="SEDEC">
                    </a>                                               
                </div>
                <div class="col-12 col-sm-6 col-md-3 py-5 text-center" style="width: 18rem;">
                    <a href="https://cadastur.turismo.gov.br/hotsite/" target="_blank" role="button">
                        <img class="img-fluid" src="{{url('/portal/assets/imgs/logoCadastur.png')}}" alt="cadastur">
                    </a>                                               
                </div>
            </div>
        </div>
        <div class="text-center">
            <div class="container">                  
                <h2 style="font-size: 15px; color:white; margin-bottom:0px;">Secretaria Adjunta de Turismo</h2>
                <p style="font-size: 14px; margin-bottom:0px;">Rua Voluntários da Pátria, 118 - Centro Norte - Cuiabá/MT - CEP 78.005-180</p>                
                <p style="font-size: 14px; ">+55 (65) 3613-9320 / +55 (65) 3613-9300</p>
                <p style="font-size: 12px">Cedido gratuitamente pela Secretaria de Estado de Turismo de Minas Gerais e desenvolvido pela empresa de software Oro Digital LTDA. </p>
            </div>
        </div>
    </footer>
    <style>
    footer {
        background-color: #009DC7;
    }
    </style>

        <!-- javascript -->
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script type="text/javascript" src="https://code.jquery.com/ui/1.11.0/jquery-ui.min.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?language=pt-BR&key=AIzaSyBLc_8beOK5wVSM2NeSk1Bl4mLwtq6jtGw"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.3.1/jquery.maskedinput.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/jquery.validate.min.js"></script>
<script type="text/javascript" src="http://www.descubramatogrosso.com.br/portal/assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="http://www.descubramatogrosso.com.br/portal/assets/js/all.js"></script>
<script src="https://unpkg.com/minigrid@3.1.1/dist/minigrid.min.js"></script>

<script src="{{url('/vendor/select2/select2.min.js')}}" type="text/javascript"></script>
    <script src="{{url('/vendor/select2/i18n/pt-BR.js')}}" type="text/javascript"></script>
    <script>
        var v_Types = {!! json_encode($p_TripTypes) !!};
        $(document).ready(function()
        {
            $('.select2').select2(
                @if($p_Language == 'pt')
                    {language:'pt-BR'}
                @endif
            );

            $('#categoriaViagem').change(function(){
                var v_SelectedCategoryId = $(this).val();
                var v_DataString = '';
                $.each(v_Types, function (c_Key, c_Field)
                {
                    if(c_Field.trip_category_id == v_SelectedCategoryId)
                        v_DataString += '<option value="' + c_Field.id + '">' + JSON.parse(c_Field.nome).{{$p_Language}} + '</option>';
                });
                $('#tipoViagem').html(v_DataString);
            }).trigger('change');
        });

        function submitFilter()
        {


function IsJsonString(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}            
            var v_Data = {
                trip_type_id: $('#tipoViagem').val(),
                destination_id: $('#cidade').val()
            };
            $.get("{{url($p_Language . '/filtro-para-onde-ir')}}",v_Data).done(function(p_Data){
                if (p_Data.error == 'ok')
                {
                    var v_DataString = '';
                    if(p_Data.data.length < 1){
                        v_DataString = '<div class="col-xs-12" id="no-results">' +
                                        '<p>' + "{{trans('destination.no_results')}}" + '</p>' +
                                        '</div>';
                    }

                    $.each(p_Data.data, function (c_Key, c_Field)
                    {

                        if (IsJsonString(c_Field.nome)) {
                            var v_Nome = JSON.parse(c_Field.nome)['pt'] ;
                        } else {
                            var v_Nome = c_Field.nome;
                        }
                        
                        var v_Descricao = JSON.parse(c_Field.descricao_curta).{{$p_Language}};
                        if(c_Field.tipo == 'roteiros' || (c_Field.tipo == 'atracoes' && c_Field.trade == 0))
                            v_Nome = JSON.parse(v_Nome).{{$p_Language}};
                        v_DataString += '<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 list-thumbs" style="margin-bottom:25px;">'+
                            '<a href="{{url($p_Language)}}/' + c_Field.tipo + '/' + c_Field.slug + '">'+
                            '<div class="hoverzoom">'+
                            '<div class="thumbs-mini-four">'+
                            '<img src="' + c_Field.url + '">'+
                            '</div>'+
                            '<div class="retina-hover">'+
                            '<div class="col-lg-12 title">'+
                                '<p>' + v_Nome + '</p>' +
                            '</div>'+
                            '<div class="col-lg-12 no-padding">'+
                            '<hr>'+
                            '</div>'+
                            '<div class="col-lg-12 text">'+
                            '<p>' + v_Descricao + '</p>'+
                            '</div>'+
                            '</div>'+
                            '<div class="retina">'+
                                '<p>' + v_Nome + '</p>' +
                            '</div>'+
                            '</div>'+
                            '</a>'+
                            '</div>';
                    });
                    $('#filterResultsDiv').html(v_DataString);
                }
                else
                    showNotify('danger', p_Data.error);
            }).error(function(){
            });
            return false;
        }
    </script>

        
    </body>

</html>