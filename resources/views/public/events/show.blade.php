@extends('public.templates.base') 
@section('pageCSS')
<link rel="stylesheet" type="text/css" href="{{url('/portal/assets/libs/portal/css/eventsShow.css')}}">
<link rel="stylesheet" type="text/css" href="{{url('/portal/assets/libs/portal/css/eventsSearchbar.css')}}">
@endsection
 
@section('header')
        @include('public.templates.header')
@endsection
 
@section('main-content')
<main class="container-fluid mt-4">
<!-- Barra de Pesquisa -->
  <div id="accordion">
    <div class="card">
      <div class="card-header m-0 p-0 d-md-none" id="headingOne">
        <h5 class="mb-0 p-0">
          <button class="btn btn-sm btn-link title w-100 collapsed m-0 " data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
            <span class="oi oi-magnifying-glass"></span> Mais Filtros
          </button>
        </h5>
      </div>
  
      <div id="collapseOne" class="collapse d-md-block" aria-labelledby="headingOne" data-parent="#accordion">
        <div class="card-body m-0 p-0 pt-3">
          @include('public.templates.eventsSearchbar')
        </div>
      </div>
    </div>
  </div>
  
<section class="container">

       <!-- START THE FEATURETTES -->

        @if (isset($event))

       <hr class="featurette-divider">

       <div class="row featurette">
        <div class="col-md-5 order-md-1">

        @if (count($event->EventPhotos) == 0)
              <img class="d-block w-100" src="/image/430/322/true/true/http://www.minasgerais.com.br/imagens/eventos/1516886575Dh2B2LCh6f.jpg" alt="First slide">
        @else

        {{--*/ $carouselActive=0 /*--}}

            <div id="carouselEvento" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                @foreach ($event->EventPhotos as $photos)
                  <div class="carousel-item @if ($carouselActive == 0) {{'active'}} {{--*/ $carouselActive=1 /*--}} @endif">
                    <img class="d-block w-100" src="{{url("/image/430/322/true/true")}}/{{$photos->url}}" alt="">
                  </div>
                @endforeach

                </div>
                <a class="carousel-control-prev" href="#carouselEvento" role="button" data-slide="prev">
                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                  <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselEvento" role="button" data-slide="next">
                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                  <span class="sr-only">Next</span>
                </a>
              </div>


        @endif      
              <div class="text-left pt-2">
                    @if (!$event->facebook == '' || !$event->instagram == '' || !$event->twitter == '' || !$event->flickr == '')
                        <small class="title">Visite:</small> 
                    @endif
                    @if (!$event->facebook == '')  
                    <a href="{{$event->facebook}}" target="_blank"><img src="{{url('/portal/assets/imgs/social/facebook_p.png')}}" alt="Facebook"></a>
                    @endif
                    @if (!$event->instagram == '')  
                        <a href="{{$event->instagram}}" target="_blank"><img src="{{url('portal/assets/imgs/social/instagram_p.png')}}" alt="Instagram"></a>
                    @endif
                    @if (!$event->twitter == '')  
                        <a href="{{$event->twitter}}" target="_blank"><img src="{{url('portal/assets/imgs/social/twitter_p.png')}}" alt="Twitter"></a>
                    @endif
                    @if (!$event->flickr == '')  
                        <a href="{{$event->flickr}}" target="_blank"><img src="{{url('portal/assets/imgs/social/flicker_p.png')}}" alt="Flickr"></a>
                    @endif
              </div>
        </div>
        <div class="col-md-7 order-md-2">
           <h3 class="title text-uppercase text-bold">{{$event->nome}}</h3>
           <p><span class="oi oi-map-marker"></span>&nbsp;
            
            <a href="https://www.google.com/maps/dir/Current+Location/{{$event->latitude}},{{$event->longitude}}">
            
            <b>  {{$event->City->name}}  </b>
            

            
            @if (!$event->local_evento == '') - {{$event->local_evento}}@endif<small> - {{trim($event->logradouro)}}@if (trim($event->numero) != '' && !in_array(strtoupper(trim($event->numero)), array("0","S/N", "S/Nº", "S/N°", "SN", "SEM N°", "SEM NUMERO", "SEM NÚMERO", "SEM NúMERO")) ), {{$event->numero}}@endif, {{trim($event->bairro)}}</small> </a></p>

           <p><span class="oi oi-clock"></span>&nbsp; {{getLiteralDate($event->inicio, ($event->inicio != $event->fim ) ? $event->fim: null)}}</p>

           @if (!trim($event->site == ''))
           <p><span class="oi oi-home"></span>&nbsp;<a href="{{$event->site}}" target="_blank">{{$event->site}}</a> </p>
           @endif

           @if (!trim($event->telefone == ''))
           <p><span class="oi oi-phone"></span>&nbsp;{{$event->telefone}}</p>
           @endif
           @if (!trim($event->email == ''))
           <p><span class="oi oi-envelope-closed"></span>&nbsp;<a href="mailto:{{$event->email}}">{{$event->email}}</a></p>
           @endif
           @if (!trim(json_decode($event->dados_relevantes,true)[$p_Language][0]['dado']) == '')
           <p><span class="oi oi-clipboard"></span>&nbsp;<b>{{trim(json_decode($event->dados_relevantes,true)[$p_Language][0]['dado'])}}:</b> {{trim(json_decode($event->dados_relevantes,true)[$p_Language][0]['descricao'])}}</p>
           @endif
           @if (!trim(json_decode($event->dados_relevantes,true)[$p_Language][1]['dado']) == '')
           <p><span class="oi oi-clipboard"></span>&nbsp;<b>{{trim(json_decode($event->dados_relevantes,true)[$p_Language][1]['dado'])}}:</b> {{trim(json_decode($event->dados_relevantes,true)[$p_Language][1]['descricao'])}}</p>
           @endif
           @if (!trim(json_decode($event->dados_relevantes,true)[$p_Language][2]['dado']) == '')
           <p><span class="oi oi-clipboard"></span>&nbsp;<b>{{trim(json_decode($event->dados_relevantes,true)[$p_Language][2]['dado'])}}:</b> {{trim(json_decode($event->dados_relevantes,true)[$p_Language][2]['descricao'])}}</p>
           @endif
         </div>
       </div>

       <hr>

       <div class="row">
         <div class="col-md-7">
           @if (!trim(json_decode($event->sobre,true)[$p_Language]) == '')      
           <h2 class="title">SOBRE O EVENTO</h2>
           @if (!trim(json_decode($event->descricao_curta,true)[$p_Language]) == '')
           <p class="text-justify">{{json_decode($event->descricao_curta,true)[$p_Language]}}</p>
           @endif
           <p class="text-justify">{{json_decode($event->sobre,true)[$p_Language]}}</p>
           @endif
           
           @if (!trim(json_decode($event->informacoes_uteis,true)[$p_Language]) == '')      
           <h5 class="title"><span class="oi oi-clipboard"></span>&nbsp; Mais informações</h5>
           <p class="text-justify">{{json_decode($event->informacoes_uteis,true)[$p_Language]}}</p>
           @endif           
        </div>

         <div class="col-md-5 text-left">

        @if (isset($cityPhoto))
        
        <div class="title">
          <h3 class="text-danger-dark p-2" >
              <a class="link-no-hover title" href="@if (isset($citySlug)){{url('/'.$p_Language.'/destinos/'.$citySlug)}}@else{{"#"}}@endif">
            <span class="oi oi-map-marker"></span> {{$event->City->name}}
              </a>
          </h3>
        </div>
        <a href="@if (isset($citySlug)){{url('/'.$p_Language.'/destinos/'.$citySlug)}}@else{{"#"}}@endif">
           <img class="img-fluid mx-auto fade-image" src="{{url('/image/450/322/true/true/'.$cityPhoto)}}" alt="">
        </a>
          @if (isset($citySlug))
           <div class="text-center pt-2">
                <a href="{{url('/'.$p_Language.'/apoio-destino/'.$citySlug.'?atrativos=')}}" class="link-no-hover"> <img class="img-fluid" style="height: 32px" src="/portal/assets/imgs/atrativoOrange.png" alt=""><span class="fnt-gradients"><b>Atrativos</b></span></a>
                <a href="{{url('/'.$p_Language.'/apoio-destino/'.$citySlug.'?tipo_acomodacao=')}}" class="link-no-hover"> <img class="img-fluid" style="height: 32px" src="/portal/assets/imgs/camaOrange.gif" alt=""> <span class="fnt-gradients"><b>Hospedagem</b></span></a>
           </div>
          @endif
           
        @endif
         </div>
       </div>



       @endif

       <hr>
       @if (isset($featuredEvent) && count($featuredEvent)>0)
       <div class="row">
         <div class="col-md-12">
           <h2 class="title">Eventos Similares</h2>





        <section class="top-eventos slider">

                        @foreach ($featuredEvent as $vEvent)
                        {{-- Verifica se o evento tem foto de capa --}}
                        @if ($vEvent->EventPhotos(true)->first() != null)

                        <div class="slide text-center">
            
                                        <div class="">
                                                        <div class="card mb-4 shadow">
                                                          <?php
                                                            $d  = $destination->where('city_id',$vEvent->city_id)
                                                                              ->whereNull('district_id')
                                                                              ->first();
                                                            ?>   
                                                            <a href="{{url($p_Language.'/eventos/'.$d['slug'].'/'.$vEvent->slug)}}">
                                                          <img class="card-img-top fade-image" src="/image/330/247/true/true/{{$vEvent->EventPhotos(true)->first()->url}}">
                                                                        </a>
                                                          <div class="card-body pb-4">
                          
                                                                          <div class="border border-top-0 border-left-0 border-right-0 text-center text-uppercase text-truncate"><a class="link-no-hover title" href="{{url($p_Language.'/eventos/'.$d['slug'].'/'.$vEvent->slug)}}" title="{{$vEvent->nome}}"
                                                                                  data-toggle="popover" data-trigger="hover" data-content="{{json_decode($vEvent->descricao_curta, true)[$p_Language]}}"
                                                                                  data-placement="top"><small>{{$vEvent->nome}}</small></a></div>
                          
                          
                                                                          <table class="float-left">
                                                                                          <tr>
                                                                                                  <th class="border border-0 calendar-month-single text-light px-2 text-uppercase">{{getMonthName(date_format(date_create($vEvent->inicio),'m'))}}</th>
                                                                                          </tr>
                                                                                              @if (($vEvent->fim != '') && (date_format(date_create($vEvent->inicio), 'd') != date_format(date_create($vEvent->fim), 'd')) && (date_format(date_create($vEvent->inicio), 'm') == date_format(date_create($vEvent->fim), 'm')))
                                                                                              {{--*/ $twoDays=true /*--}} @else {{--*/ $twoDays=false /*--}}
                                                                                              @endif
                                          
                                                                                          <tr>
                                                                                                  <td class="border border-0 calendar-day-single text-light text-center px-2"> <strong>{{date_format(date_create($vEvent->inicio), 'd')}}</strong></td>
                                                                                          </tr>
                                          
                                                                                          <tr>
                                                                                                  <td class="border border-0 calendar-day-single text-light text-center px-2"><strong @if (!$twoDays) style="visibility: hidden" @endif>{{date_format(date_create($vEvent->fim), 'd')}}</strong></td>
                                                                                          </tr>
                                                                          </table>
                                                                          <div class="text-uppercase mt-1 text-left pl-4 ml-3">
                                                                                              <div class="font-weight-bold pl-3"><small><span class="oi oi-map-marker text-info"></span></small><small><b class="text-dark"> &nbsp;  {{$vEvent->City->name}} </b></small>
                                                                                                      <p class="text-truncate"><small><small style="color: black">{{$vEvent->local_evento}}</small></small>
                                                                                                      </p>
                                                                                              </div>
                                                                          </div>
                                                          </div>
                                                        </div>
                                                      </div> 

                        </div>
                        @endif
                        @endforeach
            
                </section>





         </div>
       </div>

       <hr class="featurette-divider">
@endif
       <!-- /END THE FEATURETTES -->

</section>
</main>
        @include('public.templates.menuMobile')
@endsection
 
@section('footer')
        @include('public.templates.footer')
@endsection
 
@section('pageScript')
<script type="text/javascript" src="{{url('/portal/assets/libs/portal/js/eventsShow.js')}}"></script>
<script type="text/javascript" src="{{url('/portal/assets/libs/portal/js/eventsSearchbar.js')}}"></script>
@endsection
