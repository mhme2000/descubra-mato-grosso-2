
<!DOCTYPE html>
<html lang="pt-br">

    <head>
        <script async
            src="http://analytics.turismo.mg.gov.br/analytics/default/b?url=http%3A%2F%2Fwww%7Cminasgerais%7Ccom%7Cbr%2Fpt%2Fdoacao-de-midias&ref=http%3A%2F%2Fwww%7Cminasgerais%7Ccom%7Cbr%2Fpt&c=GA1.3.1481861829.1583153592&t=Portal Minas Gerais&env=1">
        </script>

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-125337813-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-125337813-1');
        </script>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="no-cache" http-equiv="pragma">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
        <meta name="description" property="description" content="">
    

        <meta name="image" property="image" content="">
    

        <meta property="og:url" content="http://www.descubramatogrosso.com.br/pt/doacao-de-midias">
    <meta property="og:title" content="Portal Minas Gerais">
    <meta property="og:type" content="website">
    <meta property="og:site_name" content="Portal Minas Gerais">
    

        <meta name="twitter:card" content="Portal Minas Gerais">
    <meta name="twitter:site" content="Portal Minas Gerais">
    <meta name="twitter:title" content="Portal Minas Gerais">
    <meta name="twitter:creator" content="Portal Minas Gerais">
    <meta name="twitter:domain" content="www.descubramatogrosso.com.br">
    

                <meta name="image" property="image" content="">
    

                <meta name="author" content="Portal Minas Gerais">
        <link rel="icon" href="http://www.descubramatogrosso.com.br/favicon.ico" />
        <title>Portal Minas Gerais</title>
        <link rel="stylesheet" type="text/css" href="http://www.descubramatogrosso.com.br/portal/assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="http://www.descubramatogrosso.com.br/portal/assets/css/main.css">
        <style>
            @font-face {
                font-family: 'FontAwesome';
                src: url('http://www.descubramatogrosso.com.br/assets/fonts/font-awesome/fontawesome-webfont78ce.eot?v=4.2.0');
                src: url('http://www.descubramatogrosso.com.br/assets/fonts/font-awesome/fontawesome-webfontd41d.eot?#iefix&amp;v=4.2.0') format('embedded-opentype'),
                url('http://www.descubramatogrosso.com.br/assets/fonts/font-awesome/fontawesome-webfont78ce.woff?v=4.2.0') format('woff'),
                url('http://www.descubramatogrosso.com.br/assets/fonts/font-awesome/fontawesome-webfont78ce.ttf?v=4.2.0') format('truetype'),
                url('http://www.descubramatogrosso.com.br/assets/fonts/font-awesome/fontawesome-webfont78ce.svg?v=4.2.0#fontawesomeregular') format('svg');
                font-weight: normal;
                font-style: normal;
            }

            .fa {
                display: inline-block;
                font: normal normal normal 14px/1 FontAwesome;
                font-size: inherit;
                text-rendering: auto;
                -webkit-font-smoothing: antialiased;
                -moz-osx-font-smoothing: grayscale;
            }

            .fa-chevron-left:before {
                content: "\f053";
            }

            .fa-chevron-right:before {
                content: "\f054";
            }

            #destinos .nav-tabs .nav-item .nav-link.active .hospedagem p {
                color: #ef662f;
            }

            body>.alert.alert-danger {
                border-color: #bd1a3a;
                border-radius: 0;
                background-color: #c71a3d;
                color: #fff;
            }

            body>.alert.alert-info {
                border-color: #b3e1a9;
                border-radius: 0;
                background-color: #b7e6ad;
                color: #555;
            }

            body>.alert>button.close {
                opacity: 0.4;
            }

            .menu-resumido-desk {
                margin-top: -200px;
                -webkit-transition: margin-top 1s;
                /* Safari */
                transition: margin-top 1s;
            }

            .menu-resumido-desk.menu-fixo {
                margin-top: 0;
                display: block !important;

            }

            em {
                font-style: italic;
            }

            @media (max-width: 543px) {
                #destinos {
                    margin-top: 80px;
                    padding: 1% 0;
                    "

                }
            }

            @media (max-width: 980px) {
                .menu-resumido-desk.menu-fixo {
                    margin-top: 0;
                    display: none !important;

                }
            }

            #video-container {
                display: none;
            }

            #titleVisiteMinas {
                display: none;
            }

            @media (min-width: 544px) and (max-width:767) {
                #destinos {

                    padding: 1% 0;
                    "

                }
            }

            @media (min-width: 768px) {
                #destinos {

                    padding: 1% 0;
                    "

                }
            }

            .chamada__carnaval {
                position: fixed;
                left: 0;
                bottom: 58px;


                -webkit-animation-name: chamada;
                -webkit-animation-duration: 0.5s;
                -webkit-animation-timing-function: linear;
                -webkit-animation-direction: alternate;
            }

            @media (max-width:560px){
                .chamada__carnaval img{
                    width:60%;
                }
                .chamada__carnaval.alert-dismissible .close {

                    top: 25px !important;
                    right: 131px !important;
                }
                body > .alert > button.close {
                    opacity: 1;
                }
            }

            @media (min-width:561px) and (max-width:720px){
                .chamada__carnaval img{
                    width:80%;
                }
                .chamada__carnaval.alert-dismissible .close{
                    top: 33px !important;
                    right: 60px !important;
                }
                body > .alert > button.close {
                    opacity: 1;
                }
                
            }

            @keyframes  chamada {
                from {
                    left: -200px;
                }

                to {
                    left: 0;
                }
            }

            .chamada__carnaval.alert-dismissible .close {

                position: absolute;
                top: 42px;
                right: -9px;
                padding: 5px;
                color: inherit;
                background: rgba(255, 147, 24, 0.79);
                border-radius: 50%;
            }
            body > .alert > button.close {
                    opacity: 1;
                }
            .chamada__carnaval.alert{
                padding: unset;
                margin:unset;
            }

.chamada__carnaval.alert-warning{
  background: unset;
  border:unset;
}

.chamada__carnaval .close {

font-size: 1rem;
width:25px;
opacity:unset;
color: white !important;

}

            
        </style>
            <style>
        .arrow_box {
            position: relative;
            background: #fff;
            border: 1px solid #ccc;
            padding: 4rem;
        }
        .arrow_box:after, .arrow_box:before {
            right: 100%;
            top: 50%;
            border: solid transparent;
            content: " ";
            height: 0;
            width: 0;
            position: absolute;
            pointer-events: none;
        }

        .arrow_box:after {
            border-color: rgba(255, 255, 255, 0);
            border-right-color: #fff;
            border-width: 30px;
            margin-top: -30px;
        }
        .arrow_box:before {
            border-color: rgba(204, 204, 204, 0);
            border-right-color: #ccc;
            border-width: 31px;
            margin-top: -31px;
        }

        #destinos #tarja_intern #options #donation .form-group select {
            background: #e6e6e6;
            border-radius: 0;
            border: .0625rem solid #ccc;
            width: 100%;
            font-family: Signika, sans-serif;
        }
        .modal-body p{
            margin-bottom: 30px;
        }
        .modal-content{
            width: 830px;
        }
        .regrasPDF p {
            margin-bottom: 25px;
            font-size: 14px;
        }
        .regrasPDF h2{
            text-align: center !important;
            font-weight: bold;
            margin-bottom: 30px;
            margin-left:50px;
        }
        .regrasPDF ul li:last-child{
            margin-bottom: 30px;
            font-size: 14px;
        }
    </style>
    </head>

    <body data-spy="scroll" data-target=".navbar-fixed-top">
                <style>
.language-dropdown::after {
    display: inline-block;
    width: 0;
    height: 0;
    margin-left: 0.25rem;
    vertical-align: middle;
    content: "";
    visibility: visible;
    border-top: 0.3em solid;
    border-right: 0.3em solid transparent;
    border-left: 0.3em solid transparent;
}
.dropdown-toggle:after {
    display: none;
}
.open .dropdown-menu.lang-menu{
    display: block !important;
}
#list-city.favorites-list li a{
    color:#fff;
}
    
#nav-icon1{
  width: 30px;
  height: 25px;
  position: relative;
  /*margin: 50px auto;*/
  -webkit-transform: rotate(0deg);
  -moz-transform: rotate(0deg);
  -o-transform: rotate(0deg);
  transform: rotate(0deg);
  -webkit-transition: .5s ease-in-out;
  -moz-transition: .5s ease-in-out;
  -o-transition: .5s ease-in-out;
  transition: .5s ease-in-out;
  cursor: pointer;
}

#nav-icon1 span {
  display: block;
  position: absolute;
  height: 6px;
  width: 100%;
  background: #666;
  border-radius: 0px;
  opacity: 1;
  left: 0;
  -webkit-transform: rotate(0deg);
  -moz-transform: rotate(0deg);
  -o-transform: rotate(0deg);
  transform: rotate(0deg);
  -webkit-transition: .25s ease-in-out;
  -moz-transition: .25s ease-in-out;
  -o-transition: .25s ease-in-out;
  transition: .25s ease-in-out;
}

#nav-icon1 span:nth-child(1) {
  top: 0px;
}

#nav-icon1 span:nth-child(2) {
  top: 10px;
}

#nav-icon1 span:nth-child(3) {
  top: 20px;
}

#nav-icon1.open span:nth-child(1) {
  top: 12px;
  -webkit-transform: rotate(135deg);
  -moz-transform: rotate(135deg);
  -o-transform: rotate(135deg);
  transform: rotate(135deg);
}

#nav-icon1.open span:nth-child(2) {
  opacity: 0;
  left: -60px;
}

#nav-icon1.open span:nth-child(3) {
  top: 12px;
  -webkit-transform: rotate(-135deg);
  -moz-transform: rotate(-135deg);
  -o-transform: rotate(-135deg);
  transform: rotate(-135deg);
}
.link-gastronomia{
    width: 110px;
    margin: -4px 0 0px 0;
}


</style>

    <!-- MENU RESUMIDO -->
    
        <header class=" container-fluid bg shadow-sm-1" style="background-color:white;display:flex;align-items:center;">
            <nav class="navbar navbar-expand-lg bg-white navbar-light"> 
                <a class="navbar-brand mb-0" href="{{url($p_Language)}}">
                    <img class="img-fluid" src="{{url('/portal/assets/imgs/descubraMatoGrosso.png')}}" alt="Descubra Mato Grosso" title="Descubra Mato Grosso">
                </a>
                <button class="navbar-toggler ml-auto" type="button" data-toggle="collapse" data-target="#navbarPortal">
                    <span class="navbar-toggler-icon"></span>
                </button>
               
            </nav>
        </header>


                                <div class="row-fluid" id="useful-information">
       
                <h2>Doa&ccedil;&atilde;o de M&iacute;dias</h2>
          
    </div>

    <div class="row-fluid" id="destinos" style="padding:1% 0;">
        <div class="container">
            <div class="col-lg-12">
                <div class="col-lg-8 col-lg-offset-2">
                    <div class="col-lg-12 text-center">
                        <img src="http://www.descubramatogrosso.com.br/portal/assets/imgs/creative.png" alt="Creative Commons">
                        <p></p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row-fluid" id="destinos" style="padding:1% 0;">
        <div class="col-lg-12" id="tarja_intern">
            <div class="container">
                <div class="col-lg-10 col-lg-offset-1">

                    <div class="row" id="options">

                        <form method="POST" action="http://www.descubramatogrosso.com.br/pt/doacao-de-midias" accept-charset="UTF-8" id="donation" class="form-inline" enctype="multipart/form-data"><input name="_token" type="hidden" value="nyvWUdAxx79nagsLlY6FcMj6pwSGjjvyX75ViHpY">
                            <div class="col-lg-12" style="padding:0 0 2rem;">
                                <div class="col-lg-6 form-group">
                                    <div class="col-lg-12 no-padding" style="padding:0 0 0.5rem;">
                                        <label for="codigo" style="text-align:left;">Permitir que adapta&ccedil;&otilde;es do seu trabalho sejam compartilhadas?</label>
                                        <label class="radio-inline">
                                            <input type="radio" name="doacao[permite_compartilhar]" class="license-type permite_compartilhar" value="1" checked> Sim
                                        </label>

                                        <label class="radio-inline">
                                            <input type="radio" name="doacao[permite_compartilhar]" class="license-type permite_compartilhar" value="0"> N&atilde;o
                                        </label>
                                    </div>

                                    <div class="col-lg-12 no-padding" style="padding:0 0 0.5rem;">
                                        <label class="radio-inline">
                                            <input type="radio" name="doacao[permite_compartilhar]" class="license-type permite_compartilhar" value="2"> Sim, desde que os outros compartilhem igual
                                        </label>
                                    </div>

                                    <div class="col-lg-12 no-padding" style="padding:0 0 0.5rem;">
                                        <label for="codigo" style="text-align:left;">Permitir usos comerciais do seu trabalho?</label>
                                    </div>
                                    <div class="col-lg-12 no-padding">
                                        <label class="radio-inline">
                                            <input type="radio" name="doacao[permite_uso_comercial]" class="license-type permite_uso_comercial" value="1" checked> Sim
                                        </label>

                                        <label class="radio-inline">
                                            <input type="radio" name="doacao[permite_uso_comercial]" class="license-type permite_uso_comercial" value="0"> N&atilde;o
                                        </label>
                                    </div>
                                </div>

                                <div class="col-lg-6 form-group">
                                    <div class="col-lg-12 arrow_box">
                                        <div class="col-lg-12">
                                            <p style="font-size:16px;color:#4b4b4b;padding:0;margin:0 0 10px;font-weight:300;">Licen&ccedil;a selecionada:<br>
                                                <span id="license_type">Atribui&ccedil;&atilde;o 4.0 Internacional</span></p>
                                        </div>
                                        <div class="col-lg-12">
                                            <p style="font-size:14px;color:#cccccc;padding:0;margin:0;font-weight:300;"><small id="license_type_subtitle">Essa &eacute; uma licen&ccedil;a de cultura livre.</small></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-lg-12">
                                <hr style="float:left;margin-left:auto;margin-right:auto;display:block;width:100%;height:1px;background:#cccccc;">
                            </div>

                            <div class="row-fluid">
                                <div class="col-md-6 form-group" >
                                    <div class="col-lg-12 no-padding">
                                        <label for="nome">Nome completo do doador da m&iacute;dia:</label>
                                    </div>
                                    <div class="col-lg-12 no-padding">
                                        <input name="doacao[nome]" type="text" class="form-control" id="nome" placeholder="Informe um nome - pessoa ou empresa" required>
                                    </div>
                                </div>

                                <div class="col-md-6 form-group">
                                    <div class="col-lg-12 no-padding">
                                        <label for="numberDoc">CPF:</label>
                                    </div>
                                    <div class="col-lg-12 no-padding">
                                        <input name="doacao[documento]" type="text" class="form-control" id="numberDoc" placeholder="N&uacute;mero do documento" required>
                                    </div>
                                </div>
                                
                            </div>

                            

                           
                                <!--<div class="col-md-2 form-group">
                                    <div class="col-lg-12 no-padding">
                                        <label for="typeDoc">CPF:</label>
                                    </div>
                                    <div class="col-lg-4" style="display: none;">
                                        <label class="radio-inline">
                                            <input type="radio" class="pessoa_juridica" value="0" checked> CPF
                                        </label>

                                        <label class="radio-inline">
                                            <input name="doacao[pessoa_juridica]" type="radio" class="pessoa_juridica" value="1"> CNPJ
                                        </label>
                                    </div>

                                </div>-->
       

                            <div class="col-lg-12 no-padding">
                                <div class="col-lg-6 form-group no-padding">
                                    <div class="col-lg-12 no-padding">
                                       <div class="col-lg-12">
                                            <label for="email">E-mail:</label>
                                        </div>
                                       <div class="col-lg-12">
                                            <input name="doacao[email]" type="email" class="form-control" id="email" placeholder="E-mail" required>
                                       </div>
                                    </div>
                                </div>

                                <div class="col-lg-6 form-group no-padding">
                                   <div class="col-lg-12">
                                        <label for="phone">Telefone:</label>
                                    </div>
                                    <div class="col-lg-12">
                                        <input name="doacao[telefone]" type="text" class="form-control" id="phone" placeholder="Telefone" required>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-12 no-padding form-group">
                                <div class="col-lg-6 form-group no-padding">
                                    <div class="col-lg-12">
                                        <label for="city">Cidade da foto/v&iacute;deo:</label>
                                    </div>
                                    <div class="col-lg-12">
                                        <input name="doacao[cidade]" type="text" class="form-control" id="cidade" placeholder="Cidade" required>
                                    </div>
                                </div>

                                <div class="col-lg-12 form-group no-padding">
                                    <div class="col-lg-12">
                                        <label for="description">Nos conte um pouco sobre o local da foto/v&iacute;deo:</label>
                                    </div>
                                    <div class="col-lg-12">
                                    <textarea name="doacao[descricao]" class="form-control style-area" style="width: 100%; background:#e6e6e6; min-height: 200px; max-height: 200px;" id="description" maxlength="250" required></textarea>
                                        
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-12 no-padding form-group">
                                <div class="col-lg-4">
                                    <span>
                                        <label for="typeDonation">Tipo de m&iacute;dia para doa&ccedil;&atilde;o:</label>
                                    </span>
                                </div>
                                <div class="col-lg-8">
                                    <label class="radio-inline">
                                        <input type="radio" name="doacao[tipo_midia]" class="tipo_midia" value="Imagem" checked> Imagem
                                    </label> 

                                    <label class="radio-inline">
                                        <input type="radio" name="doacao[tipo_midia]" class="tipo_midia" value="Áudio" > &Aacute;udio
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="doacao[tipo_midia]" class="tipo_midia" value="Vídeo"> V&iacute;deo
                                    </label>
                                    
                                </div>
                            </div>

                            <div class="col-lg-12 no-padding form-group">
                                <div class="col-lg-3">
                                    <label for="files">Arquivo para upload:</label>
                                </div>
                                <div class="col-lg-9">
                                    <input id="fakeupload" class="fakeupload" type="text">
                                    <input id="realupload" name="arquivo" class="realupload" type="file" onchange="onChangeFile(this)" required accept="image/*">
                                </div>
                            </div>

                            <div class="col-lg-12 form-group">
                                <div class="col-md-6 no-padding form-group rules">
                                    
                                        <div class="col-lg-1 text-left no-padding">
                                            <input type="radio" name="accept_rules" class="form-control validate radio-rules" id="rules" required>
                                        </div>
                                        <div class="col-lg-11 no-padding rules-agree">
                                            <label for="rules" style="padding:0;">Concordo com as <a href="javascript:;" id="btnPDF">regras</a> de doa&ccedil;&atilde;o de m&iacute;dia</label>
                                        </div>
                                    
                                </div>

                                <!--Retirado a pedido do Thiago.
                                <div class="col-md-6 no-padding form-group rules text-right">
                                    <a href="" download>Termo de compromisso</a>
                                </div>-->
                            </div>

                            <div class="col-lg-12 rules">
                                <button type="submit" class="enviar pull-right margint" id="enviar">Enviar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Regras de doa&ccedil;&atilde;o de m&iacute;dia</h4>
                </div>
                <div class="modal-body">
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary btn-close" data-dismiss="modal">OK</button>
                </div>
            </div>
        </div>
    </div>

    <div id="geraPDF" style="display:none;">
        <div class="content">

        </div>
     </div>
     
     
       

        <!-- javascript -->
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script type="text/javascript" src="https://code.jquery.com/ui/1.11.0/jquery-ui.min.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?language=pt-BR&key=AIzaSyBLc_8beOK5wVSM2NeSk1Bl4mLwtq6jtGw"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.3.1/jquery.maskedinput.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/jquery.validate.min.js"></script>
<script type="text/javascript" src="http://www.descubramatogrosso.com.br/portal/assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="http://www.descubramatogrosso.com.br/portal/assets/js/all.js"></script>
<script src="https://unpkg.com/minigrid@3.1.1/dist/minigrid.min.js"></script>



<script type="text/javascript">
    (function(){
        var grid;
        function init() {
            grid = new Minigrid({
                container: '.cards',
                item: '#services',
                gutter: 6
            });
            grid.mount();
        }

        // mount
        function update() {
            grid.mount();
        }

        document.addEventListener('DOMContentLoaded', init);
        window.addEventListener('resize', update);
    })();
</script>
<script src="http://www.descubramatogrosso.com.br/vendor/bootstrap-notify/bootstrap-notify.min.js"></script>

<script>
    function showNotify(p_Type, p_Message)
    {
        $.notify({
            message: p_Message
        },{
            type: p_Type,
            delay: 8000,
            z_index: 9999,
            placement:{
                align: 'center'
            }
        });
    }

    function submitNewsletter()
    {
        $.post("http://www.descubramatogrosso.com.br/pt/newsletter", {email: $('#basic-news').val(), _token: $('#newsletterForm input[name="_token"]').val()})
            .done(function(p_Data)
            {
                showNotify(p_Data[0] ? 'info' : 'danger', p_Data[1]);
            });
        return false;
    }

//    function focusOnModal()
//    {
//        if($('#myModal form input[name="q"]').is(':visible'))
//            $('#myModal form input[name="q"]').focus();
//    }
//    window.setInterval(focusOnModal, 250);

    function setCookie(p_Key,p_Value,p_Days)
    {
        var v_Expires = "";
        if (p_Days)
        {
            var v_Date = new Date();
            v_Date.setTime(v_Date.getTime() + (p_Days*24*60*60*1000));
            v_Expires = "; expires=" + v_Date.toGMTString();
        }

        document.cookie = p_Key + "=" + p_Value + v_Expires + "; path=/";
    }

    function getCookie(p_Key)
    {
        var p_KeyEQ = p_Key + "=";
        var v_CookieArray = document.cookie.split(';');
        for(var i=0; i < v_CookieArray.length; i++)
        {
            var v_Cookie = v_CookieArray[i];
            while (v_Cookie.charAt(0)==' ')
                v_Cookie = v_Cookie.substring(1, v_Cookie.length);
            if (v_Cookie.indexOf(p_KeyEQ) == 0)
                return v_Cookie.substring(p_KeyEQ.length, v_Cookie.length);
        }
        return null;
    }


    function toggleFavorite(p_Url, p_Name, p_Img, p_Description)
    {
        var v_IsFavorite = false;
        var v_FavoritesList = JSON.parse(localStorage.getItem("favoritesList")) || [];
        $(v_FavoritesList).each(function(c_Index, c_Item){
            if(c_Item.url == p_Url)
            {
                v_FavoritesList.splice(c_Index, 1);
                v_IsFavorite = true;
                return false;
            }
        });
        if(!v_IsFavorite)
            v_FavoritesList.push({url: p_Url,name:p_Name,img:p_Img,description:p_Description});
        
        localStorage.setItem("favoritesList",JSON.stringify(v_FavoritesList));
        updateFavoriteList();
    }

    function isFavorite(p_Url)
    {
        var v_IsFavorite = false;
        var v_FavoritesList = JSON.parse(localStorage.getItem("favoritesList")) || [];
        $(v_FavoritesList).each(function(c_Index, c_Item){
            if(c_Item.url == p_Url)
            {
                v_IsFavorite = true;
                return false;
            }
        });
        return v_IsFavorite;
    }

    $(document).ready(function()
    {
        updateFavoriteList();

                            });

    function updateFavoriteList()
    {
        var v_FavoritesList = JSON.parse(localStorage.getItem("favoritesList")) || [];
        v_FavoritesList = v_FavoritesList.slice(Math.max(v_FavoritesList.length - 4, 0));

        var v_FavoritesString = '';
        $(v_FavoritesList).each(function(c_Index, c_Item){
            v_FavoritesString += '<li>' +
                                    '<a href="' + c_Item.url + '">' +
                                        c_Item.name +
                                    '</a>' +
                                '</li>';
        });
        $('.favorites-list').html(v_FavoritesString);
        $('#toggleFavorite span:last').html((isFavorite(location.href) ? 'Remova dos favoritos' : 'Adicione aos favoritos'));
    }

      // Nao deve ser usado 0o UA-24127191-2
    // o correto e o UA-69793661-1

  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-69793661-1', 'auto');
  ga('send', 'pageview');


    $(function(){   
        var nav = $('.menu-resumido-desk');   
        $(window).scroll(function () { 
            if ($(this).scrollTop() > 155) { 
                nav.addClass("menu-fixo"); 
            } else { 
                nav.removeClass("menu-fixo"); 
            } 
        });  
    });

    window.onload = function() {
        if(location.href == 'http://www.descubramatogrosso.com.br/pt'){
            // Video
            var video = document.getElementById("video");

            // Buttons
            var playButton = document.getElementById("play-pause");
            var playButtondois = document.getElementById("play-pause-dois");
            var muteButton = document.getElementById("mute");
            var fullScreenButton = document.getElementById("full-screen");

            // Sliders
            var seekBar = document.getElementById("seek-bar");
            var volumeBar = document.getElementById("volume-bar");


            // Event listener for the play/pause button
            playButton.addEventListener("click", function() {
                if (video.paused == true) {
                    // Play the video
                    video.play();
                    playButtondois.innerHTML = "Pause";
                    // Update the button text to 'Pause'
                   // playButton.innerHTML = "Pause";
                } else {
                    // Pause the video
                    video.pause();

                    // Update the button text to 'Play'
                    //playButton.innerHTML = "Play";
                }
            });

            // Event listener for the play/pause button
            playButtondois.addEventListener("click", function() {
                if (video.paused == true) {
                    // Play the video
                    video.play();

                    // Update the button text to 'Pause'
                    playButtondois.innerHTML = "Pause";
                } else {
                    // Pause the video
                    video.pause();
                    $('.btn_video').addClass("show");
                     $('#video-controls').removeClass("show");
                    // Update the button text to 'Play'
                    playButtondois.innerHTML = "Play";
                }
            });


            // Event listener for the mute button
            muteButton.addEventListener("click", function() {
                if (video.muted == false) {
                    // Mute the video
                    video.muted = true;

                    // Update the button text
                    muteButton.innerHTML = "Unmute";
                } else {
                    // Unmute the video
                    video.muted = false;

                    // Update the button text
                    muteButton.innerHTML = "Mute";
                }
            });


            // Event listener for the full-screen button
            fullScreenButton.addEventListener("click", function() {
                if (video.requestFullscreen) {
                    video.requestFullscreen();
                } else if (video.mozRequestFullScreen) {
                    video.mozRequestFullScreen(); // Firefox
                } else if (video.webkitRequestFullscreen) {
                    video.webkitRequestFullscreen(); // Chrome and Safari
                }
            });


            // Event listener for the seek bar
            seekBar.addEventListener("change", function() {
                // Calculate the new time
                var time = video.duration * (seekBar.value / 100);

                // Update the video time
                video.currentTime = time;
            });


            // Update the seek bar as the video plays
            video.addEventListener("timeupdate", function() {
                // Calculate the slider value
                var value = (100 / video.duration) * video.currentTime;

                // Update the slider value
                seekBar.value = value;
            });

            // Pause the video when the seek handle is being dragged
            seekBar.addEventListener("mousedown", function() {
                video.pause();
            });

            // Play the video when the seek handle is dropped
            seekBar.addEventListener("mouseup", function() {
                video.play();
            });

            // Event listener for the volume bar
            volumeBar.addEventListener("change", function() {
                // Update the video volume
                video.volume = volumeBar.value;
            });
        }
    }
</script>            <script>
        
        var MAX_FILE_SIZE = 1024*1024*10; //10MB
        $(document).ready(function(){
            $('#numberDoc').mask('999.999.999-99');
//            $('.pessoa_juridica').change(function(){
//                if(this.value == 0)
//                    $('#numberDoc').mask('999.999.999-99');
//                else
//                    $('#numberDoc').mask('99.999.999/9999-99');
//            });

            $('.license-type').change(function(){
                var v_AllowShare = $('.permite_compartilhar:checked').val();
                var v_AllowCommercialUsage = $('.permite_uso_comercial:checked').val();
                var v_LicenseType = '';
                var v_LicenseTypeSubtitle = 'Esta não é uma licença de cultura livre.';
                if(v_AllowShare == 2) {
                    if(v_AllowCommercialUsage == 1)
                        v_LicenseType = 'Atribuição 4.0 Internacional';
                    else
                        v_LicenseType = 'Atribuição-Não-Comercial 4.0 internacional';
                }
                else if(v_AllowShare == 1) {
                    if(v_AllowCommercialUsage == 1) {
                        v_LicenseType = 'Atribuição-CompartilhaIgual 4.0 internacional';
                        v_LicenseTypeSubtitle = 'Essa é uma licença de cultura livre.'
                    }
                    else
                        v_LicenseType = 'Atribuição-NãoComercial-CompartilhaIgual 4.0 internacional';
                }
                else {
                    if(v_AllowCommercialUsage == 1)
                        v_LicenseType = 'Atribuição-SemDerivações 4.0 internacional';
                    else
                        v_LicenseType = 'Atribuição-NãoComercial-SemDerivações 4.0 internacional';
                }
                $('#license_type').text(v_LicenseType);
                $('#license_type_subtitle').text(v_LicenseTypeSubtitle);
            });

            $('.tipo_midia').change(function(){
                if($('.tipo_midia').val() == 'Áudio')
                    $('#realupload').replaceWith('<input id="realupload" name="arquivo" class="realupload" type="file" onchange="onChangeFile(this)" required accept="audio/*">');
                else if($('.tipo_midia').val() == 'Vídeo')
                    $('#realupload').replaceWith('<input id="realupload" name="arquivo" class="realupload" type="file" onchange="onChangeFile(this)" required accept="video/*">');
                else
                    $('#realupload').replaceWith('<input id="realupload" name="arquivo" class="realupload" type="file" onchange="onChangeFile(this)" required accept="image/*">');
            }).change();
        });

        function RetornaDataHoraAtual(){
            var dNow = new Date();
            var localdate = dNow.getDate() + '/' + (dNow.getMonth()+1) + '/' + dNow.getFullYear() + ' ' + dNow.getHours() + ':' + dNow.getMinutes();
            return localdate;
        }
       

        $('#btnPDF').on('click', function(){
            
            
             let nome = $('#nome').val();
             let cpf = $('#numberDoc').val();
             let permite_adapt = $('.permite_compartilhar:checked').val();
             let uso_comercial =$('.permite_uso_comercial:checked').val();
             let tipo_de_midia = $('.tipo_midia:checked').val();
             let email = $('#email').val();
             let telefone=$('#phone').val();
             let dataHora = RetornaDataHoraAtual();
             let data = dataHora.split(' ');
             let dia = data[0];
             let hora = data[1];
             let tipo_licenca = '';
             let ip = userip;
             let arquivo = $('#fakeupload').val();
             
      
            //Tipo de licença
            if(permite_adapt == 2) {
                    if(uso_comercial == 1)
                        tipo_licenca = 'Atribuição 4.0 Internacional';
                    else
                        tipo_licenca = 'Atribuição-Não-Comercial 4.0 internacional';
                }
                else if(permite_adapt == 1) {
                    if(uso_comercial == 1) {
                        tipo_licenca = 'Atribuição-CompartilhaIgual 4.0 internacional';
                        v_LicenseTypeSubtitle = 'Essa é uma licença de cultura livre.'
                    }
                    else
                        tipo_licenca = 'Atribuição-NãoComercial-CompartilhaIgual 4.0 internacional';
                }
                else {
                    if(uso_comercial == 1)
                        tipo_licenca = 'Atribuição-SemDerivações 4.0 internacional';
                    else
                        tipo_licenca = 'Atribuição-NãoComercial-SemDerivações 4.0 internacional';
                }
            
            //Atribuição valores para campos de licença marcados   
            if(permite_adapt ==1) 
                permite_adapt = 'Sim'
            else if(permite_adapt == 2)
                permite_adapt = 'Sim, desde que os outros compartihem igual'; 
                
            else
                permite_adapt = 'Não'   

            if(uso_comercial == 1) 
                uso_comercial = 'Sim'
            else
                uso_comercial = 'Não';



            let regras = `<div class='regrasPDF'>
            <h2>TERMO DE DOAÇÃO DE MÍDIA</h2>
            <p>Eu, <strong>${nome ? nome : 'não informado'}</strong>, inscrito no CPF sob o número <strong>${cpf ? cpf : 'não informado'}</strong>, portador do e-mail<br>
            <strong>${email ? email : 'não informado'}</strong> e telefone <strong>${telefone ? telefone : 'não informado'}</strong> concedo a doação da mídia <br>
            ${tipo_de_midia}, de acordo com os padrões de licença do Creative Commons, à <strong>SECRETARIA DE<br>
            TURISMO DE ESTADO DE MATO GROSSO.</strong></p>
            <p>Cujo arquivo de mídia doado foi nomeado como <strong>${arquivo ? arquivo : 'não informado'}</strong>, enviado a partir do IP<br>
            <strong>${ip}</strong>, em <strong>${hora} ${dia}</strong> no Portal de Turismo de Mato Grosso, <a href="www.descubramatogrosso.com.br">www.descubramatogrosso.com.br</a>.</p>
            <p>Regras da doação:
            <ul>
                <li>Permitir que adaptações do seu trabalho sejam compartilhadas? ${permite_adapt}</li>
                <li>Permitir usos comerciais do seu trabalho? ${uso_comercial}</li>
                <li>Licença selecionada: ${tipo_licenca}</li>
            </ul>
            </p>
            <p>Todas as licenças serão utilizadas por um período de até 100 (cem) anos a partir da data de
            doação.</p>
            <p>Declaro para devidos fins que minha participação é voluntária e sem ônus.</p>
            <p>
            Cuiabá, ${dia}.<br>
            <strong>${nome ? nome : 'não informado'}</strong>
            </p>
            
            <p>Saiba mais sobre as licenças do Creative Commons em: <a href='https://br.creativecommons.org/licencas/'>https://br.creativecommons.org/licencas/</a></p>
            </div>`;
            $('#geraPDF .content').html(regras);
            savePDF(document.querySelector('#geraPDF'));

            

            // $('.btn-close').on('click', function(){
            //     $('.modal-body').html('');
            // });
            
            // alert(regras);
            
            //gerar pdf

            // $(document).ready(function(){
            //     $('#btnPDF').click(function() {
            //         savePDF(document.querySelector('#geraPDF'));
            //     });
            // });
  
            function savePDF(codigoHTML) {
                var doc = new jsPDF('potrait', 'pt', 'a4'),
                data = new Date();
                margins = {
                top: 40,
                bottom: 60,
                left: 40,
                right:500,
                width: 1000
            };
                doc.fromHTML(codigoHTML,
                margins.left, // x coord
                margins.top, { pagesplit: true },
                function(dispose){
                    doc.save("TermoDoacaoMidia - "+data.getDate()+"/"+data.getMonth()+"/"+data.getFullYear()+".pdf");
                });
            }

            
        }); //fim regras

        function onChangeFile(p_FileInput)
        {
            var v_CancelClicked = (p_FileInput.value == '');

            if(v_CancelClicked)
                $('#fakeupload').val('');
            else
            {
                if(p_FileInput.files[0].size > MAX_FILE_SIZE)
                {
                    showNotify('danger', 'Tamanho máximo de 10MB excedido.');
                    $('#fakeupload').val('');
                    return false;
                }
                $('#fakeupload').val(p_FileInput.value.split('\\').pop());
            }
        }
    </script>
<script type="text/javascript" src="https://l2.io/ip.js?var=userip"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.5/jspdf.min.js"></script>

        
    </body>

</html>