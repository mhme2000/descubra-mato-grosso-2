<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script type="text/javascript" src="https://code.jquery.com/ui/1.11.0/jquery-ui.min.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?language=pt-BR&key=AIzaSyBLc_8beOK5wVSM2NeSk1Bl4mLwtq6jtGw"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.3.1/jquery.maskedinput.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/jquery.validate.min.js"></script>
<script type="text/javascript" src="{{url('/portal/assets/js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{url('/portal/assets/js/all.js')}}"></script>
<script src="https://unpkg.com/minigrid@3.1.1/dist/minigrid.min.js"></script>



<script type="text/javascript">
    (function(){
        var grid;
        function init() {
            grid = new Minigrid({
                container: '.cards',
                item: '#services',
                gutter: 6
            });
            grid.mount();
        }

        // mount
        function update() {
            grid.mount();
        }

        document.addEventListener('DOMContentLoaded', init);
        window.addEventListener('resize', update);
    })();
</script>
<script src="{{url('vendor/bootstrap-notify/bootstrap-notify.min.js')}}"></script>

<script>
    function showNotify(p_Type, p_Message)
    {
        $.notify({
            message: p_Message
        },{
            type: p_Type,
            delay: 8000,
            z_index: 9999,
            placement:{
                align: 'center'
            }
        });
    }

    function submitNewsletter()
    {
        $.post("{{url($p_Language . '/newsletter')}}", {email: $('#basic-news').val(), _token: $('#newsletterForm input[name="_token"]').val()})
            .done(function(p_Data)
            {
                showNotify(p_Data[0] ? 'info' : 'danger', p_Data[1]);
            });
        return false;
    }

//    function focusOnModal()
//    {
//        if($('#myModal form input[name="q"]').is(':visible'))
//            $('#myModal form input[name="q"]').focus();
//    }
//    window.setInterval(focusOnModal, 250);

    function setCookie(p_Key,p_Value,p_Days)
    {
        var v_Expires = "";
        if (p_Days)
        {
            var v_Date = new Date();
            v_Date.setTime(v_Date.getTime() + (p_Days*24*60*60*1000));
            v_Expires = "; expires=" + v_Date.toGMTString();
        }

        document.cookie = p_Key + "=" + p_Value + v_Expires + "; path=/";
    }

    function getCookie(p_Key)
    {
        var p_KeyEQ = p_Key + "=";
        var v_CookieArray = document.cookie.split(';');
        for(var i=0; i < v_CookieArray.length; i++)
        {
            var v_Cookie = v_CookieArray[i];
            while (v_Cookie.charAt(0)==' ')
                v_Cookie = v_Cookie.substring(1, v_Cookie.length);
            if (v_Cookie.indexOf(p_KeyEQ) == 0)
                return v_Cookie.substring(p_KeyEQ.length, v_Cookie.length);
        }
        return null;
    }


    function toggleFavorite(p_Url, p_Name, p_Img, p_Description)
    {
        var v_IsFavorite = false;
        var v_FavoritesList = JSON.parse(localStorage.getItem("favoritesList")) || [];
        $(v_FavoritesList).each(function(c_Index, c_Item){
            if(c_Item.url == p_Url)
            {
                v_FavoritesList.splice(c_Index, 1);
                v_IsFavorite = true;
                return false;
            }
        });
        if(!v_IsFavorite)
            v_FavoritesList.push({url: p_Url,name:p_Name,img:p_Img,description:p_Description});
        
        localStorage.setItem("favoritesList",JSON.stringify(v_FavoritesList));
        updateFavoriteList();
    }

    function isFavorite(p_Url)
    {
        var v_IsFavorite = false;
        var v_FavoritesList = JSON.parse(localStorage.getItem("favoritesList")) || [];
        $(v_FavoritesList).each(function(c_Index, c_Item){
            if(c_Item.url == p_Url)
            {
                v_IsFavorite = true;
                return false;
            }
        });
        return v_IsFavorite;
    }

    $(document).ready(function()
    {
        updateFavoriteList();

        @if(Session::has('message'))
            showNotify('info', '{{Session::get('message')}}');
        @endif
        @if(Session::has('error_message'))
            showNotify('danger', '{{Session::get('error_message')}}');
        @endif
        @foreach($errors->all() as $error)
            showNotify('danger', '{{ $error }}');
        @endforeach
    });

    function updateFavoriteList()
    {
        var v_FavoritesList = JSON.parse(localStorage.getItem("favoritesList")) || [];
        v_FavoritesList = v_FavoritesList.slice(Math.max(v_FavoritesList.length - 4, 0));

        var v_FavoritesString = '';
        $(v_FavoritesList).each(function(c_Index, c_Item){
            v_FavoritesString += '<li>' +
                                    '<a href="' + c_Item.url + '">' +
                                        c_Item.name +
                                    '</a>' +
                                '</li>';
        });
        $('.favorites-list').html(v_FavoritesString);
        $('#toggleFavorite span:last').html((isFavorite(location.href) ? '{{trans("menu.remove_favorite")}}' : '{{trans("menu.add_favorite")}}'));
    }

      // Nao deve ser usado 0o UA-24127191-2
    // o correto e o UA-69793661-1

  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'REPLACEME, 'auto');
  ga('send', 'pageview');


    $(function(){   
        var nav = $('.menu-resumido-desk');   
        $(window).scroll(function () { 
            if ($(this).scrollTop() > 155) { 
                nav.addClass("menu-fixo"); 
            } else { 
                nav.removeClass("menu-fixo"); 
            } 
        });  
    });

    window.onload = function() {
        if(location.href == '{{url($p_Language)}}'){
            // Video
            var video = document.getElementById("video");

            // Buttons
            var playButton = document.getElementById("play-pause");
            var playButtondois = document.getElementById("play-pause-dois");
            var muteButton = document.getElementById("mute");
            var fullScreenButton = document.getElementById("full-screen");

            // Sliders
            var seekBar = document.getElementById("seek-bar");
            var volumeBar = document.getElementById("volume-bar");


            // Event listener for the play/pause button
            playButton.addEventListener("click", function() {
                if (video.paused == true) {
                    // Play the video
                    video.play();
                    playButtondois.innerHTML = "Pause";
                    // Update the button text to 'Pause'
                   // playButton.innerHTML = "Pause";
                } else {
                    // Pause the video
                    video.pause();

                    // Update the button text to 'Play'
                    //playButton.innerHTML = "Play";
                }
            });

            // Event listener for the play/pause button
            playButtondois.addEventListener("click", function() {
                if (video.paused == true) {
                    // Play the video
                    video.play();

                    // Update the button text to 'Pause'
                    playButtondois.innerHTML = "Pause";
                } else {
                    // Pause the video
                    video.pause();
                    $('.btn_video').addClass("show");
                     $('#video-controls').removeClass("show");
                    // Update the button text to 'Play'
                    playButtondois.innerHTML = "Play";
                }
            });


            // Event listener for the mute button
            muteButton.addEventListener("click", function() {
                if (video.muted == false) {
                    // Mute the video
                    video.muted = true;

                    // Update the button text
                    muteButton.innerHTML = "Unmute";
                } else {
                    // Unmute the video
                    video.muted = false;

                    // Update the button text
                    muteButton.innerHTML = "Mute";
                }
            });


            // Event listener for the full-screen button
            fullScreenButton.addEventListener("click", function() {
                if (video.requestFullscreen) {
                    video.requestFullscreen();
                } else if (video.mozRequestFullScreen) {
                    video.mozRequestFullScreen(); // Firefox
                } else if (video.webkitRequestFullscreen) {
                    video.webkitRequestFullscreen(); // Chrome and Safari
                }
            });


            // Event listener for the seek bar
            seekBar.addEventListener("change", function() {
                // Calculate the new time
                var time = video.duration * (seekBar.value / 100);

                // Update the video time
                video.currentTime = time;
            });


            // Update the seek bar as the video plays
            video.addEventListener("timeupdate", function() {
                // Calculate the slider value
                var value = (100 / video.duration) * video.currentTime;

                // Update the slider value
                seekBar.value = value;
            });

            // Pause the video when the seek handle is being dragged
            seekBar.addEventListener("mousedown", function() {
                video.pause();
            });

            // Play the video when the seek handle is dropped
            seekBar.addEventListener("mouseup", function() {
                video.play();
            });

            // Event listener for the volume bar
            volumeBar.addEventListener("change", function() {
                // Update the video volume
                video.volume = volumeBar.value;
            });
        }
    }
</script>