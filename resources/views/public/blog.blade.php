@extends('public.base')
@section('pageCSS')
    <style>
        a .at-icon-wrapper {
            cursor: pointer !important;
            border-radius: 50% !important;
        }

        .paginatioBlog .pagination>li>a{
            color: #a7a7a7;
            border: none;
        }
        .paginatioBlog .pagination>li.active>a{
            color: #ef662f;
            background: transparent;
            border: none;
        }
        .pagination>li>a:focus, .pagination>li>a:hover, .pagination>li>span:focus, .pagination>li>span:hover{
            color: #ef662f;
            background: transparent;
            border: none;
        }

        /*gafit code*/
        .hashtags-div a, .hashtags-div a:hover{
            color:#fff;
        }
        /*pagination*/
        #destinos .paginatioBlog .pagination>li>a,
        #destinos .paginatioBlog .pagination>li>span{
            color: #a7a7a7;
            border: none;
        }
        #destinos .paginatioBlog .pagination>li.active>span{
            color: #ef662f;
            background: transparent;
            border: none;
        }
        #destinos .pagination>li>a:focus,
        #destinos .pagination>li>a:hover,
        #destinos .pagination>li>span:focus,
        #destinos .pagination>li>span:hover{
            color: #ef662f;
            background: transparent;
            border: none;
        }
        #destinos .paginatioBlog .pagination>li.disabled>span{
            color: #a7a7a7;
        }
    </style>
@stop
@section('main-content')

 <div class="row-fluid margin-titlemenu" id="useful-information">
        <div class="container">
            <div class="col-lg-12" id="list-title">
                <h2 class="laranja" style="font-size: 5vw;">Blog</h2>
            </div>
        </div>
    </div>
<div class="row-fluid" id="destinos">
    <div class="container no-padding">
        
        <div class="col-lg-9 col-md-8">
            
            <div class="row-fluid" >
                <div class="container no-padding">
                    @if(count($p_Articles) > 1)
                        @foreach($p_Articles as $c_Article)
                        <div class="col-lg-12" id="listaPostagens">
                            
                                <p class="text-center" style="color:#67676c;">
                                    @foreach($c_Article->hashtags as $c_Hashtag)
                                        #{{$c_Hashtag}}
                                    @endforeach
                                </p>
                            <h2 style="color:#67676c;">{{json_decode($c_Article->titulo,1)[$p_Language]}}</h2>
                            
                            <div class="col-lg-12 no-padding" id="imagePost">
                                <img src="{{$c_Article->foto_capa_url}}" alt="{{json_decode($c_Article->titulo,1)[$p_Language]}}">
                            </div>
                            
                            <div class="col-lg-12 no-padding" id="callPost" style="padding-top:1rem;">
                                <div class="col-lg-6 col-xs-5 no-padding">
                                    <p style="padding-top:0.5rem;"><span id="datePost">{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $c_Article->created_at)->format('d/m/Y')}}</span></p>
                                </div>
                                <div class="col-lg-6 col-xs-7 no-padding social">
                                    <ul id="icons-social" style="float:right;">
                                        <div class="addthis_sharing_toolbox" data-url="{{url($p_Language . '/blog/artigo/' . $c_Article->slug)}}"></div>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-lg-12 no-padding" id="callPost" style="margin-top:2rem;">
                                <p id="textpost">{{json_decode($c_Article->descricao_curta,1)[$p_Language]}}</p>
                            </div>
                            <div class="col-lg-12" id="call-to-action">
                                <a href="{{url($p_Language . '/blog/artigo/' . $c_Article->slug)}}" style="width: 30%;display:block;margin-left:auto;margin-right:auto;">{{trans('institutional.read_more')}}</a>
                            </div>
                            <hr class="separa-blog">
                        </div>
                        @endforeach
                        <div class="col-lg-12">
                            <nav class="paginatioBlog" style="display: flex;">
                                {!! $p_Articles->render() !!}
                            </nav>
                        </div>
                    @endif
                </div>
            </div>

        </div>

        <div class="col-lg-3 col-md-4">
            <div class="col-lg-12 no-padding">
                {!! Form::open(array('method' => 'get', 'url'=> url($p_Language . '/blog/busca'))) !!}
                <div class="input-group" style="margin-top:1rem;">
                    <input name="q" type="text" class="form-control" placeholder="{{trans('blog.search_content')}}" style="padding: 9.5px;border-radius: 0;border:none;background:#e6e6e6;" required>
                    <span class="input-group-btn" style="width: 13%;">
                        <button class="btn btn-secondary" type="button" style="padding: 9.5px;border-radius: 0;border:none;background:#e6e6e6;">
                            <img src="{{url('/portal/assets/imgs/lupaOrange.png')}}" alt="">
                        </button>
                    </span>
                </div>
                {!! Form::close() !!}
            </div>


            @if(count($p_Hashtags) > 0 )
                <div class="col-lg-12 no-padding" style="margin-top: 5rem;">
                    <h2 class="laranja" style="padding: 0;">Tags</h2>
                </div>

                <div class="col-lg-12 no-padding hashtags-div text-center" style="margin-top:2rem;background:#db775f;padding:1rem;">
                    <p>
                        @foreach($p_Hashtags as $c_Hashtag)
                            <a href="{{url($p_Language . '/blog/busca?hashtag=' . $c_Hashtag)}}">
                            #{{$c_Hashtag}}
                            </a>
                        @endforeach
                    </p>
                </div>
            @endif


            <div class="col-lg-12 no-padding" style="margin-top: 5rem;">
                <h2 style="padding: 0;color:#bababa;font-size:42px;">{{trans('blog.archive')}}</h2>
            </div>

            <div class="col-lg-12 calendar">
                @foreach($p_ArchiveYears as $c_Year)
                <div class="content-data">
                    <div class="col-lg-12 list-years" data-ano="{{$c_Year}}">
                        <p>{{$c_Year}}</p>
                    </div>
                    <div class="col-lg-12 list-months" id= "list-months-{{$c_Year}}">
                        <ul>
                            <li>
                                <ul class="children">
                                    @foreach($p_ArchiveMonths as $c_Month)
                                        @if($c_Month->year == $c_Year)
                                            <li><a title="{{$c_Month->month_year}}" href="{{url($p_Language . '/blog/busca?arquivo=' . $c_Year . '-' . $c_Month->month)}}">{{$c_Month->month}}</a></li>
                                        @endif
                                    @endforeach
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
@stop

@section('pageScript')
<!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-56a0debd79357d1c" async="async"></script>
@stop