@extends('public.base')
@section('pageCSS')
    <link href="{{url('/vendor/select2/select2.min.css')}}" rel="stylesheet" />
    <style>
        .select2-container--default .select2-selection--single {
            background: #e6e6e6;
            border-radius: 0;
            border: none;
            text-align: center;
            color: #818181;
            font-size: 1rem;
            line-height: 1.5;
        }
        .select2-container--default .select2-selection--single .select2-selection__rendered{
            line-height: 44px;
        }
        .select2-container--default .select2-selection--single, .select2-container--default .select2-selection--single .select2-selection__arrow{
            min-height: 44px;
        }
        #destinos #columns{
            height: auto;
        }
        #main-video{
            text-align: center;
            margin-bottom: 15px;
        }
    </style>
@stop
@section('main-content')
    <div class="row-fluid" id="destinos" style="margin-top: 180px;">
        <div class="container">
            <div class="col-lg-12" id="list-title">
                <h2>{{trans('menu.video_gallery')}}</h2>
            </div>
        </div>
    </div>
    <div class="row-fluid" id="destinos" style="padding:1% 0;">
        <div class="container">
            <div class="col-lg-12">
                @if(count($p_Videos) > 0)
                    <div id="main-video">
                    </div>
                @endif
            </div>
        </div>

        <div class="col-lg-12" id="proximidades" style="padding-top:1rem;">
            <div class="container">
                <div class="col-lg-12">
                    <div class="textGallery">
                        <p id="titulo" style="text-align:center;"></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="container">
                <div class="col-lg-8 col-lg-offset-2">
                    <div class="col-lg-12" id="columns">
                        <p id="descricao">
                        </p>
                    </div>
                </div>
            </div>
        </div>

    </div>

    @if(count($p_Videos) > 1)
        <div class="row-fluid" id="useful-information">
            <div class="container">
                <hr style="background:#4b4b4b;height:1px;width:100%;">
                <div class="col-lg-12">
                    <h2 style="color:#4b4b4b;">Mais videos</h2>
                </div>
            </div>
        </div>
        <!-- FRAGMENTO ATUALIZADO -->
        <div class="row-fluid" id="destinos" style="padding:1% 0;">
            <div class="container">
                <div class="col-lg-12 line">
                    <div id="video-gallery">
                    </div>
                </div>
            </div>
        </div>
    @endif
@stop

@section('pageScript')
    <script>
        var MAX_VIDEO_WIDTH = 800;

        var v_Videos = {!! json_encode($p_Videos) !!};
        shuffle(v_Videos);

        $(document).ready(function()
        {
            var v_UrlBase = 'https://www.googleapis.com/youtube/v3/videos?key=AIzaSyBLc_8beOK5wVSM2NeSk1Bl4mLwtq6jtGw&part=snippet&id=';
            $(v_Videos).each(function(c_Index){
                var v_VideoId = this.value.split('embed/')[1];
                var v_VideoData = JSON.parse(localStorage.getItem("videoData-" + v_VideoId));

                if(v_VideoId.length > 0 && v_VideoData == null){
                    var v_Url = v_UrlBase + v_VideoId;

                    $.ajax({
                        url: v_Url,
                        dataType: "jsonp",
                        success: function (p_VideoData) {
                            if(p_VideoData != undefined && p_VideoData.pageInfo.totalResults == 1)
                            {
                                fillVideoInfo(p_VideoData, c_Index);
                                localStorage.setItem('videoData-' + v_VideoId, JSON.stringify(p_VideoData));
                            }
                            else{

                            }
                        },
                        error: function(){
                        }
                    });
                }
                else{
                    fillVideoInfo(v_VideoData, c_Index);
                }
            });

            $(window).resize(function(){
                var newWidth = $('#main-video').width();
                if(newWidth > MAX_VIDEO_WIDTH)
                    newWidth = MAX_VIDEO_WIDTH;

                var v_iframe = $('iframe');
                v_iframe
                    .width(newWidth)
                    .height(newWidth * v_iframe.data('aspectRatio'));
            });

        });

        function fillVideoInfo(p_VideoData, p_Index){
            v_Videos[p_Index].title = p_VideoData.items[0].snippet.title;
            v_Videos[p_Index].description = p_VideoData.items[0].snippet.description;
            v_Videos[p_Index].thumbnailUrl = p_VideoData.items[0].snippet.thumbnails.medium.url;

            addToGallery(p_Index);
        }

        function setMainVideo(p_VideoInfoIndex){
            v_VideoInfo = v_Videos[p_VideoInfoIndex];
            $('#main-video').html('<iframe src="' + v_VideoInfo.value + '" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>');
            $('.gallery-card').show();
            $('#gallery-card-' + p_VideoInfoIndex).hide();
            $('#titulo').html(v_VideoInfo.title);
            $('#descricao').html(v_VideoInfo.description);

            var v_iframe = $('iframe');
            v_iframe.hide();
            v_iframe
                .data('aspectRatio', v_iframe.height() / v_iframe.width())
                .removeAttr('height')
                .removeAttr('width');
            v_iframe.load(function(){
                $(window).resize();
                v_iframe.show();
            });
        }

        function addToGallery(p_Index){
            v_VideoInfo = v_Videos[p_Index];
            $('#video-gallery').append(
                    '<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">'+
                    '<div onclick="setMainVideo(' + p_Index + ')">'+
                    '<div class="hoverzoom">'+
                    '<div class="thumbs-mini-four">'+
                    '<img src="' + v_VideoInfo.thumbnailUrl + '">'+
                    '</div>'+
                    '<div class="retina-hover">'+
                    '<div class="col-lg-12 title">'+
                    '<p>' + v_VideoInfo.title + '</p>'+
                    '</div>'+
                    '<div class="col-lg-12 no-padding">'+
                    '<hr>'+
                    '</div>'+
                    '<div class="col-lg-12 text">'+
                    '<p>' + v_VideoInfo.description + '</p>'+
                    '</div>'+
                    '</div>'+
                    '<div class="retina">'+
                    '<p>' + v_VideoInfo.title + '</p>'+
                    '</div>'+
                    '</div>'+
                    '</div>'+
                    '</div>'
            );

            if(p_Index == 0){
                setMainVideo(p_Index);
            }
        }

        function shuffle(sourceArray) {
            for (var n = 0; n < sourceArray.length - 1; n++) {
                var k = n + Math.floor(Math.random() * (sourceArray.length - n));

                var temp = sourceArray[k];
                sourceArray[k] = sourceArray[n];
                sourceArray[n] = temp;
            }
        }

    </script>



@stop