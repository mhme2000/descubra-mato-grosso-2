@extends('public.base')

@section('pageCSS')
    <link href="{{url('/vendor/select2/select2.min.css')}}" rel="stylesheet" />
    <style>
        .column-3{
            -webkit-column-count: 3; /* Chrome, Safari, Opera */
            -moz-column-count: 3; /* Firefox */
            column-count: 3;
            -webkit-column-gap: 40px; /* Chrome, Safari, Opera */
            -moz-column-gap: 40px; /* Firefox */
            column-gap: 40px;
            margin-bottom: 25px;
        }
        .select2-container--default .select2-selection--single {
            background: #e6e6e6;
            border-radius: 0;
            border: none;
            text-align: center;
            color: #818181;
            font-size: 1rem;
            line-height: 1.5;
        }
        .prediction-time .select2-container--default .select2-selection--single {
            background: #fff;
            color: #666;
        }
        .how-to-get .select2-container--default .select2-selection--single {
            background: url({{url('/portal/assets/imgs/chegada.png')}}) no-repeat 5% 50% #fff;
        }
        .select2-container--default .select2-selection--single .select2-selection__rendered{
            line-height: 38px;
        }
        .select2-container--default .select2-selection--single, .select2-container--default .select2-selection--single .select2-selection__arrow{
            min-height: 38px;
        }
        #destinos #proximidades .prediction-time .card .tempo:before {
            left: 50%;
            transform: translateX(-50%);
        }
        #destinos #proximidades .prediction-time .card .temperatura .list-temp .date {
            color: #ffffff;
            text-align: center;
            font-size: 14px;
        }
        #destinos #proximidades .prediction-time .card .temperatura .list-temp .grau {
             color: #ffffff;
             text-align: center;
             margin: 0;
         }
        .weather-city-image, #destinos #proximidades .how-to-get .content-get {
            min-height:356px;
        }
        #destinos #proximidades .prediction-time .card .temperatura .list-temp .small-temp-icon {
            max-height:26px;
        }
    </style>
@stop

@section('main-content')
    <div class="row-fluid" id="useful-information" style="margin-top:160px;">
        <div class="container">
            <div class="col-lg-12">
                <h2>{{trans('menu.plan_your_trip')}}</h2>
            </div>
        </div>
    </div>
    
    <div class="row-fluid" id="destinos">
        <div class="container">
            <div class="col-lg-12">
                <div class="col-lg-8 col-lg-offset-2">
                    <div class="col-lg-12">
                        <p>{!! array_key_exists('planeje-sua-viagem-descricao_' . $p_Language, $p_Content) ? nl2br($p_Content['planeje-sua-viagem-descricao_' . $p_Language]) : '' !!}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row-fluid" id="planeja">
        <div class="container no-padding">
            <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12 category">
                <a href="{{url($p_Language . '/roteiros-duracao?d=1')}}">
                    <div class="card card-inverse">
                        <div class="card-img-overlay overlay-full">
                            <div class="col-lg-12 no-padding" id="title-overlay">
                                <p><small>{{trans('menu.routes_duration_1_pt1')}}</small><br>{{trans('menu.routes_duration_1_pt2')}}</p>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12 category">
                <a href="{{url($p_Language . '/roteiros-duracao?d=2')}}">
                    <div class="card card-inverse">
                        <div class="card-img-overlay overlay-full">
                            <div class="col-lg-12 no-padding" id="title-overlay">
                                <p><small>{{trans('menu.routes_duration_2_pt1')}}</small><br>{{trans('menu.routes_duration_2_pt2')}}</p>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12 category">
                <a href="{{url($p_Language . '/roteiros-duracao?d=3')}}">
                    <div class="card card-inverse">
                        <div class="card-img-overlay overlay-full">
                            <div class="col-lg-12 no-padding" id="title-overlay">
                                <p><small>{{trans('menu.routes_duration_3_pt1')}}</small><br>{{trans('menu.routes_duration_3_pt2')}}</p>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>

    <div class="row-fluid" id="destinos" style="padding:1% 0;">
        <div class="container">
            <div class="col-lg-12 nav-tabs" style="padding:2% 0;">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" href="#hotel" role="tab" data-toggle="tab">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 hospedagem">
                                <p><img src="{{url('/portal/assets/imgs/camaOrange.gif')}}" alt="">
                                   <span class="hidden-sm-down">{{trans('destination.accomodation')}}</span>
                                </p>
                            </div>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#alimentacao" role="tab" data-toggle="tab">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 hospedagem">
                                <p><img src="{{url('/portal/assets/imgs/talherOrange.gif')}}" alt="">
                                   <span class="hidden-sm-down"> {{trans('destination.feeding')}}</span>
                                </p>
                            </div>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#servico" role="tab" data-toggle="tab">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 hospedagem">
                                <p><img src="{{url('/portal/assets/imgs/malaOrange.gif')}}" alt="">
                                   <span class="hidden-sm-down"> {{trans('destination.services')}}</span>
                                </p>
                            </div>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#lazer" role="tab" data-toggle="tab">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 hospedagem">
                                <p><img src="{{url('/portal/assets/imgs/coposOrange.gif')}}" alt="">
                                   <span class="hidden-sm-down"> {{trans('destination.recreation')}}</span>
                                </p>
                            </div>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- Tab panes -->
                <div class="tab-content">

            <!-- FRAGMENTO ATUALIZADO 18/03/2016-->

            <!-- Hotels -->
            <div role="tabpanel" class="tab-pane active" id="hotel">
                <div class="col-lg-12 box-hospedagem" id="tarja">
                    <div class="container no-padding">
                        <div class="col-lg-12 no-padding">
                            <div class="row" id="options">
                                {!! Form::open(array('method' => 'get', 'class' => 'destinationSupport', 'onsubmit' => 'return submitTradeForm(this)')) !!}
                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 ">
                                        <p>{{trans('destination.find_accomodation')}}</p>
                                    </div>
                                    <div class="col-lg-3 col-md-5 col-sm-12 col-xs-12" id="inputDestino">
                                        {!! Form::select('', ["" => trans('planYourTrip.select_destination')] + $p_DestinationList, null, ['class' => 'selectCities form-control select2', 'required' => 'required', 'style' => 'width:100%;padding: 9.5px;border-radius: 0;border:none;background:#e6e6e6;']) !!}
                                    </div>
                                    <?php
                                        $v_AccomodationList = [
                                            '' => trans('destination.accomodation_type'),
                                            '43' => trans('destination.accomodation_flat'),
                                            '51' => trans('destination.accomodation_camping'),
                                            '52' => trans('destination.accomodation_summer_camp'),
                                            '38,39' => trans('destination.accomodation_hotel'),
                                            '40,42' => trans('destination.accomodation_farm_hotel'),
                                            '53' => trans('destination.accomodation_hostel'),
                                            '41' => trans('destination.accomodation_inn'),
                                            '47,48,54' => trans('destination.others')
                                        ];
                                    ?>
                                    <div class="col-lg-3 col-md-5 col-sm-12 col-xs-12 selectoptions">
                                        {!! Form::select('tipo_acomodacao', $v_AccomodationList, null, ['id' => 'tipo_acomodacao', 'required' => 'required']) !!}
                                    </div>
                                    <div class="col-lg-1 col-md-2 col-sm-12 col-xs-12 " id="submit-tarja">
                                        <input class="btn" type="submit" value="{{trans('whereToGo.search')}}">
                                    </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Alimentação -->
            <div role="tabpanel" class="tab-pane" id="alimentacao">
                <div class="col-lg-12 box-restaurante" id="tarja">
                    <div class="container no-padding">
                        <div class="col-lg-12 no-padding">
                            <div class="row" id="options">
                                {!! Form::open(array('method' => 'get', 'class' => 'destinationSupport', 'onsubmit' => 'return submitTradeForm(this)')) !!}
                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 ">
                                        <p>{{trans('destination.find_feeding')}}</p>
                                    </div>
                                    <div class="col-lg-3 col-md-5 col-sm-12 col-xs-12" id="inputDestino">
                                        {!! Form::select('', ["" => trans('planYourTrip.select_destination')] + $p_DestinationList, null, ['class' => 'selectCities form-control select2', 'required' => 'required', 'style' => 'width:100%;padding: 9.5px;border-radius: 0;border:none;background:#e6e6e6;']) !!}
                                    </div>
                                    <?php
                                        $v_FoodDrinkList = [
                                            '' => trans('destination.feeding_type'),
                                            '55' => trans('destination.feeding_restaurants'),
                                            '56,59,61' => trans('destination.feeding_bars'),
                                            '57' => trans('destination.feeding_tea_house'),
                                            '58' => trans('destination.feeding_breweries'),
                                            '60' => trans('destination.feeding_ice_cream_shops'),
                                            '62' => trans('destination.others')
                                        ];
                                    ?>
                                    <div class="col-lg-3  col-md-5 col-sm-12 col-xs-12 selectoptions">
                                        {!! Form::select('tipo_estabelecimento', $v_FoodDrinkList, null, ['id' => 'tipo_estabelecimento', 'required' => 'required']) !!}
                                    </div>
                                    <div class="col-lg-1 col-md-2 col-sm-12 col-xs-12 " id="submit-tarja">
                                        <input class="btn" type="submit" value="{{trans('whereToGo.search')}}">
                                    </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Serviços -->
            <div role="tabpanel" class="tab-pane" id="servico">
                <div class="col-lg-12 box-passeio" id="tarja">
                    <div class="container no-padding">
                        <div class="col-lg-12 no-padding">
                            <div class="row" id="options">
                                {!! Form::open(array('method' => 'get', 'class' => 'destinationSupport', 'onsubmit' => 'return submitTradeForm(this)')) !!}
                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 ">
                                        <p>{{trans('destination.find_service')}}</p>
                                    </div>
                                    <div class="col-lg-3  col-md-5 col-sm-12 col-xs-12" id="inputDestino">
                                        {!! Form::select('', ["" => trans('planYourTrip.select_destination')] + $p_DestinationList, null, ['class' => 'selectCities form-control select2', 'required' => 'required', 'style' => 'width:100%;padding: 9.5px;border-radius: 0;border:none;background:#e6e6e6;']) !!}
                                    </div>
                                    <?php
                                        $v_ServiceList = [
                                            '' => trans('destination.service_type'),
                                            '64,256' => trans('destination.services_agencies'),
                                            '66' => trans('destination.services_rental_agencies')
                                        ];
                                    ?>
                                    <div class="col-lg-3 col-md-5 col-sm-12 col-xs-12 selectoptions">
                                        {!! Form::select('tipo_servico', $v_ServiceList, null, ['id' => 'tipo_servico', 'required' => 'required']) !!}
                                    </div>
                                    <div class="col-lg-1 col-md-2 col-sm-12 col-xs-12 " id="submit-tarja">
                                        <input class="btn" type="submit" value="{{trans('whereToGo.search')}}">
                                    </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Lazer -->
            <div role="tabpanel" class="tab-pane" id="lazer">
                <div class="col-lg-12 box-lazer" id="tarja">
                    <div class="container no-padding">
                        <div class="col-lg-12 no-padding">
                            <div class="row" id="options">
                                {!! Form::open(array('method' => 'get', 'class' => 'destinationSupport', 'onsubmit' => 'return submitTradeForm(this)')) !!}
                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 ">
                                        <p>{{trans('destination.find_recreation')}}</p>
                                    </div>
                                    <div class="col-lg-3 col-md-5 col-sm-12 col-xs-12" id="inputDestino">
                                        {!! Form::select('', ["" => trans('planYourTrip.select_destination')] + $p_DestinationList, null, ['class' => 'selectCities form-control select2', 'required' => 'required', 'style' => 'width:100%;padding: 9.5px;border-radius: 0;border:none;background:#e6e6e6;']) !!}
                                    </div>
                                    <?php
                                        $v_RecreationList = [
                                            '' => trans('destination.recreation_type'),
                                            '83,85' => trans('destination.recreation_nightclubs'),
                                            '84' => trans('destination.recreation_shows'),
                                            '86' => trans('destination.recreation_theater'),
                                            '77,78,79,87' => trans('destination.recreation_sports'),
                                            '76' => trans('destination.recreation_clubs'),
                                            '74' => trans('destination.recreation_parks'),
                                            '80,81,88,89' => trans('destination.recreation_others')
                                        ];
                                    ?>
                                    <div class="col-lg-3 col-md-5 col-sm-12 col-xs-12 selectoptions">
                                        {!! Form::select('tipo_lazer', $v_RecreationList, null, ['id' => 'tipo_lazer', 'required' => 'required']) !!}
                                    </div>
                                    <div class="col-lg-1 col-md-2 col-sm-12 col-xs-12 " id="submit-tarja">
                                        <input class="btn" type="submit" value="{{trans('whereToGo.search')}}">
                                    </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- FRAGMENTO ATUALIZADO 18/03/2016-->
        </div>
    </div>

    <div class="row-fluid" id="destinos" style="padding:1% 0;">
        <div class="col-lg-12 no-padding" id="proximidades" style="margin-top:3%;">
            <div class="container" id="tempo">
                <div class="col-lg-6 prediction-time">
                    <h4><img src="{{url('/portal/assets/imgs/termometro.png')}}" alt="">{{trans('planYourTrip.weather_forecast')}}</h4>
                    <div class="card card-inverse" style="border-radius:0;border:none;">
                        <img class="card-img weather-city-image" src="" alt="Card image">
                        <div class="card-img-overlay overlay-full input">
                            <div class="col-lg-8 col-lg-offset-2" id="title-overlay-time">
                                <div class="form-group">
                                    {!! Form::select('previsao_destino', [], null, ['id' => 'previsaoDestino', 'class' => 'form-control select2', 'required' => 'required', 'style' => 'width: 100%']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="card-img-overlay overlay-full tempo">
                            <div class="box-temp">
                                <img class="temp-icon" alt="" style="margin-left:auto;margin-right:auto;display:block;">
                                <p id="ceu"></p>
                                <p id="temp" class="grau">{{trans('planYourTrip.select_destination')}}</p>
                                <p class="date" style="display: none;"></p>
                                <p id="nameCity"></p>
                            </div>
                        </div>
                        <div class="card-img-overlay overlay-full temperatura">
                            <div class="col-lg-12">
                                <div class="col-lg-2 list-temp">
                                    <img class="temp-icon small-temp-icon" alt="">
                                    <p class="grau"></p>
                                    <p class="date"></p>
                                </div>
                                <div class="col-lg-2 list-temp">
                                    <img class="temp-icon small-temp-icon" alt="">
                                    <p class="grau"></p>
                                    <p class="date"></p>
                                </div>
                                <div class="col-lg-2 list-temp">
                                    <img class="temp-icon small-temp-icon" alt="">
                                    <p class="grau"></p>
                                    <p class="date"></p>
                                </div>
                                <div class="col-lg-2 list-temp">
                                    <img class="temp-icon small-temp-icon" alt="">
                                    <p class="grau"></p>
                                    <p class="date"></p>
                                </div>
                                <div class="col-lg-2 list-temp">
                                    <img class="temp-icon small-temp-icon" alt="">
                                    <p class="grau"></p>
                                    <p class="date"></p>
                                </div>
                                <div class="col-lg-2 list-temp">
                                    <img class="temp-icon small-temp-icon" alt="">
                                    <p class="grau"></p>
                                    <p class="date"></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6 how-to-get" id="como-chegar">
                    <h4><img src="{{url('/portal/assets/imgs/book.png')}}" alt="">{{trans('planYourTrip.directions')}}</h4>
                    <div class="col-lg-12 content-get">
                        <div class="col-lg-12">
                            <p>{!! array_key_exists('planeje-sua-viagem-como-chegar_' . $p_Language, $p_Content) ? nl2br($p_Content['planeje-sua-viagem-como-chegar_' . $p_Language]) : '' !!}</p>
                        </div>
                        <div class="col-lg-12 directions" id="title-overlay">
                            {!! Form::open(array('id' => 'formContact', 'method' => 'get', 'target' => '_blank', 'onsubmit' => 'return submitDirections()')) !!}
                                <div class="form-group">
                                    <input type="text" class="form-control" id="partida" placeholder="{{trans('planYourTrip.type_origin')}}" required>
                                </div>
                                <div class="form-group">
                                    {!! Form::select('', [], null, ['id' => 'destino', 'class' => 'form-control select2', 'required' => 'required', 'style' => 'width: 100%']) !!}
                                </div>
                                <input type="submit" class="btn" id="buscar" value="{{trans('whereToGo.search')}}">
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if(array_key_exists('planeje-sua-viagem-informacoes_' . $p_Language, $p_Content) && strlen($p_Content['planeje-sua-viagem-informacoes_' . $p_Language]) > 0)
        <div class="row-fluid" id="useful-information">
            <div class="container">
                <div class="col-lg-12">
                    <h2><img src="{{url('/portal/assets/imgs/icon-4.png')}}" alt="" style="margin-right:15px;">{{trans('attraction.useful_information')}}</h2>
                </div>

                <div class="col-xs-12">
                    <div class="column-3">
                        <p>{!! $p_Content['planeje-sua-viagem-informacoes_' . $p_Language] !!}</p>
                    </div>
                </div>
            </div>
        </div>
    @endif
    @if(array_key_exists('planeje-sua-viagem-guia_' . $p_Language, $p_Content) && strlen($p_Content['planeje-sua-viagem-guia_' . $p_Language]) > 0)
        
            <div class="container">
                <div class="col-md-4 col-md-offset-4">
                    <div class="col-md-6 col-md-offset-3">
                        <a href="{{$p_Content['planeje-sua-viagem-guia_' . $p_Language]}}" class="text-center" download>
                            <div class="position">
                                <img src="{{url('/portal/assets/imgs/ballOne.png')}}" alt="Download" style="padding:7% 0;">
                                <p style="color:#ef662f;padding: 1% 0;">{{trans('planYourTrip.tourism_guide')}}</p>
                                <h3 class="laranja">({{trans('planYourTrip.pdf_file')}})</h3>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        
    @endif

@stop

@section('pageScript')
    <script src="{{url('/vendor/select2/select2.min.js')}}" type="text/javascript"></script>
    <script src="{{url('/vendor/select2/i18n/pt-BR.js')}}" type="text/javascript"></script>
    <script>
        var v_Destinations = {!! json_encode($p_Destinations) !!};

        $(document).ready(function()
        {
            var v_DataString = '<option value="">{{trans('planYourTrip.select_destination')}}</option>';
            $.each(v_Destinations, function (c_Key, c_Destination)
            {
                v_DataString += '<option value="' + c_Key + '">' + c_Destination.nome + '</option>';
            });

            $('.select2').html(v_DataString).select2(
                    @if($p_Language == 'pt')
                        {language:'pt-BR'}
                    @endif
            );

            $('.select2slugs').select2(
                @if($p_Language == 'pt')
                    {language:'pt-BR'}
                @endif
            );

            $('#previsaoDestino.select2').on('change', function(){
                if(this.value != '')
                {
                    var v_SelectedDestination = v_Destinations[this.value];
                    updateWeather(v_SelectedDestination.latitude, v_SelectedDestination.longitude, v_SelectedDestination.id);
                    $('#nameCity').text(v_SelectedDestination.nome);
                    $('.weather-city-image').attr('src', v_SelectedDestination.url);
                }
            })
            .val(Math.floor(Math.random() * $('#previsaoDestino.select2 option').length) + 1)
            .trigger('change');
        });

        function updateWeather (p_Latitude, p_Longitude, p_DestinationId)
        {
            if(p_Latitude != null && p_Latitude.length > 0 && p_Longitude != null && p_Longitude.length > 0)
            {
                $('#ceu, .grau, .date').text('');
                $('.temp-icon').attr('src', '');

                var v_Data = JSON.parse(localStorage.getItem("forecastDestination-" + p_DestinationId));
                var v_Today = new Date().toDateString();
                if(v_Data != null && (new Date(v_Data.list[0].dt * 1000)).toDateString() == v_Today)
                    displayForecast(v_Data);
                else
                {
                    if(v_Data != null)
                        localStorage.removeItem("forecastDestination-" + p_DestinationId);
                    $.ajax({
                        url: "http://api.openweathermap.org/data/2.5/forecast/daily?lat="+p_Latitude+"&lon="+p_Longitude+"&cnt=7&units=metric&lang={{$p_Language}}&APPID=674d7946ea8e48336eab93ea712beb46",
                        dataType: "jsonp",
                        success: function (p_WeatherData) {
                            if(p_WeatherData != undefined && p_WeatherData.cod == 200)
                            {
                                displayForecast(p_WeatherData);
                                localStorage.setItem("forecastDestination-" + p_DestinationId, JSON.stringify(p_WeatherData));
                            }
                            else
                                forecastUnavailable();
                        },
                        error: function(){
                            forecastUnavailable();
                        }
                    });
                }
            }
            else
                forecastUnavailable();
        }

        function displayForecast(p_WeatherData)
        {
            $(p_WeatherData.list).each(function(c_Index){
                var v_Temp = this.temp.day;
                v_Temp = (Math.round(v_Temp * 10)/10) + 'º';
                v_Temp = v_Temp.replace('.',',');
                var v_Date = new Date(this.dt*1000);
                v_Date = v_Date.getDate() + '/' + (v_Date.getMonth() + 1);

                if(c_Index == 0)
                    $('#ceu').text(this.weather[0].description);
                $('.grau:eq('+c_Index+')').text(v_Temp);
                $('.date:eq('+c_Index+')').text(v_Date);
                $('.temp-icon:eq('+c_Index+')').attr('src', "{{url('/portal/assets/imgs/clima/')}}/"  + this.weather[0].icon + '.png');
            });
        }

        function forecastUnavailable()
        {
            $('#ceu').text('');
            $('.grau').text('');
            $('.grau:eq(0)').text('{!! trans('planYourTrip.forecast_unavailable') !!}');
            $('.date').text('');
            $('.temp-icon').attr('src', '');
        }

        function submitDirections()
        {
            var v_DestinationString = '';
            var v_SelectedDestination = v_Destinations[$('#destino').val()];
            if(v_SelectedDestination.latitude != null && v_SelectedDestination.latitude.length > 0
                    && v_SelectedDestination.longitude != null && v_SelectedDestination.longitude.length > 0)
                v_DestinationString = v_SelectedDestination.latitude + ',' + v_SelectedDestination.longitude;
            else
                v_DestinationString = v_SelectedDestination.nome.split(' ').join('+') + ',+MG,+Brazil';

            var v_StartingPoint = $('#partida').val().split(' ').join('+');

            document.getElementById('formContact').action = 'https://www.google.com/maps/dir/' + v_StartingPoint + '/' + v_DestinationString;
        }

        function submitTradeForm(p_Form){
            var v_Slug = $(p_Form).find('.selectCities').val();
            $(p_Form).attr('action', '{{url($p_Language . '/apoio-destino')}}/' + v_Slug);
            return true;
        }
    </script>
@stop