@extends('public.base')
@section('pageCSS')
@stop
@section('main-content')
    <?php
        if($p_Content != null)
        {
            $v_CategoryName = json_decode($p_Content->nome,1)[$p_Language];
            $v_CategoryDescription = json_decode($p_Content->descricao,1)[$p_Language];
        }
    ?>
    <div class="row-fluid" id="useful-information">
        <div class="container">
            <div class="col-lg-12" id="list-title">
                <h2>{{$p_Content == null ? '' : $v_CategoryName}}</h2>
            </div>
        </div>
    </div>

    <div class="row-fluid" id="destinos" style="padding:1% 0;">
        <div class="container">
            <div class="col-lg-12">
                <div class="col-lg-8 col-lg-offset-2">
                    <div class="col-lg-12" style="margin-bottom:50px;">
                        <p>{!! $p_Content == null ? '' : nl2br($v_CategoryDescription) !!}</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 line" style="padding:0;">
                @foreach($p_TripTypes as $c_Type)
                    <?php
                        $v_Name = json_decode($c_Type->nome,1)[$p_Language];
                        $v_Description = json_decode($c_Type->descricao_curta,1)[$p_Language];
                    ?>
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <a href="{{url($p_Language . '/o-que-fazer/' . $p_Content->slug . '/' . $c_Type->slug)}}">
                            <div class="hoverzoom">
                                <div class="thumbs-mini-three">
                                    <!--<img src="{{$c_Type->foto_capa_url}}">-->
                                    <div class="thumbs-mini-recorte" style="background: url('{{$c_Type->foto_capa_url}}') no-repeat;"></div>
                                </div>
                                <div class="retina-hover">
                                    <div class="col-lg-12 title">
                                        <p>{{$v_Name}}</p>
                                    </div>
                                    <div class="col-lg-12 no-padding">
                                        <hr>
                                    </div>
                                    <div class="col-lg-12 text">
                                        <p>{{$v_Description}}</p>
                                    </div>
                                </div>
                                <div class="retina">
                                    <p>{{$v_Name}}</p>
                                </div>
                            </div>
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@stop

@section('pageScript')
@stop