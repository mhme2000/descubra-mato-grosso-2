@extends('public.templates.base') 
@section('pageCSS')
<link rel="stylesheet" type="text/css" href="{{url('/portal/assets/libs/portal/css/homeIndex.css')}}">

@endsection
 
@section('header')
        @include('public.templates.header')
@endsection
 
@section('main-content')
<main class="container-fluid">
<!-- Menu Principal Desktop -->
<div class="row d-none d-md-flex">
<div class="col-6 col-md-4">
    <div class="row">
            <div class="col-4 bg-info p-0">
                    <img class="fade-image img-fluid" src="/image/430/430/true/true/{{url('portal/assets/libs/imgs/home_image/430x430/8.png')}}"> 
            </div>
            <div class="col-8 bg-alert p-0">
                <a class="link-no-hover" href="{{url('pt/o-que-fazer/natureza')}}">
                        <img class="fade-image img-fluid" src="/image/860/430/true/true/{{url('portal/assets/libs/imgs/home_image/860x430/2.png')}}">     
                </a>
            </div>
            <div class="col-12 bg-info p-0">
                <a class="link-no-hover" href="{{url('pt/o-que-fazer/natureza/cachoeira')}}">
                    <img class="fade-image img-fluid" src="/image/860/573/true/true/{{url('portal/assets/libs/imgs/home_image/860x573/3.png')}}"> 
                </a>
            </div>            
    </div>
</div>
<div class="d-none d-md-block col-md-4">
        <div class="row">
                <div class="col-12 bg-primary p-0">
                        <a class="link-no-hover" href="{{url('pt/o-que-fazer/gastronomia')}}">
                                <img class="fade-image img-fluid" src="/image/860/573/true/true/{{url('portal/assets/libs/imgs/home_image/860x573/1.png')}}"> 
                        </a>
                </div>
                <div class="col-12 bg-white p-0">
                        <a class="link-no-hover" href="{{url('pt/o-que-fazer/cultura/igreja')}}">
                            <img class="fade-image img-fluid" src="/image/860/287/true/true/{{url('portal/assets/libs/imgs/home_image/860x287/7.png')}}">     
                        </a>
                </div>
        </div>
</div>    
<div class="col-6 col-md-4">
        <div class="row">
                <div class="col-12 bg-light p-0">
                        <a class="link-no-hover" href="{{url('pt/o-que-fazer/natureza/parques')}}">
                                <img class="fade-image img-fluid" src="/image/860/573/true/true/{{url('portal/assets/libs/imgs/home_image/860x573/4.png')}}">
                        </a>
                </div>
                <div class="col-4 bg-secondary p-0">
                        <a class="link-no-hover" href="{{url('pt/o-que-fazer/bem-estar/estancia-hidromineral')}}">
                                <img class="fade-image img-fluid" src="/image/430/430/true/true/{{url('portal/assets/libs/imgs/home_image/430x430/6.png')}}"> 
                        </a>
                </div>
                <div class="col-8 bg-dark p-0">
                        <a class="link-no-hover" href="{{url('pt/o-que-fazer/cultura')}}">
                        <img class="fade-image img-fluid" src="/image/860/430/true/true/{{url('portal/assets/libs/imgs/home_image/860x430/5.png')}}">
                        </a>
                </div>            
        </div>
</div>
</div>
<!-- Fim de Menu Principal Desktop -->
<!-- Menu Principal Mobile -->
<div id="carouselExampleControls" class="carousel slide d-flex d-md-none" data-ride="carousel">
        <div class="carousel-inner">
          <div class="carousel-item active">
            <div class="row">
                <div class="col-6">
                        <img class="d-block w-100" src="/image/860/573/true/true/{{url('portal/assets/libs/imgs/home_image/430x430/8.png')}}" alt="First slide">
                </div>
                <div class="col-6">
                        <a class="link-no-hover" href="{{url('pt/o-que-fazer/natureza')}}">
                        <img class="d-block w-100" src="/image/860/573/true/true/{{url('portal/assets/libs/imgs/home_image/860x430/2.png')}}" alt="First slide">
                        </a>
                </div>    
            </div>  
          </div>
          <div class="carousel-item">
                <div class="row">
                        <div class="col-6">
                                <a class="link-no-hover" href="{{url('pt/o-que-fazer/natureza/cachoeira')}}">
                                <img class="d-block w-100" src="/image/860/573/true/true/{{url('portal/assets/libs/imgs/home_image/860x573/3.png')}}" alt="First slide">
                                </a>
                        </div>
                        <div class="col-6">
                                <a class="link-no-hover" href="{{url('pt/o-que-fazer/gastronomia')}}">
                                <img class="d-block w-100" src="/image/860/573/true/true/{{url('portal/assets/libs/imgs/home_image/860x573/1.png')}}" alt="First slide">
                                </a>
                        </div>    
                </div>  
            </div>
            <div class="carousel-item">
                    <div class="row">
                            <div class="col-6">
                                <a class="link-no-hover" href="{{url('pt/o-que-fazer/cultura/igreja')}}">
                                    <img class="d-block w-100" src="/image/860/573/true/true/{{url('portal/assets/libs/imgs/home_image/860x287/7.png')}}" alt="First slide">
                                </a>
                            </div>
                            <div class="col-6">
                                <a class="link-no-hover" href="{{url('pt/o-que-fazer/natureza/parques')}}">
                                    <img class="d-block w-100" src="/image/860/573/true/true/{{url('portal/assets/libs/imgs/home_image/860x573/4.png')}}" alt="First slide">
                                </a>
                            </div>    
                    </div>  
            </div>
            <div class="carousel-item">
                    <div class="row">
                            <div class="col-6">
                                <a class="link-no-hover" href="{{url('pt/o-que-fazer/bem-estar/estancia-hidromineral')}}">
                                    <img class="d-block w-100" src="/image/860/573/true/true/{{url('portal/assets/libs/imgs/home_image/430x430/6.png')}}" alt="First slide">
                                </a>
                            </div>
                            <div class="col-6">
                                <a class="link-no-hover" href="{{url('pt/o-que-fazer/cultura')}}">
                                    <img class="d-block w-100" src="/image/860/573/true/true/{{url('portal/assets/libs/imgs/home_image/860x430/5.png')}}" alt="First slide">
                                </a>
                            </div>    
                    </div>  
            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
</div>
<!-- Fim de Menu Principal Mobile -->



@if (isset($eventCarousel) && (count($eventCarousel)>0))

<div class="title container pt-3">
                <h1 class="text-center display-5">
                        Eventos em destaques
                </h1>
</div>

<div class="container pt-3">

        <div id="carouselControls" class="carousel slide py-2 px-1 bg-light shadow" data-ride="carousel">
                <div class="carousel-inner">

                        {{--*/ $carouselActive=0 /*--}}
                        {{--*/ $tema=0 /*--}} 
                        @foreach ($eventCarousel as $vEvent)
                        
                        
                {{-- Verifica se o evento tem foto de capa --}}
                @if ($vEvent->Event->EventPhotos(true)->first() != null)      
                           
                            
                        @if ($tema == 3) {{--*/ $tema=0 /*--}} @endif   

                        
                        {{--*/ $carouselColor=$tema /*--}}
                        {{--*/ $tema++ /*--}}
                        

                        <div class="carousel-item @if ($carouselActive == 0) {{'active'}} {{--*/ $carouselActive=1 /*--}} @endif">
                                <div class="row">
                                        <div class="col-md-6 d-flex justify-content-center justify-content-md-end pr-md-0">
                                                <div class="carousel-image-container">
                                                                        <a href="{{url($p_Language.'/eventos/'.$vEvent->slug)}}">
                                                                                <img class="carousel-image" src="/image/430/322/true/true/{{$vEvent->Event->EventPhotos(true)->first()->url}}"> 
                                                                        </a>
                                                </div>
                                        </div>
                                        <div class="col-md-6 d-flex justify-content-center justify-content-md-start pl-md-0">
                                                <div class="carousel-text-area-{{$carouselColor}}">
                                                        <div class="row">
                                                                <div class="col-md-12 p-3">
                                                                        <!--
                                                                                        Depuração
                                                                                        {{$vEvent->Event->id}}
                                                                                        {{getMonthName(2)}}
                                                                                        {{date_format(date_create($vEvent->start), 'm')}}
                                                                                        {{getMonthName(date_format(date_create($vEvent->start), 'm'))}}
                                                                                      -->
                                                                        <div class="text-center text-uppercase font-weight-bold @if($carouselColor != 2){{'title'}}@endif">{{$vEvent->name}}</div>

                                                                        <table class="float-left mt-4 mt-md-5 ml-md-5">
                                                                                <tr>
                                                                                        @if (($vEvent->end != '') && (date_format(date_create($vEvent->start), 'm') != date_format(date_create($vEvent->end), 'm')))

                                                                                        <th class="rounded-top carousel-calendar-month text-center px-1">{{getMonthName(date_format(date_create($vEvent->start),
                                                                                                'm'))}}/{{getMonthName(date_format(date_create($vEvent->end),
                                                                                                'm'))}}
                                                                                        </th>
                                                                                        @else
                                                                                        <th class="rounded-top carousel-calendar-month text-center px-1">{{getMonthName(date_format(date_create($vEvent->start),
                                                                                                'm'))}}
                                                                                        </th>
                                                                                        @endif
                                                                                </tr>
                                                                                <tr>
                                                                                        @if (($vEvent->end != '') && (date_format(date_create($vEvent->start), 'd') != date_format(date_create($vEvent->end), 'd')))
                                                                                        <td class="rounded-bottom border border-0 carousel-calendar-day text-center px-1">{{date_format(date_create($vEvent->start),
                                                                                                'd')}} | {{date_format(date_create($vEvent->end),
                                                                                                'd')}}
                                                                                        </td>
                                                                                        @else
                                                                                        <td class="rounded-bottom border border-0 carousel-calendar-day text-center px-1">{{date_format(date_create($vEvent->start),
                                                                                                'd')}}
                                                                                        </td>
                                                                                        @endif
                                                                                </tr>
                                                                        </table>


                                                                        <div class="text-uppercase mt-4 mt-md-5">
                                                                                <div class="row">
                                                                                        <div class="col-md-12 font-weight-bold"><b>{{$vEvent->city}}</b></div>
                                                                                        <div class="col-md-12"><small><small>{{$vEvent->events_place}}</small></small></div>
                                                                                </div>
                                                                        </div>
                                                                        <div class="text-center mt-3 mt-md-4 d-none d-md-flex mx-3"><small>{{json_decode($vEvent->short_description,
                                                                                true)[$p_Language]}}</small>
                                                                        </div>
                                                                        <div class="text-right text-md-center pt-3"> <a href="{{url($p_Language.'/eventos/'.$vEvent->slug)}}"
                                                                                        class="btn btn-dark">Detalhes</a></div>

                                                                </div>

                                                        </div>
                                                </div>
                                        </div>
                                </div>
                        </div>

                        @endif
                        @endforeach

                </div>

                <a class="carousel-control-prev" href="#carouselControls" role="button" data-slide="prev">
                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                  <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselControls" role="button" data-slide="next">
                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                  <span class="sr-only">Next</span>
                </a>
        </div>

</div>

@endif 

@if (isset($destinations) && (count($destinations)==6)) {{-- Verifica Passagem de Destinos surpreendentes --}}

<div class="title container pt-3">
                <h1 class="text-center display-5">
                        Destinos Surpreendentes
                </h1>
</div>

<div class="container">

        {{-- faz regra para exibir somente 3 destinos caso esteja na versão mobile --}}
        <div class="row">
                {{--*/ $countDestinations=0 /*--}}
                @foreach ($destinations as $destination)
                
                <div class="col-md-4 @if($countDestinations >2) {{"d-none d-md-flex"}} @endif">
                        <a class="link-no-hover" href="{{url($p_Language.'/destinos/'.$destination->slug)}}">
                                <img class="img-fluid fade-image" src="/image/860/573/true/true/{{$destination->url}}" alt="">
                                <p class="text-center overlap-title-photos"> <span class="text-light">{{$destination->nome}}<span> </p>
                        </a>
                </div>
                {{--*/ $countDestinations++ /*--}}
                @endforeach
        </div>

</div>

<div class="container text-center">
                <a href="{{url('/pt/destinos')}}" class="btn btn-page">Conheça mais destinos</a>
</div>

@endif {{-- Fim Verifica Passagem de Destinos surpreendentes --}}

@if (isset($v_InstagramPictures)){{-- Fim Verifica Passagem de Imagens do Instagram --}}

<div class="title container pt-3">
        <div class="row">
                <div class="col-md-8">
                        <h1 class="text-left display-5 title">
                                #turismoMG {{"@VisiteMinasGerais"}}
                        </h1>
                </div>
                <div class="col-md-4 text-right">
                        <p>Use a hashtag #TurismoMG em suas fotos e compartilhe a sua história com a gente! @VisiteMinasGerais.</p>
                </div>
        </div>
</div>

        <div class="container">
                <div class="row">
                {{--*/ $countPictures=0 /*--}}
                @foreach ($v_InstagramPictures as $pictures) {{-- --}}
                        @if ($countPictures < 12 ) {{-- Verifica se foi colocado 11 imagens --}}
                                <div class="@if($countPictures > 2 && $countPictures < 9) d-none d-md-flex col-md-2 @else col-6 col-md-4 @endif">
                                        <a class="" href="{{$pictures->link}}" target="_blank">
                                            <img class="img-fluid rounded m-1 fade-image" src="http://minasgerais.com.br/image/640/640/true/true/{{$pictures->images->standard_resolution->url}}">
                                        </a>
                                </div>

                                <!--
                                        {{$countPictures}}
                                -->
                        @endif {{--Fim Verifica se foi colocado 11 imagens --}}
                {{--*/ $countPictures++ /*--}}
                @endforeach
                </div>
        </div>

@endif {{-- Fim Verifica Passagem de Imagens do Instagram --}}

<div class="container pt-3">
        <a class="link-no-hover" href="{{url($p_Language.'/doacao-de-midias')}}">
        <div class="col-6 col-md-1 mr-auto ml-auto"><img class="img-fluid" src="/image/162/118/true/true/portal/assets/libs/imgs/home_image/pictograma_foto.png"></div>
        <div class="text-center title"><h5>Compartilhe suas fotos!</h5></div>
        <div class="text-center title"> E se as fotos da sua viagem ficaram incríveis, doe para o nosso banco de imagens!</div>
        </a>
</div>


</main>
        @include('public.templates.menuMobile')
@endsection
 
@section('footer')
        @include('public.templates.footer')
@endsection
 
@section('pageScript')
@endsection