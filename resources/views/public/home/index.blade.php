<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?php  $today = date("Y-m-d H:i:s");  $date = get_limite_data_especial();  ?>

@extends('public.templates.base') 
@section('pageCSS')
<style>

</style>  
<link rel="stylesheet" type="text/css" href="{{url('/portal/assets/libs/portal/css/homeIndex.css')}}">
@endsection
 
@section('header')
        @include('public.templates.header')
@endsection
 
@section('main-content')
        <!-- Carousel Principal -->


<div id="mgCarousel" class="carousel slide" data-ride="carousel">
         <ol class="carousel-indicators">
                   
              <li data-target="#mgCarousel" data-slide-to="0" class="active"></li>
                             
              <li class="d-none displayOnLoad" data-target="#mgCarousel" data-slide-to="1"></li>
                             
              <li class="d-none displayOnLoad" data-target="#mgCarousel" data-slide-to="2"></li>
                             
              <li class="d-none displayOnLoad" data-target="#mgCarousel" data-slide-to="3"></li>
                        </ol>
      
        <div class="carousel-inner">
                <div class="carousel-item active main-carousel-item">
                  
                <a href="http://www.descubramatogrosso.com.br/pt/o-que-fazer/polo-pantanal" > 
             
            <img class="third-slide banner__home--9" style="min-width:100%;" data-lazyload="http://www.descubramatogrosso.com.br/portal/assets/libs/imgs/home_image/carousel/capa.jpg" alt="Rimas das Gerais ">
         
                  </a> 
              
                  
            </div>
                <div class="carousel-item main-carousel-item d-none displayOnLoad">
                  
                <a href="http://www.descubramatogrosso.com.br/pt/o-que-fazer/polo-pantanal" > 
             
                        <img width=1400px class="third-slide banner__home--42" data-lazyload="http://www.descubramatogrosso.com.br/portal/assets/libs/imgs/home_image/carousel/capa2.jpg" alt="Rimas das Gerais ">
         
                  </a> 
              
                  {{-- <div class="container">
                          <div class="carousel-caption text-center">
                            <h1 class=" text-white text-bold">POLO ARAGUAIA</h1>
                                      <p class="text-white text-bold">O lugar perfeito para você</p>
                         
                 
                  <p><a class="btn btn-lg btn-outline-light" href="http://www.descubramatogrosso.com.br/pt/o-que-fazer/polo-cerrado" role="button">Conheça mais</a></p>
                          </div>
              </div> --}}
                  
            </div>
                  <div class="carousel-item main-carousel-item d-none displayOnLoad">
                  
                <a href="http://www.descubramatogrosso.com.br/pt/o-que-fazer/polo-amazonia" > 
             
            <img class="third-slide banner__home--9" data-lazyload="http://www.descubramatogrosso.com.br/portal/assets/libs/imgs/home_image/carousel/capa3.jpg" width=1500px alt="Patrim&ocirc;nios da Humanidade">
         
                  </a> 
              
                  {{-- <div class="container">
                          <div class="carousel-caption text-center">
                            <h1 class="text-white text-bold">POLO CERRADO</h1>
                                      <p class="text-white text-bold">Aqui não tem igual</p>
                         
                 
                  <p><a class="btn btn-lg btn-outline-light" href="http://www.descubramatogrosso.com.br/pt/o-que-fazer/polo-araguaia" role="button">Conheça mais</a></p>
                          </div>
              </div> --}}
                  
            </div>
                  <div class="carousel-item main-carousel-item d-none displayOnLoad">
                  
                <a href="http://www.descubramatogrosso.com.br/pt/o-que-fazer/polo-amazonia" > 
             
            <img class="third-slide banner__home--8" data-lazyload="http://www.descubramatogrosso.com.br/portal/assets/libs/imgs/home_image/carousel/capa4.jpg" alt="Gastronomia">
         
                  </a> 
              
                  {{-- <div class="container">
                          <div class="carousel-caption text-center">
                            <h1 class="text-white text-bold">POLO AMAZÔNIA</h1>
                                      <p class="text-white text-bold">Preservar a natureza é preservar a si mesmo</p>
                         
                 
                  <p><a class="btn btn-lg btn-outline-light" href="http://www.descubramatogrosso.com.br/pt/o-que-fazer/polo-araguaia" role="button">Conheça mais</a></p>
                          </div>
              </div>  --}}
                  
            </div> 
        </div>
          
          <a class="carousel-control-prev" href="#mgCarousel" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Voltar</span>
          </a>
          <a class="carousel-control-next" href="#mgCarousel" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Avançar</span>
          </a>
        </div>
      
        
     
        <!-- Carousel Principal -->
      


        <div class="container">
                <div class="text-center mb-5"><p style="text-align:justify; font-size:22px; font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;">Mato Grosso possui três dos mais importantes biomas do planeta: Amazônia, Cerrado e Pantanal, além da especial região do Araguaia. De localização privilegiada, ao centro da América do Sul, abriga rica biodiversidade e paisagens naturais de encher os olhos, que somadas a uma expressiva história e variada cultura, fascina turistas do Brasil e do mundo, seja em busca de vivenciar intensas emoções ou realizar bons negócios. Mato Grosso é, sem dúvida, destino de inesquecíveis experiências e envolvente hospitalidade. Descubra Mato Grosso!</p></div>
                <div class="text-center title mb-5"><h2 style="font-family: Trebuchet MS; font-size:28px;"><strong>O que gostaria de descobrir?</strong></h2></div>
                <div class="row">
                        <div class="col-12 col-sm-6 col-md-3 text-center" style="width: 18rem;">
                                <a href="{{url($p_Language.'/o-que-fazer/polo-pantanal')}}" role="button">
                                        <img class="rounded" src="{{url('portal/assets/libs/imgs/home_image/pictogramas/Pantanal.png')}}" alt="Pantanal" width="140" height="140">
                                </a>                       
                                <p style="font-family: Trebuchet MS;">Uma das maiores planícies alagáveis do planeta, possui 52 mil km e esbanja beleza em rios, ninhais e fauna, com mais de 650 espécies de aves, 260 espécies de peixes e 80 espécies de mamíferos, incluindo a famosa onça-pintada.</p>                        
                        </div>
                        <div class="col-12 col-sm-6 col-md-3 text-center" style="width: 18rem;">
                                <a href="{{url($p_Language.'/o-que-fazer/polo-cerrado')}}" role="button">
                                        <img class="rounded" src="{{url('portal/assets/libs/imgs/home_image/pictogramas/Cerrado.png')}}" alt="Cerrado" width="140" height="140">
                                </a>                       
                                <p style="font-family: Trebuchet MS;">Berço das águas, tem sua paisagem composta por árvores retorcidas, rios de águas cristalinas, cahoeiras deslumbrantes, montanhas desafiadoras e fauna exuberante, além de inúmeros sítios arqueológicos.</p>                        
                        </div>
                        <div class="col-12 col-sm-6 col-md-3 text-center" style="width: 18rem;">
                                <a href="{{url($p_Language.'/o-que-fazer/polo-amazonia')}}" role="button">
                                        <img class="rounded" src="{{url('portal/assets/libs/imgs/home_image/pictogramas/Amazonia.png')}}" alt="Amazônia" width="140" height="140">
                                </a>                       
                                <p style="font-family: Trebuchet MS;">Ao norte de Mato Grosso, a Floresta Amazônica abriga mais de 20% de todas as espécies existentes no mundo, incluindo vegetais, peixes, mamíferos e pássaros. O território possui áreas de preservação ambiental e conta com o etnoturismo para visitação das aldeias indígenas locais.</p>                        
                        </div>
                        <div class="col-12 col-sm-6 col-md-3 text-center" style="width: 18rem;">
                                <a href="{{url($p_Language.'/o-que-fazer/polo-araguaia')}}" role="button">
                                        <img class="rounded" src="{{url('portal/assets/libs/imgs/home_image/pictogramas/Araguaia.png')}}" alt="Araguaia" width="140" height="140">
                                </a>                       
                                <p style="font-family: Trebuchet MS;">Conhecida pela prática da pesca esportiva, a região atrai milhares de turistas com seu vale de praias fluviais, conhecido pelas areias brancas e finas, além das águas transparentes. O local é cercado por lendas místicas, atraindo pesquisadores, esotéricos e ufólogos do mundo inteiro.</p>                        
                        </div>
                </div>
                <hr>
        </div>


        @if (isset($eventCarousel) && (count($eventCarousel)>0)) if Carousel de Eventos 
                <div class="title container">
                        <h2 class="text-center display-5" style="font-family: Arial, Helvetica, sans-serif"><strong>Descubra os melhores eventos</strong></h2>
                </div>
                <div class="container mt-2">
                        <div id="carouselControls_" class="carousel slide mb-3" data-ride="carousel">
                                <div class="carousel-inner text-center">           
                                        {{--*/ $carouselActive=0 /*--}} {{--*/ $tema=0 /*--}} @foreach ($eventCarousel as $vEvent) {{-- Verifica se o evento tem foto de capa --}} @if ($vEvent->Event->EventPhotos(true)->first() != null) @if ($tema == 3) {{--*/ $tema=0 /*--}} @endif {{--*/ $carouselColor=$tema /*--}} {{--*/ $tema++ /*--}}         
                                                <div class="carousel-item border @if ($carouselActive == 0) {{'active'}} @else {{'d-none displayOnLoad'}} @endif">                                           
                                                        <div class="row featurette">
                                                                <div class="col-md-5 order-2 order-md-2 ">
                                                                        <div class="text-left text-uppercase mt-4">
                                                                                <?php $d = $destination->where('city_id',$vEvent->city_id)->whereNull('district_id')->first();?>   
                                                                                <a class="link-no-hover title" href="{{url($p_Language.'/eventos/'.$d['slug'].'/'.$vEvent->slug)}}" title="{{$vEvent->name}}" data-content="{{json_decode($vEvent->short_description, true)[$p_Language]}}" data-placement="top">
                                                                                        <h5 class="text-bold" style="color: #343a40;">{{$vEvent->name}}</h5>
                                                                                </a>
                                                                                <small style="font-family:'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif">{{json_decode($vEvent->short_description,true)[$p_Language]}}</small>
                                                                        </div>
                                                                        <table class="float-left mt-4">
                                                                                <div class="text-left mt-3 mt-md-4 d-none d-md-flex mx-3"></div>
                                                                                        <tr>
                                                                                                @if (($vEvent->end != '') && (date_format(date_create($vEvent->start), 'm') != date_format(date_create($vEvent->end), 'm')))
                                                                                                        <th class="rounded-top carousel-calendar-month text-center px-1">{{getMonthName(date_format(date_create($vEvent->start),'m'))}}/{{getMonthName(date_format(date_create($vEvent->end),'m'))}}</th>
                                                                                                @else
                                                                                                        <th class="rounded-top carousel-calendar-month text-center px-1">{{getMonthName(date_format(date_create($vEvent->start),'m'))}}</th>
                                                                                                @endif
                                                                                        </tr>
                                                                                        <tr>
                                                                                                @if (($vEvent->end != '') && (date_format(date_create($vEvent->start), 'd') != date_format(date_create($vEvent->end), 'd')))
                                                                                                        <td class="rounded-bottom border border-0 carousel-calendar-day text-center px-1">{{date_format(date_create($vEvent->start),'d')}} | {{date_format(date_create($vEvent->end),'d')}}</td>
                                                                                                @else
                                                                                                        <td class="rounded-bottom border border-0 carousel-calendar-day text-center px-1">{{date_format(date_create($vEvent->start),'d')}}</td>
                                                                                                @endif
                                                                                        </tr>
                                                                        </table>
                                                                        <div class="text-uppercase mt-4">
                                                                                <div class="row">
                                                                                        <div class="col-md-12 font-weight-bold text-left"><small><span class="oi oi-map-marker text-danger"></span></small><b>&nbsp; {{$vEvent->city}}</b></div>
                                                                                                <div class="col-md-12 text-left"><small><small>{{$vEvent->events_place}}</small></small></div>
                                                                                </div>
                                                                        </div>
                                                                        <div class="text-right text-md-center pt-3 mb-3 mr-3"> <a href="{{url($p_Language.'/eventos/'.$d['slug'].'/'.$vEvent->slug)}}" class="btn btn-info">Detalhes</a></div>
                                                                </div>
                                                                <div class="col-md-7 order-1 order-md-1 border pr-md-0">
                                                                        <img class="featurette-image img-fluid mx-auto"  @if ($carouselActive == 0) {{'src'}} {{--*/ $carouselActive=1 /*--}}@else {{'data-lazyload'}} @endif="{{url('/image/650/261/true/true/'.$vEvent->Event->EventPhotos(true)->first()->url)}}" alt="{{$vEvent->name}}">
                                                                </div>
                                                        </div>
                                                </div>
                                        @endif 
                                        @endforeach
                                </div>
                                <a class="carousel-control-prev" href="#carouselControls_" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#carouselControls_" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                </a>
                        </div>
                        <hr>
                </div>
        @endif {{--Fim de if Carousel de Eventos--}}


        <main class="container-fluid">                        
                @if (isset($destinations) && (count($destinations)==6)) {{-- Verifica Passagem de Destinos surpreendentes --}}
                        <div class="title container pt-3">
                                <h2 class="text-justify display-5">Descubra destinos surpreendentes</h2>
                        </div>
                        <div class="container mt-3">
                        {{-- faz regra para exibir somente 3 destinos caso esteja na versão mobile --}}
                                <div class="row">
                                        {{--*/ $countDestinations=0 /*--}} @foreach ($destinations as $destination)
                                        <div class="col-md-4 grow  @if($countDestinations >2) {{" d-none d-md-flex "}} @endif">
                                                <a class="link-no-hover" href="{{url($p_Language.'/destinos/'.$destination->slug)}}">
                                                        <img class="img-fluid fade-image rounded-top rounded-bottom shadow" src="/image/360/240/true/true/{{$destination->url}}" alt="">
                                                        <p class="text-center overlap-title-photos rounded-bottom"> <span class="text-light lead">{{$destination->nome}}<span> </p>
                                                </a>
                                        </div>
                                        {{--*/ $countDestinations++ /*--}} @endforeach
                                </div>
                        </div>
                        <div class="container text-center">
                                <a href="{{url('/pt/destinos')}}" class="btn btn-page">Conheça mais destinos</a>
                                <hr>
                        </div>
                @endif {{-- Fim Verifica Passagem de Destinos surpreendentes --}}


                <!-- @if (isset($v_InstagramPictures)){{-- Fim Verifica Passagem de Imagens do Instagram --}}
                        <div class="title container pt-3">
                                <div class="row">
                                        <div class="col-md-12 text-center">
                                                <h2 class="display-5 title" style="font-family: Trebuchet MS; font-size: 25px;"><strong> Siga nosso Instagram {{"@descubramatogrosso"}}</strong></h2>
                                                <h5 style="font-family: Trebuchet MS;">Use a hashtag #descubramt em suas fotos e compartilhe a sua experiência!</h5>
                                        </div>
                                </div>
                        </div>
                        <div class="container">
                                <div class="row">
                                        {{--*/ $countPictures=0 /*--}} 
                                        @foreach ($v_InstagramPictures as $pictures) {{-- --}} 
                                                @if ($countPictures <= 8 ) {{-- Verifica se foi colocado 9 imagens --}} 
                                                        <div class="@if($countPictures >= 1 && $countPictures <= 8) col-6 col-md-6 @else col-md-4 @endif">
                                                                <a class="" href="{{$pictures->link}}" target="_blank">
                                                                        @if($countPictures >= 1 && $countPictures <= 8) 
                                                                                <img class="img-fluid rounded m-1 fade-image" src="{{url($pictures->images->standard_resolution->url)}}"> 
                                                                        @else 
                                                                                <img class="img-fluid rounded m-1 fade-image" src="{{url($pictures->images->standard_resolution->url)}}"> 
                                                                        @endif 
                                                                </a>
                                                        </div>
                                                        @if ($countPictures == 0 )
                                                                <div class="col-12 col-md-4 pt-2">
                                                                        <div class="row">
                                                        @endif
                                                        @if ($countPictures == 4 )
                                                                        </div>
                                                                </div>
                                                                <div class="d-none d-md-block col-md-4 pt-2">
                                                                        <div class="row">
                                                        @endif
                                                        @if ($countPictures == 8 )
                                                                        </div>
                                                                </div>
                                                        @endif
                                                @endif {{--Fim Verifica se foi colocado 11 imagens --}}
                                        {{--*/ $countPictures++ /*--}} 
                                        @endforeach
                                </div>
                                <hr>
                        </div>
                @endif {{-- Fim Verifica Passagem de Imagens do Instagram --}} -->

<!-- SnapWidget -->
<script src="https://snapwidget.com/js/snapwidget.js"></script>
<iframe src="https://snapwidget.com/embed/822670" class="snapwidget-widget" allowtransparency="true" frameborder="0" scrolling="no" style="border:none; overflow:hidden;  width:100%; height: 300px "></iframe>

<hr>
                
                <div class="container">
                        <div class="" style= ""><h2 style="text-align:center; margin-bottom:0px;font-family: Trebuchet MS; font-size: 22px;"><strong>Apoio</strong></h2></div>
                        <div style="margin-left: 95px;position: relative; height:125px; width:917px; background:url(http://descubramatogrosso.com.br/portal/assets/libs/imgs/home_image/pictogramas_conselheiros/apoio_teste.png) 0 0 no-repeat;"><a style="position:absolute; top:15px; left:20px; width:500px; height:74px;" title="" alt="" href="http://www.abav.com.br/" target="_self"></a><a style="position:absolute; top:16px; left:213px; width:111px; height:74px;" title="" alt="" href="https://abrasel.com.br/" target="_self"></a><a style="position:absolute; top:19px; left:397px; width:117px; height:74px;" title="" alt="" href="https://www.fenagtur.org.br/sindicatos" target="_self"></a><a style="position:absolute; top:29px; left:600px; width:106px; height:75px;" title="" alt="" href="http://www.shrbs-mt.com.br/" target="_self"></a><a style="position:absolute; top:30px; left:780px; width:114px; height:87px;" title="" alt="" href="https://abh.org.br/" target="_self"></a></div>
                </div>
               
        </main>
        @include('public.templates.menuMobile')
@endsection
 
@section('footer')
        @include('public.templates.footer')
@endsection
 
@section('pageScript')
        <script type="text/javascript" src="{{url('/portal/assets/libs/portal/js/homeIndex.js')}}"></script>
@endsection