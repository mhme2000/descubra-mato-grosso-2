@extends('public.base')

@section('pageCSS')
    <link href="{{url('/vendor/select2/select2.min.css')}}" rel="stylesheet" />
    <style>
        .select2-container--default .select2-selection--single {
            background: #e6e6e6;
            border-radius: 0;
            border: none;
            text-align: center;
            color: #818181;
            font-size: 1rem;
            line-height: 1.5;
        }
        .select2-container--default .select2-selection--single .select2-selection__rendered{
            line-height: 39px;
        }
        .select2-container--default .select2-selection--single, .select2-container--default .select2-selection--single .select2-selection__arrow{
            min-height: 39px;
        }
    </style>
@stop

@section('main-content')
    <div class="row-fluid" id="useful-information" style="margin-top:160px;">
        <div class="container">
            <div class="col-lg-12">
                <h2>{{trans('menu.destinations')}}</h2>
            </div>
        </div>
    </div>

    <div class="row-fluid" id="destinos" style="padding:1% 0;">
        <div class="container">
            <div class="col-lg-12">
                <!-- <iframe src="https://www.youtube.com/embed?v=B6NTGPDkkL8" height="480" width="100%" allowfullscreen="" frameborder="0"></iframe> -->
                <div class="col-lg-8 col-lg-offset-2">
                    <div class="col-lg-12" style="margin-bottom:50px;">
                        <p>{!! $p_Content == null ? '' : nl2br($p_Content->descricao) !!}</p>
                    </div>
                </div>
            </div>

            <div class="col-lg-12 line">
                @foreach($p_Destinations as $c_Index => $c_Destination)
                    @if($c_Index < 2)
                    <?php $v_Description = json_decode($c_Destination->descricao_curta,1)[$p_Language]; ?>
                    <div class="col-lg-6 list-thumbs-full">
                        <a href="{{url($p_Language . '/destinos/' . $c_Destination->slug)}}">
                            <div class="hoverzoom">
                                <div class="thumbs-full">
                                    <img src="{{$c_Destination->url}}">
                                </div>
                                <div class="retina-hover">
                                    <div class="col-lg-12 title">
                                        <p>{{$c_Destination->nome}}</p>
                                    </div>
                                    <div class="col-lg-12 no-padding">
                                        <hr>
                                    </div>
                                    <div class="col-lg-12 text">
                                        <p>{{$v_Description}}</p>
                                    </div>
                                </div>
                                <div class="retina">
                                    <p>{{$c_Destination->nome}}</p>
                                </div>
                            </div>
                        </a>
                    </div>
                    @endif
                @endforeach
            </div>
            <div class="col-lg-12 line">
                @foreach($p_Destinations as $c_Index => $c_Destination)
                    @if($c_Index >= 2 && $c_Index < 5)
                    <?php $v_Description = json_decode($c_Destination->descricao_curta,1)[$p_Language]; ?>
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <a href="{{url($p_Language . '/destinos/' . $c_Destination->slug)}}">
                            <div class="hoverzoom">
                                <div class="thumbs-mini-three">
                                    <img src="{{$c_Destination->url}}">
                                </div>
                                <div class="retina-hover">
                                    <div class="col-lg-12 title">
                                        <p>{{$c_Destination->nome}}</p>
                                    </div>
                                    <div class="col-lg-12 no-padding">
                                        <hr>
                                    </div>
                                    <div class="col-lg-12 text">
                                        <p>{{$v_Description}}</p>
                                    </div>
                                </div>
                                <div class="retina">
                                    <p>{{$c_Destination->nome}}</p>
                                </div>
                            </div>
                        </a>
                    </div>
                    @endif
                @endforeach
            </div>


            <div class="col-lg-12" style="padding:2% 0;">
                <div class="col-lg-12 hospedagem">
                    <h2 style="color:#b4b4b4;">
                        {{trans('whereToGo.find_destination')}}
                    </h2>
                    <p style="color:#4b4b4b;font-size:16px;font-family: Signika;">
                        {!! $p_Content == null ? '' : nl2br($p_Content->descricao_outra) !!}
                    </p>
                </div>
            </div>
        </div>

        <div class="col-lg-12" id="tarja">
            <div class="container">
                <div class="col-lg-12">
                    <div class="row" id="options">
                        <fieldset>
                            <div class="col-lg-9">
                                <div class="col-lg-4">
                                    <p style="font-size:20px;">{{trans('whereToGo.destinations_a_z')}}</p>
                                </div>
                                <div class="col-lg-8" id="alphabet">
                                    <p id="alphabeticFilter" style="font-size:20px;padding: 3px 0;font-family: Signika;font-weight:400;color:#FFFFFF;">
                                        <a href="javascript:void(0)"><span>A</span></a>
                                        <a href="javascript:void(0)"><span>B</span></a>
                                        <a href="javascript:void(0)"><span>C</span></a>
                                        <a href="javascript:void(0)"><span>D</span></a>
                                        <a href="javascript:void(0)"><span>E</span></a>
                                        <a href="javascript:void(0)"><span>F</span></a>
                                        <a href="javascript:void(0)"><span>G</span></a>
                                        <a href="javascript:void(0)"><span>H</span></a>
                                        <a href="javascript:void(0)"><span>I</span></a>
                                        <a href="javascript:void(0)"><span>J</span></a>
                                        <a href="javascript:void(0)"><span>K</span></a>
                                        <a href="javascript:void(0)"><span>L</span></a>
                                        <a href="javascript:void(0)"><span>M</span></a>
                                        <a href="javascript:void(0)"><span>N</span></a>
                                        <a href="javascript:void(0)"><span>O</span></a>
                                        <a href="javascript:void(0)"><span>P</span></a>
                                        <a href="javascript:void(0)"><span>Q</span></a>
                                        <a href="javascript:void(0)"><span>R</span></a>
                                        <a href="javascript:void(0)"><span>S</span></a>
                                        <a href="javascript:void(0)"><span>T</span></a>
                                        <a href="javascript:void(0)"><span>U</span></a>
                                        <a href="javascript:void(0)"><span>V</span></a>
                                        <a href="javascript:void(0)"><span>W</span></a>
                                        <a href="javascript:void(0)"><span>X</span></a>
                                        <a href="javascript:void(0)"><span>Y</span></a>
                                        <a href="javascript:void(0)"><span>Z</span></a>
                                    </p>
                                </div>
                            </div>

                            <div class="col-lg-3 no-padding" id="inputDestino">
                                {!! Form::select('cidade', [], null, ['id' => 'cidade', 'class' => 'form-control select2', 'required' => 'required', 'style' => 'width: 100%']) !!}
                                <!-- <input type="text" class="form-control" aria-label="Digite sua busca" placeholder="Digite seu Destino"> -->
                                {{--<form action="#" method="post" id="formSearch">--}}
                                    {{--<div class="input-group" style="width: 100%;display:block;">--}}
                                        {{--<input type="text" class="form-control" placeholder="Busque por uma cidade" required="">--}}
                                                {{--<span class="input-group-btn" id="btn-search">--}}
                                                    {{--<button class="btn btn-secondary btn-submit" type="submit" id="btn-submit"></button>--}}
                                                {{--</span>--}}
                                    {{--</div>--}}
                                {{--</form>--}}
                            </div>

                        </fieldset>
                    </div>
                </div>
            </div>
        </div>

        {!! Form::open(array('onsubmit' => 'return submitFilter()')) !!}
            <div class="col-lg-12" id="tarja_intern">
                <div class="container">
                    <div class="col-lg-10 col-lg-offset-1">
                        <div class="row" id="options">
                            <div class="col-lg-4 no-padding">
                                <p style="font-size:20px;">{{trans('whereToGo.what_do_you_expect')}}</p>
                            </div>
                            <div class="col-lg-3 selectoptions no-padding">
                                <?php
                                    $v_CategoriesArray = [];
                                    foreach($p_TripCategories as $c_CategoryIndex => $c_Value)
                                        $v_CategoriesArray += [$c_CategoryIndex => json_decode($c_Value,1)[$p_Language]];
                                ?>
                                {!! Form::select('categoria_viagem', [''=>trans('whereToGo.choose_trip_category')] + $v_CategoriesArray, null, ['id' => 'categoriaViagem']) !!}
                            </div>

                            <div class="col-lg-3 selectoptions">
                                {!! Form::select('tipo_viagem', [''=>trans('whereToGo.choose_trip_type')], null, ['id' => 'tipoViagem']) !!}
                            </div>
                            <div class="col-lg-2 selectoptions no-padding" id="submit-tarja">
                                <input class="btn" type="submit" value="{{trans('whereToGo.search')}}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        {!! Form::close() !!}
        <div class="col-lg-12"  style="padding:2rem 0;">
            <div class="container">
                <div class="col-lg-12 line" id="filterResultsDiv">
                    @foreach($p_Destinations as $c_Index => $c_Destination)
                        @if($c_Index >= 5)
                            <?php $v_Description = json_decode($c_Destination->descricao_curta,1)[$p_Language]; ?>
                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                <a href="{{url($p_Language . '/destinos/' . $c_Destination->slug)}}">
                                    <div class="hoverzoom">
                                        <div class="thumbs-mini-four">
                                            <img src="{{$c_Destination->url}}">
                                        </div>
                                        <div class="retina-hover">
                                            <div class="col-lg-12 title">
                                                <p>{{$c_Destination->nome}}</p>
                                            </div>
                                            <div class="col-lg-12 no-padding">
                                                <hr>
                                            </div>
                                            <div class="col-lg-12 text">
                                                <p>{{$v_Description}}</p>
                                            </div>
                                        </div>
                                        <div class="retina">
                                            <p>{{$c_Destination->nome}}</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@stop

@section('pageScript')
    <script src="{{url('/vendor/select2/select2.min.js')}}" type="text/javascript"></script>
    <script src="{{url('/vendor/select2/i18n/pt-BR.js')}}" type="text/javascript"></script>
    <script>
        var v_Destinations = {!! json_encode($p_DestinationList) !!};
        var v_Types = {!! json_encode($p_TripTypes) !!};
        $(document).ready(function()
        {
            var v_DataString = '<option value="">{{trans('planYourTrip.select_destination')}}</option>';
            $.each(v_Destinations, function (c_Key, c_Destination)
            {
                v_DataString += '<option value="' + c_Key + '">' + c_Destination.nome + '</option>';
            });

            $('.select2').html(v_DataString).select2(
                @if($p_Language == 'pt')
                    {language:'pt-BR'}
                @endif
            ).change(function(){
                if(this.value != '') {
                    $('#alphabeticFilter a').removeClass('active');
                    $('#categoriaViagem').val('');
                    $('#tipoViagem').html('');
                    filterDestinations([v_Destinations[this.value]]);
                }
            });

            $('#categoriaViagem').change(function(){
                var v_SelectedCategoryId = $(this).val();
                var v_DataString = '<option></option>';
                $.each(v_Types, function (c_Key, c_Field)
                {
                    if(c_Field.trip_category_id == v_SelectedCategoryId)
                        v_DataString += '<option value="' + c_Field.id + '">' + JSON.parse(c_Field.nome).{{$p_Language}} + '</option>';
                });
                $('#tipoViagem').html(  v_DataString);
            }).trigger('change');

            $('#alphabeticFilter a').click(function(){
                var v_WasActive = $(this).hasClass('active');
                $('#alphabeticFilter a').removeClass('active');
                if(v_WasActive) {
                    $('#letra').val('');
                }
                else
                {
                    $('.select2').select2('val', '');
                    $('#categoriaViagem').val('');
                    $('#tipoViagem').html('');
                    $(this).addClass('active');
                    var v_SelectedLetter = $(this).text();
                    var v_FilterDestinations = v_Destinations.filter(function(p_Destination){
                        return p_Destination.nome[0] == v_SelectedLetter;
                    });
                    filterDestinations(v_FilterDestinations);
                }
            });
        });
        function filterDestinations(p_Destinations){
            var v_DataString = '';

            if(p_Destinations.length < 1){
                v_DataString = '<div class="col-xs-12" id="no-results">' +
                        '<p>' + "{{trans('destination.no_results')}}" + '</p>' +
                        '</div>';
            }

            $.each(p_Destinations, function (c_Key, c_Field)
            {
                var v_Descricao = JSON.parse(c_Field.descricao_curta).{{$p_Language}};
                v_DataString += '<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">'+
                        '<a href="{{url($p_Language . '/destinos/')}}/' + c_Field.slug + '">'+
                        '<div class="hoverzoom">'+
                        '<div class="thumbs-mini-four">'+
                        '<img src="' + c_Field.url + '">'+
                        '</div>'+
                        '<div class="retina-hover">'+
                        '<div class="col-lg-12 title">'+
                        '<p>' + c_Field.nome + '</p>' +
                        '</div>'+
                        '<div class="col-lg-12 no-padding">'+
                        '<hr>'+
                        '</div>'+
                        '<div class="col-lg-12 text">'+
                        '<p>' + v_Descricao + '</p>'+
                        '</div>'+
                        '</div>'+
                        '<div class="retina">'+
                        '<p>' + c_Field.nome + '</p>' +
                        '</div>'+
                        '</div>'+
                        '</a>'+
                        '</div>';
            });
            $('#filterResultsDiv').html(v_DataString);
        }

        function submitFilter()
        {
            if($('#tipoViagem').val().length > 0)
            {
                $('.select2').select2('val', '');
                $('#alphabeticFilter a').removeClass('active');
                $.get("{{url($p_Language . '/filtro-destinos')}}",{
                    'trip_type_id': $('#tipoViagem').val()
                }).done(function(p_Data){
                    if (p_Data.error == 'ok')
                        filterDestinations(p_Data.data);
                    else
                        showNotify('danger', p_Data.error);
                }).error(function(){
                });
            }
            else
                showNotify('danger', '{!!trans('events.fill_form_message')!!}');

            return false;
        }
    </script>
@stop