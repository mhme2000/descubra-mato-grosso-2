@extends('public.base')

@section('pageCSS')
    <style>
        a .at-icon-wrapper {
            cursor: pointer !important;
            border-radius: 50% !important;
        }
       
        /*gafit code*/
        .hashtags-div a, .hashtags-div a:hover{
            color:#fff;
        }
    </style>
@stop

@section('main-content')
    <div class="row-fluid" id="destinos">
        <div class="container" style="margin-top: 10rem;">
            <div class="col-lg-9 col-md-8">
                <div class="row-fluid" id="breadcrumb" >
                    <div class="col-lg-12 line no-padding" style="padding:0.5% 0 0;">
                        <div class="col-lg-12 hashs">
                            <p style="margin-top: 0.6rem;color:#67676c;font-size:16px;font-weight:400;text-align:center;">
                                @foreach($p_ArticleHashtags as $c_Hashtag)
                                    #{{$c_Hashtag->name}}&nbsp
                                @endforeach
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row-fluid">
                    <h2 style="color:#67676c;font-size:36px;margin-bottom:1rem;">{{json_decode($p_Article->titulo,1)[$p_Language]}}</h2>
                    <img src="{{$p_Article->foto_capa_url}}" alt="" style="width: 100%;background-size: cover;background-position: 50%;">
                    <div class="col-lg-8 col-xs-5 no-padding"><p style="margin:1rem 0;color: #666666;"><span id="datePost">{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $p_Article->created_at)->format('d/m/Y')}}</span></p></div>
                    <div class="col-lg-4 col-xs-7 social no-padding">
                        <ul id="icons-social" style="float:right;">
                            <div class="addthis_sharing_toolbox"></div>
                        </ul>
                    </div>
                </div>
                <div class="row-fluid" id="destinos" style="padding:0;border-top:1px solid #c1c1c1;">
                    <div style="margin:2rem 0;">
                        <div style="margin-bottom: 15px;">
                            {!! $p_Article == null ? '' : nl2br(json_decode($p_Article->conteudo,1)[$p_Language]) !!}
                        </div>
                    </div>
                </div>
                <div class="row-fluid" id="useful-information">
                  
                        <div class="col-lg-12 social no-padding">
                            <a href="javascript:history.back()" id="goBack">{{trans('blog.back')}}</a>
                        </div>
                        
                        <hr class="separa-blog">
                      
                        @if(count($p_RelatedArticles) > 0)
                        <div class="col-lg-12 no-padding">
                            <h2>{{trans('blog.related_content')}}</h2>
                        </div>
                        @else
                        <div class="col-lg-12 no-padding">
                            <h6>Não há posts relacionados.</h6>
                        </div>
                        @endif

                </div>
                @if(count($p_RelatedArticles) > 0)
                    <div class="col-lg-12 line no-padding">
                        @foreach($p_RelatedArticles as $c_Article)
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <a href="{{url($p_Language . '/blog/artigo/' . $c_Article->slug)}}">
                                    <p style="margin-bottom:0.3rem;color:#b1b1b4;font-size: 14px;">{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $c_Article->created_at)->format('d/m/Y')}}</p>
                                    <div class="hoverzoom" style="margin:0;">
                                        <div class="thumbs-mini-three">
                                            <!--<img src="{{$c_Article->foto_capa_url}}" style="height:132px;">-->
                                            <div class="thumbs-mini-recorte" style="background: url('{{$c_Article->foto_capa_url}}') no-repeat;"></div>
                                        </div>
                                    </div>
                                    <p style="margin-top:0.5rem;margin-bottom:0.5rem;color:#b1b1b4">
                                        @foreach($c_Article->hashtags as $c_Hashtag)
                                            #{{$c_Hashtag}}
                                        @endforeach
                                    </p>
                                    <p style="margin-bottom:0.5rem;color:#67676c">{{json_decode($c_Article->titulo,1)[$p_Language]}}</p>
                                    <a href="{{url($p_Language . '/blog/artigo/' . $c_Article->slug)}}" style="color:#ef662f;">{{trans('institutional.read_more')}}</a>
                                </a>
                            </div>
                        @endforeach
                    </div>
                @endif
            </div>

            <div class="col-md-3 col-md-4">
            <div class="col-lg-12 no-padding">
                {!! Form::open(array('method' => 'get', 'url'=> url($p_Language . '/blog/busca'))) !!}
                <div class="input-group" style="margin-top:1rem;">
                    <input name="q" type="text" class="form-control" placeholder="{{trans('blog.search_content')}}" style="padding: 9.5px;border-radius: 0;border:none;background:#e6e6e6;" required>
                    <span class="input-group-btn" style="width: 13%;">
                        <button class="btn btn-secondary" type="button" style="padding: 9.5px;border-radius: 0;border:none;background:#e6e6e6;">
                            <img src="{{url('/portal/assets/imgs/lupaOrange.png')}}" alt="">
                        </button>
                    </span>
                </div>
                {!! Form::close() !!}
            </div>


            @if(count($p_Hashtags) > 0 )
                <div class="col-lg-12 no-padding" style="margin-top: 5rem;">
                    <h2 class="laranja" style="padding: 0;">Tags</h2>
                </div>

                <div class="col-lg-12 no-padding hashtags-div text-center" style="margin-top:2rem;background:#db775f;padding:1rem;">
                    <p>
                        @foreach($p_Hashtags as $c_Hashtag)
                            <a href="{{url($p_Language . '/blog/busca?hashtag=' . $c_Hashtag)}}">
                            #{{$c_Hashtag}}
                            </a>
                        @endforeach
                    </p>
                </div>
            @endif


            <div class="col-lg-12 no-padding" style="margin-top: 5rem;">
                <h2 style="padding: 0;color:#bababa;font-size:42px;">{{trans('blog.archive')}}</h2>
            </div>

            <div class="col-lg-12 calendar">
                @foreach($p_ArchiveYears as $c_Year)
                <div class="content-data">
                    <div class="col-lg-12 list-years" data-ano="{{$c_Year}}">
                        <p>{{$c_Year}}</p>
                    </div>
                    <div class="col-lg-12 list-months" id= "list-months-{{$c_Year}}">
                        <ul>
                            <li>
                                <ul class="children">
                                    @foreach($p_ArchiveMonths as $c_Month)
                                        @if($c_Month->year == $c_Year)
                                            <li><a title="{{$c_Month->month_year}}" href="{{url($p_Language . '/blog/busca?arquivo=' . $c_Year . '-' . $c_Month->month)}}">{{$c_Month->month}}</a></li>
                                        @endif
                                    @endforeach
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
                @endforeach
            </div>
        </div>

        </div>
    </div>
@stop

@section('pageScript')
<!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-56a0debd79357d1c" async="async"></script>
@stop