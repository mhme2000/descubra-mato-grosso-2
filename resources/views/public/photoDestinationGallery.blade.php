@extends('public.base')
@section('pageCSS')
    <link href="{{url('/vendor/lightbox/css/lightbox.css')}}" rel="stylesheet">
@stop

@section('main-content')
    <div class="row-fluid" id="destinos" style="margin-top: 180px;">
        <div class="container">
            <div class="col-lg-12" id="list-title">
                <h2 class="laranja">{{$p_Content->nome}}</h2>
            </div>
            <div class="col-lg-6 col-lg-offset-3">
                <div class="input-group" style="margin-top:1rem;">
                    <p>{{ json_decode($p_Content->descricao_curta,1)[$p_Language] }}</p>
                </div>
            </div>

            <div class="col-lg-6 col-lg-offset-3" style="padding-top: 1.5rem;">
                {{--<div class="col-lg-6 ">--}}
                    {{--<div class="col-lg-12">--}}
                        {{--<ul style="display: inline-block;float: right;padding-top: 1.3rem;">--}}
                            {{--<li style="float: left;margin-right: 5px;"><a href=""><img src="assets/imgs/facebookGray.png" alt=""></a></li>--}}
                            {{--<li style="float: left;margin-right: 5px;"><a href=""><img src="assets/imgs/instagramGray.png" alt=""></a></li>--}}
                            {{--<li style="float: left;margin-right: 5px;"><a href=""><img src="assets/imgs/twitterGray.png" alt=""></a></li>--}}
                            {{--<li style="float: left;margin-right: 5px;"><a href=""><img src="assets/imgs/youtubeGray.png" alt=""></a></li>--}}
                            {{--<li style="float: left;margin-right: 5px;"><a href=""><img src="assets/imgs/flickerGray.png" alt=""></a></li>--}}
                        {{--</ul>--}}
                    {{--</div>--}}
                {{--</div>--}}
                <div class="col-lg-8 col-lg-offset-2">
                    <div class="col-lg-12" id="call-to-action">
                        <a href="{{url($p_Language . '/destinos/' . $p_Content->slug)}}" style="padding: 4% 2%;width: 100%;">{{trans('gallery.access_destination')}}</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row-fluid" id="destinos" style="padding:1% 0;">
        <div class="container">
            <div class="col-lg-12 line">
                @foreach($p_PhotoGallery as $c_Index => $c_Photo)
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <a href="{{$c_Photo->url}}" data-lightbox="image-gallery">
                            <div class="hoverzoom">
                                <div class="thumbs-mini-four">
    
                                    <div class="thumbs-mini-recorte" style="background: url('{{$c_Photo->url}}') no-repeat;"></div>
                                </div>
                                <div class="retina-hover">
                                    <img src="{{url('/portal/assets/imgs/hover.png')}}" alt="{{$p_Content->nome}}" style="position: relative;top: 45%;">
                                </div>
                            </div>
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@stop

@section('pageScript')
    <script src="{{url('/vendor/lightbox/js/lightbox.min.js')}}"></script>
@stop