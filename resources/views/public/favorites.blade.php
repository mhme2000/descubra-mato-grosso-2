@extends('public.base')
@section('pageCSS')
@stop
@section('main-content')
    <div class="row-fluid" id="useful-information">
        <div class="container">
            <div class="col-lg-12" id="list-title">
                <h2>{{trans("menu.favorites")}}</h2>
            </div>
        </div>
    </div>
    <div class="col-lg-12" id="destinos" style="margin-top:1%;">
        <div class="container">
            <!-- FRAGMENTO ATUALIZADO -->
            <div class="col-lg-12 line no-padding" id="allFavoritesDiv"></div>
        </div>
    </div>
@stop

@section('pageScript')
    <script>
        $(document).ready(function(){

            var v_FavoritesList = JSON.parse(localStorage.getItem("favoritesList")) || [];

            var v_FavoritesString = '';
            $(v_FavoritesList).each(function(c_Index, c_Item){

                v_FavoritesString += '<div class="col-lg-3 list-thumbs" style="margin-bottom:25px;">'+
                        '<a href="' + c_Item.url + '">'+
                        '<div class="hoverzoom">'+
                        '<div class="thumbs-mini-four">'+
                        '<div class="thumbs-mini-recorte" style="background: url('+ c_Item.img +') no-repeat;"></div>'+
                        '</div>'+
                        '<div class="retina-hover">'+
                        '<div class="col-lg-12 title">'+
                        '<p>' + c_Item.name + '</p>' +
                        '</div>'+
                        '<div class="col-lg-12 no-padding">'+
                        '<hr>'+
                        '</div>'+
                        '<div class="col-lg-12 text">'+
                        '<p>' + c_Item.description + '</p>'+
                        '</div>'+
                        '</div>'+
                        '<div class="retina">'+
                        '<p>' + c_Item.name + '</p>' +
                        '</div>'+
                        '</div>'+
                        '</a>'+
                        '</div>';
            });
            $('#allFavoritesDiv').html(v_FavoritesString);
        });
    </script>

@stop