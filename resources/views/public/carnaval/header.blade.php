<style>
@media (min-width: 769px) {
    #bannercarnaval {
        height:145px;
        min-width:100%;


    }
    #bannercarnavala {
        display:none;
    }

    h1{
        font-size:2.2em;
    }

}    
@media (max-width: 768px) {
    #bannercarnaval {
        display:none;
    }
    #bannercarnavala {
        height:auto;
        width:100%;

    }
    h1{
        font-size:1em;
    }

}   
body{
    background-image:url("/assets/img/backgroundcarnaval.jpg");
}
header{
    background-color:white;
    padding-top:1em;
}
.form-group{
    margin-bottom:0px;
}

.input-group{
    margin-bottom:0px;
}

</style>
<div>
<a href="{{url('/pt/carnaval-2019')}}" style="align-self:center;">
<img id='bannercarnaval' src="{{url('/assets/img/banner-pc.jpg')}}" > 
<img id='bannercarnavala' src="{{url('/assets/img/banner-celular.jpg')}}" > 
</a> 
</div>

<header>
<div class="title container">
        <h1 style="font-family:pwchalk; color:#3f429b;">
            <STRONG> EXPERIMENTE NOSSOS CARNAVAIS</STRONG>
        </h1>

    </div>
    <!-- Barra de Pesquisa -->
    @include('public.carnaval.carnavalSearchbar')
    <!--    
    include('public.templates.eventsCategory')
    -->


</header>