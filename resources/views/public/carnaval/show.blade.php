@extends('public.templates.base')
@section('pageCSS')
<link rel="stylesheet" type="text/css" href="{{url('/portal/assets/libs/portal/css/eventsShow.css')}}">
<link rel="stylesheet" type="text/css" href="{{url('/portal/assets/libs/portal/css/eventsSearchbar.css')}}">
<link rel="stylesheet" type="text/css" href="{{url('/assets/fonts/pwchalk/stylesheet.css')}}">
@endsection

@section('header')
@include('public.carnaval.header')
@endsection

@section('main-content')
<main class="container-fluid mt-4">


    <section class="container">

        <!-- START THE FEATURETTES -->

        @if (isset($event))

        <hr class="featurette-divider">

        <div class="row featurette">
            <div class="col-md-5 order-md-1">

                @if (count($event->EventPhotos) == 0)
                <img class="d-block w-100"
                    src="/image/430/322/true/true/http://www.minasgerais.com.br/imagens/eventos/1516886575Dh2B2LCh6f.jpg"
                    alt="First slide">
                @else

                {{--*/ $carouselActive=0 /*--}}

                <div id="carouselEvento" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        @foreach ($event->EventPhotos as $photos)
                        <div
                            class="carousel-item @if ($carouselActive == 0) {{'active'}} {{--*/ $carouselActive=1 /*--}} @endif">
                            <img class="d-block w-100" src="{{url("/image/430/322/true/true")}}/{{$photos->url}}"
                                alt="">
                        </div>
                        @endforeach

                    </div>
                    <a class="carousel-control-prev" href="#carouselEvento" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselEvento" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>


                @endif
                <div class="text-left pt-2">
                    @if (!$event->facebook == '' || !$event->instagram == '' || !$event->twitter == '' ||
                    !$event->flickr == '')
                    <small class="title">Visite:</small>
                    @endif
                    @if (!$event->facebook == '')
                    <a href="{{$event->facebook}}" target="_blank"><img
                            src="{{url('/portal/assets/imgs/social/facebook_p.png')}}" alt="Facebook"></a>
                    @endif
                    @if (!$event->instagram == '')
                    <a href="{{$event->instagram}}" target="_blank"><img
                            src="{{url('portal/assets/imgs/social/instagram_p.png')}}" alt="Instagram"></a>
                    @endif
                    @if (!$event->twitter == '')
                    <a href="{{$event->twitter}}" target="_blank"><img
                            src="{{url('portal/assets/imgs/social/twitter_p.png')}}" alt="Twitter"></a>
                    @endif
                    @if (!$event->flickr == '')
                    <a href="{{$event->flickr}}" target="_blank"><img
                            src="{{url('portal/assets/imgs/social/flicker_p.png')}}" alt="Flickr"></a>
                    @endif
                </div>
            </div>
            <div class="col-md-7 order-md-2">
                <h2 class="text-uppercase text-bold" style="font-family:pwchalk; color:red;">
                  {{$event->nome}}
                </h2>
                <p><span class="oi oi-map-marker"></span>&nbsp;

                    <a href="@if (isset($citySlug)){{url('/'.$p_Language.'/destinos/'.$citySlug)}}@else{{"#"}}@endif">

                        <b> {{$event->City->name}} </b>

                    </a>

                    @if (!$event->local_evento == '') - {{$event->local_evento}}@endif<small> -
                        {{trim($event->logradouro)}}@if (trim($event->numero) != '' &&
                        !in_array(strtoupper(trim($event->numero)), array("0","S/N", "S/Nº", "S/N°", "SN", "SEM N°",
                        "SEM NUMERO", "SEM NÚMERO", "SEM NúMERO")) ), {{$event->numero}}@endif,
                        {{trim($event->bairro)}}</small> </p>

                <p><span
                        class="oi oi-clock"></span>&nbsp;{{getLiteralDate($event->inicio, ($event->inicio != $event->fim ) ? $event->fim: null)}}
                </p>

                @if (!trim($event->site == ''))
                <p><span class="oi oi-home"></span>&nbsp;<a href="{{$event->site}}" target="_blank">{{$event->site}}</a>
                </p>
                @endif

                @if (!trim($event->telefone == ''))
                <p><span class="oi oi-phone"></span>&nbsp;{{$event->telefone}}</p>
                @endif
                @if (!trim($event->email == ''))
                <p><span class="oi oi-envelope-closed"></span>&nbsp;<a
                        href="mailto:{{$event->email}}">{{$event->email}}</a></p>
                @endif
                @if (!trim(json_decode($event->dados_relevantes,true)[$p_Language][0]['dado']) == '')
                <p><span
                        class="oi oi-clipboard"></span>&nbsp;<b>{{trim(json_decode($event->dados_relevantes,true)[$p_Language][0]['dado'])}}:</b> {{trim(json_decode($event->dados_relevantes,true)[$p_Language][0]['descricao'])}}
                </p>
                @endif
                @if (!trim(json_decode($event->dados_relevantes,true)[$p_Language][1]['dado']) == '')
                <p><span
                        class="oi oi-clipboard"></span>&nbsp;<b>{{trim(json_decode($event->dados_relevantes,true)[$p_Language][1]['dado'])}}:</b> {{trim(json_decode($event->dados_relevantes,true)[$p_Language][1]['descricao'])}}
                </p>
                @endif
                @if (!trim(json_decode($event->dados_relevantes,true)[$p_Language][2]['dado']) == '')
                <p><span
                        class="oi oi-clipboard"></span>&nbsp;<b>{{trim(json_decode($event->dados_relevantes,true)[$p_Language][2]['dado'])}}:</b> {{trim(json_decode($event->dados_relevantes,true)[$p_Language][2]['descricao'])}}
                </p>
                @endif
            </div>
            <!--
            @if (($event->site) || ($event->city_id == 66))
            <div class="col-md-2 order-md-2">
                            @if ($event->city_id == 66)
                                <a href="http://www.carnavaldebelohorizonte.com.br/"   target="_blank">
                            @else
                                <a href="{{$event->site}}"   target="_blank">
                            @endif
                            <img src="{{url('/assets/img/programacaocidade.png')}}" > 
                        </a>     
            </div>    
            @endif
            -->
        </div>

        <hr>

        <div class="row">
            <div class="col-md-7">
                @if (!trim(json_decode($event->sobre,true)[$p_Language]) == '')
                <h2 class="text-bold"  style="font-family:pwchalk; color:#00AEEF;"> SOBRE O EVENTO</h2>
                @if (!trim(json_decode($event->descricao_curta,true)[$p_Language]) == '')
                <p class="text-justify">{{json_decode($event->descricao_curta,true)[$p_Language]}}</p>
                @endif
                <p class="text-justify">{{json_decode($event->sobre,true)[$p_Language]}}</p>
                @endif

                @if (!trim(json_decode($event->informacoes_uteis,true)[$p_Language]) == '')
                <h5 style="font-family:pwchalk; color:#00AEEF;">
                <img  src="{{url('/assets/img/informacoes.png')}}" > 
                  &nbsp; MAIS INFORMAÇÕES</h5>
                <p class="text-justify">{{json_decode($event->informacoes_uteis,true)[$p_Language]}}</p>
                @endif
            </div>

            <div class="col-md-5 text-left">

                    <div class="col-md-12"> 
                        @if (isset($cityPhoto))       
                            <h3 class="text-danger-dark p-2 text-uppercase align-top ">
                                <a class="link-no-hover"
                                    href="@if (isset($citySlug)){{url('/'.$p_Language.'/destinos/'.$citySlug)}}@else{{"#"}}@endif"
                                    style="font-family:pwchalk; color:#00AEEF;">
                                    <span class="oi oi-map-marker"></span> {{$event->City->name}}
                                </a>
                            </h3>
                    </div>
      

                    <div class="col-md-12"> 
                        <a href="@if (isset($citySlug)){{url('/'.$p_Language.'/destinos/'.$citySlug)}}@else{{"#"}}@endif">
                            <img class="img-fluid mx-auto fade-image" src="{{url('/image/450/322/true/true/'.$cityPhoto)}}"
                                alt="">
                        </a>
                        @if (isset($citySlug))
                        <div class="text-center pt-2">
                            <a href="{{url('/'.$p_Language.'/apoio-destino/'.$citySlug.'?atrativos=')}}" class="link-no-hover">
                                <img class="img-fluid" style="height: 32px" src="/portal/assets/imgs/atrativoOrange.png"
                                    alt=""><span class="fnt-gradients"><b>Atrativos</b></span></a>
                            <a href="{{url('/'.$p_Language.'/apoio-destino/'.$citySlug.'?tipo_acomodacao=')}}"
                                class="link-no-hover"> <img class="img-fluid" style="height: 32px"
                                    src="/portal/assets/imgs/camaOrange.gif" alt=""> <span
                                    class="fnt-gradients"><b>Hospedagem</b></span></a>
                        </div>
                        @endif
                        </div>
                    </div>
                </div>
            
            </div>
   




                @endif


            </div>
        </div>



        @endif

        <hr>

        <!-- /END THE FEATURETTES -->

    </section>
</main>
@include('public.carnaval.menuMobile')
@endsection

@section('footer')
@include('public.carnaval.footer')
@endsection

@section('pageScript')
<script type="text/javascript" src="{{url('/portal/assets/libs/portal/js/eventsShow.js')}}"></script>
<script type="text/javascript" src="{{url('/portal/assets/libs/portal/js/eventsSearchbar.js')}}"></script>
@endsection