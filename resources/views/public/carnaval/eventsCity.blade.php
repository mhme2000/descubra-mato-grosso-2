<style>
.btnSearchCity{
        cursor:pointer;
}
</style>

        <div class="container mt-2 fadeInOnLoad" style="display: none;">
        <h1 class="text-center text-black-10 display-4">&nbsp</h1>
                <section class="customer-logos slider overlap-category-title">
                        @if (in_array('86',$cids))
                        <div class="slide text-center btnSearchCity"  city-id="86">
                                        <div class="@if (isset($filter) && isset($filter['city_id']) && $filter['city_id']==86) selected-category @endif  w-75  mt-2 mb-2 mr-auto ml-auto">
                                                <img class="fade-image w-100 mr-auto ml-auto" src="{{url('/assets/img/carnaval/bonfim.png')}}">
                                        </div>
                                        <span class="text-category-span mt-auto mb-auto"><small><b>BONFIM</b></small></span>
                        </div>
                        @endif
                        @if (in_array('97',$cids))
                        <div class="slide text-center btnSearchCity"  city-id="97">
                                        <div class="@if (isset($filter) && isset($filter['city_id']) && $filter['city_id']==97) selected-category @endif  w-75  mt-2 mb-2 mr-auto ml-auto">
                                                <img class="fade-image w-100 mr-auto ml-auto" src="{{url('/assets/img/carnaval/inhotim.png')}}">
                                        </div>
                                        <span class="text-category-span mt-auto mb-auto"><small><b>BRUMADINHO</b></small></span>
                        </div>
                        @endif                        
                        @if (in_array('166',$cids))
                        <div class="slide text-center btnSearchCity"  city-id="166">
                                        <div class="@if (isset($filter) && isset($filter['city_id']) && $filter['city_id']==166) selected-category @endif  w-75  mt-2 mb-2 mr-auto ml-auto">
                                                <img class="fade-image w-100 mr-auto ml-auto" src="{{url('/assets/img/carnaval/cataguases.png')}}">
                                        </div>
                                        <span class="text-category-span mt-auto mb-auto"><small><b>CATAGUASES</b></small></span>
                        </div>
                        @endif
                        @if (in_array('167',$cids))
                        <div class="slide text-center btnSearchCity"  city-id="167">
                                        <div class="@if (isset($filter) && isset($filter['city_id']) && $filter['city_id']==167) selected-category @endif  w-75  mt-2 mb-2 mr-auto ml-auto">
                                                <img class="fade-image w-100 mr-auto ml-auto" src="{{url('/assets/img/carnaval/catasaltas.png')}}">
                                        </div>
                                        <span class="text-category-span mt-auto mb-auto"><small><b>CATAS ALTAS</b></small></span>
                        </div>
                        @endif                                              
                        @if (in_array('241',$cids))
                        <div class="slide text-center btnSearchCity"  city-id="241">
                                        <div class="rounded-circle bg-page @if (isset($filter) && isset($filter['city_id']) && $filter['city_id']==241) selected-category @endif shadow-sm-3 w-75 mt-2 mb-2 mr-auto ml-auto">
                                                <img class="fade-image w-100 mr-auto ml-auto" src="{{url('/assets/img/carnaval/diamantina.png')}}">
                                        </div>
                                        <span class="text-category-span mt-auto mb-auto"><small><b>DIAMANTINA</b></small></span>
                        </div>
                        @endif
                        @if (in_array('281',$cids))
                        <div class="slide text-center btnSearchCity"  city-id="281">
                                        <div class="@if (isset($filter) && isset($filter['city_id']) && $filter['city_id']==281) selected-category @endif  w-75  mt-2 mb-2 mr-auto ml-auto">
                                                <img class="fade-image w-100 mr-auto ml-auto" src="{{url('/assets/img/carnaval/extrema.png')}}">
                                        </div>
                                        <span class="text-category-span mt-auto mb-auto"><small><b>EXTREMA</b></small></span>
                        </div>
                        @endif          
                        @if (in_array('315',$cids))
                        <div class="slide text-center btnSearchCity"  city-id="315">
                                        <div class="@if (isset($filter) && isset($filter['city_id']) && $filter['city_id']==315) selected-category @endif  w-75  mt-2 mb-2 mr-auto ml-auto">
                                                <img class="fade-image w-100 mr-auto ml-auto" src="{{url('/assets/img/carnaval/governadorvaladares.png')}}">
                                        </div>
                                        <span class="text-category-span mt-auto mb-auto"><small><b>GOVERNADOR VALADARES</b></small></span>
                        </div>
                        @endif                                           
                        @if (in_array('363',$cids))
                        <div class="slide text-center btnSearchCity"  city-id="363">
                                        <div class="rounded-circle bg-page @if (isset($filter) && isset($filter['city_id']) && $filter['city_id']==363) selected-category @endif shadow-sm-3 w-75  mt-2 mb-2 mr-auto ml-auto">
                                                <img class="fade-image w-100 mr-auto ml-auto" src="{{url('/assets/img/carnaval/itabirito.png')}}">
                                        </div>
                                       <span class="text-category-span mt-auto mb-auto"><small><b>ITABIRITO</b></small></span>
                        </div>
                        @endif
                        @if (in_array('466',$cids))
                        <div class="slide text-center btnSearchCity"  city-id="466">
                                        <div class="@if (isset($filter) && isset($filter['city_id']) && $filter['city_id']==466) selected-category @endif  w-75  mt-2 mb-2 mr-auto ml-auto">
                                                <img class="fade-image w-100 mr-auto ml-auto" src="{{url('/assets/img/carnaval/martinhocampos.png')}}">
                                        </div>
                                        <span class="text-category-span mt-auto mb-auto"><small><b>MARTINHO CAMPOS</b></small></span>
                        </div>
                        @endif                        
                        @if (in_array('540',$cids))
                        <div class="slide text-center btnSearchCity"  city-id="540">
                                        <div class="rounded-circle bg-page @if (isset($filter) && isset($filter['city_id']) && $filter['city_id']==540) selected-category @endif shadow-sm-3 w-75  mt-2 mb-2 mr-auto ml-auto">
                                                <img class="fade-image w-100 mr-auto ml-auto" src="{{url('/assets/img/carnaval/ouropreto.png')}}">
                                        </div>
                                        <span class="text-category-span mt-auto mb-auto"><small><b>OURO PRETO</b></small></span>
                        </div>
                        @endif
                        @if (in_array('600',$cids))
                        <div class="slide text-center btnSearchCity"  city-id="600">
                                        <div class="@if (isset($filter) && isset($filter['city_id']) && $filter['city_id']==600) selected-category @endif  w-75  mt-2 mb-2 mr-auto ml-auto">
                                                <img class="fade-image w-100 mr-auto ml-auto" src="{{url('/assets/img/carnaval/pirapora.png')}}">
                                        </div>
                                        <span class="text-category-span mt-auto mb-auto"><small><b>PIRAPORA</b></small></span>
                        </div>
                        @endif 
                        @if (in_array('603',$cids))
                        <div class="slide text-center btnSearchCity"  city-id="603">
                                        <div class="@if (isset($filter) && isset($filter['city_id']) && $filter['city_id']==603) selected-category @endif  w-75  mt-2 mb-2 mr-auto ml-auto">
                                                <img class="fade-image w-100 mr-auto ml-auto" src="{{url('/assets/img/carnaval/piumhi.png')}}">
                                        </div>
                                        <span class="text-category-span mt-auto mb-auto"><small><b>PIUMHI</b></small></span>
                        </div>
                        @endif                                              
                        @if (in_array('608',$cids))
                        <div class="slide text-center btnSearchCity" city-id="608">
                                        <div class="rounded-circle bg-page @if (isset($filter) && isset($filter['city_id']) && $filter['city_id']==608) selected-category @endif shadow-sm-3 w-75  mt-2 mb-2 mr-auto ml-auto">
                                                <img class="fade-image w-100 mr-auto ml-auto" src="{{url('/assets/img/carnaval/pompeu.png')}}">
                                        </div>
                                        <span class="text-category-span mt-auto mb-auto"><small><b>POMPÉU</b></small></span>
                        </div>
                        @endif
                        @if (in_array('629',$cids))
                        <div class="slide text-center btnSearchCity"  city-id="629">
                                        <div class="@if (isset($filter) && isset($filter['city_id']) && $filter['city_id']==629) selected-category @endif  w-75  mt-2 mb-2 mr-auto ml-auto">
                                                <img class="fade-image w-100 mr-auto ml-auto" src="{{url('/assets/img/carnaval/raulsoares.png')}}">
                                        </div>
                                        <span class="text-category-span mt-auto mb-auto"><small><b>RAUL SOARES</b></small></span>
                        </div>
                        @endif                        
                        @if (in_array('659',$cids))
                        <div class="slide text-center btnSearchCity" city-id="659">
                                <div class="rounded-circle bg-page @if (isset($filter) && isset($filter['city_id']) && $filter['city_id']==659) selected-category @endif shadow-sm-3 w-75  mt-2 mb-2 mr-auto ml-auto">
                                        <img class="fade-image w-100 mr-auto ml-auto" src="{{url('/assets/img/carnaval/sabara.png')}}">
                                </div>
                                <span class="text-category-span"><small><b>SABARÁ</b></small></span>
                        </div>
                        @endif

                </section>
        </div>

