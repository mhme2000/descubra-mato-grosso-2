<?php
    $v_Languages = ['pt', 'en', 'es', 'fr'];
    if(!in_array($p_Language, $v_Languages))
        $p_Language = 'pt';
?>
<footer id="footer"><!--
    <div class="row-fluid line-footer">
        <center>
        <img class="img-fluid py-0" src="{{url('/portal/assets/imgs/rodape_animado.gif')}}" alt="Mato Grosso">
        </center>
    </div>-->
    <div class="bg-footer">
    
        <div class="row-fluid mapSite ">
            <div class="container">
                <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 no-padding hidden-md-down">
                    <ul class="col-lg-1 col-md-1 col-sm-6 col-xs-12 no-padding" style="margin-bottom:15px;">
                        <li class="list-group-item"><a href="{{url($p_Language . '/conheca')}}" class="list-group-item active">{{trans('menu.discover')}}</a></li>
                        <li class="list-group-item"><a href="{{url($p_Language . '/conheca/galeria')}}">{{trans('menu.gallery')}}</a></li>
                    </ul>
                    <!-- Alterada a classe lg e md, de 2 para 3-->
                    <ul class="col-lg-2 col-md-2 col-sm-6 col-xs-12 " style="margin-bottom:15px;">
                        <li class="list-group-item"><a href="{{url($p_Language . '/o-que-fazer')}}" class="list-group-item active">{{trans('menu.what_to_do')}}</a></li>
                        @foreach($p_FooterCategories as $c_Index => $c_Category)
                            @if($c_Index < 5)
                                <?php $v_Name = json_decode($c_Category->nome,1)[$p_Language]; ?>
                        <li class="list-group-item"><a href="{{url($p_Language . '/o-que-fazer/' . $c_Category->slug)}}">{{$v_Name}}</a></li>
                            @endif
                        @endforeach
                    </ul>
                    <ul class="col-lg-2 col-md-2 col-sm-6 col-xs-12 no-padding" style="margin-bottom:15px;">
                        <li class="list-group-item"><a href="{{url($p_Language . '/para-onde-ir')}}" class="list-group-item active">{{trans('menu.where_to_go')}}</a></li>
                        <li class="list-group-item"><a href="{{url($p_Language . '/roteiros')}}">{{trans('menu.routes')}}</a></li>
                        <li class="list-group-item"><a href="{{url($p_Language . '/destinos')}}">{{trans('menu.destinations')}}</a></li>
                        <li class="list-group-item"><a href="{{url($p_Language . '/parques')}}">{{trans('menu.parks')}}</a></li>
                    </ul>
                    <ul class="col-lg-2 col-md-2 col-sm-6 col-xs-12 no-padding" style="margin-bottom:15px;">
                        <li class="list-group-item"><a href="{{url($p_Language . '/explore-o-mapa')}}" class="list-group-item active">{{trans('menu.explore_the_map')}}</a></li>
                    </ul>
                    <ul class="col-lg-3 col-md-3 col-sm-6 col-xs-12 no-padding" style="margin-bottom:15px;">
                        <li class="list-group-item"><a href="{{url($p_Language . '/planeje-sua-viagem')}}" class="list-group-item active">{{trans('menu.plan_your_trip')}}</a></li>
                        <li class="list-group-item"><a href="{{url($p_Language . '/roteiros-duracao?d=1')}}"><{{trans('menu.routes_duration_1')}}</a></li>
                        <li class="list-group-item"><a href="{{url($p_Language . '/roteiros-duracao?d=2')}}"><{{trans('menu.routes_duration_2')}}</a></li>
                        <li class="list-group-item"><a href="{{url($p_Language . '/roteiros-duracao?d=3')}}">{{trans('menu.routes_duration_3')}}</a></li>
                        <li class="list-group-item"><a href="{{url($p_Language . '/planeje-sua-viagem#como-chegar')}}">{{trans('planYourTrip.directions')}}</a></li>
                    </ul>
                    <ul class="col-lg-2 col-md-2 col-sm-6 col-xs-12 no-padding" style="margin-bottom:15px;">
                        <li class="list-group-item"><a href="{{url($p_Language . '/favoritos')}}" class="list-group-item active">{{trans('menu.favorites')}}</a></li>
                        <!--
					    <li class="list-group-item"><a href="{{url($p_Language . '/minas360')}}" class="list-group-item active">Minas em 360°</a></li>
                        -->
					    <li class="list-group-item"><a href="{{url($p_Language . '/blog')}}" class="list-group-item active">Blog</a></li>
                        <li class="list-group-item"><a href="{{url($p_Language . '/fale-conosco')}}" class="list-group-item active">{{trans('menu.contact')}}</a></li>
                        <li class="list-group-item"><a href="{{url('/admin')}}" class="list-group-item active">{{trans('menu.register_your_business')}}</a></li>
                        <li class="list-group-item"><a href="{{url($p_Language . '/doacao-de-midias')}}" class="list-group-item active">{{trans('menu.media_donation')}}</a></li>
                    </ul>
                </div>
            
                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 hidden-md-down" id="newsletters">
                    <p id="label-news">{{trans('menu.subscribe_newsletter')}}</p>
                    <div class="input-group">
                        {!! Form::open(array('id' => 'newsletterForm', 'onsubmit' => 'return submitNewsletter()')) !!}
                            <input type="email" name="email" id="basic-news" class="form-control" aria-describedby="basic-news" placeholder="{{trans('menu.type_email')}}" required>
                            <input type="submit" id="input-news" value="">
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>

    <!--assinaturas-->

        <div class="row-fluid trademarks-government hidden-md-down">
            <div class="container">
                <!--
                <div class="col-md-6" class="logoGov">
                    <a href="http://www.voeminasgerais.com.br/" title="Voe Minas Gerais" target="_blank" class="pull-right">
                        <img src="{{url('/portal/assets/imgs/logo_voe_minas.png')}}" alt="Voe Minas Gerais" class="logo-voeminas">
                    </a>
                    <a href="http://www.cadastur.turismo.gov.br/cadastur/index.action#" title="Cadastur" target="_blank" class="pull-right">
                        <img src="{{url('/portal/assets/imgs/cadastur.png')}}" alt="Cadastur" class="logo-cadastur">
                    </a>
                </div>
            
                <div class="col-md-6" id="logoGov">
                    <a href="http://www.codemig.com.br/" title="CODEMIG" target="_blank" class="pull-left">
                        <img src="{{url('/portal/assets/imgs/logo_codemig.png')}}" alt="CODEMIG">
                    </a>
                
                    <a href="http://www.turismo.mg.gov.br/" title="Governo de Minas Geras" target="_blank" class="pull-left">
                        <img src="{{url('/portal/assets/imgs/logo_minas_gerais.png')}}" alt="Governo de Minas Geras" class="logo-governominas">
                    </a>
                </div>
                -->
            </div>
        </div>

        <div class="row-fluid trademarks-government hidden-lg-up">
            <div class="container">
                <ul class="nav flex-column">
                    <br/><li class="nav-item"><a class="col-2 ml-5" href="www.sedec.mt.gov.br/" title="Sedec" target="_blank" class="text-center"><img class="img-fluid" src="{{url('portal/assets/imgs/logoSEDEC.png')}}" alt="Cadastur" class="mb-small text-center" ></a></li>
                    <br/><li class="nav-item"><a class="col-2 ml-5" href="http://www.cadastur.turismo.gov.br/cadastur/index.action#" title="Cadastur" target="_blank" class="text-center"><img class="img-fluid" src="{{url('portal/assets/imgs/logoCadastur.png')}}" alt="Cadastur" class="mb-small text-center" ></a></li>
                    <br/><li class="nav-item"><a class="col-2 ml-5" title="PIT" target="_blank" class="text-center"><img class="img-fluid" src="{{url('portal/assets/imgs/logoPIT.png')}}" alt="Cadastur" class="mb-small text-center" ></a></li>
                </ul>
                <!--
                <div class="col-sm-12 text-center">
                    <a href="http://www.cadastur.turismo.gov.br/cadastur/index.action#" title="Cadastur" target="_blank" class="text-center">
                        <img src="{{url('/portal/assets/imgs/cadastur.png')}}" alt="Cadastur" class="mb-small text-center" >
                    </a>
                </div>
                <div class="col-sm-12 text-center">
                    <a href="http://www.voeminasgerais.com.br/" title="Voe Minas Gerais" target="_blank" class="text-center">
                        <img src="{{url('/portal/assets/imgs/logo_voe_minas.png')}}" alt="Voe Minas Gerais" class="mb-small text-center" >
                    </a>
                </div>
                <div class="col-sm-12 text-center">
                    <a href="http://www.codemig.com.br/" title="CODEMIG" target="_blank" class="text-center">
                        <img src="{{url('/portal/assets/imgs/logo_codemig.png')}}" alt="CODEMIG" class="mb-small text-center">
                    </a>
                </div>
                <div class="col-sm-12 text-center"> 
                    <a href="http://www.turismo.mg.gov.br/" title="Governo de Minas Geras" target="_blank" class="text-center">
                        <img src="{{url('/portal/assets/imgs/logo_minas_gerais.png')}}" alt="Governo de Minas Geras" class="mb-small text-center">
                    </a>
                </div>
                -->
            </div>
        </div>

        <div class="container mt-2">
            <div class="text-center text-light">
                <p class="small">Secretaria de Estado de Desenvolvimento Econômico - SEDEC <br/>Secretaria Adjunta de Turismo</p>
            </div>
            <div class="text-center small text-light">
                <p>Rua Voluntários da Pátria, 118 – Bairro Centro Norte – CEP 78.005-180 - Cuiabá - Mato Grosso</p>
				<p>Cedido gratuitamente pela SECRETARIA DE ESTADO DE TURISMO DE MINAS GERAIS e desenvolvido pela empresa de software ORO DIGITAL LTDA.
                    <a class="float-right" href="http://orodigital.com.br/" target="_blank">
                        <img src="{{url('/portal/assets/imgs/oro_ouro.png')}}" alt="Oro Digital" title="Oro Digital" id="ouro">
                    </a>
                </p>
            </div>
        </div>
    </div><!--final -->
</footer>

