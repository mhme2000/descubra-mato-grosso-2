@extends('public.templates.base')
<!DOCTYPE html>
<html lang="pt-br">

    <head>
        <script async
            src="http://analytics.turismo.mg.gov.br/analytics/default/b?url=http%3A%2F%2Fwww%7Cminasgerais%7Ccom%7Cbr%2Fpt%2Fo-que-fazer&ref=http%3A%2F%2Fwww%7Cminasgerais%7Ccom%7Cbr%2Fpt&c=GA1.3.1481861829.1583153592&t=Portal Minas Gerais&env=1">
        </script>

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-125337813-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-125337813-1');
        </script>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="no-cache" http-equiv="pragma">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
        <meta name="description" property="description" content="">
    

        <meta name="image" property="image" content="">
    

        <meta property="og:url" content="http://www.descubramatogrosso.com.br/pt/o-que-fazer">
    <meta property="og:title" content="Portal Minas Gerais">
    <meta property="og:type" content="website">
    <meta property="og:site_name" content="Portal Minas Gerais">
    

        <meta name="twitter:card" content="Portal Minas Gerais">
    <meta name="twitter:site" content="Portal Minas Gerais">
    <meta name="twitter:title" content="Portal Minas Gerais">
    <meta name="twitter:creator" content="Portal Minas Gerais">
    <meta name="twitter:domain" content="www.descubramatogrosso.com.br">
    

                <meta name="image" property="image" content="">
    

                <meta name="author" content="Portal Minas Gerais">
        <link rel="icon" href="http://www.descubramatogrosso.com.br/favicon.ico" />
        <title>Descubra Mato Grosso</title>
        <link rel="stylesheet" type="text/css" href="http://www.descubramatogrosso.com.br/portal/assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="http://www.descubramatogrosso.com.br/portal/assets/css/main.css">
        <style>
            @font-face {
                font-family: 'FontAwesome';
                src: url('http://www.descubramatogrosso.com.br/assets/fonts/font-awesome/fontawesome-webfont78ce.eot?v=4.2.0');
                src: url('http://www.descubramatogrosso.com.br/assets/fonts/font-awesome/fontawesome-webfontd41d.eot?#iefix&amp;v=4.2.0') format('embedded-opentype'),
                url('http://www.descubramatogrosso.com.br/assets/fonts/font-awesome/fontawesome-webfont78ce.woff?v=4.2.0') format('woff'),
                url('http://www.descubramatogrosso.com.br/assets/fonts/font-awesome/fontawesome-webfont78ce.ttf?v=4.2.0') format('truetype'),
                url('http://www.descubramatogrosso.com.br/assets/fonts/font-awesome/fontawesome-webfont78ce.svg?v=4.2.0#fontawesomeregular') format('svg');
                font-weight: normal;
                font-style: normal;
            }

            .fa {
                display: inline-block;
                font: normal normal normal 14px/1 FontAwesome;
                font-size: inherit;
                text-rendering: auto;
                -webkit-font-smoothing: antialiased;
                -moz-osx-font-smoothing: grayscale;
            }

            .fa-chevron-left:before {
                content: "\f053";
            }

            .fa-chevron-right:before {
                content: "\f054";
            }

            #destinos .nav-tabs .nav-item .nav-link.active .hospedagem p {
                color: #ef662f;
            }

            body>.alert.alert-danger {
                border-color: #bd1a3a;
                border-radius: 0;
                background-color: #c71a3d;
                color: #fff;
            }

            body>.alert.alert-info {
                border-color: #b3e1a9;
                border-radius: 0;
                background-color: #b7e6ad;
                color: #555;
            }

            body>.alert>button.close {
                opacity: 0.4;
            }

            .menu-resumido-desk {
                margin-top: -200px;
                -webkit-transition: margin-top 1s;
                /* Safari */
                transition: margin-top 1s;
            }

            .menu-resumido-desk.menu-fixo {
                margin-top: 0;
                display: block !important;

            }

            em {
                font-style: italic;
            }

            @media (max-width: 543px) {
                #destinos {
                    margin-top: 80px;
                    padding: 1% 0;
                    "

                }
            }

            @media (max-width: 980px) {
                .menu-resumido-desk.menu-fixo {
                    margin-top: 0;
                    display: none !important;

                }
            }

            #video-container {
                display: none;
            }

            #titleVisiteMinas {
                display: none;
            }

            @media (min-width: 544px) and (max-width:767) {
                #destinos {

                    padding: 1% 0;
                    "

                }
            }

            @media (min-width: 768px) {
                #destinos {

                    padding: 1% 0;
                    "

                }
            }

            .chamada__carnaval {
                position: fixed;
                left: 0;
                bottom: 58px;


                -webkit-animation-name: chamada;
                -webkit-animation-duration: 0.5s;
                -webkit-animation-timing-function: linear;
                -webkit-animation-direction: alternate;
            }

            @media (max-width:560px){
                .chamada__carnaval img{
                    width:60%;
                }
                .chamada__carnaval.alert-dismissible .close {

                    top: 25px !important;
                    right: 131px !important;
                }
                body > .alert > button.close {
                    opacity: 1;
                }
            }

            @media (min-width:561px) and (max-width:720px){
                .chamada__carnaval img{
                    width:80%;
                }
                .chamada__carnaval.alert-dismissible .close{
                    top: 33px !important;
                    right: 60px !important;
                }
                body > .alert > button.close {
                    opacity: 1;
                }
                
            }

            @keyframes  chamada {
                from {
                    left: -200px;
                }

                to {
                    left: 0;
                }
            }

            .chamada__carnaval.alert-dismissible .close {

                position: absolute;
                top: 42px;
                right: -9px;
                padding: 5px;
                color: inherit;
                background: rgba(255, 147, 24, 0.79);
                border-radius: 50%;
            }
            body > .alert > button.close {
                    opacity: 1;
                }
            .chamada__carnaval.alert{
                padding: unset;
                margin:unset;
            }

.chamada__carnaval.alert-warning{
  background: unset;
  border:unset;
}

.chamada__carnaval .close {

font-size: 1rem;
width:25px;
opacity:unset;
color: white !important;

}

            
        </style>
            </head>

    <body data-spy="scroll" data-target=".navbar-fixed-top">
                <style>
.language-dropdown::after {
    display: inline-block;
    width: 0;
    height: 0;
    margin-left: 0.25rem;
    vertical-align: middle;
    content: "";
    visibility: visible;
    border-top: 0.3em solid;
    border-right: 0.3em solid transparent;
    border-left: 0.3em solid transparent;
}
.dropdown-toggle:after {
    display: none;
}
.open .dropdown-menu.lang-menu{
    display: block !important;
}
#list-city.favorites-list li a{
    color:#fff;
}
    
#nav-icon1{
  width: 30px;
  height: 25px;
  position: relative;
  /*margin: 50px auto;*/
  -webkit-transform: rotate(0deg);
  -moz-transform: rotate(0deg);
  -o-transform: rotate(0deg);
  transform: rotate(0deg);
  -webkit-transition: .5s ease-in-out;
  -moz-transition: .5s ease-in-out;
  -o-transition: .5s ease-in-out;
  transition: .5s ease-in-out;
  cursor: pointer;
}

#nav-icon1 span {
  display: block;
  position: absolute;
  height: 6px;
  width: 100%;
  background: #666;
  border-radius: 0px;
  opacity: 1;
  left: 0;
  -webkit-transform: rotate(0deg);
  -moz-transform: rotate(0deg);
  -o-transform: rotate(0deg);
  transform: rotate(0deg);
  -webkit-transition: .25s ease-in-out;
  -moz-transition: .25s ease-in-out;
  -o-transition: .25s ease-in-out;
  transition: .25s ease-in-out;
}

#nav-icon1 span:nth-child(1) {
  top: 0px;
}

#nav-icon1 span:nth-child(2) {
  top: 10px;
}

#nav-icon1 span:nth-child(3) {
  top: 20px;
}

#nav-icon1.open span:nth-child(1) {
  top: 12px;
  -webkit-transform: rotate(135deg);
  -moz-transform: rotate(135deg);
  -o-transform: rotate(135deg);
  transform: rotate(135deg);
}

#nav-icon1.open span:nth-child(2) {
  opacity: 0;
  left: -60px;
}

#nav-icon1.open span:nth-child(3) {
  top: 12px;
  -webkit-transform: rotate(-135deg);
  -moz-transform: rotate(-135deg);
  -o-transform: rotate(-135deg);
  transform: rotate(-135deg);
}
.link-gastronomia{
    width: 110px;
    margin: -4px 0 0px 0;
}


</style>
<nav class="navbar navbar-custom navbar-fixed-top" role="navigation" style="padding:0;">
   
    <header class=" container-fluid bg shadow-sm-1" style="background-color:white;">
        <nav class="navbar navbar-expand-lg bg-white navbar-light"> 
            <a class="navbar-brand mb-0" href="{{url($p_Language)}}">
                <img class="img-fluid" src="{{url('/portal/assets/imgs/descubraMatoGrosso.png')}}" alt="Descubra Mato Grosso" title="Descubra Mato Grosso">
            </a>
            <button class="navbar-toggler ml-auto" type="button" data-toggle="collapse" data-target="#navbarPortal">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarPortal" style="overflow:auto;">
                <ul class="navbar-nav small mr-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="{{url($p_Language . '/para-onde-ir')}}" style="font-size: 18px; color: #0000FF; font-family: Trebuchet MS;""><strong>Onde ir</strong></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{url($p_Language . '/o-que-fazer')}}" style="font-size: 18px; color: #0000FF; font-family: Trebuchet MS;""><strong>Atrações</strong></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{url($p_Language . '/eventos')}}" style="font-size: 18px; color: #0000FF; font-family: Trebuchet MS;""><strong>Eventos</strong></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{url($p_Language . '/explore-o-mapa')}}" style="font-size: 18px; color: #0000FF; font-family: Trebuchet MS;""><strong>Mapa</strong></a>
                    </li>
                </ul>
                <style>
                    a:hover {
                        text-transform:uppercase;
                        
                    }
                </style>
                <ul class="navbar-nav">
                    <li class="nav-item pl-3 d-none d-md-flex w-100">
                        <form method="GET" action="http://localhost:8000/pt/busca"  accept-charset="UTF-8" class="form-inline py-1">
                            
                            <input type="text" name="q" class="form-control" placeholder="Buscar..." aria-label="" aria-describedby="btnGroupAddon2">
                                
                                    <button type="submit" class="btn btn-outline-secondary" id="btnGroupAddon2"><span class="oi oi-magnifying-glass"></span></button>
                                
                                                                                                             
                    </form>
                    </li>
                </ul>
                <ul class="navbar-nav small my-2 my-lg-0 pl-5">
                    <li class="nav-item">
                        <a class="nav-link" href="https://www.facebook.com/descubraMatoGrosso/" target="_blank">
                            <img src="{{url('/portal/assets/imgs/facebookGray.png')}}" alt="Facebook" title="Facebook">
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="https://www.instagram.com/descubramatogrosso/" target="_blank">
                            <img src="{{url('/portal/assets/imgs/instagramGray.png')}}" alt="Instagram" title="Instagram">
                        </a>
                    </li>
                    <!--<li class="nav-item">
                        <a class="nav-link" href="{{url('/pt')}}"><img src="{{url('/portal/assets/imgs/flag-' . "pt" . '.png')}}"></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('/fr')}}"><img class="lang-flag" src="{{url('/portal/assets/imgs/flag-' . "fr" . '.png')}}"></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('/en')}}"><img class="lang-flag" src="{{url('/portal/assets/imgs/flag-' . "en" . '.png')}}"></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('/es')}}"><img class="lang-flag" src="{{url('/portal/assets/imgs/flag-' . "es" . '.png')}}"></a>
                    </li>-->
                </ul>
                <!-- Futuramente quando precisar
                    <li class="nav-item">
                        <a class="nav-link" href="{{url($p_Language . '/roteiros-duracao')}}">{{trans('menu.plan_your_trip')}}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{url($p_Language . '/explore-o-mapa')}}">{{trans('menu.explore_the_map')}}</a>
                    </li>
                    <li class="nav-item">
                            <a class="nav-link strings px-1" href="{{url($p_Language . '/blog')}}">Blog</a>
                    </li> 
                <ul class="navbar-nav ">
                    <li class="nav-item">
                        <div class="dropdown">
                            <button class="btn btn-light dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img class="lang-flag" src="{{url('/portal/assets/imgs/flag-' . $p_Language . '.png')}}">
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                <a class="dropdown-item" href="{{url('/pt')}}"><img class="lang-flag" src="{{url('/portal/assets/imgs/flag-' . "pt" . '.png')}}"> Português</a>
                                <a class="dropdown-item disabled" href="{{url('/fr')}}"><img class="lang-flag" src="{{url('/portal/assets/imgs/flag-' . "fr" . '.png')}}"> Français</a>
                                <a class="dropdown-item disabled" href="{{url('/en')}}"><img class="lang-flag" src="{{url('/portal/assets/imgs/flag-' . "en" . '.png')}}"> English</a>
                                <a class="dropdown-item disabled" href="{{url('/es')}}"><img class="lang-flag" src="{{url('/portal/assets/imgs/flag-' . "es" . '.png')}}"> Español</a>
                            </div>
                        </div>
                    </li>
                </ul>-->
            </div>
        </nav>
    </header>
   

</nav>                                
    <div class="row-fluid" id="useful-information">
        <div class="container">
            <div class="col-lg-12">
                <h2 style="color:#0B9BC4" >O que fazer</h2>
            </div>
        </div>
    </div>
    <div class="col-lg-12">
        <h2 style="color:#b4b4b4; text-align:center">
            Mato Grosso tem a viagem ideal para voc&ecirc;
        </h2>
        <p style="text-align:center;margin-bottom:30px;">
            
        </p>
    </div>
    <div class="row-fluid " id="destinos" style="padding:1% 0;">
        <div class="container">
            <div class="col-lg-12">
                <div class="col-lg-8 col-lg-offset-2">
                    <div class="col-lg-12">
                        <p></p>
                    </div>
                </div>
            </div>

            <div class="col-lg-12 line no-padding">
                                                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 list-thumbs">
                        <a href="">
                            <div class="hoverzoom">
                                <div class="thumbs-mini-three">
                                    <!--<img src="http://descubramatogrosso.com.br/imagens/tipo-viagem/1535996078sqYxh4KRQa.jpg">-->
                                    <div class="thumbs-mini-recorte" style="background: url('http://descubramatogrosso.com.br/imagens/tipo-viagem/1535996078sqYxh4KRQa.jpg') no-repeat;"></div>
                                </div>
                                <div class="retina-hover">
                                    <div class="col-lg-12 title">
                                        <p>Pantanal</p>
                                    </div>
                                    <div class="col-lg-12 no-padding">
                                        <hr>
                                    </div>
                                    
                                </div>
                                <div class="retina">
                                    <p>Pantanal</p>
                                </div>
                            </div>
                        </a>
                    </div>
                                                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 list-thumbs">
                        <a href="">
                            <div class="hoverzoom">
                                <div class="thumbs-mini-three">
                                    <!--<img src="http://descubramatogrosso.com.br/imagens/tipo-viagem/1535996875OHsSuK5sh5.jpg">-->
                                    <div class="thumbs-mini-recorte" style="background: url('http://descubramatogrosso.com.br/imagens/tipo-viagem/1535996875OHsSuK5sh5.jpg') no-repeat;"></div>
                                </div>
                                <div class="retina-hover">
                                    <div class="col-lg-12 title">
                                        <p>Cerrado</p>
                                    </div>
                                    <div class="col-lg-12 no-padding">
                                        <hr>
                                    </div>
                                    <div class="col-lg-12 text">
                                        <p></p>
                                    </div>
                                </div>
                                <div class="retina">
                                    <p>Cerrado</p>
                                </div>
                            </div>
                        </a>
                    </div>
                                                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 list-thumbs">
                        <a href="">
                            <div class="hoverzoom">
                                <div class="thumbs-mini-three">
                                    <!--<img src="http://descubramatogrosso.com.br/imagens/tipo-viagem/15359990529cOpnyDW8l.jpg">-->
                                    <div class="thumbs-mini-recorte" style="background: url('http://descubramatogrosso.com.br/imagens/tipo-viagem/15359990529cOpnyDW8l.jpg') no-repeat;"></div>
                                </div>
                                <div class="retina-hover">
                                    <div class="col-lg-12 title">
                                        <p>Amazônia</p>
                                    </div>
                                    <div class="col-lg-12 no-padding">
                                        <hr>
                                    </div>
                                    <div class="col-lg-12 text">
                                        <p></p>
                                    </div>
                                </div>
                                <div class="retina">
                                    <p>Amazônia</p>
                                </div>
                            </div>
                        </a>
                    </div>
                                                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 list-thumbs">
                        <a href="">
                            <div class="hoverzoom">
                                <div class="thumbs-mini-three">
                                    <!--<img src="http://descubramatogrosso.com.br/imagens/tipo-viagem/1535997978L5KwE2RLbe.jpg">-->
                                    <div class="thumbs-mini-recorte" style="background: url('http://descubramatogrosso.com.br/imagens/tipo-viagem/1535997978L5KwE2RLbe.jpg') no-repeat;"></div>
                                </div>
                                <div class="retina-hover">
                                    <div class="col-lg-12 title">
                                        <p>Araguaia</p>
                                    </div>
                                    <div class="col-lg-12 no-padding">
                                        <hr>
                                    </div>
                                    <div class="col-lg-12 text">
                                        <p></p>
                                    </div>
                                </div>
                                <div class="retina">
                                    <p>Araguaia</p>
                                </div>
                            </div>
                        </a>
                    </div>
                            </div>

                            <div class="col-lg-12 no-padding">
                   
                    
                                            
                </div>
                    </div>
    </div>
    <footer class="text-muted">
        <div class="row-fluid line-footer">
            <img class="img-fluid py-0" src="{{url('/portal/assets/imgs/_ondas.png')}}" alt="">
        </div>
        <div class="container">
            <div class="row">
                <!--<div class="col-12 col-sm-6 col-md-3 text-justify" style="width: 18rem;">                     
                    <h6 style="font-size: 16px">CONTATOS:</h6>
                    <p style="font-size: 14px">+55 65 3613-9300 / 3613-9313</p>
                    <h6 style="font-size: 16px">E-MAIL:</h6>
                    <p style="font-size: 14px">marketing@sedec.mt.gov.br</p>
                    <p style="font-size: 14px">Rua Voluntários da Pátria, 118 Centro Norte - Cuiabá - MT CEP: 780005-000</p>                
                </div>-->
                <div class="col-12 col-sm-6 col-md-3 py-5 text-left pl-5">            
                    
                        
                    
                    <a class="nav-link" href="{{url($p_Language . '/fale-conosco')}}" style="color: white; font-family: Trebuchet MS;"><strong>Fale conosco</strong></a>
                    <a class="nav-link" href="{{url($p_Language . '/conheca/galeria')}}" style="color: white; font-family: Trebuchet MS;"><strong>Galeria</strong></a>
                    <a href="{{url($p_Language . '/doacao-de-midias')}}" class="nav-link" style="color: white; font-family: Trebuchet MS;"><strong>Doe suas fotos/vídeos</strong></a>   
                    <a class="nav-link" href="{{url('/admin/login')}}" target="_blank" ><h2 style="font-size: 19px; color: orange; font-family: Trebuchet MS;">Área do Prestador</h2></a>
                </div>
                <div class="col-12 col-sm-6 col-md-3 py-5 text-center" style="width: 18rem;">
                    <a href="#" role="button">
                        <img class="img-fluid" src="{{url('/portal/assets/imgs/descubraMatoGrosso.png')}}" alt="Descubra Mato Grosso">
                    </a>                                             
                </div>
                <div class="col-12 col-sm-6 col-md-3 py-5 text-center" style="width: 18rem;">
                    <a href="http://www.sedec.mt.gov.br/" target="_blank" role="button">
                        <img class="img-fluid" src="{{url('/portal/assets/imgs/sedec.png')}}" alt="SEDEC">
                    </a>                                               
                </div>
                <div class="col-12 col-sm-6 col-md-3 py-5 text-center" style="width: 18rem;">
                    <a href="https://cadastur.turismo.gov.br/hotsite/" target="_blank" role="button">
                        <img class="img-fluid" src="{{url('/portal/assets/imgs/logoCadastur.png')}}" alt="cadastur">
                    </a>                                               
                </div>
            </div>
        </div>
        <div class="text-center">
            <div class="container">                  
                <h2 style="font-size: 15px; color:white; margin-bottom:0px;">Secretaria Adjunta de Turismo</h2>
                <p style="font-size: 14px; margin-bottom:0px;">Rua Voluntários da Pátria, 118 - Centro Norte - Cuiabá/MT - CEP 78.005-180</p>                
                <p style="font-size: 14px; ">+55 (65) 3613-9320 / +55 (65) 3613-9300</p>
                <p style="font-size: 12px">Cedido gratuitamente pela Secretaria de Estado de Turismo de Minas Gerais e desenvolvido pela empresa de software Oro Digital LTDA. </p>
            </div>
        </div>
    </footer>
    <style>
    footer {
        background-color: #009DC7;
    }
    </style>

        <!-- javascript -->
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script type="text/javascript" src="https://code.jquery.com/ui/1.11.0/jquery-ui.min.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?language=pt-BR&key=AIzaSyBLc_8beOK5wVSM2NeSk1Bl4mLwtq6jtGw"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.3.1/jquery.maskedinput.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/jquery.validate.min.js"></script>
<script type="text/javascript" src="http://www.descubramatogrosso.com.br/portal/assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="http://www.descubramatogrosso.com.br/portal/assets/js/all.js"></script>
<script src="https://unpkg.com/minigrid@3.1.1/dist/minigrid.min.js"></script>



<script type="text/javascript">
    (function(){
        var grid;
        function init() {
            grid = new Minigrid({
                container: '.cards',
                item: '#services',
                gutter: 6
            });
            grid.mount();
        }

        // mount
        function update() {
            grid.mount();
        }

        document.addEventListener('DOMContentLoaded', init);
        window.addEventListener('resize', update);
    })();
</script>
<script src="http://www.descubramatogrosso.com.br/vendor/bootstrap-notify/bootstrap-notify.min.js"></script>

<script>
    function showNotify(p_Type, p_Message)
    {
        $.notify({
            message: p_Message
        },{
            type: p_Type,
            delay: 8000,
            z_index: 9999,
            placement:{
                align: 'center'
            }
        });
    }

    function submitNewsletter()
    {
        $.post("http://www.descubramatogrosso.com.br/pt/newsletter", {email: $('#basic-news').val(), _token: $('#newsletterForm input[name="_token"]').val()})
            .done(function(p_Data)
            {
                showNotify(p_Data[0] ? 'info' : 'danger', p_Data[1]);
            });
        return false;
    }

//    function focusOnModal()
//    {
//        if($('#myModal form input[name="q"]').is(':visible'))
//            $('#myModal form input[name="q"]').focus();
//    }
//    window.setInterval(focusOnModal, 250);

    function setCookie(p_Key,p_Value,p_Days)
    {
        var v_Expires = "";
        if (p_Days)
        {
            var v_Date = new Date();
            v_Date.setTime(v_Date.getTime() + (p_Days*24*60*60*1000));
            v_Expires = "; expires=" + v_Date.toGMTString();
        }

        document.cookie = p_Key + "=" + p_Value + v_Expires + "; path=/";
    }

    function getCookie(p_Key)
    {
        var p_KeyEQ = p_Key + "=";
        var v_CookieArray = document.cookie.split(';');
        for(var i=0; i < v_CookieArray.length; i++)
        {
            var v_Cookie = v_CookieArray[i];
            while (v_Cookie.charAt(0)==' ')
                v_Cookie = v_Cookie.substring(1, v_Cookie.length);
            if (v_Cookie.indexOf(p_KeyEQ) == 0)
                return v_Cookie.substring(p_KeyEQ.length, v_Cookie.length);
        }
        return null;
    }


    function toggleFavorite(p_Url, p_Name, p_Img, p_Description)
    {
        var v_IsFavorite = false;
        var v_FavoritesList = JSON.parse(localStorage.getItem("favoritesList")) || [];
        $(v_FavoritesList).each(function(c_Index, c_Item){
            if(c_Item.url == p_Url)
            {
                v_FavoritesList.splice(c_Index, 1);
                v_IsFavorite = true;
                return false;
            }
        });
        if(!v_IsFavorite)
            v_FavoritesList.push({url: p_Url,name:p_Name,img:p_Img,description:p_Description});
        
        localStorage.setItem("favoritesList",JSON.stringify(v_FavoritesList));
        updateFavoriteList();
    }

    function isFavorite(p_Url)
    {
        var v_IsFavorite = false;
        var v_FavoritesList = JSON.parse(localStorage.getItem("favoritesList")) || [];
        $(v_FavoritesList).each(function(c_Index, c_Item){
            if(c_Item.url == p_Url)
            {
                v_IsFavorite = true;
                return false;
            }
        });
        return v_IsFavorite;
    }

    $(document).ready(function()
    {
        updateFavoriteList();

                            });

    function updateFavoriteList()
    {
        var v_FavoritesList = JSON.parse(localStorage.getItem("favoritesList")) || [];
        v_FavoritesList = v_FavoritesList.slice(Math.max(v_FavoritesList.length - 4, 0));

        var v_FavoritesString = '';
        $(v_FavoritesList).each(function(c_Index, c_Item){
            v_FavoritesString += '<li>' +
                                    '<a href="' + c_Item.url + '">' +
                                        c_Item.name +
                                    '</a>' +
                                '</li>';
        });
        $('.favorites-list').html(v_FavoritesString);
        $('#toggleFavorite span:last').html((isFavorite(location.href) ? 'Remova dos favoritos' : 'Adicione aos favoritos'));
    }

      // Nao deve ser usado 0o UA-24127191-2
    // o correto e o UA-69793661-1

  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-69793661-1', 'auto');
  ga('send', 'pageview');


    $(function(){   
        var nav = $('.menu-resumido-desk');   
        $(window).scroll(function () { 
            if ($(this).scrollTop() > 155) { 
                nav.addClass("menu-fixo"); 
            } else { 
                nav.removeClass("menu-fixo"); 
            } 
        });  
    });

    window.onload = function() {
        if(location.href == 'http://www.descubramatogrosso.com.br/pt'){
            // Video
            var video = document.getElementById("video");

            // Buttons
            var playButton = document.getElementById("play-pause");
            var playButtondois = document.getElementById("play-pause-dois");
            var muteButton = document.getElementById("mute");
            var fullScreenButton = document.getElementById("full-screen");

            // Sliders
            var seekBar = document.getElementById("seek-bar");
            var volumeBar = document.getElementById("volume-bar");


            // Event listener for the play/pause button
            playButton.addEventListener("click", function() {
                if (video.paused == true) {
                    // Play the video
                    video.play();
                    playButtondois.innerHTML = "Pause";
                    // Update the button text to 'Pause'
                   // playButton.innerHTML = "Pause";
                } else {
                    // Pause the video
                    video.pause();

                    // Update the button text to 'Play'
                    //playButton.innerHTML = "Play";
                }
            });

            // Event listener for the play/pause button
            playButtondois.addEventListener("click", function() {
                if (video.paused == true) {
                    // Play the video
                    video.play();

                    // Update the button text to 'Pause'
                    playButtondois.innerHTML = "Pause";
                } else {
                    // Pause the video
                    video.pause();
                    $('.btn_video').addClass("show");
                     $('#video-controls').removeClass("show");
                    // Update the button text to 'Play'
                    playButtondois.innerHTML = "Play";
                }
            });


            // Event listener for the mute button
            muteButton.addEventListener("click", function() {
                if (video.muted == false) {
                    // Mute the video
                    video.muted = true;

                    // Update the button text
                    muteButton.innerHTML = "Unmute";
                } else {
                    // Unmute the video
                    video.muted = false;

                    // Update the button text
                    muteButton.innerHTML = "Mute";
                }
            });


            // Event listener for the full-screen button
            fullScreenButton.addEventListener("click", function() {
                if (video.requestFullscreen) {
                    video.requestFullscreen();
                } else if (video.mozRequestFullScreen) {
                    video.mozRequestFullScreen(); // Firefox
                } else if (video.webkitRequestFullscreen) {
                    video.webkitRequestFullscreen(); // Chrome and Safari
                }
            });


            // Event listener for the seek bar
            seekBar.addEventListener("change", function() {
                // Calculate the new time
                var time = video.duration * (seekBar.value / 100);

                // Update the video time
                video.currentTime = time;
            });


            // Update the seek bar as the video plays
            video.addEventListener("timeupdate", function() {
                // Calculate the slider value
                var value = (100 / video.duration) * video.currentTime;

                // Update the slider value
                seekBar.value = value;
            });

            // Pause the video when the seek handle is being dragged
            seekBar.addEventListener("mousedown", function() {
                video.pause();
            });

            // Play the video when the seek handle is dropped
            seekBar.addEventListener("mouseup", function() {
                video.play();
            });

            // Event listener for the volume bar
            volumeBar.addEventListener("change", function() {
                // Update the video volume
                video.volume = volumeBar.value;
            });
        }
    }
</script>        
        
    </body>

</html>