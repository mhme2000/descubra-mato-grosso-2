@extends('public.base')
@section('pageCSS')
@stop

@section('main-content')
    <?php
    
        $v_ContentName = json_decode($p_Content->nome,1)[$p_Language];
        $v_ShortDescription = json_decode($p_Content->descricao_curta,1)[$p_Language];
        $v_Description = json_decode($p_Content->descricao,1)[$p_Language];
     ?>

    <div class="row-fluid destino-real" id="destinos" style="padding:0;">
        <div class="col-lg-12 no-padding">
            <div class="caption" style="opacity:1;width: 100%;height: 100%;top:0;">
                <div class="caption-content">
                    <div class="container" id="map-destinos">
                        <div class="col-lg-12 no-padding">
                            <div class="col-lg-12 text-highlight">
                                <img id="tempImg" alt="" style="margin-left:auto;margin-right:auto;display:block;">
                                <p id="temp" style="text-align:center;font-size:30px;padding-top:10px;"></p>
                                <h2 id="nameCity" style="text-align:center;">{{$p_Content == null ? '' : $v_ContentName}}</h2>
                                <a class="text-highlight" href="{{url($p_Language . '/destinos/' . $p_Destination->slug)}}">
                                    <p style="text-align:center;font-size:28px;">
                                            <img src="{{url('/portal/assets/imgs/geotag-white.png')}}" alt="" style="margin-right:10px;margin-top: -8px;">
                                            {{$p_Content == null ? '' : $p_Destination->nome}}
                                    </p>
    
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-12 no-padding favorite">
                            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 no-padding" id="tripadvisor">
                                <p id="toggleFavorite">
                                    <a href="javascript:void(0)">
                                        <span class="icon-adicionar_favoritos"></span>
                                        <span class="text"></span>
                                    </a>
                                </p>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 no-padding" id="favoritos">
                                <div class="social">
                                    <ul id="icons-social" style="float:left;">
                                        <div class="addthis_sharing_toolbox"></div>
                                    </ul>
                                    <p id="share">{{trans('string.share')}}</p>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 no-padding" id="share">
                                <p>
                                    <a id="directions" href="#" target="_blank">
                                        <span class="icon-como_chegar"></span>
                                        <span class="text">{{trans('planYourTrip.directions')}}</span>
                                    </a>
                                </p>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 no-padding" id="mapa">
                                {{--@if($p_MapPhoto != null)--}}
                                    {{--<img src="{{ $p_MapPhoto->url }}" alt="" style="position:relative;bottom:90px;">--}}
                                {{--@endif--}}
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <!-- FRAGMENTO ATUALIZADO -->
            <div class="highlight-active-larger">
                <!--<img src="{{$p_Content->url}}" alt="">-->
                <div style="background: url('{{$p_Content->url}}') no-repeat;" class="corte-destaque-interna"></div>
            </div>
            <!-- FRAGMENTO ATUALIZADO -->
        </div>
    </div>

    {{--<!-- FRAGMENTO ATUALIZADO -->--}}
    {{--@if($p_Content != null && strlen($v_Testimonial) > 0)--}}
    {{--<div class="row-fluid" id="testimonials" style="margin-top:0;">--}}
        {{--<div class="container">--}}
            {{--<div class="col-lg-2 col-lg-offset-2">--}}
                {{--@if($p_TestimonialPhoto != null)--}}
                    {{--<img src="{{ $p_TestimonialPhoto->url }}" alt="">--}}
                {{--@endif--}}
            {{--</div>--}}
            {{--<div class="col-lg-8">--}}
                {{--<p>{!! $v_Testimonial !!}</p>--}}
                {{--<span>{{ $v_TestimonialAuthor }}</span>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
    {{--@endif--}}
    {{--<!-- FRAGMENTO ATUALIZADO -->--}}

    <div class="row-fluid" style="margin:4% 0;">
        <div class="container">

            {{--<div class="col-lg-12 title">--}}
                {{--<h2 style="color:#b4b4b4;font-size:48px;font-family: Signika;font-weight:400;text-align:center;">--}}
                    {{--{!! $p_Content == null ? '' : $v_Title !!}--}}
                {{--</h2>--}}
            {{--</div>--}}

            <div class="col-lg-12" style="margin:1% 0;">
                <p style="margin-bottom:15px;">{!! $p_Content == null ? '' : $v_Description !!}</p>
            </div>
        </div>
    </div>

    <div class="row-fluid" id="localization" style="display:block;">
        <div class="container">
            <div class="content-address" style="margin-bottom: 3%;">
                <div class="col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-4" id="contactArea" style="padding:2% 0;border-top:1px solid #dadada;border-bottom:1px solid #dadada;">
                    <?php $v_HasContact = false; ?>
                    @if(strlen($p_Content->telefone) > 0)
                        <?php $v_HasContact = true; ?>
                        <div class="col-lg-12" style="margin:0.2rem 0;">
                            <p>Tel.: {{$p_Content->telefone}}</p>
                        </div>
                    @endif

                    @if(strlen($p_Content->whatsapp) > 0)
                        <?php $v_HasContact = true; ?>
                        <div class="col-lg-12" style="margin:0.2rem 0;">
                            <p>WhatsApp.: {{$p_Content->whatsapp}}</p>
                        </div>
                    @endif
                    @if(strlen($p_Content->site) > 0)
                        <?php $v_HasContact = true; ?>
                        <div class="col-lg-12" style="margin:0.2rem 0;">
                            <p><a href="{{$p_Content->site}}">{{$p_Content->site}}</a></p>
                        </div>
                    @endif
                    @if(strlen($p_Content->site) > 0)
                        <?php $v_HasContact = true; ?>
                        <div class="col-lg-12" style="margin:0.2rem 0;">
                            <p><a href="mailto:{{$p_Content->email}}">{{$p_Content->email}}</a></p>
                        </div>
                    @endif
                    <div class="col-lg-12" style="display:flex;margin:0.5rem 0;">
                        <ul class="canais">
                            @if(strlen($p_Content->facebook) > 0)
                                <?php $v_HasContact = true; ?>
                                <li style="float: left;margin-right: 5px;"><a href="{{$p_Content->facebook}}" target="_blank"><img src="{{url('/portal/assets/imgs/icon-fb-gray.png')}}" alt=""></a></li>
                            @endif
                            @if(strlen($p_Content->instagram) > 0)
                                <?php $v_HasContact = true; ?>
                                <li style="float: left;margin-right: 5px;"><a href="{{$p_Content->instagram}}" target="_blank"><img src="{{url('/portal/assets/imgs/icon-insta-gray.png')}}" alt=""></a></li>
                            @endif
                            @if(strlen($p_Content->twitter) > 0)
                                <?php $v_HasContact = true; ?>
                                <li style="float: left;margin-right: 5px;"><a href="{{$p_Content->twitter}}" target="_blank"><img src="{{url('/portal/assets/imgs/icon-tw-gray.png')}}" alt=""></a></li>
                            @endif
                            @if(strlen($p_Content->youtube) > 0)
                                <?php $v_HasContact = true; ?>
                                <li style="float: left;margin-right: 5px;"><a href="{{$p_Content->youtube}}" target="_blank"><img src="{{url('/portal/assets/imgs/icon-youtube-gray.png')}}" alt=""></a></li>
                            @endif
                            @if(strlen($p_Content->flickr) > 0)
                                <?php $v_HasContact = true; ?>
                                <li style="float: left;margin-right: 5px;"><a href="{{$p_Content->flickr}}" target="_blank"><img src="{{url('/portal/assets/imgs/icon-flicker-gray.png')}}" alt=""></a></li>
                            @endif
                        </ul>
                    </div>
                </div>
                @if(!$v_HasContact)
                    <style>
                        #contactArea{
                            display:none!important;
                        }
                    </style>
                @endif
                <div class="col-lg-4 col-lg-offset-4" style="padding:2% 0;">
                    <p>{{$p_Content->logradouro . ', ' . $p_Content->numero . (empty($p_Content->complemento) ? '' : (', ' . $p_Content->complemento)) . ' - ' . $p_Content->bairro}}</p>
                    <p>CEP {{$p_Content->cep . ' - ' . $p_Content->cidade}} - MG</p>
                </div>                
            </div>
        </div>
    </div>

    <div class="row-fluid" id="destinos" style="padding:1% 0;">
        <div class="container">
            @if(count($p_CityAttractions) > 0)
                <div class="col-lg-12">
                    <h2>{{trans('destination.main_attractions')}}</h2>
                </div>

                <!-- FRAGMENTO ATUALIZADO -->
                <div class="col-lg-12 line">
                    @foreach($p_CityAttractions as $c_Index => $c_Attraction)
                        @if($c_Index < 2)
                        <?php
                            $v_Description = json_decode($c_Attraction->descricao_curta,1)[$p_Language];
                            if($c_Attraction->trade)
                                $v_Name = $c_Attraction->nome;
                            else
                                $v_Name = json_decode($c_Attraction->nome,1)[$p_Language];
                        ?>
                        <div class="col-lg-6 list-thumbs-full">
                            <a href="{{url($p_Language . '/atracoes/'. get_city_slug($c_Attraction->city_id) .  '/' .$c_Attraction->slug)}}">
                                <div class="hoverzoom">
                                    <div class="thumbs-full">
                                        <img src="{{$c_Attraction->url}}">
                                    </div>
                                    <div class="retina-hover">
                                        <div class="col-lg-12 title">
                                            <p>{{$v_Name}}</p>
                                        </div>
                                        <div class="col-lg-12 no-padding">
                                            <hr>
                                        </div>
                                        <div class="col-lg-12 text">
                                            <p>{{$v_Description}}</p>
                                        </div>
                                    </div>
                                    <div class="retina">
                                        <p>{{$v_Name}}</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                        @endif
                    @endforeach
                </div>
                <div class="col-lg-12 line">
                    @foreach($p_CityAttractions as $c_Index => $c_Attraction)
                        @if($c_Index >= 2)
                        <?php
                            $v_Description = json_decode($c_Attraction->descricao_curta,1)[$p_Language];
                            if($c_Attraction->trade)
                                $v_Name = $c_Attraction->nome;
                            else
                                $v_Name = json_decode($c_Attraction->nome,1)[$p_Language];
                        ?>
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <a href="{{url($p_Language . '/atracoes/' . get_city_slug($c_Attraction->city_id) .  '/' .$c_Attraction->slug)}}">
                                <div class="hoverzoom">
                                    <div class="thumbs-mini-three">
                                        <!--<img src="{{$c_Attraction->url}}">-->
                                        <div class="thumbs-mini-recorte" style="background: url('{{$c_Attraction->url}}') no-repeat;"></div>
                                    </div>
                                    <div class="retina-hover">
                                        <div class="col-lg-12 title">
                                            <p>{{$v_Name}}</p>
                                        </div>
                                        <div class="col-lg-12 no-padding">
                                            <hr>
                                        </div>
                                        <div class="col-lg-12 text">
                                            <p>{{$v_Description}}</p>
                                        </div>
                                    </div>
                                    <div class="retina">
                                        <p>{{$v_Name}}</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                        @endif
                    @endforeach
                </div>
                <!-- FRAGMENTO ATUALIZADO -->

                @if(count($p_CityAttractions) == 5)
                <div class="col-lg-12" id="call-to-action">
                    <a href="{{url($p_Language . '/apoio-destino/' . $p_Destination->slug . '?tipo_atracao=')}}">{{trans('destination.all_attractions')}}</a>
                </div>
                @endif
            @endif
        </div>

        <div class="row-fluid" style="margin-bottom:30px;">
            @if(count($p_PhotoGallery) > 0)
            <div class="container">
                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner" role="listbox">
                        @foreach($p_PhotoGallery as $c_Index => $c_Photo)
                        <div class="carousel-item {{$c_Index == 0 ? 'active' : ''}}">
                            <img src="{{$c_Photo->url}}" alt="First slide">
                            <!--
                            <div class="carousel-caption">
                                <div class="col-lg-12" id="call-to-action-home" style="margin:0;">
                                    <a href="">{{trans('institutional.slideshow')}}</a>
                                </div>
                            </div>
                            -->
                        </div>
                        @endforeach
                    </div>
                    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev" style="opacity: 1;">
                        <span class="icon-prev-prev" aria-hidden="true" style="background:none;"><img src="{{url('/portal/assets/imgs/prev.jpg')}}"></span>
                        <span class="sr-only">{{trans('institutional.previous')}}</span>
                    </a>
                    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next" style="opacity: 1;">
                        <span class="icon-next-next" aria-hidden="true" style="background:none;"><img src="{{url('/portal/assets/imgs/next.jpg')}}"></span>
                        <span class="sr-only">{{trans('institutional.next')}}</span>
                    </a>
                </div>
            </div>
            @endif
        </div>

        <div class="row-fluid">
            <div class="container">
                <div class="col-lg-12 nav-tabs" style="padding:2% 0;">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" href="#hotel" role="tab" data-toggle="tab">
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 hospedagem">
                                    <p><img src="{{url('/portal/assets/imgs/camaOrange.gif')}}" alt="">
                                        {{trans('destination.accomodation')}}
                                    </p>
                                </div>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#alimentacao" role="tab" data-toggle="tab">
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 hospedagem">
                                    <p><img src="{{url('/portal/assets/imgs/talherOrange.gif')}}" alt="">
                                        {{trans('destination.feeding')}}
                                    </p>
                                </div>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#servico" role="tab" data-toggle="tab">
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 hospedagem">
                                    <p><img src="{{url('/portal/assets/imgs/malaOrange.gif')}}" alt="">
                                        {{trans('destination.services')}}
                                    </p>
                                </div>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#lazer" role="tab" data-toggle="tab">
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 hospedagem">
                                    <p><img src="{{url('/portal/assets/imgs/coposOrange.gif')}}" alt="">
                                        {{trans('destination.recreation')}}
                                    </p>
                                </div>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- Tab panes -->
            <div class="tab-content">

                <!-- FRAGMENTO ATUALIZADO 18/03/2016-->

                <!-- Hotels -->
                <div role="tabpanel" class="tab-pane active" id="hotel">
                    <div class="col-lg-12 box-hospedagem" id="tarja">
                        <div class="container no-padding">
                            <div class="col-lg-12 no-padding">
                                <div class="row" id="options">
                                    <form action="{{url($p_Language . '/apoio-destino/' . $p_Destination->slug)}}" method="get" id="formSearch">
                                        <div class="col-lg-4 col-lg-offset-2 col-md-4 col-sm-4 col-xs-4 ">
                                            <p style="font-size:20px;">{{trans('destination.find_accomodation')}}</p>
                                        </div>
                                        <?php
                                            $v_AccomodationList = [
                                                '' => trans('destination.accomodation_type'),
                                                '43' => trans('destination.accomodation_flat'),
                                                '51' => trans('destination.accomodation_camping'),
                                                '52' => trans('destination.accomodation_summer_camp'),
                                                '38,39' => trans('destination.accomodation_hotel'),
                                                '40,42' => trans('destination.accomodation_farm_hotel'),
                                                '53' => trans('destination.accomodation_hostel'),
                                                '41' => trans('destination.accomodation_inn'),
                                                '47,48,54' => trans('destination.others')
                                            ];
                                        ?>
                                        <div class="col-lg-3  col-md- col-sm-3 col-xs-3 selectoptions">
                                            {!! Form::select('tipo_acomodacao', $v_AccomodationList, '', ['id' => 'tipo_acomodacao', 'required' => 'required']) !!}
                                        </div>
                                        <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 " id="submit-tarja">
                                            <input class="btn" type="submit" value="{{trans('whereToGo.search')}}">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 line no-padding" style="margin-top:4rem;">
                        <div class="container">
                            @foreach($p_Accomodations as $c_Service)
                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                    <a href="{{url($p_Language . '/apoio/' . get_city_slug($c_Attraction->city_id) .  '/' .$c_Service->slug)}}">
                                        <div class="hoverzoom">
                                            <div class="thumbs-mini-four">
                                                <img src="{{$c_Service->url}}">
                                            </div>
                                            <div class="retina-hover">
                                                <div class="col-lg-12 title">
                                                    <p>{{$c_Service->nome}}</p>
                                                </div>
                                                <div class="col-lg-12 no-padding">
                                                    <hr>
                                                </div>
                                                <div class="col-lg-12 text">
                                                    <p>{{$c_Service->cidade}}</p>
                                                </div>
                                            </div>
                                            <div class="retina">
                                                <p>{{$c_Service->nome}} <br><small>{{$c_Service->cidade}}</small></p>
                                            </div>
                                        </div>
                                    </a>
                                    <div class="details-hotel">
                                        <h4>{{$c_Service->sub_type_id ? $p_AccomodationList[$c_Service->sub_type_id] : ''}}</h4>
                                        <hr>
                                        <p id="address" style="text-align:center;">{{$c_Service->logradouro . ', ' . $c_Service->numero}}</p>
                                        <p id="phone" style="text-align:center;">Tel: {{$c_Service->telefone}}</p>
                                        <p id="email" style="text-align:center;">{{$c_Service->email}}</p>
                                    </div>
                                    <div class="col-lg-12" id="call-to-action-hotel">
                                        <a href="{{url($p_Language . '/apoio/' . get_city_slug($c_Attraction->city_id) .  '/' .$c_Service->slug)}}">{{trans('destination.more_info')}}</a>
                                    </div>
                                </div>
                            @endforeach

                            @if(count($p_Accomodations) == 4)
                                <div class="col-lg-12" id="call-to-action">
                                    <a href="{{url($p_Language . '/apoio-destino/' . $p_Destination->slug . '?tipo_acomodacao=')}}">{{trans('destination.more_accomodation')}}</a>
                                </div>
                            @endif

                        </div>
                    </div>
                </div>
                <!-- Restaurantes -->
                <div role="tabpanel" class="tab-pane" id="alimentacao">
                    <div class="col-lg-12 box-restaurante" id="tarja">
                        <div class="container">
                            <div class="col-lg-12">
                                <div class="row" id="options">
                                    <form action="{{url($p_Language . '/apoio-destino/' . $p_Destination->slug)}}" method="get" id="formSearch">
                                        <div class="col-lg-4 col-lg-offset-2 col-md-4 col-sm-4 col-xs-4 ">
                                            <p style="font-size:20px;">{{trans('destination.find_feeding')}}</p>
                                        </div>
                                        <?php
                                            $v_FoodDrinkList = [
                                                '' => trans('destination.feeding_type'),
                                                '55' => trans('destination.feeding_restaurants'),
                                                '56,59,61' => trans('destination.feeding_bars'),
                                                '57' => trans('destination.feeding_tea_house'),
                                                '58' => trans('destination.feeding_breweries'),
                                                '60' => trans('destination.feeding_ice_cream_shops'),
                                                '62' => trans('destination.others')
                                            ];
                                        ?>
                                        <div class="col-lg-3  col-md- col-sm-3 col-xs-3 selectoptions">
                                            {!! Form::select('tipo_estabelecimento', $v_FoodDrinkList, '', ['id' => 'tipo_estabelecimento', 'required' => 'required']) !!}
                                        </div>
                                        <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 " id="submit-tarja">
                                            <input class="btn" type="submit" value="{{trans('whereToGo.search')}}">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 line no-padding" style="margin-top:4rem;">
                        <div class="container">
                            @foreach($p_FoodDrinks as $c_Service)
                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                    <a href="{{url($p_Language . '/apoio/' . get_city_slug($c_Attraction->city_id) .  '/' .$c_Service->slug)}}">
                                        <div class="hoverzoom">
                                            <div class="thumbs-mini-four">
                                                <img src="{{$c_Service->url}}">
                                            </div>
                                            <div class="retina-hover">
                                                <div class="col-lg-12 title">
                                                    <p>{{$c_Service->nome}}</p>
                                                </div>
                                                <div class="col-lg-12 no-padding">
                                                    <hr>
                                                </div>
                                                <div class="col-lg-12 text">
                                                    <p>{{$c_Service->cidade}}</p>
                                                </div>
                                            </div>
                                            <div class="retina">
                                                <p>{{$c_Service->nome}} <br><small>{{$c_Service->cidade}}</small></p>
                                            </div>
                                        </div>
                                    </a>
                                    <div class="details-hotel">
                                        <h4>{{$p_FoodDrinkList[$c_Service->type_id]}}</h4>
                                        <hr>
                                        <p id="address" style="text-align:center;">{{$c_Service->logradouro . ', ' . $c_Service->numero}}</p>
                                        <p id="phone" style="text-align:center;">Tel: {{$c_Service->telefone}}</p>
                                        <p id="email" style="text-align:center;">{{$c_Service->email}}</p>
                                    </div>
                                    <div class="col-lg-12" id="call-to-action-hotel">
                                        <a href="{{url($p_Language . '/apoio/' . get_city_slug($c_Attraction->city_id) .  '/' .$c_Service->slug)}}">{{trans('destination.more_info')}}</a>
                                    </div>
                                </div>
                            @endforeach

                            @if(count($p_FoodDrinks) == 4)
                                <div class="col-lg-12" id="call-to-action">
                                    <a href="{{url($p_Language . '/apoio-destino/' . $p_Destination->slug . '?tipo_estabelecimento=')}}">{{trans('destination.more_feeding')}}</a>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
                <!-- Passeios -->
                <div role="tabpanel" class="tab-pane" id="servico">
                    <div class="col-lg-12 box-passeio" id="tarja">
                        <div class="container">
                            <div class="col-lg-12">
                                <div class="row" id="options">
                                    <form action="{{url($p_Language . '/apoio-destino/' . $p_Destination->slug)}}" method="get" id="formSearch">
                                        <div class="col-lg-4 col-lg-offset-2 col-md-4 col-sm-4 col-xs-4 ">
                                            <p style="font-size:20px;">{{trans('destination.find_service')}}</p>
                                        </div>
                                        <?php
                                            $v_ServiceList = [
                                                '' => trans('destination.service_type'),
                                                '64,256' => trans('destination.services_agencies'),
                                                '66' => trans('destination.services_rental_agencies')
                                            ];
                                        ?>
                                        <div class="col-lg-3  col-md- col-sm-3 col-xs-3 selectoptions">
                                            {!! Form::select('tipo_servico', $v_ServiceList, '', ['id' => 'tipo_servico', 'required' => 'required']) !!}
                                        </div>
                                        <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 " id="submit-tarja">
                                            <input class="btn" type="submit" value="{{trans('whereToGo.search')}}">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 line no-padding" style="margin-top:4rem;">
                        <div class="container">
                            @foreach($p_Services as $c_Service)
                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                    @if($c_Service->integrante_minas_recebe)
                                        <div class="minas-recebe">
                                            <img src="{{url('/portal/assets/imgs/logo-minas-recebe.png')}}" alt="Minas Recebe" class="logo-recebe" id="dropdownRecebe" >
                                            <div class="col-lg-12 divdropdownrecebe">
                                                <p class="text-center">Minas Recebe!<br/> Um programa de fortalecimento do turismo receptivo do Estado de Minas Gerais.</p>
                                            </div>
                                        </div>
                                    @endif
                                    <a href="{{url($p_Language . '/apoio/' . get_city_slug($c_Attraction->city_id) .  '/' .$c_Service->slug)}}">
                                        <div class="hoverzoom">
                                            <div class="thumbs-mini-four">
                                                <img src="{{$c_Service->url}}">
                                            </div>
                                            <div class="retina-hover">
                                                <div class="col-lg-12 title">
                                                    <p>{{$c_Service->nome}}</p>
                                                </div>
                                                <div class="col-lg-12 no-padding">
                                                    <hr>
                                                </div>
                                                <div class="col-lg-12 text">
                                                    <p>{{$c_Service->cidade}}</p>
                                                </div>
                                            </div>
                                            <div class="retina">
                                                <p>{{$c_Service->nome}} <br><small>{{$c_Service->cidade}}</small></p>
                                            </div>
                                        </div>
                                    </a>
                                    <div class="details-hotel">
                                        <h4>{{$p_ServiceList[$c_Service->type_id]}}</h4>
                                        <hr>
                                        <p id="address" style="text-align:center;">{{$c_Service->logradouro . ', ' . $c_Service->numero}}</p>
                                        <p id="phone" style="text-align:center;">Tel: {{$c_Service->telefone}}</p>
                                        <p id="email" style="text-align:center;">{{$c_Service->email}}</p>
                                    </div>
                                    <div class="col-lg-12" id="call-to-action-hotel">
                                        <a href="{{url($p_Language . '/apoio/' . get_city_slug($c_Attraction->city_id) .  '/' .$c_Service->slug)}}">{{trans('destination.more_info')}}</a>
                                    </div>
                                </div>
                            @endforeach

                            @if(count($p_Services) == 4)
                                <div class="col-lg-12" id="call-to-action">
                                    <a href="{{url($p_Language . '/apoio-destino/' . $p_Destination->slug . '?tipo_servico=')}}">{{trans('destination.more_services')}}</a>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
                <!-- lazer -->
                <div role="tabpanel" class="tab-pane" id="lazer">
                    <div class="col-lg-12 box-lazer" id="tarja">
                        <div class="container">
                            <div class="col-lg-12">
                                <div class="row" id="options">
                                    <form action="{{url($p_Language . '/apoio-destino/' . $p_Destination->slug)}}" method="get" id="formSearch">
                                        <div class="col-lg-4 col-lg-offset-2 col-md-4 col-sm-4 col-xs-4 ">
                                            <p style="font-size:20px;">{{trans('destination.find_recreation')}}</p>
                                        </div>
                                        <?php
                                            $v_RecreationList = [
                                                '' => trans('destination.recreation_type'),
                                                '83,85' => trans('destination.recreation_nightclubs'),
                                                '84' => trans('destination.recreation_shows'),
                                                '86' => trans('destination.recreation_theater'),
                                                '77,78,79,87' => trans('destination.recreation_sports'),
                                                '76' => trans('destination.recreation_clubs'),
                                                '74' => trans('destination.recreation_parks'),
                                                '80,81,88,89' => trans('destination.recreation_others')
                                            ];
                                        ?>
                                        <div class="col-lg-3  col-md- col-sm-3 col-xs-3 selectoptions">
                                            {!! Form::select('tipo_lazer', $v_RecreationList, '', ['id' => 'tipo_lazer', 'required' => 'required']) !!}
                                        </div>
                                        <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 " id="submit-tarja">
                                            <input class="btn" type="submit" value="{{trans('whereToGo.search')}}">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 line no-padding" style="margin-top:4rem;">
                        <div class="container">
                            @foreach($p_Recreations as $c_Service)
                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                    <a href="{{url($p_Language . '/apoio/'. get_city_slug($c_Attraction->city_id) .  '/' .$c_Service->slug)}}">
                                        <div class="hoverzoom">
                                            <div class="thumbs-mini-four">
                                                <img src="{{$c_Service->url}}">
                                            </div>
                                            <div class="retina-hover">
                                                <div class="col-lg-12 title">
                                                    <p>{{$c_Service->nome}}</p>
                                                </div>
                                                <div class="col-lg-12 no-padding">
                                                    <hr>
                                                </div>
                                                <div class="col-lg-12 text">
                                                    <p>{{$c_Service->cidade}}</p>
                                                </div>
                                            </div>
                                            <div class="retina">
                                                <p>{{$c_Service->nome}} <br><small>{{$c_Service->cidade}}</small></p>
                                            </div>
                                        </div>
                                    </a>
                                    <div class="details-hotel">
                                        <h4>{{$p_RecreationList[$c_Service->type_id]}}</h4>
                                        <hr>
                                        <p id="address" style="text-align:center;">{{$c_Service->logradouro . ', ' . $c_Service->numero}}</p>
                                        <p id="phone" style="text-align:center;">Tel: {{$c_Service->telefone}}</p>
                                        <p id="email" style="text-align:center;">{{$c_Service->email}}</p>
                                    </div>
                                    <div class="col-lg-12" id="call-to-action-hotel">
                                        <a href="{{url($p_Language . '/apoio/' . get_city_slug($c_Attraction->city_id) .  '/' .$c_Service->slug)}}">{{trans('destination.more_info')}}</a>
                                    </div>
                                </div>
                            @endforeach

                            @if(count($p_Recreations) == 4)
                                <div class="col-lg-12" id="call-to-action">
                                    <a href="{{url($p_Language . '/apoio-destino/' . $p_Destination->slug . '?tipo_lazer=')}}">{{trans('destination.more_recreation')}}</a>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
                <!-- FRAGMENTO ATUALIZADO 18/03/2016-->
            </div>
        </div>
    </div>

    @if(count($p_OtherCitiesAttractions) > 0)
    <div class="row-fluid" id="destinos" style="padding:0;">
        <div class="row-fluid">
            <div class="container">
                <div class="col-lg-12">
                    <h2 style="color:#4b4b4b;font-size: 48px;">{{trans('destination.interest_points')}}</h2>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="col-lg-12 line" style="padding:0;">
                @foreach($p_OtherCitiesAttractions as $c_Attraction)
                    <?php
                        $v_Description = json_decode($c_Attraction->descricao_curta,1)[$p_Language];
                        if($c_Attraction->trade)
                            $v_Name = $c_Attraction->nome;
                        else
                            $v_Name = json_decode($c_Attraction->nome,1)[$p_Language];
                    ?>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <a href="{{url($p_Language . '/atracoes/' . get_city_slug($c_Attraction->city_id) .  '/' .$c_Attraction->slug)}}">
                            <div class="hoverzoom">
                                <div class="thumbs-mini-four">
                                    <img src="{{$c_Attraction->url}}">
                                </div>
                                <div class="retina-hover">
                                    <div class="col-lg-12 title">
                                        <p>{{$v_Name}}</p>
                                    </div>
                                    <div class="col-lg-12 no-padding">
                                        <hr>
                                    </div>
                                    <div class="col-lg-12 text">
                                        <p>{{$v_Description}}</p>
                                    </div>
                                </div>
                                <div class="retina">
                                    <p>{{$v_Name}}</p>
                                </div>
                            </div>
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    @endif
@stop

@section('pageScript')
<!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-56a0debd79357d1c" async="async"></script>
<script>
    var WEATHER_LIFETIME = 2*60*60*1000; //2h in ms
    $(document).ready(function()
    {
        @if($p_Content != null && $p_Content->latitude != null && $p_Content->longitude != null)
            var v_Data = JSON.parse(localStorage.getItem("weatherDestination-{{$p_Content->id}}"));
            var v_Today = new Date().getTime();
            if(v_Data != null && ((v_Today - (v_Data.dt * 1000)) < WEATHER_LIFETIME ))
                displayWeather(v_Data);
            else {
                if (v_Data != null)
                    localStorage.removeItem("weatherAttraction-{{$p_Content->id}}");
                $.ajax({
                    url: "http://api.openweathermap.org/data/2.5/weather?lat={{$p_Content->latitude}}&lon={{$p_Content->longitude}}&units=metric&lang=pt&APPID=674d7946ea8e48336eab93ea712beb46",
                    dataType: "jsonp",
                    success: function (p_WeatherData) {
                        if (p_WeatherData != undefined && p_WeatherData.cod == 200) {
                            displayWeather(p_WeatherData);
                            localStorage.setItem("weatherAttraction-{{$p_Content->id}}", JSON.stringify(p_WeatherData));
                        }
                    }
                });
            }
        @endif

        $("#toggleFavorite").click(function(){
            var v_Name = '{{$p_Content == null ? '' : $v_ContentName}}';
            var v_Description = '{{$p_Content == null ? '' : $v_ShortDescription}}';
            var v_Img = '{{$p_Content == null ? '' : $p_Content->url}}';
            toggleFavorite(location.href, v_Name, v_Img, v_Description);
        });

        var v_DirectionsUrl;
        @if($p_Content != null && $p_Content->latitude != null && $p_Content->longitude != null)
            v_DirectionsUrl = 'https://www.google.com/maps/dir/Current+Location/{{$p_Content->latitude}},{{$p_Content->longitude}}';
        @elseif($p_Content != null)
            var v_DestinationName = '{{$p_Content->nome}}'.split(' ').join('+');
            v_DirectionsUrl = 'https://www.google.com/maps/dir/Current+Location/' + v_DestinationName + ',MG,Brazil';
        @endif
        $('#directions').attr('href', v_DirectionsUrl);
    });

    function displayWeather(p_WeatherData){
        var v_Temp = p_WeatherData.main.temp;
        v_Temp = Math.round(v_Temp * 10)/10;
        v_Temp = (v_Temp + 'º').replace('.',',');
        $('#temp').html(v_Temp);
        $('#tempImg').attr('src', "{{url('/portal/assets/imgs/clima/')}}/"  + p_WeatherData.weather[0].icon + '.png');
    }

//    function setTripadvisorRatingImg(p_Image)
//    {
//        var v_TripAdvisorId = $(p_Image).attr('tripadvisor-url');
//        v_TripAdvisorId = v_TripAdvisorId.split(/-g\d+-d/)[1];
//        if(v_TripAdvisorId)
//            v_TripAdvisorId = v_TripAdvisorId.split('-')[0];
//        var v_RatingImageUrl = getCookie('tripadvisor-' + v_TripAdvisorId);
//        if(v_RatingImageUrl == null)
//        {
//            $.ajax({
//                url: "http://api.tripadvisor.com/api/partner/2.0/location/" +  v_TripAdvisorId + "?key=287185045b8d4d12abd7423acd489a81",
//                dataType: "json",
//                success: function (p_TripAdvisorData) {
//                    if(p_TripAdvisorData.error == undefined && p_TripAdvisorData.rating_image_url != undefined)
//                    {
//                        setCookie('tripadvisor-' + v_TripAdvisorId, p_TripAdvisorData.rating_image_url, 7);
//                        $(p_Image).attr('src', p_TripAdvisorData.rating_image_url);
//                    }
//                }
//            });
//        }
//        else
//            $(p_Image).attr('src', v_RatingImageUrl);
//    }
</script>
@stop
