@extends('admin.base')
@section('main-content')
    <section id="content" class="error-page pn">
        <div class="center-block mt50 mw800">
            <h1 class="error-title"> 401! </h1>
            <h2 class="error-subtitle">Acesso não autorizado.</h2>
        </div>
    </section>
@stop

