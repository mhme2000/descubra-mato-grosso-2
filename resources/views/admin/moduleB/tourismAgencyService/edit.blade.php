@extends('admin.mainTabs')
@section('pageCSS')
    <link href="{{url('/vendor/select2/select2.min.css')}}" rel="stylesheet" />
    <style type="text/css">
        .full-width{
            width: 100%;
        }
        .align-center{
            text-align: center;
        }
        .margin-auto{
            margin: auto;
        }
    </style>
@stop
@section('panel-header')
    @if(\App\UserType::isTrade())
        Agenciamento
    @else
        B3 - Serviço e equipamento de agência de turismo
    @endif
    <ul class="nav panel-tabs-border panel-tabs">
        <li class="active">
            <a href="#tab0" data-toggle="tab" aria-expanded="true">Português</a>
        </li>
        <li class="">
            <a href="#tab1" data-toggle="tab" aria-expanded="false">Inglês</a>
        </li>
        <li class="">
            <a href="#tab2" data-toggle="tab" aria-expanded="false">Espanhol</a>
        </li>
        <li class="">
            <a href="#tab3" data-toggle="tab" aria-expanded="false">Francês</a>
        </li>
        <a title="Imprimir" type="button" class="btn btn-success pull-right" onclick="print()">
            <i class="fa fa-print"></i>
        </a>
    </ul>
@stop
@section('content')
    <div class="row visible-print">
        <div class="mb20 col-sm-12">
            <h2>B3 - Serviço e equipamento de agência de turismo</h2>
        </div>
    </div>
    <?php \App\BaseInventoryModel::startFormFieldIndexing(); ?>
    @if(!\App\UserType::isParceiro())
    {!! Form::open(['id' => 'mainForm', 'url'=> url('/admin/inventario/servicos-agencias-turismo'), 'onsubmit' => 'return submitForm()', 'files' => true]) !!}
    @else
    <div id="mainForm">
    @endif

    <div class="tab-content pn br-n">
        <?php
            $v_TabLanguages = ['pt', 'en', 'es', 'fr'];
            if($p_TourismAgencyService != null){
                $v_EmptyData = ['pt'=>'', 'en'=>'', 'es'=>'', 'fr'=>''];
                $v_Descriptions = $p_TourismAgencyService->descricoes_observacoes_complementares != null ? json_decode($p_TourismAgencyService->descricoes_observacoes_complementares,1) : $v_EmptyData;
                $v_ShortDescriptions = $p_TourismAgencyService->descricao_curta != null ? json_decode($p_TourismAgencyService->descricao_curta,1) : $v_EmptyData;
            }
        ?>
        @foreach($v_TabLanguages as $c_TabIndex => $c_TabLanguage)
        <div id="{{'tab' . $c_TabIndex}}" class="tab-pane {{ $c_TabIndex == 0 ? 'active' : '' }}">
            <div class="row">
                @if($c_TabLanguage == 'pt')
                    @if($p_TourismAgencyService != null)
                        <input type="hidden" name="id" value="{{$p_TourismAgencyService->id}}">
                        <input type="hidden" name="formulario[validade_cadastur]" value="{{$p_TourismAgencyService->validade_cadastur}}">
                        <input type="hidden" id="tem_cadastur" name="formulario[tem_cadastur]" value="{{$p_TourismAgencyService->tem_cadastur}}">
                    @else
                        <input type="hidden" id="validade_cadastur" name="formulario[validade_cadastur]">
                        <input type="hidden" id="tem_cadastur" name="formulario[tem_cadastur]" value="0">
                    @endif
                    @include('admin.util.headerIdentification', ['p_Form' => $p_TourismAgencyService, 'p_Types' => $p_Types, 'p_WithoutSubtype' => true, 'p_CadasturActivity' => 10])

                    <input type="hidden" id="default_tipo_atividade_cadastur" value="10">
                    
                    <div class="form-group col-sm-6">
                        <label for="telefone">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Telefone (exemplo: (DDD) 9 9999-9999 / (DDD) 9999-9999)<span class="mandatory-field">*</span></label>
                        <input type="text" name="formulario[telefone]" class="form-control phone-field" id="telefone" placeholder="Digite Aqui" value="{{$p_TourismAgencyService == null ? '' : $p_TourismAgencyService->telefone}}" required>
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="whatsapp">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Whatsapp</label>
                        <input type="text" name="formulario[whatsapp]" class="form-control phone-field" id="whatsapp" placeholder="Digite Aqui" value="{{$p_TourismAgencyService == null ? '' : $p_TourismAgencyService->whatsapp}}">
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="site">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Site</label>
                        <input type="url" name="formulario[site]" class="form-control" id="site" placeholder="Digite Aqui" value="{{$p_TourismAgencyService == null ? '' : $p_TourismAgencyService->site}}">
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="email">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Email</label>
                        <input type="text" name="formulario[email]" class="email-field form-control" id="email" placeholder="Digite Aqui" value="{{$p_TourismAgencyService == null ? '' : $p_TourismAgencyService->email}}">
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="facebook">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Facebook</label>
                        <input type="url"  name="formulario[facebook]" class="form-control" id="facebook" placeholder="Digite Aqui" value="{{$p_TourismAgencyService == null ? '' : $p_TourismAgencyService->facebook}}">
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="instagram">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Instagram</label>
                        <input type="url"  name="formulario[instagram]" class="form-control" id="instagram" placeholder="Digite Aqui" value="{{$p_TourismAgencyService == null ? '' : $p_TourismAgencyService->instagram}}">
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="twitter">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Twitter</label>
                        <input type="url"  name="formulario[twitter]" class="form-control" id="twitter" placeholder="Digite Aqui" value="{{$p_TourismAgencyService == null ? '' : $p_TourismAgencyService->twitter}}">
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="youtube">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Youtube</label>
                        <input type="url"  name="formulario[youtube]" class="form-control" id="youtube" placeholder="Digite Aqui" value="{{$p_TourismAgencyService == null ? '' : $p_TourismAgencyService->youtube}}">
                    </div>
                    <input type="hidden" id="descricao_curta" name="formulario[descricao_curta]">
                    <div class="form-group col-sm-12">
                        <label for="descricao_curta_{{$c_TabLanguage}}">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Descrição curta</label>
                        <textarea id="descricao_curta_{{$c_TabLanguage}}" class="form-control" rows="4" placeholder="Digite Aqui">{{$p_TourismAgencyService == null ? '' : $v_ShortDescriptions[$c_TabLanguage]}}</textarea>
                    </div>


                    <div class="form-group col-sm-12">
                        <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Localização e ambiência</h3>
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="cep">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}CEP<span class="mandatory-field">*</span></label>
                        <input name="formulario[cep]" type="text" class="form-control cep-field" id="cep" placeholder="Digite Aqui" value="{{$p_TourismAgencyService == null ? '' : $p_TourismAgencyService->cep}}" required>
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="bairro">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Bairro<span class="mandatory-field">*</span></label>
                        <input type="text" name="formulario[bairro]" class="form-control" id="bairro" placeholder="Digite Aqui" value="{{$p_TourismAgencyService == null ? '' : $p_TourismAgencyService->bairro}}" required>
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="logradouro">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Logradouro<span class="mandatory-field">*</span></label>
                        <input type="text" name="formulario[logradouro]" class="form-control" id="logradouro" placeholder="Digite Aqui" value="{{$p_TourismAgencyService == null ? '' : $p_TourismAgencyService->logradouro}}" required>
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="numero">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Número<span class="mandatory-field">*</span></label>
                        <input type="text" name="formulario[numero]" class="form-control" id="numero" placeholder="Digite Aqui" value="{{$p_TourismAgencyService == null ? '' : $p_TourismAgencyService->numero}}" required>
                    </div>
                    <div class="form-group col-sm-12">
                        <label for="complemento">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Complemento</label>
                        <input type="text" name="formulario[complemento]" class="form-control geolocalizacao" id="complemento" placeholder="Digite Aqui" value="{{$p_TourismAgencyService == null ? '' : $p_TourismAgencyService->complemento}}">
                        <a title="Pesquisar geolocalização" type="button" class="btn btn-success btn-search-geolocation" onclick="updateMarkerPosition(1)">
                            <i class="fa fa-map-marker"></i>
                        </a>
                    </div>
                    <div class="form-group col-sm-12">
                        <?php
                            $v_Locations = [
                                ''=>'',
                                'Urbana'=>'Urbana',
                                'Rururbana'=>'Rururbana',
                                'Rural'=>'Rural'
                            ];
                        ?>
                        <label for="localizacao">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Localização<span class="mandatory-field">*</span></label>
                        {!! Form::select('formulario[localizacao]', $v_Locations, $p_TourismAgencyService == null ? '' : $p_TourismAgencyService->localizacao, ['id' => 'localizacao', 'class' => 'form-control', 'required' => 'required']) !!}
                    </div>
                    <div class="form-group col-sm-12">
                        <p>* Movimente o marcador no mapa para ajustar a latitude e longitude.</p>
                        <div id="map_canvas" style="height:320px;"></div>
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="latitude">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Latitude <i>(formato decimal)</i></label>
                        <input type="number" name="formulario[latitude]" step="0.0000001" class="form-control" id="latitude" placeholder="Digite Aqui" value="{{$p_TourismAgencyService == null ? '' : $p_TourismAgencyService->latitude}}">
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="longitude">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Longitude <i>(formato decimal)</i></label>
                        <input type="number" name="formulario[longitude]" step="0.0000001" class="form-control" id="longitude" placeholder="Digite Aqui" value="{{$p_TourismAgencyService == null ? '' : $p_TourismAgencyService->longitude}}">
                    </div>
                    <div class="form-group col-sm-12">
                        <label for="latitude_longitude_decimal">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Latitude e Longitude em decimal</label>
                        <input type="text" class="form-control" id="latitude_longitude_decimal" placeholder="Digite Aqui">
                    </div>
                   
                    @include('admin.util.photos', ['p_CoverPhoto' => $p_CoverPhoto, 'p_PhotoGallery' => $p_PhotoGallery, 'p_Cover' => true])
                    <div class="col-sm-12">
                        <p>* É necessário ter ao menos uma foto de capa para que possa aparecer no portal.</p>
                    </div>
                @else
                    <div class="form-group col-sm-12">
                        <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Nome/Entidade</h3>
                    </div>
                    <div class="form-group col-sm-12">
                        <label for="descricao_curta_{{$c_TabLanguage}}">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Descrição curta</label>
                        <textarea id="descricao_curta_{{$c_TabLanguage}}" class="form-control" rows="4" placeholder="Digite Aqui">{{$p_TourismAgencyService == null ? '' : $v_ShortDescriptions[$c_TabLanguage]}}</textarea>
                    </div>
                @endif

                <div class="form-group col-sm-12">
                    <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Funcionamento</h3>
                </div>
                @if($c_TabLanguage == 'pt')
                    @include('admin.moduleB.tourismAgencyService.util.accessibility', ['p_Form' => $p_TourismAgencyService, 'p_InAgency' => true])
                @endif
                @include('admin.util.workingPeriod', ['p_Form' => $p_TourismAgencyService, 'p_Translation' => true, 'p_TabLanguage' => $c_TabLanguage, 'p_ClosedFields' => true, 'p_NoTitle' => true])

                @if($c_TabLanguage == 'pt')
                    <div class="form-group col-sm-12">
                        <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Serviços oferecidos</h3>
                    </div>
                    <div class="form-group col-sm-12">
                        <label for="municipios_participantes">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Municípios comercializados por sua empresa <i>(permite mais de uma opção)</i></label>
                        <input type="hidden" id="municipios_participantes" name="formulario[municipios_participantes]" value="{{$p_TourismAgencyService == null ? '' : $p_TourismAgencyService->municipios_participantes}}">
                        {!! Form::select('', $p_Cities, null, ['id' => 'municipios_participantes_select', 'class' => 'form-control select2', 'multiple' => 'multiple', 'style' => 'width: 100%']) !!}
                    </div>
                    <div class="form-group col-sm-6">
                        <?php
                            $v_AgeGroups = [
                                    ''=>'',
                                    'Até 12'=>'Até 12',
                                    '13 a 25'=>'13 a 25',
                                    '26 a 38'=>'26 a 38',
                                    '39 a 52'=>'39 a 52',
                                    '53 a 65'=>'53 a 65',
                                    'Acima de 65'=>'Acima de 65'
                            ];
                        ?>
                        <label for="principal_faixa_etaria_clientes">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Principal faixa etária dos clientes (anos)<span class="mandatory-field">*</span></label>
                        {!! Form::select('formulario[principal_faixa_etaria_clientes]', $v_AgeGroups, $p_TourismAgencyService == null ? '' : $p_TourismAgencyService->principal_faixa_etaria_clientes, ['id' => 'principal_faixa_etaria_clientes', 'class' => 'form-control', 'required' => 'required', 'style' => 'width: 100%']) !!}
                    </div>
                    <div class="form-group col-sm-12">
                        <?php
                            $v_TourismSegments = [
                                'Aventura'=>'Aventura',
                                'Ecoturismo'=>'Ecoturismo',
                                'Rural'=>'Rural',
                                'Sol e praia'=>'Sol e praia',
                                'Estudos e intercâmbio'=>'Estudos e intercâmbio',
                                'Negócios e eventos'=>'Negócios e eventos',
                                'Cultural'=>'Cultural',
                                'Náutico'=>'Náutico',
                                'Saúde (bem-estar e médico)'=>'Saúde (bem-estar e médico)',
                                'Pesca'=>'Pesca',
                                'Não é especializado em nenhum segmento'=>'Não é especializado em nenhum segmento'
                            ];
                        ?>
                        <label for="segmentos_agencia">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Em quais segmentos ou tipos de turismo sua empresa atua? <i>(permite mais de uma opção)</i><span class="mandatory-field">*</span></label>
                        <input type="hidden" id="segmentos_agencia" name="formulario[segmentos_agencia]" value="{{$p_TourismAgencyService == null ? '' : $p_TourismAgencyService->segmentos_agencia}}">
                        {!! Form::select('', $v_TourismSegments, null, ['id' => 'segmentos_agencia_select', 'class' => 'form-control select2', 'style' => 'width: 100%', 'multiple' => 'multiple', 'required' => 'required']) !!}
                    </div>

                    @include('admin.moduleB.tourismAgencyService.util.accessibility', ['p_Form' => $p_TourismAgencyService])

                    <div class="form-group col-sm-12">
                        <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Atendimento</h3>
                    </div>
                    <div class="form-group col-sm-12">
                        <label for="atendimento_bilingue">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Atendimento bilingue?</label>
                        <p><input type="checkbox" value="1" name="formulario[atendimento_bilingue]" id="atendimento_bilingue" class="ml5 mt10" {{($p_TourismAgencyService == null || $p_TourismAgencyService->atendimento_bilingue == 0) ? '' : 'checked'}}></p>
                    </div>
                    <div class="form-group col-sm-12 atendimento-bilingue-fields">
                        <label>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(2)}}Atendimento - Idiomas <i>(permite mais de uma opção)</i></label>
                        <input type="hidden" id="atendimento_bilingue_idiomas" name="formulario[atendimento_bilingue_idiomas]" value="{{$p_TourismAgencyService == null ? '' : $p_TourismAgencyService->atendimento_bilingue_idiomas}}">
                        {!! Form::select('', $p_LanguageNames, null, ['id' => 'atendimento_bilingue_idiomas_select', 'class' => 'form-control select2', 'multiple' => 'multiple', 'style' => 'width: 100%']) !!}
                    </div>

                    <div class="integrante-minas-recebe-fields">
                        <div class="col-sm-12">
                            <h4>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Informativos impressos</h4>
                        </div>
                        <div class="form-group col-sm-4">
                            <label for="possui_informativos_impressos">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(2)}}Possui informativo impresso?</label>
                            <p><input type="checkbox" value="1" name="formulario[possui_informativos_impressos]" id="possui_informativos_impressos" class="ml5 mt10" {{($p_TourismAgencyService == null || $p_TourismAgencyService->possui_informativos_impressos == 0) ? '' : 'checked'}}></p>
                        </div>
                        <div class="form-group col-sm-4 mandatory informativos-impressos-fields">
                            <label for="informativos_impressos_bilingue">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(2)}}É bilingue?</label>
                            <p><input type="checkbox" value="1" name="formulario[informativos_impressos_bilingue]" id="informativos_impressos_bilingue" class="ml5 mt10" {{($p_TourismAgencyService == null || $p_TourismAgencyService->informativos_impressos_bilingue == 0) ? '' : 'checked'}}></p>
                        </div>
                        <div class="form-group col-sm-12 mandatory informativos-impressos-languages-fields">
                            <label for="informativos_impressos_linguas">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(2)}}Quais línguas? <i>(permite mais de uma opção)</i></label>
                            <input type="hidden" id="informativos_impressos_linguas" name="formulario[informativos_impressos_linguas]" value="{{$p_TourismAgencyService == null ? '' : $p_TourismAgencyService->informativos_impressos_linguas}}">
                            {!! Form::select('', $p_Languages, null, ['id' => 'informativos_impressos_linguas_select', 'class' => 'form-control select2', 'multiple' => 'multiple', 'style' => 'width: 100%']) !!}
                        </div>
                        <div class="form-group col-sm-12 informativos-impressos-languages-fields">
                            <label for="informativos_impressos_linguas_outras">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(2)}}Outras línguas</label>
                            <textarea name="formulario[informativos_impressos_linguas_outras]" id="informativos_impressos_linguas_outras" class="form-control" rows="4" placeholder="Digite Aqui">{{$p_TourismAgencyService == null ? '' : $p_TourismAgencyService->informativos_impressos_linguas_outras}}</textarea>
                        </div>
                        <div class="col-sm-12"></div>
                    </div>

                    <?php
                        $v_CheckBoxFields = [
                            'possui_email_marketing'=>'Possui email marketing',
                            'possui_blog'=>'Possui blog'
                        ];
                    ?>
                    @foreach($v_CheckBoxFields as $c_Id => $c_Field)
                        <div class="form-group col-sm-3">
                            <label for="{{$c_Id}}">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}{{$c_Field}}?</label>
                            <p><input type="checkbox" value="1" name="formulario[{{$c_Id}}]" id="{{$c_Id}}" class="ml5 mt10" {{($p_TourismAgencyService == null || $p_TourismAgencyService[$c_Id] == 0) ? '' : 'checked'}}></p>
                        </div>
                    @endforeach
                @endif

                <div class="form-group col-sm-12">
                    <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Dados complementares</h3>
                </div>
                @if($c_TabLanguage == 'pt')
                    <input type="hidden" id="descricoes_observacoes_complementares" name="formulario[descricoes_observacoes_complementares]">
                @endif
                <div class="form-group col-sm-12">
                    <label for="descricoes_observacoes_complementares_{{$c_TabLanguage}}">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Descrições e observações complementares</label>
                    <textarea id="descricoes_observacoes_complementares_{{$c_TabLanguage}}" class="form-control" rows="4" placeholder="Digite Aqui">{{$p_TourismAgencyService == null ? '' : $v_Descriptions[$c_TabLanguage]}}</textarea>
                </div>

                @if($c_TabLanguage == 'pt')
                    @include('admin.util.hashtags', ['p_Form' => $p_TourismAgencyService])

                        <div class="form-group col-sm-12">
                        <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Uso interno da SEDEC/TUR</h3>
                    </div>

                    <input type="hidden" id="tipos_viagem" name="formulario[tipos_viagem]" value="{{$p_TourismAgencyService == null ? '' : $p_TourismAgencyService->tipos_viagem}}">
                    <div class="form-group col-sm-12">
                        <label for="tipos_viagem">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Tipo de polo <i>(permite mais de uma opção)</i></label>
                        {!! Form::select('', $p_TripTypes, null, ['id' => 'tipos_viagem_select', 'class' => 'form-control select2', 'style' => 'width: 100%', 'multiple' => 'multiple']) !!}
                    </div>

                    @if($p_TourismAgencyService == null || $p_TourismAgencyService->revision_status_id != 6)
                        @include('admin.util.revision', ['p_Form' => $p_TourismAgencyService, 'p_Publish' => true])
                    @endif

                    @include('admin.util.responsibleTeam', ['p_Form' => $p_TourismAgencyService])
                @endif
            </div>
        </div>
        @endforeach

        @if(!\App\UserType::isParceiro())
            <div class="row">
                <div class="form-group col-sm-12">
                    <input type="submit" class="btn btn-default mt15 mb25 pull-right" value="Salvar">
                </div>
            </div>
        @endif
    </div>
    @if(!\App\UserType::isParceiro())
    {!! Form::close() !!}
    @else
    </div>
    @endif
@stop
@section('pageScript')
    <script src="{{url('/vendor/select2/select2.min.js')}}" type="text/javascript"></script>
    <script src="{{url('/vendor/select2/i18n/pt-BR.js')}}" type="text/javascript"></script>
    <script src="{{url('/assets/js/inputmask.js')}}" type="text/javascript"></script>
    <script src="{{url('/assets/js/jquery.inputmask.js')}}" type="text/javascript"></script>
    @include('admin.util.workingPeriodScript', ['p_Translation' => true])
    @include('admin.util.headerIdentificationScript', ['p_WithoutSubtype' => true])
    @include('admin.util.photosScript', ['p_MaxPhotoCount' => -1])
    <script>
        $(document).ready(function()
        {
            $(".select2").select2({language:'pt-BR'});
            $(".select2-custom").select2({language:'pt-BR', tags: true});

            $('.phone-field').keypress(function (e) {
                var key = e.keyCode || e.charCode;
                if( key == 8 || key == 37 || key == 39 || key == 46 )
                    return true;

                var regex = new RegExp(/^[\d\s\(\)\-\+\.\,\;\\\/]+$/);
                var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
                if (regex.test(str))
                    return true;

                e.preventDefault();
                return false;
            });
            $('input[type="url"]').focus(function(){
                if(this.value == '')
                    this.value = 'http://';
            }).focusout(function(){
                if(this.value == 'http://')
                    this.value = '';
            });

            $('#possui_informativos_impressos').change(function(){
                if($(this).is(":checked"))
                    $('.informativos-impressos-fields').show();
                else
                {
                    $('.informativos-impressos-fields').hide();
                    $('#informativos_impressos_bilingue').attr('checked', false).trigger('change');
                }
            }).change();

            $('#atendimento_bilingue').change(function(){
                if($(this).is(':checked'))
                    $('.atendimento-bilingue-fields').show();
                else
                    $('.atendimento-bilingue-fields').hide();
            }).change();

            var v_AtendimentoIdiomas = $('#atendimento_bilingue_idiomas').val();
            if(v_AtendimentoIdiomas.length > 0)
            {
                v_AtendimentoIdiomas = JSON.parse(v_AtendimentoIdiomas);
                $('#atendimento_bilingue_idiomas_select').select2("val", v_AtendimentoIdiomas);
            }

            $('#informativos_impressos_bilingue').change(function(){
                if($(this).is(":checked") && $('#possui_informativos_impressos').is(":checked"))
                    $('.informativos-impressos-languages-fields').show();
                else
                    $('.informativos-impressos-languages-fields').hide();
            }).change();

//            $('#possui_site').change(function(){
//                if($(this).is(":checked"))
//                    $('.site-fields').show();
//                else
//                {
//                    $('.site-fields').hide();
//                    $('#site_bilingue').attr('checked', false).trigger('change');
//                }
//            }).change();
//
//            $('#site_bilingue').change(function(){
//                if($(this).is(":checked") && $('#possui_site').is(":checked"))
//                    $('.site-languages-fields').show();
//                else
//                    $('.site-languages-fields').hide();
//            }).change();

            carregaDadosSegmentosTurismo();
            carregaDadosLinguas();
            carregaSelectMultiplo('municipios_participantes');

            @if(\App\UserType::isTrade())
                $('#cnpj').attr('disabled', true);
                @if($p_TourismAgencyService == null)
                    var v_Data = localStorage.getItem("newTradeItem");
                    if(v_Data != null) {
                        localStorage.removeItem("newTradeItem");
                        v_Data = JSON.parse(v_Data);
                        $('#cnpj').val(v_Data.cnpj).blur();
                        $('#tipo_atividade_cadastur').val(v_Data.tipoAtividade);
                        $('#validade_cadastur').val(v_Data.validade + ' 00:00:00');
                        if(v_Data.token.length > 0)
                            v_CadasturToken = v_Data.token;
                        if(v_CadasturToken.length > 0)
                            loadCadasturData();
                    }
                    else
                        location.href = '{{ url('admin/inventario/servicos-agencias-turismo') }}';
                @endif
            @endif

        @if($p_TourismAgencyService != null && $p_TourismAgencyService->revision_status_id == 6)
            $.confirm({
                text: 'Este cadastro está vencido no CADASTUR. Atualize-o para reativar seu cadastro no Descubra Mato Grosso. Saiba mais: www.cadastur.turismo.mg.gov.br',
                title: 'Atenção',
                confirmButton: 'OK',
                cancelButton: 'Não',
                cancelButtonClass:'hidden'
            });
        @endif

            $('#equipamento_fechado').change(function(){
                if($(this).is(":checked")) {
                    $('.closed-equipment-fields').show();
                    $('.closed-equipment-fields.mandatory .form-control').attr('required', true);
                }
                else {
                    $('.closed-equipment-fields').hide();
                    $('.closed-equipment-fields.mandatory .form-control').removeAttr('required');
                }
            }).change();

            var v_TripTypes = $('#tipos_viagem').val();
            if(v_TripTypes.length > 0)
            {
                v_TripTypes = JSON.parse(v_TripTypes);
                $('#tipos_viagem_select').select2("val", v_TripTypes);
            }
        });

        function carregaDadosSegmentosTurismo()
        {
            var v_Dados = $('#segmentos_agencia').val();
            if(v_Dados.length > 0)
            {
                v_Dados = JSON.parse(v_Dados);
                $('#segmentos_agencia_select').select2("val", v_Dados);
            }
        }

        function carregaDadosLinguas()
        {
            carregaSelectMultiplo('informativos_impressos_linguas');
//            carregaSelectMultiplo('site_linguas');
        }

        function carregaSelectMultiplo(p_Campo)
        {
            var v_Dados = $('#'+p_Campo).val();
            if(v_Dados.length > 0)
            {
                v_Dados = JSON.parse(v_Dados);
                var v_SelectedItems = [];
                $(v_Dados).each(function(){
                    v_SelectedItems.push(this.id);
                });
                $('#'+p_Campo+'_select').select2("val", v_SelectedItems);
            }
        }

        function submitForm()
        {
            if(!validateCNPJ($('.cnpj-field').val()))
            {
                alert('CNPJ inválido!');
                return false;
            }

            $('.cnpj-field').each(function (c_Key, c_Field)
            {
                var v_Value = $(c_Field).val().replace(/[^\d]+/g,'');
                $(c_Field).parent().find('input[type="hidden"]').val(v_Value);
            });

            processaDadosSegmentosTurismo();
            processaDadosAcessibilidade();
            processaDadosFuncionamentos();
            processaDadosLinguas();


            <?php $v_Languages = ['pt', 'en', 'es', 'fr']; ?>

            var v_DescriptionsJson = {};
            var v_ShortDescriptionsJson = {};
            @foreach($v_Languages as $c_Language)
                v_DescriptionsJson.{{$c_Language}} = $('#descricoes_observacoes_complementares_{{$c_Language}}').val();
                v_ShortDescriptionsJson.{{$c_Language}} = $('#descricao_curta_{{$c_Language}}').val();
            @endforeach
            $('#descricoes_observacoes_complementares').val(JSON.stringify(v_DescriptionsJson));
            $('#descricao_curta').val(JSON.stringify(v_ShortDescriptionsJson));

            $('#atendimento_bilingue_idiomas').val(JSON.stringify(getSelectValue($('#atendimento_bilingue_idiomas_select').val())));

            $('#tipos_viagem').val(JSON.stringify(getSelectValue($('#tipos_viagem_select').val())));

            var v_Hashtags = $('#hashtags_select').val();
            $('#hashtags').val(v_Hashtags == null ? '' : v_Hashtags.join(';'));

            return true;
        }

        function getSelectValue(p_SelectValue){
            if(p_SelectValue == null || p_SelectValue == undefined)
                return [];
            else return p_SelectValue;
        }

        function processaDadosSegmentosTurismo()
        {
            var v_Dados = $('#segmentos_agencia_select').val();
            $('#segmentos_agencia').val(JSON.stringify(v_Dados));
        }

        function processaDadosLinguas()
        {
            processaSelectMultiplo('municipios_participantes');
            processaSelectMultiplo('informativos_impressos_linguas');
//            processaSelectMultiplo('site_linguas');
        }

        function processaSelectMultiplo(p_Campo)
        {
            var v_Dados = [];
            $('#'+p_Campo+'_select option:selected').each(function(){
                v_Dados.push({
                    id:$(this).attr('value'),
                    nome:$(this).text()
                });
            });
            $('#'+p_Campo).val(JSON.stringify(v_Dados));
        }






    </script>
    @include('admin.util.mapScript')
    @include('admin.moduleB.tourismAgencyService.util.accessibilityScript')
    @include('admin.util.responsibleTeamScript')
    @if($p_TourismAgencyService == null || $p_TourismAgencyService->revision_status_id != 6)
    @include('admin.util.revisionScript')
    @endif
    @include('admin.util.cadastur30')
    @include('admin.util.tooltipScript', ['p_Type' => 'B3'])
@stop