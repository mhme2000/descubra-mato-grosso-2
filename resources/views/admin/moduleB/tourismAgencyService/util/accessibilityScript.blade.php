<script>
    $(document).ready(function()
    {
        carregaDadosAcessibilidade();
    });

    function carregaDadosAcessibilidade()
    {
        var v_Acessibilidade = $('#acessibilidade').val();
        if(v_Acessibilidade.length > 0)
        {
            v_Acessibilidade = JSON.parse(v_Acessibilidade);
            $(v_Acessibilidade).each(function(){
                if(this.tipo == 'text')
                    $('.accessibility-fields .form-control[rel="'+this.nome+'"]').val(this.valor);
                else
                    $('.accessibility-fields .form-control[rel="'+this.nome+'"]').select2("val", this.valor);
            });
        }
    }

    function processaDadosAcessibilidade()
    {
        var v_Acessibilidade = [];
        $('.accessibility-fields .form-control').each(function(){
            v_Acessibilidade.push({
                nome:$(this).attr('rel'),
                tipo:$(this).hasClass('select2') ? 'select' : 'text',
                valor:$(this).val()
            });
        });
        $('#acessibilidade').val(JSON.stringify(v_Acessibilidade));
    }
</script>