@if(isset($p_InAgency))
    <input type="hidden" id="acessibilidade" name="formulario[acessibilidade]" value="{{$p_Form == null ? '' : $p_Form->acessibilidade}}">

    <?php
        $v_InAgency = [
            'Não'=>'Não',
            'Vagas de estacionamento especiais'=>'Vagas de estacionamento especiais',
            'Corrimão'=>'Corrimão',
            'Rampas de acesso'=>'Rampas de acesso',
            'Funcionários capacitados'=>'Funcionários capacitados',
            'Sinalização tátil (deficiência visual)'=>'Sinalização tátil (deficiência visual)',
            'Sanitário acessível'=>'Sanitário acessível',
            'Espaço para deslocamento (cadeirantes)'=>'Espaço para deslocamento (cadeirantes)',
            'Assento especial para obesos'=>'Assento especial para obesos',
            'Porta larga o suficiente para entrada de obesos'=>'Porta larga o suficiente para entrada de obesos',
            'Sanitário com assento especial para obesos'=>'Sanitário com assento especial para obesos'
        ];
        $v_Field = 'Possui acessibilidade na estrutura do espaço físico';
    ?>
    <div class="accessibility-fields">
        <div class="form-group col-sm-12">
            <label>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}{{$v_Field}} <i>(permite mais de uma opção)</i></label>
            {!! Form::select('', $v_InAgency, null, ['rel'=>$v_Field,'class' => 'form-control select2', 'style' => 'width: 100%', 'multiple' => 'multiple']) !!}
        </div>
    </div>
@else
    <?php
        $v_AdaptedFor = [
                'Física ou motora'=>'Física ou motora',
                'Visual'=>'Visual',
                'Auditiva'=>'Auditiva',
                'Mental'=>'Mental',
                'Mobilidade reduzida (idosos, gestantes, obesos entre outros)'=>'Mobilidade reduzida (idosos, gestantes, obesos entre outros)'
        ];
        $v_FieldsNamesArray = [
                'Roteiros Adaptados',
                'Guias preparados para público com deficiência ou mobilidade reduzida'
        ];
    ?>
    <div class="accessibility-fields">
        @foreach($v_FieldsNamesArray as $c_FieldIndex => $c_Field)
            <div class="form-group col-sm-12 integrante-minas-recebe-fields">
                <label>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}{{$c_Field}} <i>(permite mais de uma opção)</i></label>
                {!! Form::select('', $v_AdaptedFor, null, ['rel'=>$c_Field,'class' => 'form-control select2', 'style' => 'width: 100%', 'multiple' => 'multiple']) !!}
            </div>
        @endforeach
    </div>
@endif
