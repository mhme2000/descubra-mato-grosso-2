<div class="form-group col-sm-12">
    <h3>Serviços e equipamentos de apoio</h3>
</div>
<input type="hidden" id="servicos_equipamentos_apoio" name="formulario[servicos_equipamentos_apoio]" value="{{$p_Form == null ? '' : $p_Form->servicos_equipamentos_apoio}}">
<div class="servicos-equipamentos-apoio-fields">
    <?php
        $v_SupportFields = [
                'Restaurante',
                'Bar/Lanchonete',
                'Elevadores',
                'Instalação sanitária'
        ];
    ?>
    @foreach($v_SupportFields as $c_Field)
        <div class="col-sm-12">
            <h4>{{$c_Field}}</h4>
        </div>
        <div class="form-group col-sm-6">
            <label>Quantidade</label>
            <input type="number" min="0" class="form-control field" rel="{{$c_Field}} - Quantidade" placeholder="Digite Aqui">
        </div>
        @if($c_Field == 'Restaurante' || $c_Field == 'Bar/Lanchonete')
            <div class="form-group col-sm-6">
                <label>Capacidade</label>
                <input type="text" class="form-control integer-field field" rel="{{$c_Field}} - Capacidade" placeholder="Digite Aqui">
            </div>
        @endif
        <div class="form-group col-sm-12">
            <label>Adaptado (especificar)?</label>
            <textarea class="form-control field" rel="{{$c_Field}} - Adaptado" rows="4" placeholder="Digite Aqui"></textarea>
        </div>
    @endforeach
    <div class="col-sm-12">
        <h4>Estacionamento</h4>
    </div>
    <div class="form-group col-sm-6">
        <label>Vagas</label>
        <input type="text" class="form-control integer-field field" rel="Estacionamento - Vagas" placeholder="Digite Aqui">
    </div>
    <div class="form-group col-sm-6">
        <label>Manobrista?</label>
        <p><input type="checkbox" rel="Estacionamento - Manobrista" value="1" class="ml5 mt10 checkbox field"></p>
    </div>
    <div class="form-group col-sm-12">
        <label>Adaptado (especificar)?</label>
        <textarea class="form-control field" rel="Estacionamento - Adaptado" rows="4" placeholder="Digite Aqui"></textarea>
    </div>
    <?php
        $v_SupportFields = [
                'Serviço de copa',
                'Serviço de segurança',
                'Serviços médicos',
                'Serviços ambulatoriais',
                'Salão de beleza',
                'Tradução simultânea',
                'Serviços de café/buffet',
                'Farmácia',
                'Serviços de limpeza',
                'Informações turísticas',
                'Lojas',
                'Sala de leitura',
                'Ar condicionado',
                'Circuito interno de TV',
                'Data show',
                'TV/Vídeo',
                'Guarda bagagem',
                'Central telefônica',
                'Telefone',
                'Internet',
                'Banco 24 horas'
        ];
    ?>
    @foreach($v_SupportFields as $c_Field)
        <div class="form-group col-sm-3">
            <label>{{$c_Field}}?</label>
            <p><input type="checkbox" rel="{{$c_Field}}" class="ml5 mt10 checkbox field"></p>
        </div>
    @endforeach
</div>