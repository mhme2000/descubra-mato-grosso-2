@extends('admin.main')
@section('pageCSS')
    <link href="{{url('/vendor/select2/select2.min.css')}}" rel="stylesheet" />
    <style type="text/css">
        .align-center
        {
            text-align: center;
        }
        .margin-auto
        {
            margin: auto;
        }
        #physicalSpaceModal .modal-dialog{
            z-index:1051;
        }
        #tablePhysicalSpace tbody{
            max-height:400px;
            overflow-y: scroll;
        }
        #tablePhysicalSpace tbody p{
            margin-bottom: 1px;
        }
        #tablePhysicalSpace tbody .table-actions > .btn{
            width: 36px;
            margin-right: 3px;
            margin-top: 1px;
            margin-bottom: 1px;
        }
        @media (min-width: 768px){
            .modal-dialog {
                width: 750px;
            }
        }
        .panel-title > a.btn-success{
            color: white!important;
        }
    </style>
@stop
@section('panel-header')
    B5 - Serviço e equipamento para eventos
    <a title="Imprimir" type="button" class="btn btn-success pull-right" onclick="print()">
        <i class="fa fa-print"></i>
    </a>
@stop
@section('content')
    <?php \App\BaseInventoryModel::startFormFieldIndexing(); ?>
    <div class="row">
        <div class="mb20 col-sm-12 visible-print">
            <h2>B5 - Serviço e equipamento para eventos</h2>
        </div>
        @if(!\App\UserType::isParceiro())
        {!! Form::open(['id' => 'mainForm', 'url'=> url('/admin/inventario/servicos-eventos'), 'onsubmit' => 'return submitForm()', 'files' => true]) !!}
        @else
        <div id="mainForm">
        @endif

        @if($p_EventService != null && $p_EventService->id != null)
            <input type="hidden" name="id" value="{{$p_EventService->id}}">
        @endif
        @include('admin.util.headerIdentification', ['p_Form' => $p_EventService, 'p_Types' => $p_Types, 'p_Holding' => true, 'p_CadasturActivityOptions' => [''=>'',25=>'Centro de Convenções',40=>'Prestador de Infraestrutura de Apoio para Eventos',80=>'Organizadora de Eventos']])

        <input type="hidden" id="default_tipo_atividade_cadastur" value="80">
        
        <div class="form-group col-sm-6">
            <label for="telefone">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Telefone (exemplo: (DDD) 9 9999-9999 / (DDD) 9999-9999)<span class="mandatory-field">*</span></label>
            <input type="text" name="formulario[telefone]" class="form-control phone-field" id="telefone" placeholder="Digite Aqui" value="{{$p_EventService == null ? '' : $p_EventService->telefone}}" required>
        </div>
        <div class="form-group col-sm-6">
            <label for="whatsapp">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Whatsapp</label>
            <input type="text" name="formulario[whatsapp]" class="form-control phone-field" id="whatsapp" placeholder="Digite Aqui" value="{{$p_EventService == null ? '' : $p_EventService->whatsapp}}">
        </div>
        <div class="form-group col-sm-6">
            <label for="site">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Site</label>
            <input type="url" name="formulario[site]" class="form-control" id="site" placeholder="Digite Aqui" value="{{$p_EventService == null ? '' : $p_EventService->site}}">
        </div>
        <div class="form-group col-sm-6">
            <label for="email">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Email</label>
            <input type="text" name="formulario[email]" class="email-field form-control" id="email" placeholder="Digite Aqui" value="{{$p_EventService == null ? '' : $p_EventService->email}}">
        </div>

        <div class="form-group col-sm-12">
            <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Localização e ambiência</h3>
        </div>
        <div class="form-group col-sm-6">
            <label for="cep">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}CEP<span class="mandatory-field">*</span></label>
            <input name="formulario[cep]" type="text" class="form-control cep-field" id="cep" placeholder="Digite Aqui" value="{{$p_EventService == null ? '' : $p_EventService->cep}}" required>
        </div>
        <div class="form-group col-sm-6">
            <label for="bairro">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Bairro<span class="mandatory-field">*</span></label>
            <input type="text" name="formulario[bairro]" class="form-control" id="bairro" placeholder="Digite Aqui" value="{{$p_EventService == null ? '' : $p_EventService->bairro}}" required>
        </div>
        <div class="form-group col-sm-6">
            <label for="logradouro">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Logradouro<span class="mandatory-field">*</span></label>
            <input type="text" name="formulario[logradouro]" class="form-control" id="logradouro" placeholder="Digite Aqui" value="{{$p_EventService == null ? '' : $p_EventService->logradouro}}" required>
        </div>
        <div class="form-group col-sm-6">
            <label for="numero">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Número<span class="mandatory-field">*</span></label>
            <input type="text" name="formulario[numero]" class="form-control" id="numero" placeholder="Digite Aqui" value="{{$p_EventService == null ? '' : $p_EventService->numero}}" required>
        </div>
        <div class="form-group col-sm-12">
            <label for="complemento">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Complemento</label>
            <input type="text" name="formulario[complemento]" class="form-control geolocalizacao" id="complemento" placeholder="Digite Aqui" value="{{$p_EventService == null ? '' : $p_EventService->complemento}}">
            <a title="Pesquisar geolocalização" type="button" class="btn btn-success btn-search-geolocation" onclick="updateMarkerPosition(1)">
                <i class="fa fa-map-marker"></i>
            </a>
        </div>
        <div class="form-group col-sm-12">
            <label for="descricao_arredores_distancia_pontos">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Descrição dos arredores e distância dos principais pontos turísticos<span class="mandatory-field">*</span></label>
            <textarea name="formulario[descricao_arredores_distancia_pontos]" id="descricao_arredores_distancia_pontos" class="form-control" rows="4" placeholder="Digite Aqui" required>{{$p_EventService == null ? '' : $p_EventService->descricao_arredores_distancia_pontos}}</textarea>
        </div>
        <div class="form-group col-sm-12">
            <?php
                $v_Locations = [
                    ''=>'',
                    'Urbana'=>'Urbana',
                    'Rururbana'=>'Rururbana',
                    'Rural'=>'Rural'
                ];
            ?>
            <label for="localizacao">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Localização<span class="mandatory-field">*</span></label>
            {!! Form::select('formulario[localizacao]', $v_Locations, $p_EventService == null ? '' : $p_EventService->localizacao, ['id' => 'localizacao', 'class' => 'form-control', 'required' => 'required']) !!}
        </div>
        <div class="form-group col-sm-12">
            <p>* Movimente o marcador no mapa para ajustar a latitude e longitude.</p>
            <div id="map_canvas" style="height:320px;"></div>
        </div>
        <div class="form-group col-sm-6">
            <label for="latitude">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Latitude <i>(formato decimal)</i></label>
            <input type="number" name="formulario[latitude]" step="0.0000001" class="form-control" id="latitude" placeholder="Digite Aqui" value="{{$p_EventService == null ? '' : $p_EventService->latitude}}">
        </div>
        <div class="form-group col-sm-6">
            <label for="longitude">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Longitude <i>(formato decimal)</i></label>
            <input type="number" name="formulario[longitude]" step="0.0000001" class="form-control" id="longitude" placeholder="Digite Aqui" value="{{$p_EventService == null ? '' : $p_EventService->longitude}}">
        </div>
        <div class="form-group col-sm-12">
            <label for="latitude_longitude_decimal">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Latitude e Longitude em decimal</label>
            <input type="text" class="form-control" id="latitude_longitude_decimal" placeholder="Digite Aqui">
        </div>
        

        <div class="form-group col-sm-12">
            <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Distância de Aeroportos</h3>
        </div>
        <div class="form-group col-sm-6">
            <label for="nome_aeroporto1">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Nome aeroporto 1</label>
            <input type="text" name="formulario[nome_aeroporto1]" id="nome_aeroporto1" class="form-control" placeholder="Digite Aqui" value="{{$p_EventService == null ? '' : $p_EventService->nome_aeroporto1}}">
        </div>
        <div class="form-group col-sm-6">
            <label for="distancia_aeroporto1">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Distância em km</label>
            <input type="text" name="formulario[distancia_aeroporto1]" id="distancia_aeroporto1" class="form-control integer-field" placeholder="Digite Aqui" value="{{$p_EventService == null ? '' : $p_EventService->distancia_aeroporto1}}">
        </div>
        <div class="form-group col-sm-6">
            <label for="nome_aeroporto2">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Nome aeroporto 2</label>
            <input type="text" name="formulario[nome_aeroporto2]" id="nome_aeroporto2" class="form-control" placeholder="Digite Aqui" value="{{$p_EventService == null ? '' : $p_EventService->nome_aeroporto2}}">
        </div>
        <div class="form-group col-sm-6">
            <label for="distancia_aeroporto2">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Distância em km</label>
            <input type="text" name="formulario[distancia_aeroporto2]" id="distancia_aeroporto2" class="form-control integer-field" placeholder="Digite Aqui" value="{{$p_EventService == null ? '' : $p_EventService->distancia_aeroporto2}}">
        </div>

        @include('admin.util.photos', ['p_CoverPhoto' => $p_CoverPhoto, 'p_PhotoGallery' => $p_PhotoGallery, 'p_Cover' => true])

        @include('admin.util.workingPeriod', ['p_Form' => $p_EventService, 'p_ClosedFields' => true])

        @include('admin.moduleB.eventService.util.servicesEquipments', ['p_Form' => $p_EventService])

        <div class="form-group col-sm-12">
            <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Segurança</h3>
        </div>
        <div class="form-group col-sm-12">
            <label for="apresenta_projeto_seguranca">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Apresenta Projeto de Segurança contra Incêndio e Antipânico (PSCIP)?</label>
            <p><input type="checkbox" value="1" name="formulario[apresenta_projeto_seguranca]" id="apresenta_projeto_seguranca" class="ml5 mt10" {{($p_EventService == null || $p_EventService->apresenta_projeto_seguranca == 0) ? '' : 'checked'}}></p>
        </div>

        <div class="form-group col-sm-12">
            <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Dados Complementares</h3>
        </div>
        <div class="form-group col-sm-12 mt20">
            <label for="descricoes_observacoes_complementares">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Descrições e observações complementares</label>
            <textarea name="formulario[descricoes_observacoes_complementares]" id="descricoes_observacoes_complementares" class="form-control" rows="4" placeholder="Digite Aqui">{{$p_EventService == null ? '' : $p_EventService->descricoes_observacoes_complementares}}</textarea>
        </div>

        @include('admin.util.accessibility', ['p_Form' => $p_EventService])

        <div class="form-group col-sm-12">
            <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Uso interno da SEDEC/TUR</h3>
        </div>
        <input type="hidden" id="tipos_viagem" name="formulario[tipos_viagem]" value="{{$p_EventService == null ? '' : $p_EventService->tipos_viagem}}">
        <div class="form-group col-sm-12">
            <label for="tipos_viagem">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Tipo de polo <i>(permite mais de uma opção)</i></label>
            {!! Form::select('', $p_TripTypes, null, ['id' => 'tipos_viagem_select', 'class' => 'form-control select2', 'style' => 'width: 100%', 'multiple' => 'multiple']) !!}
        </div>

        @include('admin.util.revision', ['p_Form' => $p_Linked == 1 ? null : $p_EventService])

        @include('admin.util.responsibleTeam', ['p_Form' => $p_EventService])


        @if(!\App\UserType::isParceiro())
        <div class="form-group col-sm-12">
            <input type="submit" class="btn btn-default mt15 mb25 pull-right" value="Salvar">
        </div>
        {!! Form::close() !!}
        @else
        </div>
        @endif
    </div>
    <div id="modalContainer">
        @include('admin.moduleB.eventService.util.servicesEquipmentsModal')
    </div>
@stop
@section('pageScript')
    <script src="{{url('/vendor/select2/select2.min.js')}}" type="text/javascript"></script>
    <script src="{{url('/vendor/select2/i18n/pt-BR.js')}}" type="text/javascript"></script>
    <script src="{{url('/assets/js/inputmask.js')}}" type="text/javascript"></script>
    <script src="{{url('/assets/js/jquery.inputmask.js')}}" type="text/javascript"></script>
    @include('admin.util.workingPeriodScript')
    @include('admin.util.headerIdentificationScript', ['p_CadasturActivities' => $p_CadasturActivities])
    @include('admin.moduleB.eventService.util.servicesEquipmentsScript')
    @include('admin.util.photosScript', ['p_MaxPhotoCount' => -1])
    <script>
        $(document).ready(function()
        {
            $(".select2").select2({language:'pt-BR'});

            $('.phone-field').keypress(function (e) {
                var key = e.keyCode || e.charCode;
                if( key == 8 || key == 37 || key == 39 || key == 46 )
                    return true;

                var regex = new RegExp(/^[\d\s\(\)\-\+\.\,\;\\\/]+$/);
                var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
                if (regex.test(str))
                    return true;

                e.preventDefault();
                return false;
            });
            $('input[type="url"]').focus(function(){
                if(this.value == '')
                    this.value = 'http://';
            }).focusout(function(){
                if(this.value == 'http://')
                    this.value = '';
            });

            $('#equipamento_fechado').change(function(){
                if($(this).is(":checked")) {
                    $('.closed-equipment-fields').show();
                    $('.closed-equipment-fields.mandatory .form-control').attr('required', true);
                }
                else {
                    $('.closed-equipment-fields').hide();
                    $('.closed-equipment-fields.mandatory .form-control').removeAttr('required');
                }
            }).change();

            var v_TripTypes = $('#tipos_viagem').val();
            if(v_TripTypes.length > 0)
            {
                v_TripTypes = JSON.parse(v_TripTypes);
                $('#tipos_viagem_select').select2("val", v_TripTypes);
            }
        });


        function submitForm()
        {
            if(!validateCNPJ($('.cnpj-field').val()))
            {
                alert('CNPJ inválido!');
                return false;
            }

            $('.cnpj-field').each(function (c_Key, c_Field)
            {
                var v_Value = $(c_Field).val().replace(/[^\d]+/g,'');
                $(c_Field).parent().find('input[type="hidden"]').val(v_Value);
            });

            processaDadosFuncionamentos();

            processaDadosAcessibilidade();

            processaDadosServicosEquipamentos();

            $('#tipos_viagem').val(JSON.stringify(getSelectValue($('#tipos_viagem_select').val())));

            return true;
        }

        function getSelectValue(p_SelectValue){
            if(p_SelectValue == null || p_SelectValue == undefined)
                return [];
            else return p_SelectValue;
        }



    </script>
    @include('admin.util.mapScript')
    @include('admin.util.accessibilityScript')
    @include('admin.util.responsibleTeamScript')
    @include('admin.util.revisionScript')
    @include('admin.util.cadastur30')
    @include('admin.util.tooltipScript', ['p_Type' => 'B5'])
@stop