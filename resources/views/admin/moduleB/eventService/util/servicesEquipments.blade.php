<div class="form-group col-sm-12">
    <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Espaço físico</h3>
</div>
<input type="hidden" id="espaco_fisico" name="formulario[espaco_fisico]" value="{{$p_Form == null ? '' : $p_Form->espaco_fisico}}">
<div class="espaco-fisico-fields col-sm-12">
    <table class="table table-striped" id="tablePhysicalSpace">
        <thead>
            <tr>
                <th>Nome</th>
                <th>Tipo</th>
                <th>Área (m<sup>2</sup>)</th>
                <th>Pé-direito (m)</th>
                <th>Dimensões (CxL)</th>
                <th>Capacidade</th>
                <th>Espaço</th>
                <th>Formato</th>
                <th>Ação</th>
            </tr>
        </thead>
        <tbody>
            <tr id="tablePhysicalSpaceNewItem" style="text-align: center">
                <td colspan="9">
                    <button type="button" class="btn btn-success" onclick="editPhysicalSpaceItem('')">
                        Adicionar espaço
                    </button>
                </td>
            </tr>
        </tbody>
    </table>
</div>


<div class="form-group col-sm-12">
    <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Serviços e equipamentos</h3>
</div>
<input type="hidden" id="servicos_equipamentos_apoio" name="formulario[servicos_equipamentos_apoio]" value="{{$p_Form == null ? '' : $p_Form->servicos_equipamentos_apoio}}">
<div class="servicos-equipamentos-apoio-fields">
    <?php
        $v_SupportFields = [
                'Restaurante',
                'Bar/Lanchonete',
                'Elevadores'
        ];
    ?>
    @foreach($v_SupportFields as $c_Field)
        <div class="col-sm-12">
            <h4>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}{{$c_Field}}</h4>
        </div>
        @if($c_Field == 'Restaurante')
            <div class="form-group col-sm-6">
                <label>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(2)}}Nome</label>
                <input type="text" class="form-control field" rel="{{$c_Field}} - Nome" placeholder="Digite Aqui">
            </div>
        @endif
        <div class="form-group col-sm-3">
            <label>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(2)}}Quantidade</label>
            <input type="number" min="0" class="form-control field" rel="{{$c_Field}} - Quantidade" placeholder="Digite Aqui">
        </div>
        @if($c_Field == 'Restaurante' || $c_Field == 'Bar/Lanchonete')
            <div class="form-group col-sm-3">
                <label>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(2)}}Capacidade</label>
                <input type="text" rel="{{$c_Field}} - Capacidade" class="form-control integer-field field" placeholder="Digite Aqui">
            </div>
        @endif
    @endforeach
    <div class="col-sm-12">
        <h4>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Instalação sanitária</h4>
    </div>
    <div class="form-group col-sm-3 sanitario-fields">
        <label>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(2)}}Feminino?</label>
        <p><input type="checkbox" rel="Instalação sanitária - Feminino" value="sanitario-feminino-field" class="ml5 mt10 checkbox field"></p>
    </div>
    <div class="form-group col-sm-3 sanitario-feminino-field">
        <label>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(3)}}Quantidade</label>
        <input type="number" min="0" class="form-control field" rel="Instalação sanitária - Feminino - Quantidade" placeholder="Digite Aqui">
    </div>
    <div class="col-sm-12"></div>
    <div class="form-group col-sm-3 sanitario-fields">
        <label>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(2)}}Masculino?</label>
        <p><input type="checkbox" rel="Instalação sanitária - Masculino" value="sanitario-masculino-field" class="ml5 mt10 checkbox field"></p>
    </div>
    <div class="form-group col-sm-3 sanitario-masculino-field">
        <label>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(3)}}Quantidade</label>
        <input type="number" min="0" class="form-control field" rel="Instalação sanitária - Masculino - Quantidade" placeholder="Digite Aqui">
    </div>

    <div class="col-sm-12">
        <h4>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Estacionamento</h4>
    </div>
    <div class="form-group col-sm-6">
        <label>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(2)}}Vagas</label>
        <input type="text" rel="Estacionamento - Vagas" class="form-control integer-field field" placeholder="Digite Aqui">
    </div>
    <div class="form-group col-sm-6">
        <label>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(2)}}Manobrista?</label>
        <p><input type="checkbox" rel="Estacionamento - Manobrista" value="1" class="ml5 mt10 checkbox field"></p>
    </div>

    <div class="col-sm-12">
        <h4>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Acessórios e multimídia</h4>
    </div>
    <?php
        $v_SupportFields = [
            'Ar central',
            'TV',
            'Projetor',
            'DVD',
            'Telas móveis',
            'Computador',
            'Wi Fi',
            'Wi Fi liberada (free zone)',
            'Cadeiras com pranchetas',
            'Sistema de som'
        ];
    ?>
    @foreach($v_SupportFields as $c_Field)
        <div class="form-group col-sm-3">
            <label>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(2)}}{{$c_Field}}?</label>
            <p><input type="checkbox" rel="{{$c_Field}}" class="ml5 mt10 checkbox field"></p>
        </div>
    @endforeach
</div>

<div class="form-group col-sm-12">
    <label for="outras_instalacoes_equipamentos">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Outros serviços e equipamentos</label>
    <textarea name="formulario[outras_instalacoes_equipamentos]" id="outras_instalacoes_equipamentos" class="form-control" rows="4" placeholder="Digite Aqui">{{$p_Form == null ? '' : $p_Form->outras_instalacoes_equipamentos}}</textarea>
</div>