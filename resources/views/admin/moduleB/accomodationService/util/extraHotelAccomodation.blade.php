<div class="form-group col-sm-12">
    <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Meios de hospedagem extra-hoteleiros</h3>
</div>
<input type="hidden" id="meios_hospedagem_extra_hoteleiros" name="formulario[meios_hospedagem_extra_hoteleiros]" value="{{$p_Form == null ? '' : $p_Form->meios_hospedagem_extra_hoteleiros}}">
<div class="meios-hospedagem-extra-hoteleiros-fields">
    <div class="col-sm-12">
        <h4>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Barracas</h4>
    </div>
    <div class="form-group col-sm-6">
        <label>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(2)}}Quantidade</label>
        <input type="number" min="0" rel="Barracas - Quantidade" class="form-control field" placeholder="Digite Aqui">
    </div>
    <div class="form-group col-sm-6">
        <label>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(2)}}Capacidade de pessoas</label>
        <input type="text" rel="Barracas - Capacidade de pessoas" class="form-control field integer-field" placeholder="Digite Aqui">
    </div>
    <div class="col-sm-12">
        <h4>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Trailers</h4>
    </div>
    <div class="form-group col-sm-6">
        <label>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(2)}}Quantidade</label>
        <input type="number" min="0" rel="Trailers - Quantidade" class="form-control field" placeholder="Digite Aqui">
    </div>
    <div class="form-group col-sm-6">
        <label>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(2)}}Capacidade de pessoas</label>
        <input type="text" rel="Trailers - Capacidade de pessoas" class="form-control field integer-field" placeholder="Digite Aqui">
    </div>
    <div class="col-sm-12">
        <h4>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Sanitários</h4>
    </div>
    <div class="form-group col-sm-6">
        <label>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(2)}}Quantidade</label>
        <input type="number" min="0" rel="Sanitários - Quantidade" class="form-control field" placeholder="Digite Aqui">
    </div>
    <div class="col-sm-12">
        <h4>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Chuveiro quente</h4>
    </div>
    <div class="form-group col-sm-6">
        <label>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(2)}}Quantidade</label>
        <input type="number" min="0" rel="Chuveiro quente - Quantidade" class="form-control field" placeholder="Digite Aqui">
    </div>
    <div class="col-sm-12">
        <h4>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Chuveiro frio</h4>
    </div>
    <div class="form-group col-sm-6">
        <label>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(2)}}Quantidade</label>
        <input type="number" min="0" rel="Chuveiro frio - Quantidade" class="form-control field" placeholder="Digite Aqui">
    </div>

    <div class="col-sm-12"></div>
    <?php
        $v_ExtraHotelAccomodationFields = [
                'Iluminação',
                'Ponto de energia',
                'Aluguel de barracas',
                'Segurança',
                'Aluguel de trailers',
                'Lava pratos',
                'Área verde para lazer',
                'Cofres',
                'Esgoto tratado'
        ];
    ?>
    @foreach($v_ExtraHotelAccomodationFields as $c_Field)
        <div class="form-group col-sm-3">
            <label>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}{{$c_Field}}?</label>
            <p><input type="checkbox" rel="{{$c_Field}}" class="ml5 mt10 checkbox field"></p>
        </div>
    @endforeach

    <div class="form-group col-sm-12">
        <label>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Outros</label>
        <textarea rel="Outros" class="form-control field" rows="4" placeholder="Digite Aqui"></textarea>
    </div>

    <div class="form-group col-sm-12">
        <label>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Voltagem <i>(permite mais de uma opção)</i></label>
        {!! Form::select('', $p_Voltages, null, ['rel' => 'Voltagem', 'class' => 'voltagem select2 field form-control', 'style' => 'width: 100%', 'multiple' => 'multiple']) !!}
    </div>
</div>