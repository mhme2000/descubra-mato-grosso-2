<div class="form-group col-sm-12">
    <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Serviços e equipamentos</h3>
</div>
<input type="hidden" id="servicos_equipamentos_area_social" name="formulario[servicos_equipamentos_area_social]" value="{{$p_Form == null ? '' : $p_Form->servicos_equipamentos_area_social}}">
<div class="social-area-fields">
    <div class="form-group col-sm-12">
        <label>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Restaurante aberto para o público?</label>
        <p><input name="restaurante_aberto_publico" id="restaurante_aberto_publico" type="checkbox" rel="Restaurante aberto para o público" class="ml5 mt10 checkbox field" value="1"></p>
    </div>
    <div class="restaurant-fields">
        <div class="form-group col-sm-6">
            <label>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(2)}}Capacidade do restaurante</label>
            <input type="text" rel="Restaurante - Capacidade" class="form-control field integer-field" placeholder="Digite Aqui">
        </div>
    </div>
    <div class="form-group col-sm-12">
        <h4>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Área social</h4>
    </div>
    <div class="col-sm-12"></div>
    <?php
        $v_SocialAreaFields = [
                'Bar/Lanchonente',
                'Churrasqueiras',
                'Estacionamento',
                'Manobristas',
                'Garagem',
                'Reservas p/ espetáculos',
                'Serviço bilingue',
                'Sala TV/Video',
                'Serviço de copa',
                'Circuito interno de TV',
                'Serviços médicos',
                'Sala de leitura',
                'Facilidades para executivos',
                'Música ambiente',
                'Farmácia',
                'Música ao vivo',
                'Salão de beleza',
                'Boate',
                'Cofre privativo',
                'Exposição de artes/artesanatos',
                'Elevador',
                'Caixa eletrônico',
                'Central telefônica',
                'Calefação',
                'Ar condicionado',
                'Informações turísticas',
                'Guarda bagagem',
                'Agência de viagens',
                'Câmbio',
                'Loja',
                'Lavanderia',
                'Internet',
                'Wi Fi',
                'Wi Fi liberada (free zone)'
        ];
    ?>
    @foreach($v_SocialAreaFields as $c_Field)
        <div class="form-group col-sm-3">
            <label>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(2)}}{{$c_Field}}?</label>
            <p><input type="checkbox" rel="{{$c_Field}}" class="ml5 mt10 checkbox field"></p>
        </div>
    @endforeach
    <div class="form-group col-sm-12">
        <label>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(2)}}Outros</label>
        <textarea rel="Outros" class="form-control field" rows="4" placeholder="Digite Aqui"></textarea>
    </div>
</div>

<div class="form-group col-sm-12">
    <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Recreação e lazer</h3>
</div>
<input type="hidden" id="servicos_equipamentos_recreacao_lazer" name="formulario[servicos_equipamentos_recreacao_lazer]" value="{{$p_Form == null ? '' : $p_Form->servicos_equipamentos_recreacao_lazer}}">
<div class="recreation-fields">
    <?php
    $v_RecreationFields = [
            'Piscina',
            'Piscina aquecida',
            'Sauna seca',
            'Sauna a vapor',
            'Quadra de esportes',
            'Playground',
            'Hidromassagem',
            'Spa',
            'Day Use',
            'Guia turístico',
            'Guia de pesca',
            'Monitor de recreação/passeios',
            'Sala de jogos',
            'Animais para montaria',
            'Campo de futebol',
            'Boliche',
            'Barcos/Embarcações',
            'Equipamentos para pesca',
            'Serviço de animação',
            'Pista de cooper',
            'Charretes',
            'Golfe',
            'Equipamento de mergulho',
            'Pingue-pongue',
            'Jogos eletrônicos'
    ];
    ?>
    @foreach($v_RecreationFields as $c_Field)
        <div class="form-group col-sm-3">
            <label>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}{{$c_Field}}?</label>
            <p><input type="checkbox" rel="{{$c_Field}}" class="ml5 mt10 checkbox field"></p>
        </div>
    @endforeach
</div>
