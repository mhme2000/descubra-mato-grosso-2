<script>
    $(document).ready(function()
    {
        carregaDadosMeiosHospedagemExtraHoteleiros();
    });

    function carregaDadosMeiosHospedagemExtraHoteleiros()
    {
        var v_Dados = $('#meios_hospedagem_extra_hoteleiros').val();
        if(v_Dados.length > 0)
        {
            v_Dados = JSON.parse(v_Dados);
            $(v_Dados).each(function(){
                if(this.tipo == 'select2')
                {
                    var v_Campo = this;
                    var v_SelectedItems = [];
                    $(v_Campo.valor).each(function(){
                        v_SelectedItems.push(this.id);
                    });
                    $('.meios-hospedagem-extra-hoteleiros-fields .field[rel="'+v_Campo.nome+'"]').select2("val", v_SelectedItems);
                }
                else if(this.tipo == 'checkbox')
                    $('.meios-hospedagem-extra-hoteleiros-fields .field[rel="'+this.nome+'"]').prop('checked',this.valor);
                else
                    $('.meios-hospedagem-extra-hoteleiros-fields .field[rel="'+this.nome+'"]').val(this.valor);
            });
        }
    }

    function processaDadosMeiosHospedagemExtraHoteleiros()
    {
        var v_Dados = [];
        $('.meios-hospedagem-extra-hoteleiros-fields .field').each(function(){
            if($(this).hasClass('select2'))
            {
                var v_Valores = [];
                $(this).find('option:selected').each(function(){
                    v_Valores.push({
                        id:$(this).attr('value'),
                        nome:$(this).text()
                    });
                });
                v_Dados.push({
                    nome:$(this).attr('rel'),
                    tipo:'select2',
                    valor:v_Valores
                });
            }
            else
            {
                v_Dados.push({
                    nome:$(this).attr('rel'),
                    tipo:$(this).hasClass('checkbox') ? 'checkbox' : 'text-or-select',
                    valor:$(this).hasClass('checkbox') ? $(this).is(':checked') : $(this).val()
                });
            }
        });
        $('#meios_hospedagem_extra_hoteleiros').val(JSON.stringify(v_Dados));
    }
</script>