<div class="form-group col-sm-12">
    <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Serviços e equipamentos</h3>
</div>
<input type="hidden" id="servicos_equipamentos_apoio" name="formulario[servicos_equipamentos_apoio]" value="{{$p_Form == null ? '' : $p_Form->servicos_equipamentos_apoio}}">
<div class="servicos-equipamentos-apoio-fields">
    <?php
        $v_SupportFields = [
                'Restaurante',
                'Bar/Lanchonete',
                'Elevadores',
                'Instalação sanitária'
        ];
    ?>
    @foreach($v_SupportFields as $c_Field)
        <div class="col-sm-12">
            <h4>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}{{$c_Field}}</h4>
        </div>
        @if($c_Field == 'Restaurante')
            <div class="form-group col-sm-6">
                <label>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(2)}}Nome</label>
                <input type="text" class="form-control field" rel="{{$c_Field}} - Nome" placeholder="Digite Aqui">
            </div>
        @endif
        <div class="form-group col-sm-3">
            <label>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(2)}}Quantidade</label>
            <input type="number" min="0" class="form-control field" rel="{{$c_Field}} - Quantidade" placeholder="Digite Aqui">
        </div>
        @if($c_Field == 'Restaurante' || $c_Field == 'Bar/Lanchonete')
            <div class="form-group col-sm-3">
                <label>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(2)}}Capacidade</label>
                <input type="text" class="form-control integer-field field" rel="{{$c_Field}} - Capacidade" placeholder="Digite Aqui">
            </div>
        @endif
    @endforeach
    <div class="col-sm-12">
        <h4>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Estacionamento</h4>
    </div>
    <div class="form-group col-sm-6">
        <label>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(2)}}Vagas</label>
        <input type="text" class="form-control integer-field field" rel="Estacionamento - Vagas" placeholder="Digite Aqui">
    </div>
    <div class="form-group col-sm-6">
        <label>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(2)}}Manobrista?</label>
        <p><input type="checkbox" rel="Estacionamento - Manobrista" value="1" class="ml5 mt10 checkbox field"></p>
    </div>

    <div class="col-sm-12">
        <h4>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Serviços de apoio</h4>
    </div>
    <?php
        $v_SupportFields = [
                'Serviço de segurança',
                'Serviços médicos',
                'Serviços ambulatoriais',
                'Serviços de café/buffet',
                'Informações turísticas',
                'Lojas',
                'Banco 24 horas',
                'Internet',
                'Wi Fi',
                'Wi Fi liberada (free zone)'
        ];
    ?>
    @foreach($v_SupportFields as $c_Field)
        <div class="form-group col-sm-3">
            <label>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(2)}}{{$c_Field}}?</label>
            <p><input type="checkbox" rel="{{$c_Field}}" class="ml5 mt10 checkbox field"></p>
        </div>
    @endforeach
</div>