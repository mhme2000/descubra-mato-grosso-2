<html>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <style>
        .city {
            margin:110px 0;
            padding: 10px 15px 20px;
            page-break-after: always;
            vertical-align: middle;
        }
        .city:last-child {
            page-break-after: auto;
        }
        table {
            border-collapse: collapse;
            margin:20px 0 0;
        }
        table tr td{
            border: 1px solid #000!important;
            text-align: center;
            padding: 5px;
        }
        table tr td p{
            margin: 5px 0;
            font-size: 13px;
        }
        h3{
            text-transform: uppercase!important;
            margin: 5px 0;
            font-size: 17px;
        }
        h3.blank {
            margin-bottom:50px;
        }
        .two-columns {
            width: 25%;
          }
    </style>

    @foreach($p_Cities as $c_City)
        <?php
            $v_Accomodation = [];
            $v_Accomodation['total'] = $c_City->accomodations($p_Filters, null)->count();
            $v_Accomodation['cadastur'] = $c_City->accomodations($p_Filters, 1)->count();
            $v_Agency = [];
            $v_Agency['total'] = $c_City->tourismAgencyServices($p_Filters, null)->count();
            $v_Agency['cadastur'] = $c_City->tourismAgencyServices($p_Filters, 1)->count();
            $v_Transport = [];
            $v_Transport['total'] = $c_City->tourismTransportationServices($p_Filters, null)->count();
            $v_Transport['cadastur'] = $c_City->tourismTransportationServices($p_Filters, 1)->count();

            $v_NaturalAttractions = $c_City->naturalAttractions($p_Filters)->count();
            $v_CulturalAttractions = $c_City->culturalAttractions($p_Filters)->count();
        ?>
        <div class="city">
            <h3>Município: {{$c_City->name}}</h3>
            <h3 class="blank">&nbsp;</h3>
            <h3>Mapa numérico</h3>
            <table>
                <tbody>
                    <tr>
                        <td class="two-columns" colspan="2">
                            <p><strong>Hospedagem (b1)</strong></p>
                            <p>{{$v_Accomodation['total']}}</p>
                        </td>
                        <td class="two-columns" colspan="2" rowspan="2">
                            <p><strong>Gastronomia (b2)</strong></p>
                            <p>{{$c_City->foodDrinkServices($p_Filters)->count()}}</p>
                        </td>
                        <td class="two-columns" colspan="2">
                            <p><strong>Agência (b3)</strong></p>
                            <p>{{$v_Agency['total']}}</p>
                        </td>
                        <td class="two-columns" colspan="2">
                            <p><strong>Transporte (b4)</strong></p>
                            <p>{{$v_Transport['total']}}</p>
                        </td>
                    </tr>
                    <tr>
                        <td rel="accomodation">
                            <p><strong>Com CADASTUR</strong></p>
                            <p>{{$v_Accomodation['cadastur']}}</p>
                        </td>
                        <td rel="accomodation">
                            <p><strong>Sem CADASTUR</strong></p>
                            <p>{{$v_Accomodation['total'] - $v_Accomodation['cadastur']}}</p>
                        </td>
                        <td rel="agency">
                            <p><strong>Com CADASTUR</strong></p>
                            <p>{{$v_Agency['cadastur']}}</p>
                        </td>
                        <td rel="agency">
                            <p><strong>Sem CADASTUR</strong></p>
                            <p>{{$v_Agency['total'] - $v_Agency['cadastur']}}</p>
                        </td>
                        <td rel="transport">
                            <p><strong>Com CADASTUR</strong></p>
                            <p>{{$v_Transport['cadastur']}}</p>
                        </td>
                        <td rel="transport">
                            <p><strong>Sem CADASTUR</strong></p>
                            <p>{{$v_Transport['total'] - $v_Transport['cadastur']}}</p>
                        </td>
                    </tr>
                    <tr>
                        <td class="two-columns" colspan="2" rowspan="2">
                            <p><strong>Equip. Eventos (b5)</strong></p>
                            @if(isset($p_Filters['published']) && $p_Filters['published'] == 1)
                                <p>0</p>
                            @else
                                <p>{{$c_City->eventServices($p_Filters)->count()}}</p>
                            @endif
                        </td>
                        <td class="two-columns" colspan="2" rowspan="2">
                            <p><strong>Equip. Lazer (b6)</strong></p>
                            <p>{{$c_City->recreationServices($p_Filters)->count()}}</p>
                        </td>
                        <td class="two-columns" colspan="2" rowspan="2">
                            <p><strong>Equip. Turísticos (b7)</strong></p>
                            @if(isset($p_Filters['published']) && $p_Filters['published'] == 1)
                                <p>0</p>
                            @else
                                <p>{{$c_City->otherTourismServices($p_Filters)->count()}}</p>
                            @endif
                        </td>
                        <td class="two-columns" colspan="2">
                            <p><strong>Atrativos (c1 e c2)</strong></p>
                            <p>{{$v_NaturalAttractions + $v_CulturalAttractions}}</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p><strong>Naturais</strong></p>
                            <p>{{$v_NaturalAttractions}}</p>
                        </td>
                        <td>
                            <p><strong>Culturais</strong></p>
                            <p>{{$v_CulturalAttractions}}</p>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    @endforeach
</html>