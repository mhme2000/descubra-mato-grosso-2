@extends('admin.main')
@section('pageCSS')
    <link href="{{url('/vendor/select2/select2.min.css')}}" rel="stylesheet" />
    <style type="text/css">
        .full-width{
            width: 100%;
        }
        .align-center{
            text-align: center;
        }
        .margin-auto{
            margin: auto;
        }
    </style>
@stop
@section('panel-header')
    Relatório de Destinos
@stop
@section('content')
    <div class="row pt15">
        <div class="col-sm-12">
            <h3 class="mtn mb20">Filtros</h3>
        </div>
        {!! Form::open(['id' => 'mainForm', 'method' => 'GET', 'target' => '_blank', 'url'=> url('/admin/relatorios/destinos/gerar')]) !!}
        <div class="form-group col-sm-12">
            <label for="id">Município <i>(permite mais de uma opção)</i></label>
            {!! Form::select('id[]', $p_Cities, '', ['id' => 'id', 'class' => 'form-control select2', 'style' => 'width: 100%', 'multiple' => 'multiple']) !!}
        </div>
        <div class="form-group col-sm-6">
            <label for="touristic_circuit_id">Região Turística</label>
            {!! Form::select('touristic_circuit_id', $p_Circuits, '', ['id' => 'touristic_circuit_id', 'class' => 'form-control select2', 'style' => 'width: 100%']) !!}
        </div>
        <div class="form-group col-sm-6">
            <label for="region_id">Região</label>
            {!! Form::select('region_id', $p_Regions, '', ['id' => 'region_id', 'class' => 'form-control select2', 'style' => 'width: 100%']) !!}
        </div>
        <div class="form-group col-sm-12">
            <input type="submit" class="btn btn-default mt15 mb25 pull-right" value="Gerar relatório">
        </div>
        {!! Form::close() !!}
    </div>
@stop
@section('pageScript')
    <script src="{{url('/vendor/select2/select2.min.js')}}" type="text/javascript"></script>
    <script src="{{url('/vendor/select2/i18n/pt-BR.js')}}" type="text/javascript"></script>
    <script>
        $(document).ready(function()
        {
            $(".select2").select2({language:'pt-BR'});
        });
    </script>
@stop