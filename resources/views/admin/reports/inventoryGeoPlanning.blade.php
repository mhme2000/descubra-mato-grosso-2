@extends('admin.main')
@section('pageCSS')
    <link href="{{url('/vendor/select2/select2.min.css')}}" rel="stylesheet" />
    <style type="text/css">
        .full-width{
            width: 100%;
        }
        .align-center{
            text-align: center;
        }
        .margin-auto{
            margin: auto;
        }

        .panel-title > a.btn-success{
            color: white!important;
        }
        #map_canvas{
            margin-bottom:15px;
        }
        #responsive-table td{
            padding:4px 7px;
        }
        .table-pin{
            height:24px;
            margin-right:5px;
        }
        .item-filter input, .type-filter input{
            margin-right:5px;
            float:left;
        }
        .item-filter label, .type-filter label{
            font-weight: normal;
            user-select: none;
            cursor: pointer;
            float:left;
            font-size: 12px;
        }
        .type-filter p{
            margin-bottom:2px;
        }
        @media(min-width:767px){
            #responsive-table .type-filter{
                -moz-column-count: 2;
                -webkit-column-count: 2;
                column-count: 2;
            }
        }
    </style>
@stop
@section('panel-header')
    Planejamento georreferenciado do inventário
    @if(isset($p_ShowMap))
    <a title="Imprimir" type="button" class="btn btn-success pull-right" onclick="print()">
        <i class="fa fa-print"></i>
    </a>
    @endif
@stop
@section('content')
    <div class="row pt15">
        {!! Form::open(['id' => 'mainForm', 'url'=> url('/admin/relatorios/inventario/planejamento-georreferenciado')]) !!}
        <div class="col-sm-12">
            <h3 class="mtn mb20">Filtros</h3>
        </div>
        <div class="form-group col-sm-12">
            <label for="id">Município <i>(permite mais de uma opção)</i></label>
            {!! Form::select('id[]', $p_Cities, isset($p_Filters) && isset($p_Filters['id']) ? $p_Filters['id'] : '', ['id' => 'id', 'class' => 'form-control select2', 'style' => 'width: 100%', 'multiple' => 'multiple']) !!}
        </div>
        <div class="form-group col-sm-6">
            <label for="touristic_circuit_id">Região Turística<i>(permite mais de uma opção)</i></label>
            {!! Form::select('touristic_circuit_id[]', $p_Circuits, isset($p_Filters) && isset($p_Filters['touristic_circuit_id']) ? $p_Filters['touristic_circuit_id'] : '', ['id' => 'touristic_circuit_id', 'class' => 'form-control select2', 'style' => 'width: 100%', 'multiple' => 'multiple']) !!}
        </div>
        <div class="form-group col-sm-6">
            <label for="region_id">Região <i>(permite mais de uma opção)</i></label>
            {!! Form::select('region_id[]', $p_Regions, isset($p_Filters) && isset($p_Filters['region_id']) ? $p_Filters['region_id'] : '', ['id' => 'region_id', 'class' => 'form-control select2', 'style' => 'width: 100%', 'multiple' => 'multiple']) !!}
        </div>
        <div class="form-group col-sm-6">
            <label for="revision_status_id">Status <i>(permite mais de uma opção)</i></label>
            {!! Form::select('revision_status_id[]', ["Todos" => "Todos"] + $p_Status, isset($p_Filters) && isset($p_Filters['revision_status_id']) ? $p_Filters['revision_status_id'] : '', ['id' => 'revision_status_id', 'class' => 'form-control select2', 'style' => 'width: 100%', 'multiple' => 'multiple']) !!}
        </div>
        <div class="form-group col-sm-6">
            <label for="published">Somente publicados no Portal?</label>
            {!! Form::select('published', ['' => '', 0 => 'Não', 1 => 'Sim'], isset($p_Filters) && isset($p_Filters['published']) ? $p_Filters['published'] : '', ['id' => 'published', 'class' => 'form-control select2', 'style' => 'width: 100%']) !!}
        </div>
        <div class="form-group col-sm-12">
            <input type="submit" class="btn btn-default mv15 pull-right" value="Gerar relatório">
        </div>
        {!! Form::close() !!}
    </div>
    @if(isset($p_ShowMap))
        <div class="row">
            <div class="col-sm-12">
                <div id="map_canvas" style="height:600px;"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <h3 class="mtn mv15">Exibição</h3>
            </div>
            <div class="col-sm-12">
                <table class="table table-striped table-hover" id="responsive-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Origem</th>
                            <th>Nível 1</th>
                            <th>Nível 2</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><img src="{{url('/assets/img/pins/A4.png')}}" class="table-pin">A4</td>
                            <td class="item-filter"><input id="A4" type="checkbox" value="1" checked><label for="A4"> Sistema de Segurança</label></td>
                            <td class="type-filter"></td>
                        </tr>
                        <tr>
                            <td><img src="{{url('/assets/img/pins/A5.png')}}" class="table-pin">A5</td>
                            <td class="item-filter"><input id="A5" type="checkbox" value="1" checked><label for="A5"> Sistema Médico Hospitalar</label></td>
                            <td class="type-filter"></td>
                        </tr>
                        <tr>
                            <td><img src="{{url('/assets/img/pins/A7.png')}}" class="table-pin">A7</td>
                            <td class="item-filter"><input id="A7" type="checkbox" value="1" checked><label for="A7"> Serviços e Equipamentos de apoio</label></td>
                            <td class="type-filter">
                                <p class="clearfix"><input id="A7_1" data-item="A7" class="type-checkbox" type="checkbox" value="23" checked><label for="A7_1"> Comércio</label></p>
                                <p class="clearfix"><input id="A7_2" data-item="A7" class="type-checkbox" type="checkbox" value="28" checked><label for="A7_2"> Agencias bancárias/Casas de cambio</label></p>
                                <p class="clearfix"><input id="A7_3" data-item="A7" class="type-checkbox" type="checkbox" value="30" checked><label for="A7_3"> Postos de abastecimento</label></p>
                                <p class="clearfix"><input id="A7_4" data-item="A7" class="type-checkbox" type="checkbox" value="31" checked><label for="A7_4"> Locais/Templos de manifestação de fé</label></p>
                                <p class="clearfix"><input id="A7_5" data-item="A7" class="subtype-checkbox" type="checkbox" value="33,34" checked><label for="A7_5"> Representações diplomáticas</label></p>
                            </td>
                        </tr>
                        <tr>
                            <td><img src="{{url('/assets/img/pins/B1.png')}}" class="table-pin">B1</td>
                            <td class="item-filter"><input id="B1" type="checkbox" value="1" checked><label for="B1"> Meios de Hospedagem</label></td>
                            <td class="type-filter">
                                <p class="clearfix"><input id="B1_1" data-item="B1" class="subtype-checkbox" type="checkbox" value="38,39" checked><label for="B1_1"> Hotel</label></p>
                                <p class="clearfix"><input id="B1_2" data-item="B1" class="subtype-checkbox" type="checkbox" value="40,42" checked><label for="B1_2"> Hotel de Lazer/Resort</label></p>
                                <p class="clearfix"><input id="B1_3" data-item="B1" class="subtype-checkbox" type="checkbox" value="41" checked><label for="B1_3"> Pousada</label></p>
                                <p class="clearfix"><input id="B1_4" data-item="B1" class="subtype-checkbox" type="checkbox" value="43" checked><label for="B1_4"> Apart-hotel/Flat/Condohotel</label></p>
                                <p class="clearfix"><input id="B1_5" data-item="B1" class="subtype-checkbox" type="checkbox" value="47,48,53" checked><label for="B1_5"> Hospedaria/Pensao/Albergue</label></p>
                                <p class="clearfix"><input id="B1_6" data-item="B1" class="subtype-checkbox" type="checkbox" value="49" checked><label for="B1_6"> Motel</label></p>
                                <p class="clearfix"><input id="B1_7" data-item="B1" class="subtype-checkbox" type="checkbox" value="51" checked><label for="B1_7"> Camping</label></p>
                                <p class="clearfix"><input id="B1_8" data-item="B1" class="subtype-checkbox" type="checkbox" value="52" checked><label for="B1_8"> Colônia de férias</label></p>
                            </td>
                        </tr>
                        <tr>
                            <td><img src="{{url('/assets/img/pins/B2.png')}}" class="table-pin">B2</td>
                            <td class="item-filter"><input id="B2" type="checkbox" value="1" checked><label for="B2"> Serviços e Equipamentos Gastronômicos</label></td>
                            <td class="type-filter">
                                <p class="clearfix"><input id="B2_1" data-item="B2" class="type-checkbox" type="checkbox" value="55" checked><label for="B2_1"> Restaurantes</label></p>
                                <p class="clearfix"><input id="B2_2" data-item="B2" class="type-checkbox" type="checkbox" value="56" checked><label for="B2_2"> Bares/Cafés/Lanchonetes</label></p>
                                <p class="clearfix"><input id="B2_3" data-item="B2" class="type-checkbox" type="checkbox" value="58" checked><label for="B2_3"> Cervejarias</label></p>
                                <p class="clearfix"><input id="B2_4" data-item="B2" class="type-checkbox" type="checkbox" value="59" checked><label for="B2_4"> Quiosques/Barracas</label></p>
                                <p class="clearfix"><input id="B2_5" data-item="B2" class="type-checkbox" type="checkbox" value="60" checked><label for="B2_5"> Sorveterias</label></p>
                                <p class="clearfix"><input id="B2_6" data-item="B2" class="type-checkbox" type="checkbox" value="62" checked><label for="B2_6"> Outros</label></p>
                            </td>
                        </tr>
                        <tr>
                            <td><img src="{{url('/assets/img/pins/B3.png')}}" class="table-pin">B3</td>
                            <td class="item-filter"><input id="B3" type="checkbox" value="1" checked><label for="B3"> Serviços de Agenciamento Turístico</label></td>
                            <td class="type-filter">
                                <p class="clearfix"><input id="B3_1" data-item="B3" class="type-checkbox" type="checkbox" value="63" checked><label for="B3_1"> Agencia de turismo receptivo</label></p>
                                <p class="clearfix"><input id="B3_2" data-item="B3" class="type-checkbox" type="checkbox" value="64" checked><label for="B3_2"> Agencia de turismo emissivo</label></p>
                                <p class="clearfix"><input id="B3_3" data-item="B3" class="type-checkbox" type="checkbox" value="256" checked><label for="B3_3"> Agencia de turismo receptivo e emissivo</label></p>
                            </td>
                        </tr>
                        <tr>
                            <td><img src="{{url('/assets/img/pins/B4.png')}}" class="table-pin">B4</td>
                            <td class="item-filter"><input id="B4" type="checkbox" value="1" checked><label for="B4"> Serviços de transporte</label></td>
                            <td class="type-filter">
                                <p class="clearfix"><input id="B4_1" data-item="B4" class="type-checkbox" type="checkbox" value="65" checked><label for="B4_1"> Transportadoras turísticas</label></p>
                                <p class="clearfix"><input id="B4_2" data-item="B4" class="type-checkbox" type="checkbox" value="66" checked><label for="B4_2"> Locadoras</label></p>
                                <p class="clearfix"><input id="B4_3" data-item="B4" class="type-checkbox" type="checkbox" value="67" checked><label for="B4_3"> Taxis</label></p>
                                <p class="clearfix"><input id="B4_4" data-item="B4" class="type-checkbox" type="checkbox" value="68" checked><label for="B4_4"> Outros</label></p>
                            </td>
                        </tr>
                        <tr>
                            <td><img src="{{url('/assets/img/pins/B5.png')}}" class="table-pin">B5</td>
                            <td class="item-filter"><input id="B5" type="checkbox" value="1" checked><label for="B5"> Serviços e equipamentos para eventos</label></td>
                            <td class="type-filter">
                                <p class="clearfix"><input id="B5_1" data-item="B5" class="type-checkbox" type="checkbox" value="69" checked><label for="B5_1"> Centros de convenções/congressos</label></p>
                                <p class="clearfix"><input id="B5_2" data-item="B5" class="type-checkbox" type="checkbox" value="70" checked><label for="B5_2"> Parques/Pavilhões de exposições</label></p>
                                <p class="clearfix"><input id="B5_3" data-item="B5" class="type-checkbox" type="checkbox" value="71" checked><label for="B5_3"> Auditórios/Salões de convenções</label></p>
                                <p class="clearfix"><input id="B5_4" data-item="B5" class="type-checkbox" type="checkbox" value="73" checked><label for="B5_4"> Outros serviços/equip. especializados</label></p>
                            </td>
                        </tr>
                        <tr>
                            <td><img src="{{url('/assets/img/pins/B6.png')}}" class="table-pin">B6</td>
                            <td class="item-filter"><input id="B6" type="checkbox" value="1" checked><label for="B6"> Serviços de Lazer e Entretenimento</label></td>
                            <td class="type-filter">
                                <p class="clearfix"><input id="B6_1" data-item="B6" class="type-checkbox" type="checkbox" value="83,85" checked><label for="B6_1"> Casas de dança</label></p>
                                <p class="clearfix"><input id="B6_2" data-item="B6" class="type-checkbox" type="checkbox" value="75,81" checked><label for="B6_2"> Parques/Jardins/Praças/Mirantes</label></p>
                                <p class="clearfix"><input id="B6_3" data-item="B6" class="type-checkbox" type="checkbox" value="74" checked><label for="B6_3"> Parques de diversões/temáticos</label></p>
                                <p class="clearfix"><input id="B6_4" data-item="B6" class="type-checkbox" type="checkbox" value="76" checked><label for="B6_4"> Clubes</label></p>
                                <p class="clearfix"><input id="B6_5" data-item="B6" class="type-checkbox" type="checkbox" value="77" checked><label for="B6_5"> Pistas de patinação/Motocross/Bicicross</label></p>
                                <p class="clearfix"><input id="B6_6" data-item="B6" class="type-checkbox" type="checkbox" value="78" checked><label for="B6_6"> Estádios/Ginásio/Quadras</label></p>
                                <p class="clearfix"><input id="B6_7" data-item="B6" class="type-checkbox" type="checkbox" value="79" checked><label for="B6_7"> Hipódromos/Autódromos/Kartódromos</label></p>
                                <p class="clearfix"><input id="B6_8" data-item="B6" class="type-checkbox" type="checkbox" value="80" checked><label for="B6_8"> Marinas/atracadouros</label></p>
                                <p class="clearfix"><input id="B6_9" data-item="B6" class="type-checkbox" type="checkbox" value="84" checked><label for="B6_9"> Casas de espetáculos</label></p>
                                <p class="clearfix"><input id="B6_10" data-item="B6" class="type-checkbox" type="checkbox" value="86" checked><label for="B6_10"> Cinemas</label></p>
                                <p class="clearfix"><input id="B6_11" data-item="B6" class="type-checkbox" type="checkbox" value="87" checked><label for="B6_11"> Pistas de boliche/campo de golfe</label></p>
                                <p class="clearfix"><input id="B6_12" data-item="B6" class="type-checkbox" type="checkbox" value="88" checked><label for="B6_12"> Parques agropecuários/de vaquejada</label></p>
                                <p class="clearfix"><input id="B6_13" data-item="B6" class="type-checkbox" type="checkbox" value="89" checked><label for="B6_13"> Outros locais</label></p>
                            </td>
                        </tr>
                        <tr>
                            <td><img src="{{url('/assets/img/pins/B7.png')}}" class="table-pin">B7</td>
                            <td class="item-filter"><input id="B7" type="checkbox" value="1" checked><label for="B7"> Servicos e equipamentos turisticos</label></td>
                            <td class="type-filter">
                                <p class="clearfix"><input id="B7_1" data-item="B7" class="subtype-checkbox" type="checkbox" value="91" checked><label for="B7_1"> CAT</label></p>
                                <p class="clearfix"><input id="B7_2" data-item="B7" class="type-checkbox" type="checkbox" value="92,252,253" checked><label for="B7_2"> Entidades/Associações/Prestadores de Serviços Turísticos</label></p>
                                <p class="clearfix"><input id="B7_3" data-item="B7" class="type-checkbox" type="checkbox" value="93" checked><label for="B7_3"> Outros</label></p>
                            </td>
                        </tr>

                        <tr>
                            <td><img src="{{url('/assets/img/pins/C1.png')}}" class="table-pin">C1</td>
                            <td class="item-filter"><input id="C1" type="checkbox" value="1" checked><label for="C1"> Atrativos Naturais</label></td>
                            <td class="type-filter">
                                <p class="clearfix"><input id="C1_1" data-item="C1" class="subtype-checkbox" type="checkbox" value="95,96" checked><label for="C1_1"> Montanhas</label></p>
                                <p class="clearfix"><input id="C1_2" data-item="C1" class="type-checkbox" type="checkbox" value="257" checked><label for="C1_2"> Costas</label></p>
                                <p class="clearfix"><input id="C1_3" data-item="C1" class="type-checkbox" type="checkbox" value="258" checked><label for="C1_3"> Terras insulares</label></p>
                                <p class="clearfix"><input id="C1_4" data-item="C1" class="type-checkbox" type="checkbox" value="117,122" checked><label for="C1_4"> Hidrografia e Quedas D´água</label></p>
                                <p class="clearfix"><input id="C1_5" data-item="C1" class="type-checkbox" type="checkbox" value="128" checked><label for="C1_5"> Fontes hidrominerais e/ou termais</label></p>
                                <p class="clearfix"><input id="C1_6" data-item="C1" class="type-checkbox" type="checkbox" value="129" checked><label for="C1_6"> Unidade de Conservação</label></p>
                                <p class="clearfix"><input id="C1_7" data-item="C1" class="type-checkbox" type="checkbox" value="134" checked><label for="C1_7"> Cavernas/Grutas/Furnas</label></p>
                                <p class="clearfix"><input id="C1_8" data-item="C1" class="type-checkbox" type="checkbox" value="135" checked><label for="C1_8"> Áreas de caça e pesca</label></p>
                                <p class="clearfix"><input id="C1_9" data-item="C1" class="type-checkbox" type="checkbox" value="136,137" checked><label for="C1_9"> Fauna e Flora</label></p>
                                <p class="clearfix"><input id="C1_10" data-item="C1" class="type-checkbox" type="checkbox" value="138" checked><label for="C1_10"> Outros</label></p>
                            </td>
                        </tr>
                        <tr>
                            <td><img src="{{url('/assets/img/pins/C2.png')}}" class="table-pin">C2</td>
                            <td class="item-filter"><input id="C2" type="checkbox" value="1" checked><label for="C2"> Atrativos Culturais</label></td>
                            <td class="type-filter">
                                <p class="clearfix"><input id="C2_1" data-item="C2" class="type-checkbox" type="checkbox" value="139" checked><label for="C2_1"> Sitios históricos</label></p>
                                <p class="clearfix"><input id="C2_2" data-item="C2" class="type-checkbox" type="checkbox" value="150" checked><label for="C2_2"> Edificações</label></p>
                                <p class="clearfix"><input id="C2_3" data-item="C2" class="type-checkbox" type="checkbox" value="158" checked><label for="C2_3"> Obras de arte</label></p>
                                <p class="clearfix"><input id="C2_4" data-item="C2" class="type-checkbox" type="checkbox" value="165" checked><label for="C2_4"> Instituições Culturais</label></p>
                                <p class="clearfix"><input id="C2_5" data-item="C2" class="type-checkbox" type="checkbox" value="182" checked><label for="C2_5"> Artesanato</label></p>
                                <p class="clearfix"><input id="C2_6" data-item="C2" class="type-checkbox" type="checkbox" value="201" checked><label for="C2_6"> Feiras e Mercados</label></p>
                                <p class="clearfix"><input id="C2_7" data-item="C2" class="type-checkbox" type="checkbox" value="214" checked><label for="C2_7"> Saberes e Fazeres</label></p>
                            </td>
                        </tr>
                        <tr>
                            <td><img src="{{url('/assets/img/pins/C3.png')}}" class="table-pin">C3</td>
                            <td class="item-filter"><input id="C3" type="checkbox" value="1" checked><label for="C3"> Atividade Econômica</label></td>
                            <td class="type-filter">
                                <p class="clearfix"><input id="C3_1" data-item="C3" class="type-checkbox" type="checkbox" value="219" checked><label for="C3_1"> Extrativismo e Exploração</label></p>
                                <p class="clearfix"><input id="C3_2" data-item="C3" class="type-checkbox" type="checkbox" value="222" checked><label for="C3_2"> Agropecuária</label></p>
                                <p class="clearfix"><input id="C3_3" data-item="C3" class="type-checkbox" type="checkbox" value="228" checked><label for="C3_3"> Indústria</label></p>
                            </td>
                        </tr>
                        <tr>
                            <td><img src="{{url('/assets/img/pins/C4.png')}}" class="table-pin">C4</td>
                            <td class="item-filter"><input id="C4" type="checkbox" value="1" checked><label for="C4"> Centros Técnicos, Científicos ou Artístico</label></td>
                            <td class="type-filter">
                                <p class="clearfix"><input id="C4_1" data-item="C4" class="type-checkbox" type="checkbox" value="244,245,249,250" checked><label for="C4_1"> Aquário/Viveiro e Zoológico/Jardim Botánico</label></p>
                                <p class="clearfix"><input id="C4_2" data-item="C4" class="type-checkbox" type="checkbox" value="247,248" checked><label for="C4_2"> Exposições artisticas/Ateliê</label></p>
                                <p class="clearfix"><input id="C4_3" data-item="C4" class="type-checkbox" type="checkbox" value="238" checked><label for="C4_3"> Parque Tecnológico</label></p>
                                <p class="clearfix"><input id="C4_4" data-item="C4" class="type-checkbox" type="checkbox" value="239" checked><label for="C4_4"> Parque Industrial</label></p>
                                <p class="clearfix"><input id="C4_5" data-item="C4" class="type-checkbox" type="checkbox" value="240" checked><label for="C4_5"> Museu Tecnológico</label></p>
                                <p class="clearfix"><input id="C4_6" data-item="C4" class="type-checkbox" type="checkbox" value="241" checked><label for="C4_6"> Centro de Pesquisa</label></p>
                                <p class="clearfix"><input id="C4_7" data-item="C4" class="type-checkbox" type="checkbox" value="242" checked><label for="C4_7"> Usina hidrelétrica/Barragem/Eclusa/Açude</label></p>
                                <p class="clearfix"><input id="C4_8" data-item="C4" class="type-checkbox" type="checkbox" value="243" checked><label for="C4_8"> Planetário</label></p>
                                <p class="clearfix"><input id="C4_9" data-item="C4" class="type-checkbox" type="checkbox" value="246" checked><label for="C4_9"> Exposição Técnica</label></p>
                                <p class="clearfix"><input id="C4_10" data-item="C4" class="type-checkbox" type="checkbox" value="251" checked><label for="C4_10"> Outros</label></p>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    @endif
@stop
@section('pageScript')
    <script src="{{url('/vendor/select2/select2.min.js')}}" type="text/javascript"></script>
    <script src="{{url('/vendor/select2/i18n/pt-BR.js')}}" type="text/javascript"></script>
    <script>
        var v_StatusIds = {!! json_encode(array_keys($p_Status)) !!};
        $(document).ready(function()
        {
            $(".select2").select2({language:'pt-BR'});

            $('#revision_status_id').change(function(){
                var v_Field = this;
                var v_Selection = $(v_Field).val();
                if($.inArray('Todos', v_Selection) > -1)
                {
                    if($(v_Field).find('option:selected').length == v_StatusIds.length + 1)
                        $(v_Field).select2("val", "");
                    else
                        $(v_Field).select2("val", v_StatusIds);
                }
            });
        });
    </script>

    @if(isset($p_ShowMap))
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?language=pt-BR&key=AIzaSyBLc_8beOK5wVSM2NeSk1Bl4mLwtq6jtGw"></script>
    <script>
        // MAP
        var g_Map;
        var g_MapCenter = new google.maps.LatLng(-18.926058, -45.385104);
        var g_ReportItems = {!! json_encode($p_ReportData) !!};
        var g_FeaturesSets = [];
        var g_FeaturesMarkerSets = [];
        var g_IconBase = '{{url('/assets/img/pins')}}/';
        var g_InfoWindow = new google.maps.InfoWindow;
        var g_Marker = null;
        $(document).ready(function(){
            initializeMap();
            $('#map_canvas').parent().addClass('map-canvas-parent hidden-print');

            $('.panel-title .btn[onclick="print()"]').attr('onclick', 'adjustBeforePrint()');

            $('.item-filter input').change(function(){
                var v_Markers = g_FeaturesMarkerSets[$(this).attr('id')];
                if(v_Markers != undefined){
                    if($(this).is(':checked'))
                        showMarkers(v_Markers);
                    else
                        hideMarkers(v_Markers);
                }
                if($(this).is(':checked')){
                    $(this).closest('tr').find('.type-filter p').show();
                    $(this).closest('tr').find('.type-filter input').prop('checked', true);
                }
                else{
                    $(this).closest('tr').find('.type-filter p').hide();
                    $(this).closest('tr').find('.type-filter input').prop('checked', false);
                }
            });

            $('.type-filter input').change(function(){
                var v_Checkbox = this;
                var v_ItemMarkers = g_FeaturesMarkerSets[$(v_Checkbox).attr('data-item')];
                if(v_ItemMarkers != undefined){
                    var v_Markers = [];
                    var v_TypeFilter = $(v_Checkbox).hasClass('type-checkbox') ? 'type' : 'subtype';
                    var v_FilterIds = $(v_Checkbox).val().split(',');
                    $(v_ItemMarkers).each(function(){
                        if(v_FilterIds.indexOf(this[v_TypeFilter]+'') > -1)
                            v_Markers.push(this);
                    });
                    if($(this).is(':checked')){
                        showMarkers(v_Markers);
                        $(this).closest('tr').find('.item-filter input').prop('checked', true);
                    }
                    else
                        hideMarkers(v_Markers);
                }
            });
        });

        function adjustBeforePrint(){
            $('#mainForm').before($('#map_canvas'));
            $('#map_canvas').width('660px').css('margin-left','10px');
            google.maps.event.trigger(g_Map,'resize');
            g_Map.setCenter(g_MapCenter);
            setTimeout(function () {
                print();
                setTimeout(function () {
                    adjustAfterPrint();
                },300);
            },50);
        }

        function adjustAfterPrint(){
            $('.map-canvas-parent').append($('#map_canvas'));
            $('#map_canvas').width('100%').css('margin-left','0');
            google.maps.event.trigger(g_Map,'resize');
            g_Map.setCenter(g_MapCenter);
        }

        function showMarkers(p_Markers){
            p_Markers.forEach(function(c_Marker){
                c_Marker.setMap(g_Map);
            });
        }

        function hideMarkers(p_Markers){
            p_Markers.forEach(function(c_Marker){
                c_Marker.setMap(null);
            });
        }

        function toggleMarkers(p_Markers){
            if(p_Markers[0].getMap() == g_Map)
                hideMarkers(p_Markers);
            else
                showMarkers(p_Markers);
        }

        function addMarkers(p_Items){
            var v_Markers = [];
            p_Items.forEach(function(c_Item){
                var v_Marker = new google.maps.Marker({
                    position: c_Item.position,
                    icon: g_IconBase + c_Item.item + '.png',
                    map: g_Map,
                    item: c_Item.item,
                    type: c_Item.type,
                    subtype: c_Item.subtype
                });

                v_Marker.addListener('click', function() {
                    g_InfoWindow.setContent(c_Item.infoHTML);
                    g_InfoWindow.open(g_Map, v_Marker);
                });

                v_Markers.push(v_Marker);
            });
            return v_Markers;
        }


        function initializeMap(){
            var v_MapCanvas = document.getElementById('map_canvas');
            var v_MapOptions = {
                center: g_MapCenter,
                language: 'pt-BR',
                zoom: 6,
                minZoom: 6,
                mapTypeControlOptions: {
                    mapTypeIds: []
                },
                navigationControl: false,
                scrollwheel: false,
                streetViewControl: true
            };
            g_Map = new google.maps.Map(v_MapCanvas, v_MapOptions);

            var v_CustomMapType = new google.maps.StyledMapType([],{name: 'Custom Style'});
            var v_CustomMapTypeId = 'custom_style';

            g_Map.mapTypes.set(v_CustomMapTypeId, v_CustomMapType);
            g_Map.setMapTypeId(v_CustomMapTypeId);

            $.each(g_ReportItems, function(c_Key, c_Value){
                var v_FeaturesSet = [];
                var v_Item = c_Key;
                if(c_Value.length) {
                    $(c_Value).each(function(){
                        if(c_Key.indexOf('C') > -1 && c_Key != 'C3')
                            this.nome = JSON.parse(this.nome).pt;

                        v_FeaturesSet.push({
                            position: new google.maps.LatLng(this.latitude, this.longitude),
                            item: v_Item,
                            type: this.type_id,
                            subtype: this.sub_type_id,
                            infoTitle: this.nome,
                            infoHTML: getInfoWindowHTML(this)
                        });
                    });
                    g_FeaturesSets.push(v_FeaturesSet);
                }
            });
            $(g_FeaturesSets).each(function(){
                var v_Markers = addMarkers(this);
                if(v_Markers.length)
                    g_FeaturesMarkerSets[v_Markers[0].item] = v_Markers;
            });

            google.maps.event.addListener(g_Map, 'click', function(){
                g_InfoWindow.close();
            });
            google.maps.event.addListener(g_Map, 'click', function(){
                g_InfoWindow.close();
            });
        }


        function getInfoWindowHTML(p_Data){
            return  '<div>' +
                        '<div id="iw-container">' +
                            '<div class="iw-bottom-gradient">'+
                                '<h3 class="mapTitle text-center">' + p_Data.nome + '</h3>' +
                                '<h4 class="mapSubTitle text-center">' + p_Data.cidade + '</h4>' +
                            '</div>' +
                        '</div>' +
                    '</div>';
        }
    </script>
    @endif
@stop