@extends('admin.util.listDT', ['p_HasDateFilter' => true])
@section('list-css')
@stop
@section('panel-header')
    Equipes responsáveis por coleta de dados
    <a href="{{ url('admin/equipes-responsaveis/editar') }}">
        <button class="btn btn-success pull-right" title="Nova entrada">
            <i class="fa fa-plus"></i>
        </button>
    </a>
@stop
@section('list-table-head')
    <tr>
        <th>Responsável</th>
        <th>Instituição</th>
        <th>Município</th>
        <th>Atualizado em</th>
        <th>Ações</th>
    </tr>
    <tr>
        <td><input type="text" placeholder="Buscar" class="form-control" style="font-weight:normal"/></td>
        <td><input class="form-control" type="text" placeholder="Buscar"></td>
        <td><input class="form-control" type="text" placeholder="Buscar"></td>
        <td><input class="form-control dateInput" type="text" placeholder="Buscar"></td>
        <td></td>
    </tr>
@stop
@section('list-table-dt-url')
    url: "{{ url('/admin/dt/equipes-responsaveis')}}"
@stop
@section('list-table-initial-sorting')
    "aaSorting": [[ 0, "asc" ]],
@stop
@section('list-table-sortable-columns')
    {"bSortable": true},
    {"bSortable": true},
    {"bSortable": true},
    {"bSortable": true},
    {"bSortable": false}
@stop
@section('list-js')
@stop