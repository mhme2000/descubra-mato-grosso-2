@extends('admin.main')
@section('pageCSS')
    <link href="{{url('/vendor/select2/select2.min.css')}}" rel="stylesheet" />
    <style type="text/css">
        .align-center
        {
            text-align: center;
        }
        .margin-auto
        {
            margin: auto;
        }
        .road-title h4{
            color:#000;
            text-decoration: underline;
        }
    </style>
@stop
@section('panel-header')
    {{ $p_ResponsibleTeam == null ? 'Cadastro' : 'Edição' }} de equipe responsável por coleta de dados
@stop
@section('content')
    <div class="row">
        {!! Form::open(array('id' => 'mainForm', 'url'=> url('/admin/equipes-responsaveis'), 'onsubmit' => 'return submitForm()')) !!}
        @if($p_ResponsibleTeam != null)
            <input type="hidden" name="id" value="{{$p_ResponsibleTeam->id}}">
        @endif
        @if(\App\UserType::isMunicipio())
            <input type="hidden" name="responsible_team[city_id]" value="{{Auth::user()->city_id}}">
        @else
            <div class="form-group col-sm-6">
                <label for="instituicao">Município<span class="mandatory-field">*</span></label>
                <?php $v_CityOptions = count($p_Cities) == 1 ? $p_Cities : ([''=>''] + $p_Cities); ?>
                {!! Form::select('responsible_team[city_id]', $v_CityOptions, $p_ResponsibleTeam == null ? '' : $p_ResponsibleTeam->city_id, ['id' => 'city_id', 'class' => 'form-control select2', 'style' => 'width: 100%', 'required']) !!}
            </div>
        @endif
        <div class="form-group col-sm-6">
            <label for="responsavel">Responsável<span class="mandatory-field">*</span></label>
            <input type="text" name="responsible_team[responsavel]" class="form-control" id="responsavel" placeholder="Digite Aqui" value="{{$p_ResponsibleTeam == null ? '' : $p_ResponsibleTeam->responsavel}}" required>
        </div>
        <div class="form-group col-sm-6">
            <label for="instituicao">Instituição<span class="mandatory-field">*</span></label>
            <input type="text" name="responsible_team[instituicao]" class="form-control" id="instituicao" placeholder="Digite Aqui" value="{{$p_ResponsibleTeam == null ? '' : $p_ResponsibleTeam->instituicao}}" required>
        </div>
        <div class="form-group col-sm-6">
            <label for="telefone">Telefone (exemplo: (DDD) 9 9999-9999 / (DDD) 9999-9999)<span class="mandatory-field">*</span></label>
            <input type="text" name="responsible_team[telefone]" class="form-control phone-field" id="telefone" placeholder="Digite Aqui" value="{{$p_ResponsibleTeam == null ? '' : $p_ResponsibleTeam->telefone}}" required>
        </div>
        <div class="form-group col-sm-6">
            <label for="email">Email<span class="mandatory-field">*</span></label>
            <input type="text" name="responsible_team[email]" class="email-field form-control" id="email" placeholder="Digite Aqui" value="{{$p_ResponsibleTeam == null ? '' : $p_ResponsibleTeam->email}}" required>
        </div>
        <div class="form-group col-sm-12">
            <input type="submit" class="btn btn-default mt15 mb25 pull-right" value="Salvar">
        </div>
        {!! Form::close() !!}
    </div>
@stop
@section('pageScript')
    <script src="{{url('/vendor/select2/select2.min.js')}}" type="text/javascript"></script>
    <script src="{{url('/vendor/select2/i18n/pt-BR.js')}}" type="text/javascript"></script>
    <script src="{{url('/assets/js/inputmask.js')}}" type="text/javascript"></script>
    <script src="{{url('/assets/js/jquery.inputmask.js')}}" type="text/javascript"></script>
    <script>

        $(document).ready(function()
        {
            $(".select2").select2({language:'pt-BR'});

            $('.phone-field').keypress(function (e) {
                var key = e.keyCode || e.charCode;
                if( key == 8 || key == 37 || key == 39 || key == 46 )
                    return true;

                var regex = new RegExp(/^[\d\s\(\)\-\+\.\,\;\\\/]+$/);
                var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
                if (regex.test(str))
                    return true;

                e.preventDefault();
                return false;
            });
            $('.email-field').keypress(function (e) {
                var regex = new RegExp(/^[a-zA-Z\d\_\-\.\@\;]+$/);
                var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
                if (regex.test(str))
                    return true;

                e.preventDefault();
                return false;
            });

        });

        function submitForm()
        {
            return true;
        }
    </script>
@stop