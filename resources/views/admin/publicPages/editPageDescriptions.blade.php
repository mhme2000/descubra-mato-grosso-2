@extends('admin.mainTabs')
@section('pageCSS')

@stop

@section('panel-header')
    @if($p_Page == 'doacao-de-midias')
    Editar - Doação de mídias
    @else
    Editar - {{str_replace('-', ' ', $p_Page)}}
    @endif
    <ul class="nav panel-tabs-border panel-tabs">
        <li class="active">
            <a href="#tab0" data-toggle="tab" aria-expanded="true">Português</a>
        </li>
        <li class="">
            <a href="#tab1" data-toggle="tab" aria-expanded="false">Inglês</a>
        </li>
        <li class="">
            <a href="#tab2" data-toggle="tab" aria-expanded="false">Espanhol</a>
        </li>
        <li class="">
            <a href="#tab3" data-toggle="tab" aria-expanded="false">Francês</a>
        </li>
    </ul>
@stop

@section('content')
    {!! Form::open(array('id' => 'mainForm', 'url'=> url('/admin/paginas/descricoes'), 'onsubmit' => 'return submitForm()')) !!}
    <div class="tab-content pn br-n">
        <?php $v_Languages = [$p_Portuguese, $p_English, $p_Spanish, $p_French] ?>

        @foreach($v_Languages as $c_Index => $c_Language)
            <div id="{{'tab' . $c_Index}}" class="tab-pane {{ $c_Index == 0 ? 'active' : '' }}">
                <div class="row">
                    <input type="hidden" name="id[]" value="{{$c_Language == null ? '' : $c_Language->id}}">
                    <div class="form-group col-sm-12">
                        <label for="{{'descricao' . $c_Index}}">Descrição</label>
                        <textarea name="descricao[]" class="form-control" rows="4" id="{{'descricao' . $c_Index}}" placeholder="Digite Aqui">{{$c_Language == null ? '' : $c_Language->descricao}}</textarea>
                    </div>
                    <div class="form-group col-sm-12">
                        <label class="other-description-label" for="{{'descricao_outra' . $c_Index}}">Descrição - Outra</label>
                        <textarea name="descricao_outra[]" class="form-control" rows="4" id="{{'descricao_outra' . $c_Index}}" placeholder="Digite Aqui">{{$c_Language == null ? '' : $c_Language->descricao_outra}}</textarea>
                    </div>
                    <input type="hidden" name="language[]" value="{{ \App\Http\Controllers\BaseController::$m_Languages[$c_Index] }}">
                    <input type="hidden" name="pagina[]" value="{{$p_Page}}">
                </div>
            </div>
        @endforeach
        <div class="row">
            <div class="form-group col-sm-12">
                <input type="submit" class="btn btn-default mt15 mb25 pull-right" value="Salvar">
            </div>
        </div>

    </div>

    {!! Form::close() !!}
@stop

@section('pageScript')
    <script>
        $(document).ready(function(){
            @if($p_Page == 'doacao-de-midias')
                $('.other-description-label').html('Regras de doação');
            @endif
        });
        function submitForm()
        {
            return true;
        }
    </script>
@stop