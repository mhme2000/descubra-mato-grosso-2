@extends('admin.mainTabs')

@section('pageCSS')

@stop

@section('panel-header')
    Editar - Institucional
    <ul class="nav panel-tabs-border panel-tabs">
        <li class="active">
            <a href="#tab0" data-toggle="tab" aria-expanded="true">Português</a>
        </li>
        <li class="">
            <a href="#tab1" data-toggle="tab" aria-expanded="false">Inglês</a>
        </li>
        <li class="">
            <a href="#tab2" data-toggle="tab" aria-expanded="false">Espanhol</a>
        </li>
        <li class="">
            <a href="#tab3" data-toggle="tab" aria-expanded="false">Francês</a>
        </li>
    </ul>
@stop

@section('content')
    {!! Form::open(array('id' => 'mainForm', 'url'=> url('/admin/paginas/conheca'), 'onsubmit' => 'return submitForm()', 'files' => true)) !!}
    <div class="tab-content pn br-n">

        <?php $v_Languages = [$p_Portuguese, $p_English, $p_Spanish, $p_French] ?>

        @foreach($v_Languages as $c_Index => $c_Language)
            <div id="{{'tab' . $c_Index}}" class="tab-pane {{ $c_Index == 0 ? 'active' : '' }}">
                @if($c_Index == 0)
                    <div class="row">
                        <div class="col-sm-12 fixed-photo-div">
                            <div class="camera" style="{{$p_TestimonialPhoto == null ? '' : 'background-image:url(' . $p_TestimonialPhoto->url . ');'}}">
                                <img class="upload-btn-icon" style="{{$p_TestimonialPhoto == null ? '' : 'display:none'}}" src="{{url('/assets/img/camera.png')}}" alt="">
                                <h4 class="upload-btn-text" style="{{$p_TestimonialPhoto == null ? '' : 'display:none'}}">Foto<br>depoimento</h4>
                                <input class="photo-upload-input" name="photo[file][]" type="file" accept="image/gif, image/jpg, image/jpeg, image/png" onchange="onChangeImage(this)">
                                <input type="hidden" name="photo[type][]" value="testimonial">
                                <input type="hidden" name="photo[id][]" value="{{$p_TestimonialPhoto == null ? '' : $p_TestimonialPhoto->id}}">
                                @if($p_TestimonialPhoto)
                                    <a title="Baixar" href="{{$p_TestimonialPhoto->url}}" class="btn btn-success download-photo-btn" download><i class="fa fa-download"></i></a>
                                @endif
                                <a title="Excluir" type="button" class="btn btn-success remove-photo" onclick="removePhoto(this)"><i class="fa fa-trash-o"></i></a>
                            </div>
                        </div>
                    </div>
                @endif
                <div class="row">
                    <input type="hidden" name="id[]" value="{{$c_Language == null ? '' : $c_Language->id}}">
                    <div class="form-group col-sm-12">
                        <label for="{{'depoimento' . $c_Index}}">Depoimento</label>
                        <textarea name="depoimento[]" class="form-control" rows="4" id="{{'depoimento' . $c_Index}}" placeholder="Digite Aqui">{{$c_Language == null ? '' : $c_Language->depoimento}}</textarea>
                    </div>
                    <div class="form-group col-sm-12">
                        <label for="{{'depoimento_autor' . $c_Index}}">Autor do depoimento</label>
                        <input type="text" name="depoimento_autor[]" class="form-control" id="{{'depoimento_autor' . $c_Index}}" placeholder="Digite Aqui" value="{{$c_Language == null ? '' : $c_Language->depoimento_autor}}">
                    </div>
                    <div class="form-group col-sm-12">
                        <label for="{{'titulo' . $c_Index}}">Título</label>
                        <input type="text" name="titulo[]" class="form-control" id="{{'titulo' . $c_Index}}" placeholder="Digite Aqui" value="{{$c_Language == null ? '' : $c_Language->titulo}}">
                    </div>
                    <div class="form-group col-sm-12">
                        <label for="{{'descricao' . $c_Index}}">Descrição</label>
                        <textarea name="descricao[]" class="form-control" rows="4" id="{{'descricao' . $c_Index}}" placeholder="Digite Aqui">{{$c_Language == null ? '' : $c_Language->descricao}}</textarea>
                    </div>
                    @for($c_Counter = 1; $c_Counter <= 3; $c_Counter++)
                        <div class="form-group col-sm-6">
                            <label for="dado_relevante_{{ $c_Counter . $c_Index}}">Dado relevante {{ $c_Counter }}</label>
                            <input type="text" name="dado_relevante_{{ $c_Counter }}[]" class="form-control" id="dado_relevante_{{ $c_Counter . $c_Index}}" placeholder="Digite Aqui" value="{{$c_Language == null ? '' : $c_Language['dado_relevante_' . $c_Counter]}}">
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="{{'dado_relevante_descricao_' . $c_Counter . $c_Index}}">Dado relevante {{ $c_Counter }} - Descrição</label>
                            <input type="text" name="dado_relevante_descricao_{{ $c_Counter}}[]" class="form-control" id="dado_relevante_descricao_{{ $c_Counter . $c_Index}}" placeholder="Digite Aqui" value="{{$c_Language == null ? '' : $c_Language['dado_relevante_descricao_' . $c_Counter]}}">
                        </div>
                    @endfor
                    <input type="hidden" name="language[]" value="{{ \App\Http\Controllers\BaseController::$m_Languages[$c_Index] }}">

                    @if($c_Index == 0)
                        <div class="form-group col-sm-6">
                            <label for="video_url">URL do vídeo</label>
                            <input type="text" name="video_url" class="form-control" id="url_video" placeholder="Digite Aqui" value="{{$c_Language == null ? '' : $c_Language->video_url}}">
                        </div>

                        @include('admin.util.photos', array('p_PhotoGallery' => $p_PhotoGallery, 'p_Cover' => false))
                    @endif
                </div>
            </div>
        @endforeach

        <div class="row">
            <div class="form-group col-sm-12">
                <input type="submit" class="btn btn-default mt15 mb25 pull-right" value="Salvar">
            </div>
        </div>

    </div>

    {!! Form::close() !!}
@stop

@section('pageScript')
    @include('admin.util.photosScript', ['p_MaxPhotoCount' => -1])
    <script>
        function submitForm()
        {
            return true;
        }
    </script>
@stop