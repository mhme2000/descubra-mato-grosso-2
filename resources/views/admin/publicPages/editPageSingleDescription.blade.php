@extends('admin.mainTabs')
@section('pageCSS')

@stop

@section('panel-header')
    Editar - {{str_replace('-', ' ', $p_Page)}}
    <ul class="nav panel-tabs-border panel-tabs">
        <li class="active">
            <a href="#tab0" data-toggle="tab" aria-expanded="true">Português</a>
        </li>
        <li class="">
            <a href="#tab1" data-toggle="tab" aria-expanded="false">Inglês</a>
        </li>
        <li class="">
            <a href="#tab2" data-toggle="tab" aria-expanded="false">Espanhol</a>
        </li>
        <li class="">
            <a href="#tab3" data-toggle="tab" aria-expanded="false">Francês</a>
        </li>
    </ul>
@stop

@section('content')
    {!! Form::open(array('id' => 'mainForm', 'url'=> url('/admin/paginas/descricao'), 'onsubmit' => 'return submitForm()')) !!}
    <div class="tab-content pn br-n">
        <?php $v_Languages = [$p_Portuguese, $p_English, $p_Spanish, $p_French] ?>

        @foreach($v_Languages as $c_Index => $c_Language)
            <div id="{{'tab' . $c_Index}}" class="tab-pane {{ $c_Index == 0 ? 'active' : '' }}">
                <div class="row">
                    <div class="form-group col-sm-12">
                        <label for="{{'idioma' . $c_Index}}">Descrição<span class="mandatory-field">*</span></label>
                        <textarea name="idioma[{{$p_Page . '_' . \App\Http\Controllers\BaseController::$m_Languages[$c_Index]}}]" class="form-control" rows="4" id="{{'idioma' . $c_Index}}" placeholder="Digite Aqui">{{$c_Language == null ? '' : $c_Language}}</textarea>
                    </div>
                </div>
            </div>
        @endforeach
        <div class="row">
            <div class="form-group col-sm-12">
                <input type="submit" class="btn btn-default mt15 mb25 pull-right" value="Salvar">
            </div>
        </div>

    </div>

    {!! Form::close() !!}
@stop

@section('pageScript')
    <script>
        function submitForm()
        {
            return true;
        }
    </script>
@stop