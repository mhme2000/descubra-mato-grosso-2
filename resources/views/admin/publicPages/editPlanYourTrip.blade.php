@extends('admin.mainTabs')
@section('pageCSS')
<link rel="stylesheet" type="text/css" href="{{url('/vendor/summernote/summernote.css')}}">
<link rel="stylesheet" type="text/css" href="{{url('/vendor/summernote/summernote-bs3.css')}}">
@stop

@section('panel-header')
    Editar - Planeje sua viagem
    <ul class="nav panel-tabs-border panel-tabs">
        <li class="active">
            <a href="#tab0" data-toggle="tab" aria-expanded="true">Português</a>
        </li>
        <li class="">
            <a href="#tab1" data-toggle="tab" aria-expanded="false">Inglês</a>
        </li>
        <li class="">
            <a href="#tab2" data-toggle="tab" aria-expanded="false">Espanhol</a>
        </li>
        <li class="">
            <a href="#tab3" data-toggle="tab" aria-expanded="false">Francês</a>
        </li>
    </ul>
@stop

@section('content')
    {!! Form::open(array('id' => 'mainForm', 'url'=> url('/admin/paginas/planeje-sua-viagem'), 'onsubmit' => 'return submitForm()', 'files' => true)) !!}
    <div class="tab-content pn br-n">

        @foreach($p_Languages as $c_Index => $c_Language)
            <div id="{{'tab' . $c_Index}}" class="tab-pane {{ $c_Index == 0 ? 'active' : '' }}">
                <div class="row">
                    <div class="col-sm-6 admin-form">
                        <div class="section">
                            <label class="field prepend-icon append-button file">
                                <span class="button btn-success">Guia</span>
                                {!! Form::file('guia[]', array('onchange' => '$(this).parent().find(".input-file-name").val(this.value.split(/fakepath\\\/)[1])', 'accept' => 'application/pdf', 'class' => 'gui-file')) !!}
                                {!! Form::text('', $p_Files[$c_Index] == null ? '' : $p_Files[$c_Index], array('placeholder' => 'Selecione um arquivo', 'class'=>'gui-input input-file-name')) !!}
                                <label class="field-icon"><i class="fa fa-upload"></i>
                                </label>
                            </label>
                        </div>
                    </div>
                    <div class="form-group col-sm-12">
                        <label for="{{'planeje-sua-viagem-descricao_' . $c_Index}}">Descrição<span class="mandatory-field">*</span></label>
                        <textarea name="idioma[{{'planeje-sua-viagem-descricao_' . \App\Http\Controllers\BaseController::$m_Languages[$c_Index]}}]" class="form-control" rows="4" id="{{'planeje-sua-viagem-descricao_' . $c_Index}}" placeholder="Digite Aqui">{{$c_Language == null ? '' : $c_Language}}</textarea>
                    </div>
                    <div class="form-group col-sm-12">
                        <label for="{{'planeje-sua-viagem-como-chegar_' . $c_Index}}">Descrição<span class="mandatory-field">*</span></label>
                        <textarea name="idioma[{{'planeje-sua-viagem-como-chegar_' . \App\Http\Controllers\BaseController::$m_Languages[$c_Index]}}]" class="form-control" rows="4" id="{{'planeje-sua-viagem-como-chegar_' . $c_Index}}" placeholder="Digite Aqui">{{$p_Directions[$c_Index] == null ? '' : $p_Directions[$c_Index]}}</textarea>
                    </div>
                    <div class="form-group col-sm-12">
                        <label for="{{'planeje-sua-viagem-informacoes_' . $c_Index}}">Dicas úteis</label>
                        <textarea class="summernote" name="idioma[{{'planeje-sua-viagem-informacoes_' . \App\Http\Controllers\BaseController::$m_Languages[$c_Index]}}]" class="form-control" rows="10" id="{{'planeje-sua-viagem-informacoes' . $c_Index}}" placeholder="Digite Aqui">{{$p_Information[$c_Index] == null ? '' : $p_Information[$c_Index]}}</textarea>
                    </div>
                </div>
            </div>
        @endforeach
        <div class="row">
            <div class="form-group col-sm-12">
                <input type="submit" class="btn btn-default mt15 mb25 pull-right" value="Salvar">
            </div>
        </div>

    </div>

    {!! Form::close() !!}
@stop

@section('pageScript')
    <script type="text/javascript" src="{{url('/vendor/summernote/summernote.js')}}"></script>
    <script type="text/javascript" src="{{url('/vendor/summernote/summernote-pt-BR.js')}}"></script>
    <script>
        $(document).ready(function()
        {
            // Init Summernote
            $('.summernote').summernote({
                lang: 'pt-BR',
                height: 400, //set editable area's height
                focus: false, //set focus editable area after Initialize summernote
                toolbar: [
                    ['font', ['bold', 'italic', 'underline', 'clear']],
                    ['color', ['color']],
                    ['height', ['height']],
                    ['insert', ['hr']],
                    ['view', ['codeview']]
                ],
                oninit: function() {},
                onChange: function(contents, $editable) {}
            });
        });

        function prepareFields()
        {
            $('textarea#content').val($('.editor-container .note-editable').html());
            return true;
        }

        function submitForm()
        {
            return true;
        }
    </script>
@stop