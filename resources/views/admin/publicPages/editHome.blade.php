@extends('admin.mainTabs')
@section('pageCSS')
    <style>
        .app-container{
            border: 1px solid #ccc;
        }
    </style>
@stop

@section('panel-header')
    Editar - Home
    <ul class="nav panel-tabs-border panel-tabs">
        <li class="active">
            <a href="#tab0" data-toggle="tab" aria-expanded="true">Português</a>
        </li>
        <li class="">
            <a href="#tab1" data-toggle="tab" aria-expanded="false">Inglês</a>
        </li>
        <li class="">
            <a href="#tab2" data-toggle="tab" aria-expanded="false">Espanhol</a>
        </li>
        <li class="">
            <a href="#tab3" data-toggle="tab" aria-expanded="false">Francês</a>
        </li>
    </ul>
@stop

@section('content')
    {!! Form::open(array('id' => 'mainForm', 'url'=> url('/admin/paginas/home'), 'onsubmit' => 'return submitForm()', 'files' => true)) !!}
    <div class="tab-content pn br-n">
        <?php $v_Languages = [$p_Portuguese, $p_English, $p_Spanish, $p_French] ?>

        @foreach($v_Languages as $c_Index => $c_Language)
            <div id="{{'tab' . $c_Index}}" class="tab-pane {{ $c_Index == 0 ? 'active' : '' }}">
                @if($c_Index == 0)
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="camera" style="{{($c_Language == null || $c_Language->foto_capa_url == null) ? '' : 'background-image:url(' . $c_Language->foto_capa_url . ');'}}">
                                <img class="upload-btn-icon" style="{{($c_Language == null || $c_Language->foto_capa_url == null) ? '' : 'display:none'}}" src="{{url('/assets/img/camera.png')}}" alt="">
                                <h4 class="upload-btn-text" style="{{($c_Language == null || $c_Language->foto_capa_url == null) ? '' : 'display:none'}}">Foto de<br>capa</h4>
                                <input class="photo-upload-input" name="foto_capa" type="file" accept="image/gif, image/jpg, image/jpeg, image/png" onchange="onChangeImage(this)" {{ $c_Language == null ? 'required' : '' }}>
                                <input type="hidden" class="foto-capa-status" value="{{($c_Language == null || $c_Language->foto_capa_url == null) ? 'none' : 'old'}}">
                            </div>
                        </div>
                    </div>
                @endif
                <div class="row">
                    <input type="hidden" name="id[]" value="{{$c_Language == null ? '' : $c_Language->id}}">
                    <div class="form-group col-sm-6">
                        <label for="{{'titulo_o_que_fazer' . $c_Index}}">Título - O que fazer</label>
                        <input type="text" name="titulo_o_que_fazer[]" class="form-control" id="{{'titulo_o_que_fazer' . $c_Index}}" placeholder="Digite Aqui" value="{{$c_Language == null ? '' : $c_Language->titulo_o_que_fazer}}">
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="{{'titulo_destinos' . $c_Index}}">Título - Destinos</label>
                        <input type="text" name="titulo_destinos[]" class="form-control" id="{{'titulo_destinos' . $c_Index}}" placeholder="Digite Aqui" value="{{$c_Language == null ? '' : $c_Language->titulo_destinos}}">
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="{{'titulo_instagram' . $c_Index}}">Título - Instagram</label>
                        <input type="text" name="titulo_instagram[]" class="form-control" id="{{'titulo_instagram' . $c_Index}}" placeholder="Digite Aqui" value="{{$c_Language == null ? '' : $c_Language->titulo_instagram}}">
                    </div>
                    <div class="form-group col-sm-12">
                        <label for="{{'texto_instagram' . $c_Index}}">Texto Instagram</label>
                        <textarea name="texto_instagram[]" class="form-control" rows="4" id="{{'texto_instagram' . $c_Index}}" placeholder="Digite Aqui">{{$c_Language == null ? '' : $c_Language->texto_instagram}}</textarea>
                    </div>
                    @if($c_Index == 0)
                        <input type="hidden" name="perfil_instagram" value="1">
                        <div class="form-group col-sm-12">
                            <h3>Configuração do Instagram</h3>
                            <h5 class="mt15">Para que as fotos do Instagram apareçam na Home, mantenha os dados abaixo atualizados (a princípio somente o "Access Token" precisa ser atualizado). As informações podem ser obtidas nos links a seguir:</h5>
                            <p>Access Token: <a href="http://instagram.pixelunion.net" target="_blank">http://instagram.pixelunion.net</a></p>
                            <p>Demais dados: <a href="https://www.instagram.com/developer/clients/manage" target="_blank">https://www.instagram.com/developer/clients/manage</a></p>
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="instagram-client-id">Client ID</label>
                            <input type="text" name="instagram[instagram-client-id]" class="form-control" id="instagram-client-id" placeholder="Digite Aqui" value="{{$p_InstagramClientId == null ? '' : $p_InstagramClientId}}">
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="instagram-client-secret">Client Secret</label>
                            <input type="text" name="instagram[instagram-client-secret]" class="form-control" id="instagram-client-secret" placeholder="Digite Aqui" value="{{$p_InstagramClientSecret == null ? '' : $p_InstagramClientSecret}}">
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="instagram-redirect-uri">Redirect URI</label>
                            <input type="text" name="instagram[instagram-redirect-uri]" class="form-control" id="instagram-redirect-uri" placeholder="Digite Aqui" value="{{$p_InstagramRedirectUri == null ? '' : $p_InstagramRedirectUri}}">
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="instagram-access-token">Access Token</label>
                            <input type="text" name="instagram[instagram-access-token]" class="form-control" id="instagram-access-token" placeholder="Digite Aqui" value="{{$p_InstagramAccessToken == null ? '' : $p_InstagramAccessToken}}">
                        </div>
                    @endif
                    <br>
                    <div class="col-sm-12">
                        <h3>Aplicativos</h3>
                    </div>
                    <div class="form-group col-sm-12">
                        <label for="{{'titulo_aplicativos' . $c_Index}}">Título - Aplicativos</label>
                        <input type="text" name="titulo_aplicativos[]" class="form-control" id="{{'titulo_aplicativos' . $c_Index}}" placeholder="Digite Aqui" value="{{$c_Language == null ? '' : $c_Language->titulo_aplicativos}}">
                    </div>
                    @if($c_Index == 0)
                        <input type="hidden" name="apps_data" id="apps_data">
                        <?php
                            if($p_Apps != null){
                                $v_AppsData = json_decode($p_Apps,1);
                                $v_AppsData = $v_AppsData['data'];
                            }
                        ?>
                        @for($c_AppIndex = 0; $c_AppIndex < 3; $c_AppIndex++)
                        <div class="col-sm-4 mb20">
                            <div class="app-container clearfix">
                                <div class="col-sm-12">
                                    <div class="camera" style="{{($p_Apps == null || $v_AppsData[$c_AppIndex]['foto_capa_url'] == null) ? '' : 'background-image:url(' . $v_AppsData[$c_AppIndex]['foto_capa_url'] . ');'}}">
                                        <img class="upload-btn-icon" style="{{($p_Apps == null || $v_AppsData[$c_AppIndex]['foto_capa_url'] == null) ? '' : 'display:none'}}" src="{{url('/assets/img/camera.png')}}" alt="">
                                        <h4 class="upload-btn-text" style="{{($p_Apps == null || $v_AppsData[$c_AppIndex]['foto_capa_url'] == null) ? '' : 'display:none'}}">Imagem</h4>
                                        <input class="photo-upload-input app_photo" name="foto_app[]" type="file" accept="image/gif, image/jpg, image/jpeg, image/png" onchange="onChangeImage(this)">
                                        <input type="hidden" name="foto_app_nome[]" class="app_photo_nome" value="{{$p_Apps == null ? '' : $v_AppsData[$c_AppIndex]['foto_capa_url']}}">
                                        <input type="hidden" class="foto-capa-status" value="{{($p_Apps == null || $v_AppsData[$c_AppIndex]['foto_capa_url'] == null) ? 'none' : 'old'}}">
                                        <a title="Excluir" type="button" class="btn btn-success remove-photo" onclick="removeAppPhoto(this)"><i class="fa fa-trash-o"></i></a>
                                    </div>
                                </div>
                                <div class="form-group col-sm-6">
                                    <label for="app_nome">Nome</label>
                                    <input type="text" class="form-control app_nome" placeholder="Digite Aqui" value="{{$p_Apps == null ? '' : $v_AppsData[$c_AppIndex]['nome']}}">
                                </div>
                                <div class="form-group col-sm-6">
                                    <label for="app_type">Tipo</label>
                                    {!! Form::select('', ['app'=>'App','web'=>'Web'], $p_Apps == null ? '' : $v_AppsData[$c_AppIndex]['tipo'], ['class' => 'app_type form-control select2', 'style' => 'width: 100%']) !!}
                                </div>
                                <div class="form-group col-sm-6 web-link">
                                    <label for="app_url">Link</label>
                                    <input type="url" class="form-control app_url" placeholder="Digite Aqui" value="{{$p_Apps == null ? '' : $v_AppsData[$c_AppIndex]['url']}}">
                                </div>
                                <div class="form-group col-sm-6 app-link">
                                    <label for="app_url_android">Link Google Play</label>
                                    <input type="url" class="form-control app_url_android" placeholder="Digite Aqui" value="{{$p_Apps == null ? '' : $v_AppsData[$c_AppIndex]['url_android']}}">
                                </div>
                                <div class="form-group col-sm-6 app-link">
                                    <label for="app_url_ios">Link App Store</label>
                                    <input type="url" class="form-control app_url_ios" placeholder="Digite Aqui" value="{{$p_Apps == null ? '' : $v_AppsData[$c_AppIndex]['url_ios']}}">
                                </div>
                            </div>
                        </div>
                        @endfor
                    @endif

                    <input type="hidden" name="language[]" value="{{ \App\Http\Controllers\BaseController::$m_Languages[$c_Index] }}">
                </div>
            </div>
        @endforeach
        <div class="row">
            <div class="form-group col-sm-12">
                <input type="submit" class="btn btn-default mt15 mb25 pull-right" value="Salvar">
            </div>
        </div>

    </div>

    {!! Form::close() !!}
@stop

@section('pageScript')
    @include('admin.util.photosScript', ['p_MaxPhotoCount' => -1])
    <script>
        $(document).ready(function(){
            $('.app_type').change(function() {
                var $v_Container = $(this).closest('.app-container');
                if($(this).val() == 'app'){
                    $v_Container.find('.app-link').show();
                    $v_Container.find('.web-link').hide();
                }
                else{
                    $v_Container.find('.app-link').hide();
                    $v_Container.find('.web-link').show();
                }
            }).change();
        });

        function removeAppPhoto(p_DeleteBtn) {
            var $v_Container = $(p_DeleteBtn).closest('.app-container');
            $v_Container.find('.app_photo, .app_photo_nome').val('');
            setPhotoBackground(p_DeleteBtn, '');
        }

        function submitForm()
        {
            var v_Apps = {
                tem_dados:0,
                data:[]
            };
            $('.app-container').each(function () {
                var v_Nome = $(this).find('.app_nome').val();
                var v_Tipo = $(this).find('.app_type').val();
                var v_Url = $(this).find('.app_url').val();
                var v_UrlAndroid = $(this).find('.app_url_android').val();
                var v_UrlIOS = $(this).find('.app_url_ios').val();
                var $v_Foto = $(this).find('.app_photo');
                var v_UrlFoto = $(this).find('.app_photo_nome').val();
                var v_Completo = 0;

                if($v_Foto.val() != '')
                    v_UrlFoto = '{{url('/imagens/paginas/home-apps')}}/' + v_Apps.data.length + generateRandomString(15) + '.' + $v_Foto[0].files[0].name.split('.')[1];

                if(v_Nome != '' && v_UrlFoto != '' &&  ((v_Tipo == 'app' &&  (v_UrlAndroid != '' ||  v_UrlIOS != '')) ||  (v_Tipo == 'web' && v_Url != ''))){
                    v_Apps.tem_dados = 1;
                    v_Completo = 1
                }
                var v_App = {
                    nome:v_Nome,
                    tipo:v_Tipo,
                    url:v_Url,
                    url_android:v_UrlAndroid,
                    url_ios:v_UrlIOS,
                    foto_capa_url:v_UrlFoto,
                    completo:v_Completo
                };
                v_Apps.data.push(v_App);

                $(this).find('.app_photo_nome').val(v_UrlFoto);
            });
            $('#apps_data').val(JSON.stringify(v_Apps));

            return true;
        }

        function generateRandomString(p_Length){
            return Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, p_Length);
        }
    </script>
@stop