@extends('admin.base')
@section('pageCSS')
    <link rel="stylesheet" type="text/css" href="{{url('/vendor/plugins/datatables/media/css/dataTables.bootstrap.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('/vendor/plugins/datatables/media/css/dataTablesTemplate.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('/vendor/plugins/datatables/media/css/dataTables.responsive.css')}}">
    <style type="text/css">
        div ul li.paginate_button.active a
        {
            background-color: #C30F1B;
            border-color: #C30F1B;
            color: #FFF;
        }
        div ul li.paginate_button.active a:hover
        {
            background-color: #C30F1B;
            border-color: #C30F1B;
            color: #FFF;
        }
        div ul li.paginate_button a, div ul li.paginate_button a:hover
        {
            color: #000;
        }
        td
        {
            text-align: center;
        }
        .responsive-table {
            width: 99.9999% !important;
        }
        thead tr th.select-status {
            width: 100px !important;
        }
        .dtr-data
        {
            display: inline-block;
            word-wrap: break-word;
            word-break: break-all;
        }
        table.dataTable tr.child ul li
        {
            white-space: normal;
        }
        table div.actions-div a
        {
            width: 39px;
            margin-right: 3px;
            margin-top: 1px;
            margin-bottom: 1px;
        }
        .maximize-panel, .minimize-panel {
            cursor: pointer;
        }
        @media (min-width: 500px)
        {
            td
            {
                text-align: center;
            }
        }
        @media (max-width: 320px)
        {
            table div.actions-div
            {
                text-align: center;
            }
            table div.actions-div a
            {
                margin-bottom: 3px;
            }
        }
    </style>
@stop
@section('main-content')
    <div class="row">
        <div class="col-sm-12">
            <section class="panel panel-visible">
                <header class="panel-heading br-b-n">
                    <div class="panel-title">
                        Painel geral
                    </div>
                </header>
                <div class="panel-body">

                    @if(\App\UserType::isTrade())
                        @include('admin.dashboard.partials.tradeWaiting')
                        @include('admin.dashboard.partials.tradeExpiring')
                        @include('admin.dashboard.partials.tradeExpired')
                    @else
                        @if(\App\UserType::isAdmin() || \App\UserType::isMaster())
                            @include('admin.dashboard.partials.tradePending')
                            @include('admin.dashboard.partials.tradeExpiring')
                        @endif
                        @include('admin.dashboard.partials.events')
                    @endif

                    @include('admin.dashboard.partials.userData')

                    <div class="row" id="nothingToDo" style="display: none;">
                        <div class="form-group col-sm-12">
                            <p>Não existe nenhuma pendência no momento</p>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>

@stop
@section('pageScript')
    <script type="text/javascript" src="{{url('/vendor/plugins/datatables/media/js/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript" src="{{url('/vendor/plugins/datatables/media/js/dataTables.bootstrap.js')}}"></script>
    <script type="text/javascript" src="{{url('/vendor/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js')}}"></script>
    <script type="text/javascript" src="{{url('/vendor/plugins/datatables/media/js/dataTables.responsive.js')}}"></script>
    <script type="text/javascript" src="{{url('/assets/js/jquery.popconfirm.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function()
        {
            $('.table[id^="responsive-table"]').each(function(){
                initDatatable($(this).attr('id'));
            });


            $('#trade-responsive-table .delete-btn').removeClass('delete-btn').popConfirm({
                title: "Confirmar",
                content: "Tem certeza de que deseja rejeitar esta solicitação?",
                placement: "left",
                yesBtn: "Sim",
                noBtn: "Não"
            });

            initDatatable('trade-responsive-table');

            var v_FilterInput = $('.adv-table input[aria-controls="responsive-table"]').appendTo('.adv-table #responsive-table_filter');
            v_FilterInput.attr('placeholder', 'Buscar');
            $('.adv-table #responsive-table_filter label').text('');
            v_FilterInput.appendTo('.adv-table #responsive-table_filter label');


            $('.minimize-panel').click(function(){
                var v_Parent = $(this).closest('section.panel');
                v_Parent.find('.panel-body, .minimize-panel').hide();
                v_Parent.find('.maximize-panel').show();
            }).click();
            $('.maximize-panel').click(function(){
                var v_Parent = $(this).closest('section.panel');
                v_Parent.find('.panel-body, .minimize-panel').show();
                $(this).hide();
            });

            if($('.minimize-panel').length == 0)
                $('#nothingToDo').show();
        });

        function initDatatable(p_Id)
        {
            var v_Columns;
            if(p_Id == 'trade-responsive-table')
                v_Columns = [
                    {"bSortable": true},
                    {"bSortable": true},
                    {"bSortable": true},
                    {"bSortable": true},
                    {"bSortable": true},
                    {"bSortable": false}
                ];
            else if (p_Id == 'responsive-table-expiring')
                v_Columns = [
                    {"bSortable": true},
                    {"bSortable": true},
                    {"bSortable": true},
                    {"bSortable": true}
                ];
            else if (p_Id == 'responsive-table-expired')
                v_Columns = [
                    {"bSortable": true},
                    {"bSortable": true},
                    {"bSortable": true}
                ];
            else
                v_Columns = [
                    {"bSortable": true},
                    {"bSortable": true}
                ];
            /* DataTables */
            var v_Table = $('#'+p_Id).DataTable({
                responsive: true,
                "aLengthMenu": [[25, 50, 100, 250, 500, -1], [25, 50, 100, 250, 500, "Todos"]],
                "sDom": '<"dt-panelmenu clearfix"lfr>t<"dt-panelfooter clearfix"ip>',
                tableTools: {
                    "aButtons": []
                },
                "order": [[ 0, "asc" ]],
                "bAutoWidth": true,
                "sScrollX": "100%",
                "bScrollCollapse": true,
                "oLanguage":
                {
                    "sEmptyTable": "Nenhum registro encontrado",
                    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
                    "sInfoFiltered": "(Filtrados de _MAX_ registros)",
                    "sInfoPostFix": "",
                    "sInfoThousands": ".",
                    "sLengthMenu": "_MENU_ resultados por página",
                    "sLoadingRecords": "Carregando...",
                    "sProcessing": "Processando...",
                    "sZeroRecords": "Nenhum registro encontrado",
                    "sSearch": "Pesquisar",
                    "oPaginate":
                    {
                        "sNext": "",
                        "sPrevious": "",
                        "sFirst": "Primeiro",
                        "sLast": "Último"
                    },
                    "oAria":
                    {
                        "sSortAscending": ": Ordenar colunas de forma ascendente",
                        "sSortDescending": ": Ordenar colunas de forma descendente"
                    }
                },
                "aoColumns": v_Columns
            });

            var v_FilterInput = $('.adv-table input[aria-controls="'+p_Id+'"]').appendTo('.adv-table #'+p_Id+'_filter');
            v_FilterInput.attr('placeholder', 'Buscar');
            $('.adv-table #'+p_Id+'_filter label').text('');
            v_FilterInput.appendTo('.adv-table #'+p_Id+'_filter label');
        }
    </script>
@stop
