@if(count($p_Inventory['items']))
    <div class="row">
        <div class="col-sm-12">
            <section class="panel panel-visible">
                <header class="panel-heading">
                    <div class="panel-title">
                        @if(\App\UserType::isAdmin() || \App\UserType::isMaster())
                        {{$p_Inventory['name'] . ' - ' . count($p_Inventory['items'])}} item(s) aguardando aprovação
                        @elseif(\App\UserType::isCircuito())
                        {{$p_Inventory['name'] . ' - ' . count($p_Inventory['items'])}} item(s) aguardando aprovação ou rejeitado(s)
                        @else
                        {{$p_Inventory['name'] . ' - ' . count($p_Inventory['items'])}} item(s) rejeitado(s)
                        @endif
                        <div class="minimize-panel pull-right mr10" title="Minimizar">
                            <i class="fa fa-minus"></i>
                        </div>
                        <div class="maximize-panel pull-right mr10" title="Expandir" style="display: none">
                            <i class="fa fa-plus"></i>
                        </div>
                    </div>
                </header>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12 mt15">
                            <div class="panel panel-visible">
                                <div class="panel-body pn">
                                    <div class="adv-table">
                                        <table class="table table-striped table-bordered table-hover responsive-table" id="responsive-table{{$c_Index}}" cellspacing="0" width="100%">
                                            <thead>
                                                <tr>
                                                    @if(isset($p_Inventory['name_column']))
                                                        <th>{{$p_Inventory['name_column']}}</th>
                                                    @else
                                                        <th>Nome</th>
                                                    @endif
                                                    <th>Status</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($p_Inventory['items'] as $c_Item)
                                                    <tr>
                                                        <td>
                                                            <a href="{{url('admin/inventario/' . $p_Inventory['path'] . '/editar/' . $c_Item->item_id)}}" title="Editar">
                                                                {{$c_Item->nome}}
                                                            </a>
                                                        </td>
                                                        <td>{{$c_Item->status}}</td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
@endif