<?php
    $v_ExpiringItems = [];
    $v_CadasturWarningDate = \Carbon\Carbon::now()->startOfDay()->addDays(30);
    $v_WarningDate = \Carbon\Carbon::now()->startOfDay()->subDays(150);
    $v_ExpiringItems['Hospedagem'] = [];
    $v_ExpiringItems['Gastronomia'] = [];
    $v_ExpiringItems['Agenciamento'] = [];
    $v_ExpiringItems['Transporte'] = [];
    $v_ExpiringItems['Lazer e entretenimento'] = [];

    $v_IDs = \App\UserType::isTrade() ? \App\UserTradeItem::getUserTradeItems('B1') : \App\UserTradeItem::getTradeItems('B1');
    $v_Items = \App\AccomodationService::whereIn('id', $v_IDs)->where('revision_status_id', '!=', 6)
            ->where('validade_cadastur', '<=', $v_CadasturWarningDate->format('Y-m-d H:i:s'))->select('id', 'nome_fantasia', 'cnpj', 'validade_cadastur as validade')->get()->toArray();
    foreach($v_Items as $c_Item) {
        $c_Item['path'] = 'servicos-hospedagem';
        $c_Item['validade'] = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $c_Item['validade'])->format('d/m/Y');
        array_push($v_ExpiringItems['Hospedagem'], $c_Item);
    }

    $v_IDs = \App\UserType::isTrade() ? \App\UserTradeItem::getUserTradeItems('B2') : \App\UserTradeItem::getTradeItems('B2');
    $v_Items = \App\AccomodationService::whereIn('id', $v_IDs)->where('revision_status_id', '!=', 6)
            ->where('validade_cadastur', '<=', $v_WarningDate->format('Y-m-d H:i:s'))->select('id', 'nome_fantasia', 'cnpj', 'updated_at as validade')->get()->toArray();
    foreach($v_Items as $c_Item) {
        $c_Item['path'] = 'servicos-alimentos';
        $c_Item['validade'] = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $c_Item['validade'])->addDays(180)->format('d/m/Y');
        array_push($v_ExpiringItems['Gastronomia'], $c_Item);
    }

    $v_IDs = \App\UserType::isTrade() ? \App\UserTradeItem::getUserTradeItems('B3') : \App\UserTradeItem::getTradeItems('B3');
    $v_Items = \App\AccomodationService::whereIn('id', $v_IDs)->where('revision_status_id', '!=', 6)
            ->where('validade_cadastur', '<=', $v_CadasturWarningDate->format('Y-m-d H:i:s'))->select('id', 'nome_fantasia', 'cnpj', 'validade_cadastur as validade')->get()->toArray();
    foreach($v_Items as $c_Item) {
        $c_Item['path'] = 'servicos-agencias-turismo';
        $c_Item['validade'] = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $c_Item['validade'])->format('d/m/Y');
        array_push($v_ExpiringItems['Agenciamento'], $c_Item);
    }

    $v_IDs = \App\UserType::isTrade() ? \App\UserTradeItem::getUserTradeItems('B4') : \App\UserTradeItem::getTradeItems('B4');
    $v_Items = \App\AccomodationService::whereIn('id', $v_IDs)->where('revision_status_id', '!=', 6)
            ->where('validade_cadastur', '<=', $v_CadasturWarningDate->format('Y-m-d H:i:s'))->select('id', 'nome_fantasia', 'cnpj', 'validade_cadastur as validade')->get()->toArray();
    foreach($v_Items as $c_Item) {
        $c_Item['path'] = 'servicos-transporte-turistico';
        $c_Item['validade'] = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $c_Item['validade'])->format('d/m/Y');
        array_push($v_ExpiringItems['Transporte'], $c_Item);
    }

    $v_IDs = \App\UserType::isTrade() ? \App\UserTradeItem::getUserTradeItems('B6') : \App\UserTradeItem::getTradeItems('B6');
    $v_Items = \App\AccomodationService::whereIn('id', $v_IDs)->where('revision_status_id', '!=', 6)
            ->where('validade_cadastur', '<=', $v_WarningDate->format('Y-m-d H:i:s'))->select('id', 'nome_fantasia', 'cnpj', 'updated_at as validade')->get()->toArray();
    foreach($v_Items as $c_Item) {
        $c_Item['path'] = 'servicos-lazer';
        $c_Item['validade'] = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $c_Item['validade'])->addDays(180)->format('d/m/Y');
        array_push($v_ExpiringItems['Lazer e entretenimento'], $c_Item);
    }

    $v_ItemCount = 0;
    foreach($v_ExpiringItems as $c_Type => $c_Items)
        $v_ItemCount += count($c_Items);
?>

@if($v_ItemCount)
    <div class="row">
        <div class="col-sm-12">
            <section class="panel panel-visible">
                <header class="panel-heading">
                    <div class="panel-title">
                        {{(\App\UserType::isTrade() ? '' : 'Cadastre seu negócio - ') . $v_ItemCount}} cadastro(s) próximo(s) do vencimento
                        <div class="minimize-panel pull-right mr10" title="Minimizar">
                            <i class="fa fa-minus"></i>
                        </div>
                        <div class="maximize-panel pull-right mr10" title="Expandir" style="display: none">
                            <i class="fa fa-plus"></i>
                        </div>
                    </div>
                </header>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12 mt15">
                            <div class="panel panel-visible">
                                <div class="panel-body pn">
                                    <div class="adv-table">
                                        <table class="table table-striped table-bordered table-hover responsive-table" id="responsive-table-expiring" cellspacing="0" width="100%">
                                            <thead>
                                            <tr>
                                                <th>Tipo</th>
                                                <th>Nome</th>
                                                <th>CNPJ</th>
                                                <th>Data de vencimento</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($v_ExpiringItems as $c_Type => $c_Items)
                                                @foreach($c_Items as $c_Item)
                                                    <tr>
                                                        <td>{{$c_Type}}</td>
                                                        <td>
                                                            <a href="{{url('admin/inventario/' . $c_Item['path'] . '/editar/' . $c_Item['id'])}}" title="Editar">
                                                                {{$c_Item['nome_fantasia']}}
                                                            </a>
                                                        </td>
                                                        <td>{{substr($c_Item['cnpj'], 0, 2) . '.' . substr($c_Item['cnpj'], 2, 3) . '.' . substr($c_Item['cnpj'], 5, 3) . '/' . substr($c_Item['cnpj'], 8, 4) . '-' . substr($c_Item['cnpj'], 12, 2)}}</td>
                                                        <td>{{$c_Item['validade']}}</td>
                                                    </tr>
                                                @endforeach
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
@endif
