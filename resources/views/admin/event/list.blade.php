@extends('admin.util.listDT', ['p_HasDateFilter' => true])
@section('list-css')
@stop
@section('panel-header')
    Eventos
    @if((!\App\UserType::isParceiro()) && !\App\UserType::isTrade() )
    <a href="{{ url('admin/eventos/editar') }}">
        <button class="btn btn-success pull-right" title="Novo evento">
            <i class="fa fa-plus"></i>
        </button>
    </a>
    @endif

    @if(\App\UserType::isTrade())
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title">Eventos Encontrados</h4>
                </div>
                <div class="modal-body">
                    <div class="modalTableEvents" style="overflow: scroll; max-height: 30vw;">
                            <table class="table table-hover">
                                    <thead>
                                      <tr>
                                        <th>Evento</th>
                                        <th>Local</th>
                                        <th>Data</th>
                                        <th>Solicitar Posse?</th>
                                      </tr>
                                    </thead>
                                    <tbody id='tbodySearchEvents'>

                                    </tbody>
                                  </table>
                    </div>
                </div>
                <div class="modal-footer">
                  <a href="{{ url('admin/eventos/editar') }}" class="btn btn-primary">Criar Novo Evento</a>
                </div>
              </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
          </div><!-- /.modal -->


<script>




    function searchEventos(){

console.log("{{url('admin/eventos/city')}}/" + $('#search_city').val() + "/name/" + $("#search_name").val());

$.ajax({
    url: "{{url('admin/eventos/city')}}/" + $('#search_city').val() + "/name/" + $("#search_name").val(),
    dataType: "json"
    })
  .done(function(data) {
      //Elimina ítens da tabela
      $('#tbodySearchEvents').html('');

      //Coloca ítens na tabela
        data.forEach(function(element, index, array){

            //Pega data do elemento descartando as horas
            dateElement = element['start'].split(" ")[0];
            //Formata data para pt-BR
            dateElement = dateElement.split("-")[2] + '/' + dateElement.split("-")[1] + '/' + dateElement.split("-")[0];

            $('#tbodySearchEvents').append(
                $('<tr></tr>').append(
                    $('<td></td>').css('text-align', 'left').html(element['name'])  
                ).append(
                    $('<td></td>').css('text-align', 'left').html(element['city'])
                ).append(
                    $('<td></td>').css('text-align', 'left').html(dateElement)
                ).append(
                    $('<td></td>').css('text-align', 'left').append(
                        $('<a></a>').addClass('btn btn-primary').attr('href', "{{url('admin/eventos/adonarse')}}" + '/' + element['event_id'] + '/' +  {{Auth::user()->id}} ).html('<i class="fa fa-key"></i>').on('click', function(event){
                            event.preventDefault();
                            if (confirm("Deseja tornar-se dono deste evento?")){

                                $(location).attr('href', $(this).attr('href'));
                                /*
                                $.ajax({
                                    url:$(this).attr('href')
                                })
                                .done(function(){
                                    alert('Solicitação realizada com exito.');
                                    $(location).attr('href', "{{url('admin/eventos')}}");
                                })
                                .fail(function( jqXHR, textStatus ) {
                                    console.log( "Request failed: " + textStatus );
                                });
                                */
                            };
                        })
                    )
                )
            );   
        });



    



  })
  .fail(function() {
    console.log( "error" );
  })
  .always(function() {
    $('#myModal').modal('show');
  });


    }


</script>
    <div class="mb15 pv5 ph15 new-trade">
            <h4>Novo cadastro</h4>
            <div class="row">      
                <div class="form-group col-sm-5">
                    <label for="tipo_atividade_cadastur">Cidade</label>
                    {!! Form::select('', $p_city, '', ['id' => 'search_city', 'class' => 'form-control']) !!}
                </div>
                <div class="form-group col-sm-6">
                    <label for="cnpj">Nome do Evento</label>
                    <input type="text" class="form-control cnpj-field cnpj-field-with-cadastur" id="search_name" placeholder="Digite Aqui">
                    <a title="Pesquisar" type="button" class="btn btn-success search-cnpj-cadastur" onclick="searchEventos()">
                            <i class="fa fa-search"></i>
                            <img style="display:none;margin:auto -1px;height:14px" src="{{url('/assets/img/loading.gif')}}">
                    </a>
                </div>
            </div>
        </div>

    @endif
@stop
@section('list-table-head')
    <tr>
        <th>Período</th>
        <th>Atualizado em</th>
        <th>Região Turística</th>        
        <th>Município</th>
        <th>Nome</th>
        <th>Status</th>
        <th>Ações</th>
    </tr>
    <tr>
        <td><input class="form-control dateInput" type="text" placeholder="Buscar"></td>
        <td><input class="form-control dateInput" type="text" placeholder="Buscar"></td>
        <td>{!! Form::select('', ['' => 'Selecione'] + $p_Circuit, null, ['class' => 'form-control']) !!}</td>        
        <td><input type="text" placeholder="Buscar" class="form-control" style="font-weight:normal"/></td>
        <td><input type="text" placeholder="Buscar" class="form-control" style="font-weight:normal"/></td>
        <td>{!! Form::select('', ['' => 'Selecione'] + $p_Status, null, ['class' => 'form-control']) !!}</td>
        <td></td>
    </tr>
@stop
@section('list-table-dt-url')
    url: "{{ url('/admin/dt/eventos')}}"
@stop
@section('list-table-initial-sorting')
    "aaSorting": [[ 0, "desc" ]],
@stop
@section('list-table-sortable-columns')
    {"bSortable": true},
    {"bSortable": true},
    {"bSortable": true},
    {"bSortable": true},
    {"bSortable": true},
    {"bSortable": true},
    {"bSortable": false}
@stop
@section('list-js')
@stop