@extends('admin.mainTabs')
@section('pageCSS')
    <link href="{{url('/vendor/select2/select2.min.css')}}" rel="stylesheet" />
    <style type="text/css">
        .full-width
        {
            width: 100%;
        }
        .align-center
        {
            text-align: center;
        }

        .margin-auto
        {
            margin: auto;
        }
    </style>
@stop
@section('panel-header')
    {{ $p_Destination == null ? 'Cadastro' : 'Edição' }} de destino
    <ul class="nav panel-tabs-border panel-tabs">
        <li class="active">
            <a href="#tab0" data-toggle="tab" aria-expanded="true">Português</a>
        </li>
        <li class="">
            <a href="#tab1" data-toggle="tab" aria-expanded="false">Inglês</a>
        </li>
        <li class="">
            <a href="#tab2" data-toggle="tab" aria-expanded="false">Espanhol</a>
        </li>
        <li class="">
            <a href="#tab3" data-toggle="tab" aria-expanded="false">Francês</a>
        </li>
    </ul>
@stop
@section('content')
    {!! Form::open(['id' => 'mainForm', 'url'=> url('/admin/destinos'), 'onsubmit' => 'return submitForm()', 'files' => true]) !!}
    <div class="tab-content pn br-n">
        <input type="hidden" id="form_titulos" name="destino[titulo]">
        <input type="hidden" id="form_descricoes" name="destino[descricao]">
        <input type="hidden" id="form_descricoes_curtas" name="destino[descricao_curta]">
        <input type="hidden" id="form_depoimentos" name="destino[depoimento]">
        <input type="hidden" id="form_depoimentos_autores" name="destino[depoimento_autor]">
        <?php
            if($p_Destination != null){
                $v_EmptyData = ['pt'=>'', 'en'=>'', 'es'=>'', 'fr'=>''];
                $v_Titles = $p_Destination->titulo != null ? json_decode($p_Destination->titulo,1) : $v_EmptyData;
                $v_Descriptions = $p_Destination->descricao != null ? json_decode($p_Destination->descricao,1) : $v_EmptyData;
                $v_ShortDescriptions = $p_Destination->descricao_curta != null ? json_decode($p_Destination->descricao_curta,1) : $v_EmptyData;
                $v_Testimonials = $p_Destination->depoimento != null ? json_decode($p_Destination->depoimento,1) : $v_EmptyData;
                $v_TestimonialAuthors = $p_Destination->depoimento_autor != null ? json_decode($p_Destination->depoimento_autor,1) : $v_EmptyData;
            }
            $v_Languages = ['pt', 'en', 'es', 'fr'];
        ?>

        @if($p_Destination != null)
            <input type="hidden" name="destino[id]" value="{{$p_Destination->id}}">
        @endif
        @foreach($v_Languages as $c_Index => $c_Language)
            <div id="{{'tab' . $c_Index}}" class="tab-pane {{ $c_Index == 0 ? 'active' : '' }}">
                @if($c_Index == 0)
                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label for="city_id">Município<span class="mandatory-field">*</span></label>
                            <?php $v_CityOptions = count($p_Cities) == 1 ? $p_Cities : ([''=>''] + $p_Cities); ?>
                            {!! Form::select('destino[city_id]', $v_CityOptions, $p_Destination == null ? '' : $p_Destination->city_id, ['id' => 'city_id', 'class' => 'form-control select2', 'style' => 'width: 100%', 'required' => 'required']) !!}
                        </div>
                        @if($p_Type == 'distrito')
                        <div class="form-group col-sm-6">
                            <label for="district_id">Distrito<span class="mandatory-field">*</span></label>
                            {!! Form::select('destino[district_id]', [($p_Destination == null ? '' : $p_Destination->district_id) => ''], $p_Destination == null ? '' : $p_Destination->district_id, ['id' => 'district_id', 'class' => 'form-control select2', 'style' => 'width: 100%', 'required' => 'required']) !!}
                        </div>
                        @endif
                        <div class="form-group col-sm-6">
                            <label for="region_id">Região<span class="mandatory-field">*</span></label>
                            {!! Form::select('destino[region_id]', $p_Regions, $p_Destination == null ? '' : $p_Destination->region_id, ['id' => 'region_id', 'class' => 'form-control select2', 'style' => 'width: 100%', 'required' => 'required']) !!}
                        </div>
                    </div>
                    <div class="row destination-fields">
                        <div class="form-group col-sm-4">
                            <label for="destaque">Destaque?</label>
                            <p><input type="checkbox" value="1" name="destino[destaque]" id="destaque" class="ml5 mt10" {{($p_Destination == null || $p_Destination->destaque == 0) ? '' : 'checked'}}></p>
                        </div>
                        <div class="form-group col-sm-4">
                            <label for="turismo_negocios_eventos">Vocacionado para turismo de negócios e eventos?</label>
                            <p><input type="checkbox" value="1" name="destino[turismo_negocios_eventos]" id="turismo_negocios_eventos" class="ml5 mt10" {{($p_Destination == null || $p_Destination->turismo_negocios_eventos == 0) ? '' : 'checked'}}></p>
                        </div>
                        <div class="form-group col-sm-4">
                            <label for="indutor_turismo_lazer">Indutor de turismo de lazer?</label>
                            <p><input type="checkbox" value="1" name="destino[indutor_turismo_lazer]" id="indutor_turismo_lazer" class="ml5 mt10" {{($p_Destination == null || $p_Destination->indutor_turismo_lazer == 0) ? '' : 'checked'}}></p>
                        </div>
                        <div class="col-sm-4 fixed-photo-div">
                            <div class="camera" style="{{$p_CoverPhoto == null ? '' : 'background-image:url(' . $p_CoverPhoto->url . ');'}}">
                                <img class="upload-btn-icon" style="{{$p_CoverPhoto == null ? '' : 'display:none'}}" src="{{url('/assets/img/camera.png')}}" alt="">
                                <h4 class="upload-btn-text" style="{{$p_CoverPhoto == null ? '' : 'display:none'}}">Foto de<br>capa</h4>
                                <input class="photo-upload-input {{$p_CoverPhoto == null ? 'mandatory' : ''}}" name="photo[file][]" type="file" accept="image/gif, image/jpg, image/jpeg, image/png" onchange="onChangeImage(this)">
                                <input type="hidden" name="photo[type][]" value="cover">
                                <input type="hidden" name="photo[id][]" value="{{$p_CoverPhoto == null ? '' : $p_CoverPhoto->id}}">
                                @if($p_CoverPhoto)
                                    <a title="Baixar" href="{{$p_CoverPhoto->url}}" class="btn btn-success download-photo-btn" download><i class="fa fa-download"></i></a>
                                @endif
                            </div>
                            {{--<div class="form-group photo-credits-div">--}}
                                {{--<label>Créditos</label>--}}
                                {{--<input type="text" name="photo[credits][]" class="photo-credits form-control" placeholder="Digite Aqui" value="{{$p_CoverPhoto == null ? '' : $p_CoverPhoto->creditos}}">--}}
                            {{--</div>--}}
                        </div>
                        <div class="col-sm-4 fixed-photo-div">
                            <div class="camera" style="{{$p_MapPhoto == null ? '' : 'background-image:url(' . $p_MapPhoto->url . ');'}}">
                                <img class="upload-btn-icon" style="{{$p_MapPhoto == null ? '' : 'display:none'}}" src="{{url('/assets/img/camera.png')}}" alt="">
                                <h4 class="upload-btn-text" style="{{$p_MapPhoto == null ? '' : 'display:none'}}">Mapa</h4>
                                <input class="photo-upload-input" name="photo[file][]" type="file" accept="image/gif, image/jpg, image/jpeg, image/png" onchange="onChangeImage(this)">
                                <input type="hidden" name="photo[type][]" value="map">
                                <input type="hidden" name="photo[id][]" value="{{$p_MapPhoto == null ? '' : $p_MapPhoto->id}}">
                                @if($p_MapPhoto)
                                    <a title="Baixar" href="{{$p_MapPhoto->url}}" class="btn btn-success download-photo-btn" download><i class="fa fa-download"></i></a>
                                @endif
                                <a title="Excluir" type="button" class="btn btn-success remove-photo" onclick="removePhoto(this)"><i class="fa fa-trash-o"></i></a>
                            </div>
                            {{--<div class="form-group photo-credits-div">--}}
                                {{--<label>Créditos</label>--}}
                                {{--<input type="text" name="photo[credits][]" class="photo-credits form-control" placeholder="Digite Aqui" value="{{$p_MapPhoto == null ? '' : $p_MapPhoto->creditos}}">--}}
                            {{--</div>--}}
                        </div>
                        <div class="col-sm-4 fixed-photo-div">
                            <div class="camera" style="{{$p_TestimonialPhoto == null ? '' : 'background-image:url(' . $p_TestimonialPhoto->url . ');'}}">
                                <img class="upload-btn-icon" style="{{$p_TestimonialPhoto == null ? '' : 'display:none'}}" src="{{url('/assets/img/camera.png')}}" alt="">
                                <h4 class="upload-btn-text" style="{{$p_TestimonialPhoto == null ? '' : 'display:none'}}">Foto<br>depoimento</h4>
                                <input class="photo-upload-input" name="photo[file][]" type="file" accept="image/gif, image/jpg, image/jpeg, image/png" onchange="onChangeImage(this)">
                                <input type="hidden" name="photo[type][]" value="testimonial">
                                <input type="hidden" name="photo[id][]" value="{{$p_TestimonialPhoto == null ? '' : $p_TestimonialPhoto->id}}">
                                @if($p_TestimonialPhoto)
                                    <a title="Baixar" href="{{$p_TestimonialPhoto->url}}" class="btn btn-success download-photo-btn" download><i class="fa fa-download"></i></a>
                                @endif
                                <a title="Excluir" type="button" class="btn btn-success remove-photo" onclick="removePhoto(this)"><i class="fa fa-trash-o"></i></a>
                            </div>
                            {{--<div class="form-group photo-credits-div">--}}
                                {{--<label>Créditos</label>--}}
                                {{--<input type="text" name="photo[credits][]" class="photo-credits form-control" placeholder="Digite Aqui" value="{{$p_TestimonialPhoto == null ? '' : $p_TestimonialPhoto->creditos}}">--}}
                            {{--</div>--}}
                        </div>
                    </div>
                @endif
                <div class="row destination-fields">
                    <div class="form-group col-sm-12">
                        <label for="depoimento_{{$c_Language}}">Depoimento</label>
                        <textarea id="depoimento_{{$c_Language}}" class="form-control" rows="4" placeholder="Digite Aqui">{{$p_Destination == null ? '' : $v_Testimonials[$c_Language]}}</textarea>
                    </div>
                    <div class="form-group col-sm-12">
                        <label for="depoimento_autor_{{$c_Language}}">Autor do depoimento</label>
                        <input type="text" class="form-control" id="depoimento_autor_{{$c_Language}}" placeholder="Digite Aqui" value="{{$p_Destination == null ? '' : $v_TestimonialAuthors[$c_Language]}}">
                    </div>
                    <div class="form-group col-sm-12">
                        <label for="titulo_{{$c_Language}}">Título<span class="mandatory-field">*</span></label>
                        <input type="text" class="form-control" id="titulo_{{$c_Language}}" placeholder="Digite Aqui" value="{{$p_Destination == null ? '' : $v_Titles[$c_Language]}}" {{$c_Index == 0 ? 'required' : ''}}>
                    </div>
                    <div class="form-group col-sm-12">
                        <label for="descricao_curta_{{$c_Language}}">Descrição curta<span class="mandatory-field">*</span></label>
                        <textarea id="descricao_curta_{{$c_Language}}" class="form-control" rows="4" placeholder="Digite Aqui" {{$c_Index == 0 ? 'required' : ''}}>{{$p_Destination == null ? '' : $v_ShortDescriptions[$c_Language]}}</textarea>
                    </div>
                    <div class="form-group col-sm-12 ckeditor-field">
                        <label for="descricao_{{$c_Language}}">Descrição<span class="mandatory-field">*</span></label>
                        <textarea id="descricao_{{$c_Language}}" class="form-control" rows="4" placeholder="Digite Aqui" {{$c_Index == 0 ? 'required' : ''}}>{{$p_Destination == null ? '' : $v_Descriptions[$c_Language]}}</textarea>
                    </div>

                    @if($c_Index == 0)
                        <div class="form-group col-sm-6">
                            <label for="cep">CEP<span class="mandatory-field">*</span></label>
                            <input name="destino[cep]" type="text" class="form-control cep-field" id="cep" placeholder="Digite Aqui" value="{{$p_Destination == null ? '' : $p_Destination->cep}}" required>
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="bairro">Bairro<span class="mandatory-field">*</span></label>
                            <input type="text" name="destino[bairro]" class="form-control" id="bairro" placeholder="Digite Aqui" value="{{$p_Destination == null ? '' : $p_Destination->bairro}}" required>
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="logradouro">Logradouro<span class="mandatory-field">*</span></label>
                            <input type="text" name="destino[logradouro]" class="form-control" id="logradouro" placeholder="Digite Aqui" value="{{$p_Destination == null ? '' : $p_Destination->logradouro}}" required>
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="numero">Número<span class="mandatory-field">*</span></label>
                            <input type="text" name="destino[numero]" class="form-control" id="numero" placeholder="Digite Aqui" value="{{$p_Destination == null ? '' : $p_Destination->numero}}" required>
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="complemento">Complemento</label>
                            <input type="text" name="destino[complemento]" class="form-control" id="complemento" placeholder="Digite Aqui" value="{{$p_Destination == null ? '' : $p_Destination->complemento}}">
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="latitude_longitude_decimal">Latitude e Longitude em decimal</label>
                            <input type="text" class="form-control" id="latitude_longitude_decimal" placeholder="Digite Aqui">
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="latitude" >Latitude <i>(formato decimal)</i></label>
                            <input type="number" name="destino[latitude]" step="0.0000001" class="form-control" id="latitude" placeholder="Digite Aqui" value="{{$p_Destination == null ? '' : $p_Destination->latitude}}">
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="longitude">Longitude <i>(formato decimal)</i></label>
                            <input type="number" name="destino[longitude]" step="0.0000001" class="form-control" id="longitude" placeholder="Digite Aqui" value="{{$p_Destination == null ? '' : $p_Destination->longitude}}">
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="site">Site</label>
                            <input type="url"  name="destino[site]" class="form-control" id="site" placeholder="Digite Aqui" value="{{$p_Destination == null ? '' : $p_Destination->site}}">
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="telefone">Telefone (exemplo: (DDD) 9 9999-9999 / (DDD) 9999-9999)</label>
                            <input type="text" name="destino[telefone]" class="form-control phone-field" id="telefone" placeholder="Digite Aqui" value="{{$p_Destination == null ? '' : $p_Destination->telefone}}">
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="email">Email</label>
                            <input type="email" name="destino[email]" class="form-control" id="email" placeholder="Digite Aqui" value="{{$p_Destination == null ? '' : $p_Destination->email}}">
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="facebook">Facebook</label>
                            <input type="url"  name="destino[facebook]" class="form-control" id="facebook" placeholder="Digite Aqui" value="{{$p_Destination == null ? '' : $p_Destination->facebook}}">
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="instagram">Instagram</label>
                            <input type="url"  name="destino[instagram]" class="form-control" id="instagram" placeholder="Digite Aqui" value="{{$p_Destination == null ? '' : $p_Destination->instagram}}">
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="twitter">Twitter</label>
                            <input type="url"  name="destino[twitter]" class="form-control" id="twitter" placeholder="Digite Aqui" value="{{$p_Destination == null ? '' : $p_Destination->twitter}}">
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="youtube">Youtube</label>
                            <input type="url"  name="destino[youtube]" class="form-control" id="youtube" placeholder="Digite Aqui" value="{{$p_Destination == null ? '' : $p_Destination->youtube}}">
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="flickr">Flickr</label>
                            <input type="url"  name="destino[flickr]" class="form-control" id="flickr" placeholder="Digite Aqui" value="{{$p_Destination == null ? '' : $p_Destination->flickr}}">
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="tripadvisor_url">Url do Tripadvisor</label>
                            <input type="url"  name="destino[tripadvisor_url]" class="form-control" id="tripadvisor_url" placeholder="Digite Aqui" value="{{$p_Destination == null ? '' : $p_Destination->tripadvisor_url}}">
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="minas_360">Link para Minas em 360º</label>
                            <input type="url"  name="destino[minas_360]" class="form-control" id="minas_360" placeholder="Digite Aqui" value="{{$p_Destination == null ? '' : $p_Destination->minas_360}}">
                        </div>

                        <div class="form-group col-sm-12">
                            <label for="tipos_viagem">Tipo de polo <i>(permite mais de uma opção)</i></label>
                            {{-- $p_SelectedCategories é um array com as chaves das categorias selecionadas --}}
                            {!! Form::select('tipos_viagem[]', $p_TripTypes, $p_SelectedTripTypes, ['id' => 'tipos_viagem', 'class' => 'form-control select2', 'style' => 'width: 100%', 'multiple' => 'multiple']) !!}
                        </div>

                        <div class="form-group col-sm-12">
                            <label for="hashtags">Palavras-chave<span class="mandatory-field">*</span></label>
                            {{-- $p_SelectedCategories é um array com as chaves das categorias selecionadas --}}
                            {!! Form::select('hashtags[]', $p_Hashtags, array_keys($p_Hashtags), ['id' => 'hashtags', 'class' => 'form-control select2-custom mandatory', 'style' => 'width: 100%', 'multiple' => 'multiple']) !!}
                        </div>

                        @include('admin.util.photos', ['p_PhotoGallery' => $p_PhotoGallery, 'p_Cover' => false, 'p_WithoutFieldIndex' => true])
                    @endif
                </div>
            </div>
        @endforeach

        <div class="row">
            <div class="form-group col-sm-12">
                <input type="submit" class="btn btn-default mt15 mb25 pull-right" value="Salvar">
            </div>
        </div>
    </div>
    {!! Form::close() !!}
@stop
@section('pageScript')
    <script src="{{url('/vendor/select2/select2.min.js')}}" type="text/javascript"></script>
    <script src="{{url('/vendor/select2/i18n/pt-BR.js')}}" type="text/javascript"></script>
    <script src="{{url('/assets/js/inputmask.js')}}" type="text/javascript"></script>
    <script src="{{url('/assets/js/jquery.inputmask.js')}}" type="text/javascript"></script>
    <script src="{{url('/vendor/ckeditor/ckeditor.js')}}" type="text/javascript"></script>
    @include('admin.util.photosScript', ['p_MaxPhotoCount' => -1])
    <script>
        $(document).ready(function()
        {
            $('.ckeditor-field textarea').each(function(){
                CKEDITOR.replace($(this).attr('id'),{
                    allowedContent: true,
                    filebrowserImageBrowseUrl: null,
                    filebrowserFlashBrowseUrl: null,
                    filebrowserUploadUrl: "{{url('/admin/upload-arquivo')}}",
                    filebrowserImageUploadUrl: "{{url('/admin/upload-arquivo')}}",
                    filebrowserFlashUploadUrl: "{{url('/admin/upload-arquivo')}}"
                });
            });
            $(".select2").select2({language:'pt-BR'});
            $(".select2-custom").select2({language:'pt-BR', tags: true});

            $('.cep-field').mask('99.999-999');
            $('.phone-field').keypress(function (e) {
                var key = e.keyCode || e.charCode;
                if( key == 8 || key == 37 || key == 39 || key == 46 )
                    return true;

                var regex = new RegExp(/^[\d\s\(\)\-\+\.\,\;\\\/]+$/);
                var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
                if (regex.test(str))
                    return true;

                e.preventDefault();
                return false;
            });
            $('input[type="url"]').focus(function(){
                if(this.value == '')
                    this.value = 'http://';
            }).focusout(function(){
                if(this.value == 'http://')
                    this.value = '';
            });

            $('.destination-fields .mandatory').attr('required', true);

            @if($p_Type == 'distrito')
            $('#city_id').change(function(){
                $.get("{{url('/admin/' . ($p_Destination == null ? 'distritosDisponiveisMunicipio' : 'distritosMunicipio') . '?city_id=')}}" + $(this).val(), function(){
                }).done(function(data){
                    if (data.error == 'ok')
                    {
                        var v_LastVal = $('#district_id').val();
                        var v_DataString = '';
                        $.each(data.data, function (c_Key, c_Field)
                        {
                            v_DataString += '<option value="' + c_Key + '">' + c_Field + '</option>';
                        });

                        $('#district_id').html('<option value=""></option>' + v_DataString);
                        if(data.data.length == 0)
                            $('#district_id').select2("val", "");
                        else
                            $('#district_id').select2("val", v_LastVal);
                    }
                    else
                    {
                        $('#sub_type_id').html('<option value=""></option>').select2("val", "");
                    }
                }).error(function(){
                });
            }).change();
            @endif

            @if($p_Destination != null)
                $('#city_id, #district_id').prop('disabled', true);
            @endif

            $('#latitude_longitude_decimal').keyup(function(){
                var v_Value = this.value;
                if(v_Value.length){
                    v_Value = v_Value.split(',');
                    var v_Latitude = v_Value[0].trim();
                    var v_Longitude = v_Value[1].trim();
                    if(v_Latitude.length && v_Longitude.length){
                        $('#latitude').val(v_Latitude);
                        $('#longitude').val(v_Longitude).change();
                    }
                }
            });

            $('#latitude, #longitude').focus(function() {
                $(this).on('mousewheel.disableScroll', function(e) {
                    e.preventDefault();
                })
            }).blur(function() {
                $(this).off('mousewheel.disableScroll');
            }).keydown(function(e) {
                if (e.keyCode === 38 || e.keyCode === 40)
                    e.preventDefault();
            });
        });

        function submitForm()
        {
            var v_TitlesJson = {};
            var v_DescriptionsJson = {};
            var v_ShortDescriptionsJson = {};
            var v_TestimonialsJson = {};
            var v_TestimonialAuthorsJson = {};
            @foreach($v_Languages as $c_Language)
                v_TitlesJson.{{$c_Language}} = $('#titulo_{{$c_Language}}').val();
                v_DescriptionsJson.{{$c_Language}} = CKEDITOR.instances['descricao_{{$c_Language}}'].getData(); //$('#descricao_{{$c_Language}}').val();
                v_ShortDescriptionsJson.{{$c_Language}} = $('#descricao_curta_{{$c_Language}}').val();
                v_TestimonialsJson.{{$c_Language}} = $('#depoimento_{{$c_Language}}').val();
                v_TestimonialAuthorsJson.{{$c_Language}} = $('#depoimento_autor_{{$c_Language}}').val();
            @endforeach

            $('#form_titulos').val(JSON.stringify(v_TitlesJson));
            $('#form_descricoes').val(JSON.stringify(v_DescriptionsJson));
            $('#form_descricoes_curtas').val(JSON.stringify(v_ShortDescriptionsJson));
            $('#form_depoimentos').val(JSON.stringify(v_TestimonialsJson));
            $('#form_depoimentos_autores').val(JSON.stringify(v_TestimonialAuthorsJson));

            $('#city_id, #district_id').prop('disabled', false);

            return true;
        }
    </script>
@stop