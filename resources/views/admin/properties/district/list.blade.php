@extends('admin.util.listStatic', ['p_HasDateFilter' => false])
@section('list-css')
@stop
@section('panel-header')
    Distritos - {{ $p_City->name }}
    <a href="{{ url('admin/municipios/' . $p_City->id . '/distritos/editar') }}">
        <button class="btn btn-success pull-right" title="Novo distrito">
            <i class="fa fa-plus"></i>
        </button>
    </a>
@stop
@section('list-table-head')
    <tr>
        <th>Nome</th>
        <th>Ações</th>
    </tr>
@stop
@section('list-table-rows')
    @foreach($p_Districts as $c_District)
        <tr>
            <td>{{$c_District->name}}</td>
            <td>
                <div class="actions-div">
                    <a href="{{ url('admin/municipios/' . $p_City->id . '/distritos/editar/' . $c_District->id) }}" title="Editar" type="button" class="btn btn-success">
                        <i class="fa fa-edit"></i>
                    </a>
                </div>
            </td>
        </tr>
    @endforeach
@stop
@section('list-table-initial-sorting')
    "order": [[ 0, "asc" ]],
@stop
@section('list-table-sortable-columns')
    {"bSortable": true},
    {"bSortable": false}
@stop
@section('list-js')
@stop