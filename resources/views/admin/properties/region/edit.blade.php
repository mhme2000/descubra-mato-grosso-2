@extends('admin.main')
@section('pageCSS')
    <style type="text/css">
        .full-width
        {
            width: 100%;
        }
        .align-center
        {
            text-align: center;
        }

        .margin-auto
        {
            margin: auto;
        }
    </style>
@stop
@section('panel-header')
    {{ $p_Region == null ? 'Cadastro' : 'Edição' }} de Região
@stop
@section('content')
    <div class="row pt15">
        {!! Form::open(array('id' => 'mainForm', 'url'=> url('/admin/regioes'))) !!}
        @if($p_Region != null)
            <input type="hidden" name="id" value="{{$p_Region->id}}">
        @endif
        <div class="form-group col-sm-6">
            <label for="nome">Nome<span class="mandatory-field">*</span></label>
            <input type="text" name="nome" class="form-control" id="nome" placeholder="Digite Aqui" value="{{$p_Region == null ? '' : $p_Region->name}}" required>
        </div>
        <div class="form-group col-sm-12">
            <input type="submit" class="btn btn-default mt15 mb25 pull-right" value="Salvar">
        </div>
        {!! Form::close() !!}
    </div>
@stop
@section('pageScript')
    <script>
        $(document).ready(function()
        {
        });
    </script>
@stop