@extends('admin.main')
@section('pageCSS')
@stop
@section('panel-header')
    {{ $p_Language == null ? 'Cadastro' : 'Edição' }} de idioma
@stop
@section('content')
    <div class="row pt15">
        {!! Form::open(array('id' => 'mainForm', 'url'=> url('/admin/idiomas'))) !!}
        @if($p_Language != null)
            <input type="hidden" name="id" value="{{$p_Language->id}}">
        @endif
        <div class="form-group col-sm-12">
            <label for="nome">Nome<span class="mandatory-field">*</span></label>
            <input type="text" name="name" class="form-control" id="name" placeholder="Digite Aqui" value="{{$p_Language == null ? '' : $p_Language->name}}" required>
        </div>
        <div class="form-group col-sm-12">
            <input type="submit" class="btn btn-default mt15 mb25 pull-right" value="Salvar">
        </div>
        {!! Form::close() !!}
    </div>
@stop
@section('pageScript')
@stop