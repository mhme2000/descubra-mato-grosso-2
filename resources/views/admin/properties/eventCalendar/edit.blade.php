@extends('admin.main')
@section('pageCSS')
    <style type="text/css">
        .full-width{
            width: 100%;
        }
        .align-center{
            text-align: center;
        }
        .margin-auto{
            margin: auto;
        }
    </style>
@stop
@section('panel-header')
    Configurar relatório do calendário de eventos
@stop
@section('content')
    <div class="row pt15">
        {!! Form::open(['id' => 'mainForm', 'url'=> url('/admin/configurar-calendario-eventos'), 'files' => true]) !!}
            <div class="col-sm-6 fixed-photo-div text-center">
                <h5>Foto de Capa</h5>
                <div class="camera" style="{{$p_EventCalendarCover == null ? '' : 'background-image:url(' . url('/imagens/calendario-eventos/' . $p_EventCalendarCover) . ');'}}">
                    <img class="upload-btn-icon" style="{{$p_EventCalendarCover == null ? '' : 'display:none'}}" src="{{url('/assets/img/camera.png')}}" alt="">
                    <h4 class="upload-btn-text" style="{{$p_EventCalendarCover == null ? '' : 'display:none'}}">Capa</h4>
                    <input class="photo-upload-input" name="capa" type="file" accept="image/gif, image/jpg, image/jpeg, image/png" onchange="onChangeImage(this)" {{$p_EventCalendarCover == null ? 'required' : ''}}>
                    <a title="Excluir" type="button" class="btn btn-success remove-photo" onclick="removePhotoCalendarEvent(this, 'capa')"><i class="fa fa-trash-o"></i></a>
                </div>
            </div>
            <div class="col-sm-6 fixed-photo-div text-center">
                <h5>Imagem de Cabeçalho</h5>
                <div class="camera" style="{{$p_EventCalendarHeader == null ? '' : 'background-image:url(' . url('/imagens/calendario-eventos/' . $p_EventCalendarHeader) . ');'}}">
                    <img class="upload-btn-icon" style="{{$p_EventCalendarHeader == null ? '' : 'display:none'}}" src="{{url('/assets/img/camera.png')}}" alt="">
                    <h4 class="upload-btn-text" style="{{$p_EventCalendarHeader == null ? '' : 'display:none'}}">Cabeçalho</h4>
                    <input class="photo-upload-input" name="cabecalho" type="file" accept="image/gif, image/jpg, image/jpeg, image/png" onchange="onChangeImage(this)">
                    <a title="Excluir" type="button" class="btn btn-success remove-photo" onclick="removePhotoCalendarEvent(this, 'cabecalho')"><i class="fa fa-trash-o"></i></a>
                </div>
            </div>
            <div class="form-group col-sm-12 mt15">
                <label for="last_page">Última página</label>
                <textarea name="event_calendar_last_page" class="form-control" rows="4" id="last_page" placeholder="Digite Aqui">{{$p_EventCalendarLastPage == null ? '' : $p_EventCalendarLastPage}}</textarea>
            </div>
            <div class="form-group col-sm-12">
                <input type="submit" class="btn btn-default mt15 mb25 pull-right" value="Salvar">
            </div>
        {!! Form::close() !!}
    </div>
@stop
@section('pageScript')
    @include('admin.util.photosScript', ['p_MaxPhotoCount' => -1])
    <script src="{{url('/vendor/ckeditor/ckeditor.js')}}" type="text/javascript"></script>
    <script>
        $(document).ready(function(){
            CKEDITOR.replace('last_page',{
                allowedContent: true,
                filebrowserImageBrowseUrl: null,
                filebrowserFlashBrowseUrl: null,
                filebrowserUploadUrl: null,
                filebrowserImageUploadUrl: null,
                filebrowserFlashUploadUrl: null,
                height:420
            });
        });

        function removePhotoCalendarEvent(p_DeleteBtn, p_Type)
        {
            var v_DeleteDiv = $(p_DeleteBtn).closest('div[class*="photo-div"]');

            $('div[class*="photo-div"]:first').before('<input name="delete_photo[]" type="hidden" value="' + p_Type + '">');
            var v_ImageInput = v_DeleteDiv.find('.photo-upload-input');
            $(v_ImageInput).replaceWith(v_ImageInput = $(v_ImageInput).clone(true));
            setPhotoBackground(v_ImageInput, '');
        }
    </script>
@stop