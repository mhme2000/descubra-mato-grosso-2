@extends('admin.util.listDT', ['p_HasDateFilter' => false])
@section('list-css')
    <style>
        table div.actions-div .districts-btn{
            width:auto;
        }
    </style>
@stop
@section('panel-header')
    Municípios
    <a href="{{ url('admin/municipios/editar/') }}">
        <button class="btn btn-success pull-right" title="Novo município">
            <i class="fa fa-plus"></i>
        </button>
    </a>
@stop
@section('list-table-head')
    <tr>
        <th>Nome</th>
        <th class="city-actions">Ações</th>
    </tr>
    <tr>
        <td><input type="text" placeholder="Buscar" class="form-control" style="font-weight:normal"/></td>
        <td></td>
    </tr>
@stop
@section('list-table-dt-url')
    url: "{{ url('/admin/dt/municipios')}}"
@stop
@section('list-table-initial-sorting')
    "aaSorting": [[ 0, "asc" ]],
@stop
@section('list-table-sortable-columns')
    {"bSortable": true},
    {"bSortable": false}
@stop
@section('list-js')
@stop