@extends('admin.main')
@section('pageCSS')
@stop
@section('panel-header')
    {{ $p_Country == null ? 'Cadastro' : 'Edição' }} de país
@stop
@section('content')
    <div class="row pt15">
        {!! Form::open(array('id' => 'mainForm', 'url'=> url('/admin/paises'))) !!}
        @if($p_Country != null)
            <input type="hidden" name="id" value="{{$p_Country->id}}">
        @endif
        <div class="form-group col-sm-12">
            <label for="nome">Nome<span class="mandatory-field">*</span></label>
            <input type="text" name="country[name]" class="form-control" id="name" placeholder="Digite Aqui" value="{{$p_Country == null ? '' : $p_Country->name}}" required>
        </div>
        <div class="form-group col-sm-12">
            <input type="submit" class="btn btn-default mt15 mb25 pull-right" value="Salvar">
        </div>
        {!! Form::close() !!}
    </div>
@stop
@section('pageScript')
@stop