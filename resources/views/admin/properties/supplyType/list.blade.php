@extends('admin.util.listStatic', ['p_HasDateFilter' => false])
@section('list-css')
@stop
@section('panel-header')
    <?php
        $v_Types = [
                'agua' => 'Tipos de abastecimento de água',
                'esgotamento' => 'Tipos de esgotamento',
                'energia' => 'Tipos de abastecimento de energia',
                'coleta-lixo' => 'Tipos de destinação do lixo',
        ];
        echo $v_Types[$p_Type]
    ?>
    <a href="{{ url('admin/servicos-abastecimento/' . $p_Type . '/editar') }}">
        <button class="btn btn-success pull-right" title="Novo tipo">
            <i class="fa fa-plus"></i>
        </button>
    </a>
@stop
@section('list-table-head')
    <tr>
        <th>Nome</th>
        <th>Ações</th>
    </tr>
@stop
@section('list-table-rows')
    @foreach($p_SupplyTypes as $c_SupplyType)
        <tr>
            <td>{{$c_SupplyType->nome}}</td>
            <td>
                <div class="actions-div">
                    <a href="{{ url('admin/servicos-abastecimento/' . $p_Type . '/editar/' . $c_SupplyType->id) }}" title="Editar" type="button" class="btn btn-success">
                        <i class="fa fa-edit"></i>
                    </a>
                </div>
            </td>
        </tr>
    @endforeach
@stop
@section('list-table-initial-sorting')
    "order": [[ 0, "asc" ]],
@stop
@section('list-table-sortable-columns')
    {"bSortable": true},
    {"bSortable": false}
@stop
@section('list-js')
@stop