@extends('admin.util.listStatic', ['p_HasDateFilter' => false])
@section('list-css')
@stop
@section('panel-header')
    Partidos políticos
    <a href="{{ url('admin/partidos/editar') }}">
        <button class="btn btn-success pull-right" title="Novo partido">
            <i class="fa fa-plus"></i>
        </button>
    </a>
@stop
@section('list-table-head')
    <tr>
        <th>Sigla</th>
        <th>Nome</th>
        <th>Ações</th>
    </tr>
@stop
@section('list-table-rows')
    @foreach($p_PoliticalParties as $c_Party)
        <tr>
            <td>{{$c_Party->abbreviation}}</td>
            <td>{{$c_Party->name}}</td>
            <td>
                <div class="actions-div">
                    <a href="{{ url('admin/partidos/editar/' . $c_Party->id) }}" title="Editar" type="button" class="btn btn-success">
                        <i class="fa fa-edit"></i>
                    </a>
                </div>
            </td>
        </tr>
    @endforeach
@stop
@section('list-table-initial-sorting')
    "order": [[ 0, "asc" ]],
@stop
@section('list-table-sortable-columns')
    {"bSortable": true},
    {"bSortable": true},
    {"bSortable": false}
@stop
@section('list-js')
@stop