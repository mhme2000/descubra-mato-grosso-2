@extends('admin.util.listStatic', ['p_HasDateFilter' => false])
@section('list-css')
@stop
@section('panel-header')
    Classificação de evento
    <a href="{{ url('admin/eventos/classificacao/criar') }}">
        <button class="btn btn-success pull-right" title="Novo clima">
            <i class="fa fa-plus"></i>
        </button>
    </a>
@stop
@section('list-table-head')
    <tr>
        <th>Nome</th>
        <th>Publicar em</th>
        <th>Até</th>
        <th>Públicar</th>
        <th>Ações</th>
    </tr>
@stop
@section('list-table-rows')
    @if (isset($eventclassifications))
    @foreach($eventclassifications as $eventclassification)
        <tr>
            <td>{{$eventclassification->nome}}</td>
            <td>{{conv_date($eventclassification->initial_date)}}</td>
            <td>{{conv_date($eventclassification->final_date)}}</td>
            <td>@if($eventclassification->public == 0){{'Não'}}@else{{'Sim'}}@endif</td>
            <td>
                <div class="actions-div">
                    <a href="{{ url('admin/eventos/classificacao/' . $eventclassification->id) }}" title="Editar" type="button" class="btn btn-success">
                        <i class="fa fa-edit"></i>
                    </a>
                </div>
            </td>
        </tr>
    @endforeach
    @endif
@stop
@section('list-table-initial-sorting')
    "order": [[ 0, "asc" ]],
@stop
@section('list-table-sortable-columns')
    {"bSortable": true},
    {"bSortable": false}
@stop
@section('list-js')

@stop