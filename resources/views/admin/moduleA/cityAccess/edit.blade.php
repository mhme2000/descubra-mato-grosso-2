@extends('admin.main')
@section('pageCSS')
    <link href="{{url('/vendor/select2/select2.min.css')}}" rel="stylesheet" />
    <style type="text/css">
        .align-center
        {
            text-align: center;
        }
        .margin-auto
        {
            margin: auto;
        }
        .panel-title > a.btn-success{
            color: white!important;
        }
    </style>
@stop
@section('panel-header')
    A2.2 - Meios de acesso ao município
    <a title="Imprimir" type="button" class="btn btn-success pull-right" onclick="print()">
        <i class="fa fa-print"></i>
    </a>
@stop
@section('content')
    <?php \App\BaseInventoryModel::startFormFieldIndexing(); ?>
    <?php
        $v_AccessTypes = ['' => '', 'Aeroporto' => 'Aeroporto', 'Ferrovia' => 'Ferrovia', 'Metrô' => 'Metrô', 'Porto' => 'Porto', 'Rodoviária' => 'Rodoviária'];
        $v_ConservationOptions = ['' => '', 'Ruim' => 'Ruim', 'Regular' => 'Regular', 'Boa' => 'Boa'];
        $v_AdministrationOptions = ['' => '', 'Privada' => 'Privada', 'Pública' => 'Pública'];
        $v_AirportCategories = ['' => '', 'Regional' => 'Regional', 'Nacional' => 'Nacional', 'Internacional' => 'Internacional'];
        $v_UtilizationOptions = ['' => '', 'Cargas' => 'Cargas', 'Passageiros' => 'Passageiros', 'Turismo' => 'Turismo'];
    ?>

    <div class="row">
        @if(!\App\UserType::isParceiro())
        {!! Form::open(['id' => 'mainForm', 'url'=> url('/admin/inventario/meios-acesso'), 'onsubmit' => 'return submitForm()']) !!}
        @else
        <div id="mainForm">
        @endif
        <div class="form-group col-sm-12 visible-print">
            <h2>A2.2 - Meios de acesso ao município</h2>
        </div>


        <div class="form-group col-sm-12">
            <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Identificação</h3>
        </div>
        @if($p_CityAccess != null)
            <input type="hidden" name="id" value="{{$p_CityAccess->id}}">
        @endif
        <div class="form-group col-sm-6">
            <label for="city_id">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Município<span class="mandatory-field">*</span></label>
            <?php $v_CityOptions = count($p_Cities) == 1 ? $p_Cities : ([''=>''] + $p_Cities); ?>
            {!! Form::select('formulario[city_id]', $v_CityOptions, $p_CityAccess == null ? '' : $p_CityAccess->city_id, ['id' => 'city_id', 'class' => 'form-control select2', 'style' => 'width: 100%', 'required' => 'required']) !!}
        </div>
        <div class="form-group col-sm-6">
            <label for="district_id">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Distrito</label>
            {!! Form::select('formulario[district_id]', [($p_CityAccess == null ? '' : $p_CityAccess->district_id) => ''], $p_CityAccess == null ? '' : $p_CityAccess->district_id, ['id' => 'district_id', 'class' => 'form-control select2', 'style' => 'width: 100%']) !!}
        </div>
        <div class="form-group col-sm-6">
            <label for="type_id">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Tipo<span class="mandatory-field">*</span></label>
            {!! Form::select('formulario[type_id]', $p_Types, $p_CityAccess == null ? '' : $p_CityAccess->type_id, ['id' => 'type_id', 'class' => 'form-control select2', 'required' => 'required', 'style' => 'width: 100%']) !!}
        </div>
        {{--<div class="form-group col-sm-6">--}}
            {{--<label for="sub_type_id">Subtipo<span class="mandatory-field">*</span></label>--}}
            {{--{!! Form::select('formulario[sub_type_id]', [($p_CityAccess == null ? '' : $p_CityAccess->sub_type_id) => ''], $p_CityAccess == null ? '' : $p_CityAccess->sub_type_id, ['id' => 'sub_type_id', 'class' => 'form-control select2', 'required' => 'required', 'style' => 'width: 100%']) !!}--}}
        {{--</div>--}}

        <div class="form-group col-sm-6">
            <label for="tipo">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Tipo do meio de acesso<span class="mandatory-field">*</span></label>
            {!! Form::select('formulario[tipo]', $v_AccessTypes, $p_CityAccess == null ? '' : $p_CityAccess->tipo, ['id' => 'tipo', 'class' => 'form-control', 'required' => 'required']) !!}
        </div>


        <div class="form-group col-sm-12">
            <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Nome, localização e ambiência</h3>
        </div>
        <div class="form-group col-sm-6">
            <label for="nome"><span class="field-index-1">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}</span>Nome<span class="mandatory-field">*</span></label>
            <input type="text" name="formulario[nome]" class="form-control" id="nome" placeholder="Digite Aqui" value="{{$p_CityAccess == null ? '' : $p_CityAccess->nome}}" required>
        </div>
        <div class="form-group col-sm-6">
            <label for="cep"><span class="field-index-1">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}</span>CEP<span class="mandatory-field">*</span></label>
            <input name="formulario[cep]" type="text" class="form-control cep-field" id="cep" placeholder="Digite Aqui" value="{{$p_CityAccess == null ? '' : $p_CityAccess->cep}}" required>
        </div>
        <div class="form-group col-sm-6">
            <label for="bairro"><span class="field-index-1">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}</span>Bairro<span class="mandatory-field">*</span></label>
            <input type="text" name="formulario[bairro]" class="form-control" id="bairro" placeholder="Digite Aqui" value="{{$p_CityAccess == null ? '' : $p_CityAccess->bairro}}" required>
        </div>
        <div class="form-group col-sm-6">
            <label for="logradouro"><span class="field-index-1">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}</span>Logradouro<span class="mandatory-field">*</span></label>
            <input type="text" name="formulario[logradouro]" class="form-control" id="logradouro" placeholder="Digite Aqui" value="{{$p_CityAccess == null ? '' : $p_CityAccess->logradouro}}" required>
        </div>
        <div class="form-group col-sm-6">
            <label for="numero"><span class="field-index-1">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}</span>Número<span class="mandatory-field">*</span></label>
            <input type="text" name="formulario[numero]" class="form-control" id="numero" placeholder="Digite Aqui" value="{{$p_CityAccess == null ? '' : $p_CityAccess->numero}}" required>
        </div>
        <div class="form-group col-sm-6">
            <label for="complemento"><span class="field-index-1">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}</span>Complemento</label>
            <input type="text" name="formulario[complemento]" class="form-control" id="complemento" placeholder="Digite Aqui" value="{{$p_CityAccess == null ? '' : $p_CityAccess->complemento}}">
        </div>
        <div class="form-group col-sm-6">
            <label for="site"><span class="field-index-1">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}</span>Site</label>
            <input type="url" name="formulario[site]" class="form-control" id="site" placeholder="Digite Aqui" value="{{$p_CityAccess == null ? '' : $p_CityAccess->site}}">
        </div>
        <div class="form-group col-sm-6">
            <label for="email"><span class="field-index-1">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}</span>Email</label>
            <input type="text" name="formulario[email]" class="email-field form-control" id="email" placeholder="Digite Aqui" value="{{$p_CityAccess == null ? '' : $p_CityAccess->email}}">
        </div>
        <div class="form-group col-sm-6">
            <label for="telefone"><span class="field-index-1">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}</span>Telefone (exemplo: (DDD) 9 9999-9999 / (DDD) 9999-9999)<span class="mandatory-field">*</span></label>
            <input type="text" name="formulario[telefone]" class="form-control phone-field" id="telefone" placeholder="Digite Aqui" value="{{$p_CityAccess == null ? '' : $p_CityAccess->telefone}}" required>
        </div>

        <div class="ferrovia-fields">
            <div class="form-group col-sm-6">
                <label for="em_funcionamento"><span class="field-index-1">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}</span>Em funcionamento?<span class="mandatory-field">*</span></label>
                {!! Form::select('formulario[em_funcionamento]', ['' => '', '0' => 'Não', '1' => 'Sim'], $p_CityAccess == null ? '' : $p_CityAccess->em_funcionamento , ['id' => 'em_funcionamento', 'class'=>'form-control', 'required' =>'required']) !!}
            </div>
        </div>
        <div class="ferrovia-fields porto-fields">
            <div class="form-group col-sm-6">
                <label for="utilizacao"><span class="field-index-1">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}</span>Utilização<span class="mandatory-field">*</span></label>
                {!! Form::select('formulario[utilizacao]', $v_UtilizationOptions, $p_CityAccess == null ? '' : $p_CityAccess->utilizacao, ['id' => 'utilizacao', 'class' => 'form-control', 'required' => 'required']) !!}
            </div>
        </div>
        <div class="form-group col-sm-6">
            <label for="conservacao"><span class="field-index-1">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}</span>Conservação<span class="mandatory-field">*</span></label>
            {!! Form::select('formulario[conservacao]', $v_ConservationOptions, $p_CityAccess == null ? '' : $p_CityAccess->conservacao, ['id' => 'conservacao', 'class' => 'form-control', 'required' => 'required', 'required' => 'required']) !!}
        </div>


        <div class="form-group col-sm-12">
            <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Administradora do meio de acesso</h3>
        </div>
        <div class="form-group col-sm-6">
            <label for="tipo_administracao"><span class="field-index-2">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}</span>Tipo de administração<span class="mandatory-field">*</span></label>
            {!! Form::select('formulario[tipo_administracao]', $v_AdministrationOptions, $p_CityAccess == null ? '' : $p_CityAccess->tipo_administracao , ['id' => 'tipo_administracao', 'class'=>'form-control', 'required' => 'required']) !!}
        </div>
        <div class="form-group col-sm-6">
            <label for="administradora_nome"><span class="field-index-2">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}</span>Nome da administradora</label>
            <input type="text" name="formulario[administradora_nome]" class="form-control" id="administradora_nome" placeholder="Digite Aqui" value="{{$p_CityAccess == null ? '' : $p_CityAccess->administradora_nome}}">
        </div>
        <div class="form-group col-sm-6">
            <label for="administradora_cep"><span class="field-index-2">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}</span>CEP da administradora</label>
            <input name="formulario[administradora_cep]" type="text" class="form-control cep-field" id="administradora_cep" placeholder="Digite Aqui" value="{{$p_CityAccess == null ? '' : $p_CityAccess->administradora_cep}}">
        </div>
        <div class="form-group col-sm-6">
            <label for="administradora_bairro"><span class="field-index-2">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}</span>Bairro da administradora</label>
            <input type="text" name="formulario[administradora_bairro]" class="form-control" id="administradora_bairro" placeholder="Digite Aqui" value="{{$p_CityAccess == null ? '' : $p_CityAccess->administradora_bairro}}">
        </div>
        <div class="form-group col-sm-6">
            <label for="administradora_logradouro"><span class="field-index-2">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}</span>Logradouro da administradora</label>
            <input type="text" name="formulario[administradora_logradouro]" class="form-control" id="administradora_logradouro" placeholder="Digite Aqui" value="{{$p_CityAccess == null ? '' : $p_CityAccess->administradora_logradouro}}">
        </div>
        <div class="form-group col-sm-6">
            <label for="administradora_numero"><span class="field-index-2">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}</span>Número da administradora</label>
            <input type="text" name="formulario[administradora_numero]" class="form-control" id="administradora_numero" placeholder="Digite Aqui" value="{{$p_CityAccess == null ? '' : $p_CityAccess->administradora_numero}}">
        </div>
        <div class="form-group col-sm-6">
            <label for="complemento"><span class="field-index-2">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}</span>Complemento da administradora</label>
            <input type="text" name="formulario[administradora_complemento]" class="form-control" id="administradora_complemento" placeholder="Digite Aqui" value="{{$p_CityAccess == null ? '' : $p_CityAccess->administradora_complemento}}">
        </div>
        <div class="form-group col-sm-6">
            <label for="administradora_site"><span class="field-index-2">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}</span>Site da administradora</label>
            <input type="url" name="formulario[administradora_site]" class="form-control" id="administradora_site" placeholder="Digite Aqui" value="{{$p_CityAccess == null ? '' : $p_CityAccess->administradora_site}}">
        </div>
        <div class="form-group col-sm-6">
            <label for="administradora_email"><span class="field-index-2">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}</span>Email da administradora</label>
            <input type="text" name="formulario[administradora_email]" class="email-field form-control" id="administradora_email" placeholder="Digite Aqui" value="{{$p_CityAccess == null ? '' : $p_CityAccess->administradora_email}}">
        </div>
        <div class="form-group col-sm-6">
            <label for="administradora_telefone"><span class="field-index-2">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}</span>Telefone da administradora (exemplo (xx) xxxx-xxxx ou 0800-xxx-xxxx ou xxx)</label>
            <input type="text" name="formulario[administradora_telefone]" class="form-control phone-field" id="administradora_telefone" placeholder="Digite Aqui" value="{{$p_CityAccess == null ? '' : $p_CityAccess->administradora_telefone}}">
        </div>
        <div class="aeroporto-fields">
            <div class="form-group col-sm-6">
                <label for="categoria"><span class="field-index-2">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}</span>Categoria<span class="mandatory-field">*</span></label>
                {!! Form::select('formulario[categoria]', $v_AirportCategories, $p_CityAccess == null ? '' : $p_CityAccess->categoria , ['id' => 'categoria', 'class'=>'form-control', 'required' => 'required']) !!}
            </div>
        </div>
        <div class="rodoviaria-fields aeroporto-fields">
            <div class="form-group col-sm-6">
                <label for="ano_base"><span class="field-index-2">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}</span>Ano base<span class="mandatory-field">*</span></label>
                <input type="number" min="1800" max="2500" name="formulario[ano_base]" class="form-control" id="ano_base" placeholder="Digite Aqui" value="{{$p_CityAccess == null ? '' : $p_CityAccess->ano_base}}" required>
            </div>
            <div class="form-group col-sm-6">
                <label for="fluxo_passageiros_nacionais"><span class="field-index-2">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}</span>Fluxo de passageiros nacionais</label>
                <input type="text" name="formulario[fluxo_passageiros_nacionais]" class="form-control integer-field" id="fluxo_passageiros_nacionais" placeholder="Digite Aqui" value="{{$p_CityAccess == null ? '' : $p_CityAccess->fluxo_passageiros_nacionais}}">
            </div>
            <div class="form-group col-sm-6">
                <label for="fluxo_passageiros_internacionais"><span class="field-index-2">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}</span>Fluxo de passageiros internacionais</label>
                <input type="text" name="formulario[fluxo_passageiros_internacionais]" class="form-control integer-field" id="fluxo_passageiros_internacionais" placeholder="Digite Aqui" value="{{$p_CityAccess == null ? '' : $p_CityAccess->fluxo_passageiros_internacionais}}">
            </div>
        </div>
        <div class="porto-fields">
            <div class="form-group col-sm-6">
                <label for="terminal_desembarque_turistas"><span class="field-index-2">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}</span>Possui terminal de desembarque para turistas?<span class="mandatory-field">*</span></label>
                {!! Form::select('formulario[terminal_desembarque_turistas]', ['' => '', '0' => 'Não', '1' => 'Sim'], $p_CityAccess == null ? '' : $p_CityAccess->terminal_desembarque_turistas , ['id' => 'terminal_desembarque_turistas', 'class'=>'form-control', 'required' => 'required']) !!}
            </div>
        </div>

        <div class="form-group col-sm-12">
            <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Dados Complementares</h3>
        </div>
        <div class="form-group col-sm-12">
            <label for="observacao">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Descrições e informações complementares</label>
            <textarea name="formulario[observacao]" id="observacao" class="form-control" rows="4" placeholder="Digite Aqui">{{$p_CityAccess == null ? '' : $p_CityAccess->observacao}}</textarea>
        </div>

        <?php
            $v_ShowInternalUsage = false;
            if($p_CityAccess != null){
                $v_RevisionStatus = $p_CityAccess->revision_status_id;
                $v_ShowInternalUsage = ($v_RevisionStatus == 5
                                        || (\App\UserType::isMunicipio() && $v_RevisionStatus == 1)
                                        || (\App\UserType::isCircuito() && $v_RevisionStatus != 4)
                                        || ((\App\UserType::isMaster() || \App\UserType::isAdmin()) && $v_RevisionStatus != 4));
            }
        ?>
        @if($v_ShowInternalUsage)
            <div class="form-group col-sm-12">
                <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Uso interno da SEDEC/TUR</h3>
            </div>
        @endif
        @include('admin.util.revision', ['p_Form' => $p_CityAccess])

        @include('admin.util.responsibleTeam', ['p_Form' => $p_CityAccess])

        @if(!\App\UserType::isParceiro())
        <div class="form-group col-sm-12">
            <input type="submit" class="btn btn-default mt15 mb25 pull-right" value="Salvar">
        </div>
        {!! Form::close() !!}
        @else
        </div>
        @endif
    </div>
@stop
@section('pageScript')
    <script src="{{url('/vendor/select2/select2.min.js')}}" type="text/javascript"></script>
    <script src="{{url('/vendor/select2/i18n/pt-BR.js')}}" type="text/javascript"></script>
    <script src="{{url('/assets/js/inputmask.js')}}" type="text/javascript"></script>
    <script src="{{url('/assets/js/jquery.inputmask.js')}}" type="text/javascript"></script>
    <script>
        $(document).ready(function()
        {
            $(".select2").select2({language:'pt-BR'});
            $('.rodoviaria-fields, .porto-fields, .aeroporto-fields, .ferrovia-fields').hide();

            $('.cep-field').mask('99.999-999');
            $('.phone-field').keypress(function (e) {
                var key = e.keyCode || e.charCode;
                if( key == 8 || key == 37 || key == 39 || key == 46 )
                    return true;

                var regex = new RegExp(/^[\d\s\(\)\-\+\.\,\;\\\/]+$/);
                var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
                if (regex.test(str))
                    return true;

                e.preventDefault();
                return false;
            });
            $('input[type="url"]').focus(function(){
                if(this.value == '')
                    this.value = 'http://';
            }).focusout(function(){
                if(this.value == 'http://')
                    this.value = '';
            });

            $('#tipo').change(function(){
                $('.rodoviaria-fields, .porto-fields, .aeroporto-fields, .ferrovia-fields').hide();
                $('.rodoviaria-fields .form-control, .porto-fields .form-control, .aeroporto-fields .form-control,' +
                        '.ferrovia-fields .form-control').prop('disabled', true);
                if(this.value == 'Aeroporto')
                {
                    $('.aeroporto-fields').show();
                    $('.aeroporto-fields .form-control').prop('disabled', false);
                }
                if(this.value == 'Ferrovia')
                {
                    $('.ferrovia-fields').show();
                    $('.ferrovia-fields .form-control').prop('disabled', false);
                }
                if(this.value == 'Porto')
                {
                    $('.porto-fields').show();
                    $('.porto-fields .form-control').prop('disabled', false);
                }
                if(this.value == 'Rodoviária')
                {
                    $('.rodoviaria-fields').show();
                    $('.rodoviaria-fields .form-control').prop('disabled', false);
                }

                var v_ItemIndex = 1;
                $('span.field-index-1:visible').each(function(){
                    var v_Index = $(this).text().trim();
                    $(this).html(v_Index.split('.')[0] + '.' + v_ItemIndex + '. ');
                    v_ItemIndex++;
                });
                v_ItemIndex = 1;
                $('span.field-index-2:visible').each(function(){
                    var v_Index = $(this).text().trim();
                    $(this).html(v_Index.split('.')[0] + '.' + v_ItemIndex + '. ');
                    v_ItemIndex++;
                });
            }).change();

            $('#tipo_administracao').change(function(){
                if($(this).val() == 'Pública'){
                    $('#ano_base').removeAttr('required');
                    $('#ano_base').closest('.form-group').find('.mandatory-field').hide();
                }
                else{
                    $('#ano_base').attr('required', true);
                    $('#ano_base').closest('.form-group').find('.mandatory-field').show();
                }
            }).change();


            $('#city_id').change(function(){
                $.get("{{url('/admin/distritosMunicipio?city_id=')}}" + $(this).val(), function(){
                }).done(function(data){
                    if (data.error == 'ok')
                    {
                        var v_LastVal = $('#district_id').val();
                        var v_DataString = '';
                        $.each(data.data, function (c_Key, c_Field)
                        {
                            v_DataString += '<option value="' + c_Key + '">' + c_Field + '</option>';
                        });

                        $('#district_id').html('<option value=""></option>' + v_DataString);
                        if(data.data.length == 0)
                            $('#district_id').select2("val", "");
                        else
                            $('#district_id').select2("val", v_LastVal);
                    }
                    else
                        $('#district_id').html('<option value=""></option>').select2("val", "");
                }).error(function(){
                });
            }).change();
        });


        function submitForm()
        {
            return true;
        }
    </script>
    @include('admin.util.responsibleTeamScript')
    @include('admin.util.revisionScript')
    @include('admin.util.tooltipScript', ['p_Type' => 'A2.2'])
@stop