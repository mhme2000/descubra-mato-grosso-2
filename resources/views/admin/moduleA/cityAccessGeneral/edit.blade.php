@extends('admin.main')
@section('pageCSS')
    <link href="{{url('/vendor/select2/select2.min.css')}}" rel="stylesheet" />
    <style type="text/css">
        .align-center
        {
            text-align: center;
        }
        .margin-auto
        {
            margin: auto;
        }
        .road-title h4{
            color:#000;
            text-decoration: underline;
        }
        .panel-title > a.btn-success{
            color: white!important;
        }
    </style>
@stop
@section('panel-header')
    A2.1 Meios de acesso - Geral
    <a title="Imprimir" type="button" class="btn btn-success pull-right" onclick="print()">
        <i class="fa fa-print"></i>
    </a>
@stop
@section('content')
    <?php \App\BaseInventoryModel::startFormFieldIndexing(); ?>
    <div class="row">
        <div class="form-group col-sm-12 visible-print">
            <h2>A2.1 Meios de acesso - Geral</h2>
        </div>
        @if(!\App\UserType::isParceiro())
        {!! Form::open(['id' => 'mainForm', 'url'=> url('/admin/inventario/meios-acesso/geral'), 'onsubmit' => 'return submitForm()']) !!}
        @else
        <div id="mainForm">
        @endif

        <div class="form-group col-sm-12">
            <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Identificação</h3>
        </div>
        @if($p_CityAccessGeneral != null)
            <input type="hidden" name="id" value="{{$p_CityAccessGeneral->id}}">
        @endif
        <div class="form-group col-sm-6">
            <label for="city_id">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Município<span class="mandatory-field">*</span></label>
            <?php $v_CityOptions = count($p_CityOptions) == 1 ? $p_CityOptions : ([''=>''] + $p_CityOptions); ?>
            {!! Form::select('formulario[city_id]', $p_CityOptions, $p_CityAccessGeneral == null ? '' : $p_CityAccessGeneral->city_id, ['id' => 'city_id', 'class' => 'form-control select2', 'style' => 'width: 100%', 'required' => 'required']) !!}
        </div>
        <div class="col-sm-12">
            <h3 id="federal_index">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Rodovias federais <i>(permite mais de uma opção)</i></h3>
        </div>
        <div class="form-group col-sm-12">
            {!! Form::select('', $p_FederalRoads, [], ['id' => 'rodovias_federais', 'class' => 'form-control select2', 'style' => 'width: 100%', 'multiple' => 'multiple']) !!}
        </div>
        <input id="input_rodovia_federal" type="hidden" name="formulario[rodovia_federal]" value="{{$p_CityAccessGeneral == null ? '' : $p_CityAccessGeneral->rodovia_federal}}">

        <div class="col-sm-12">
            <hr>
            <h3 id="estadual_index">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Rodovias estaduais <i>(permite mais de uma opção)</i></h3>
        </div>
        <div class="form-group col-sm-12">
            {!! Form::select('', $p_StateRoads, [], ['id' => 'rodovias_estaduais', 'class' => 'form-control select2', 'style' => 'width: 100%', 'multiple' => 'multiple']) !!}
        </div>
        <input id="input_rodovia_estadual" type="hidden" name="formulario[rodovia_estadual]" value="{{$p_CityAccessGeneral == null ? '' : $p_CityAccessGeneral->rodovia_estadual}}">

        <div class="col-sm-12">
            <hr>
            <h3 id="municipal_index">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Rodovias municipais <i>(permite mais de uma opção)</i></h3>
        </div>
        <div class="form-group col-sm-12">
            {!! Form::select('', [], [], ['id' => 'rodovias_municipais', 'class' => 'form-control select2-custom', 'style' => 'width: 100%', 'multiple' => 'multiple']) !!}
        </div>
        <input id="input_rodovia_municipal" type="hidden" name="formulario[rodovia_municipal]" value="{{$p_CityAccessGeneral == null ? '' : $p_CityAccessGeneral->rodovia_municipal}}">


        <?php
            $v_ShowInternalUsage = false;
            if($p_CityAccessGeneral != null){
                $v_RevisionStatus = $p_CityAccessGeneral->revision_status_id;
                $v_ShowInternalUsage = ($v_RevisionStatus == 5
                                        || (\App\UserType::isMunicipio() && $v_RevisionStatus == 1)
                                        || (\App\UserType::isCircuito() && $v_RevisionStatus != 4)
                                        || ((\App\UserType::isMaster() || \App\UserType::isAdmin()) && $v_RevisionStatus != 4));
            }
        ?>
        @if($v_ShowInternalUsage)
            <div class="form-group col-sm-12">
                <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Uso interno da SEDEC/TUR</h3>
            </div>
        @endif
        @include('admin.util.revision', ['p_Form' => $p_CityAccessGeneral])

        @include('admin.util.responsibleTeam', ['p_Form' => $p_CityAccessGeneral])

        @if(!\App\UserType::isParceiro())
        <div class="form-group col-sm-12">
            <input type="submit" class="btn btn-default mt15 mb25 pull-right" value="Salvar">
        </div>
        {!! Form::close() !!}
        @else
        </div>
        @endif
    </div>
@stop
@section('pageScript')
    <script src="{{url('/vendor/select2/select2.min.js')}}" type="text/javascript"></script>
    <script src="{{url('/vendor/select2/i18n/pt-BR.js')}}" type="text/javascript"></script>
    <script src="{{url('/assets/js/inputmask.js')}}" type="text/javascript"></script>
    <script src="{{url('/assets/js/jquery.inputmask.js')}}" type="text/javascript"></script>
    <script>
        var v_FederalRoadInput = $('#input_rodovia_federal');
        var v_StateRoadInput = $('#input_rodovia_estadual');
        var v_CityRoadInput = $('#input_rodovia_municipal');
        $(document).ready(function()
        {
            $(".select2").select2({language:'pt-BR'});

            $('.phone-field').keypress(function (e) {
                var key = e.keyCode || e.charCode;
                if( key == 8 || key == 37 || key == 39 || key == 46 )
                    return true;

                var regex = new RegExp(/^[\d\s\(\)\-\+\.\,\;\\\/]+$/);
                var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
                if (regex.test(str))
                    return true;

                e.preventDefault();
                return false;
            });

            @if($p_CityAccessGeneral != null)
                $('#city_id').prop('disabled', true);
            @endif

            var v_FederalRoadData = v_FederalRoadInput.val().length > 0 ? JSON.parse(v_FederalRoadInput.val()) : [];
            var v_SelectedItems = [];
            $(v_FederalRoadData).each(function(c_Index){
                var v_Fields = getFieldsString('federal', this, c_Index);
                v_FederalRoadInput.before(v_Fields);
                v_SelectedItems.push(this.id);
            });
            $('#rodovias_federais').select2("val", v_SelectedItems);

            var v_StateRoadData = v_StateRoadInput.val().length > 0 ? JSON.parse(v_StateRoadInput.val()) : [];
            v_SelectedItems = [];
            $(v_StateRoadData).each(function(c_Index){
                var v_Fields = getFieldsString('estadual', this, c_Index);
                v_StateRoadInput.before(v_Fields);
                v_SelectedItems.push(this.id);
            });
            $('#rodovias_estaduais').select2("val", v_SelectedItems);

            var v_CityRoadData = v_CityRoadInput.val().length > 0 ? JSON.parse(v_CityRoadInput.val()) : [];
            v_SelectedItems = '';
            $(v_CityRoadData).each(function(c_Index){
                var v_Fields = getFieldsString('municipal', this, c_Index);
                v_CityRoadInput.before(v_Fields);
                v_SelectedItems += '<option value="'+this.nome+'" selected>'+this.nome+'</option>';
            });
            $('#rodovias_municipais').html(v_SelectedItems);
            $(".select2-custom").select2({language:'pt-BR', tags: true});

            $('#rodovias_federais').change(function(){
                var v_Data = updateJSONData('federal');
                $('.federal').remove();
                var v_Selection = $(this).select2('data');
                $(v_Selection).each(function(c_Index){
                    var v_Road = findObjectById(v_Data, this.id);
                    if(v_Road == undefined)
                        v_Road = initRoadObject(this);
                    var v_Fields = getFieldsString('federal', v_Road, c_Index);
                    v_FederalRoadInput.before(v_Fields);
                });
                toggleTollValueField();
            });
            $('#rodovias_estaduais').change(function(){
                var v_Data = updateJSONData('estadual');
                $('.estadual').remove();
                var v_Selection = $(this).select2('data');
                $(v_Selection).each(function(c_Index){
                    var v_Road = findObjectById(v_Data, this.id);
                    if(v_Road == undefined)
                        v_Road = initRoadObject(this);
                    var v_Fields = getFieldsString('estadual', v_Road, c_Index);
                    v_StateRoadInput.before(v_Fields);
                });
                toggleTollValueField();
            });
            $('#rodovias_municipais').change(function(){
                var v_Data = updateJSONData('municipal');
                $('.municipal').remove();
                var v_Selection = $(this).select2('data');
                $(v_Selection).each(function(c_Index){
                    var v_Road = findObjectById(v_Data, this.id);
                    if(v_Road == undefined)
                        v_Road = initRoadObject(this);
                    var v_Fields = getFieldsString('municipal', v_Road, c_Index);
                    v_CityRoadInput.before(v_Fields);
                });
                toggleTollValueField();
            });

            toggleTollValueField();
        });

        function toggleTollValueField()
        {
            $('.road-toll').change(function(){
                var v_Parent = $(this).closest('.form-group').parent();
                if(this.value == 'Sim')
                    v_Parent.find('.toll-value').show();
                else
                    v_Parent.find('.toll-value').hide();
            }).trigger('change');

            $('.currency-field').maskMoney({thousands:'.',decimal:',',allowZero:true});
        }

        function initRoadObject(p_Data)
        {
            var v_Road = {};
            v_Road.id = p_Data.id;
            v_Road.nome = p_Data.text;
            v_Road.sinalizacaoGeral = '';
            v_Road.sinalizacaoTuristica = '';
            v_Road.pavimentacao = '';
            v_Road.conservacao = '';
            v_Road.pedagio = '';
            v_Road.valor = '';
            return v_Road;
        }

        function findObjectById(p_Array, p_Id)
        {
            return $.grep(p_Array, function(e){ return e.id == p_Id; })[0];
        }

        function updateJSONData(p_Type)
        {
            var v_RoadDataArray = [];
            $('.' + p_Type).each(function(){
                var v_RoadData = {};
                v_RoadData.id = $(this).find('.road-id').val();
                v_RoadData.nome = $(this).find('.road-name').val();
                v_RoadData.sinalizacaoGeral = $(this).find('.road-signaling').val();
                v_RoadData.sinalizacaoTuristica = $(this).find('.road-touristic-signaling').val();
                v_RoadData.pavimentacao = $(this).find('.road-pavement').val();
                v_RoadData.conservacao = $(this).find('.road-conservation').val();
                v_RoadData.pedagio = $(this).find('.road-toll').val();
                v_RoadData.valor = $(this).find('.road-toll-value').val();
                v_RoadDataArray.push(v_RoadData);
            });
            $('#input_rodovia_' + p_Type).val(JSON.stringify(v_RoadDataArray));
            return v_RoadDataArray;
        }

        function getFieldsString(p_Type, p_Road, p_Index){
            var v_ParentIndex = $('#' + p_Type + '_index').text().split('. ')[0];
            var v_BaseIndex = v_ParentIndex + '.' + (p_Index + 1) + '.';
            return '<div class="' + p_Type + '">' +
                        '<div class="col-sm-12 road-title">' +
                            '<h4>' + v_BaseIndex + ' Rodovia - ' + p_Road.nome + '</h4>' +
                        '</div>' +
                        '<input type="hidden" class="road-id" value="' + p_Road.id + '">' +
                        '<input type="hidden" class="road-name" value="' + p_Road.nome + '">' +
                        '<div class="form-group col-sm-4">' +
                            '<label>' + v_BaseIndex + '1. Sinalização geral</label>' +
                            '<select class="form-control road-signaling" required="required">' +
                                '<option value="" ' + (p_Road.sinalizacaoGeral == '' ? 'selected' : '') + '></option>' +
                                '<option value="Não sinalizado" ' + (p_Road.sinalizacaoGeral == 'Não sinalizado' ? 'selected' : '') + '>Não sinalizado</option>' +
                                '<option value="Mal sinalizado" ' + (p_Road.sinalizacaoGeral == 'Mal sinalizado' ? 'selected' : '') + '>Mal sinalizado</option>' +
                                '<option value="Bem sinalizado" ' + (p_Road.sinalizacaoGeral == 'Bem sinalizado' ? 'selected' : '') + '>Bem sinalizado</option>' +
                            '</select>' +
                        '</div>' +
                        '<div class="form-group col-sm-4">' +
                            '<label>' + v_BaseIndex + '2. Sinalização turística</label>' +
                            '<select class="form-control road-touristic-signaling" required="required">' +
                                '<option value="" ' + (p_Road.sinalizacaoTuristica == '' ? 'selected' : '') + '></option>' +
                                '<option value="Não sinalizado" ' + (p_Road.sinalizacaoTuristica == 'Não sinalizado' ? 'selected' : '') + '>Não sinalizado</option>' +
                                '<option value="Mal sinalizado" ' + (p_Road.sinalizacaoTuristica == 'Mal sinalizado' ? 'selected' : '') + '>Mal sinalizado</option>' +
                                '<option value="Bem sinalizado" ' + (p_Road.sinalizacaoTuristica == 'Bem sinalizado' ? 'selected' : '') + '>Bem sinalizado</option>' +
                            '</select>' +
                        '</div>' +
                        '<div class="form-group col-sm-4">' +
                            '<label>' + v_BaseIndex + '3. Pavimentação</label>' +
                            '<select class="form-control road-pavement" required="required">' +
                                '<option value="" ' + (p_Road.pavimentacao == '' ? 'selected' : '') + '></option>' +
                                '<option value="Asfalto" ' + (p_Road.pavimentacao == 'Asfalto' ? 'selected' : '') + '>Asfalto</option>' +
                                '<option value="Asfalto ecológico" ' + (p_Road.pavimentacao == 'Asfalto ecológico' ? 'selected' : '') + '>Asfalto ecológico</option>' +
                                '<option value="Chão batido" ' + (p_Road.pavimentacao == 'Chão batido' ? 'selected' : '') + '>Chão batido</option>' +
                                '<option value="Concreto" ' + (p_Road.pavimentacao == 'Concreto' ? 'selected' : '') + '>Concreto</option>' +
                                '<option value="Paralelepípedo" ' + (p_Road.pavimentacao == 'Paralelepípedo' ? 'selected' : '') + '>Paralelepípedo</option>' +
                                '<option value="Saibro" ' + (p_Road.pavimentacao == 'Saibro' ? 'selected' : '') + '>Saibro</option>' +
                            '</select>' +
                        '</div>' +
                        '<div class="form-group col-sm-4">' +
                            '<label>' + v_BaseIndex + '4. Conservação da Rodovia</label>' +
                            '<select class="form-control road-conservation" required="required">' +
                                '<option value="" ' + (p_Road.conservacao == '' ? 'selected' : '') + '></option>' +
                                '<option value="Ruim" ' + (p_Road.conservacao == 'Ruim' ? 'selected' : '') + '>Ruim</option>' +
                                '<option value="Regular" ' + (p_Road.conservacao == 'Regular' ? 'selected' : '') + '>Regular</option>' +
                                '<option value="Boa" ' + (p_Road.conservacao == 'Boa' ? 'selected' : '') + '>Boa</option>' +
                            '</select>' +
                        '</div>' +
                        '<div class="form-group col-sm-4">' +
                            '<label>' + v_BaseIndex + '5. Pedágio?</label>' +
                            '<select class="form-control road-toll" required="required">' +
                                '<option value="" ' + (p_Road.pedagio == '' ? 'selected' : '') + '></option>' +
                                '<option value="Não" ' + (p_Road.pedagio == 'Não' ? 'selected' : '') + '>Não</option>' +
                                '<option value="Sim" ' + (p_Road.pedagio == 'Sim' ? 'selected' : '') + '>Sim</option>' +
                            '</select>' +
                        '</div>' +
                        '<div class="form-group col-sm-4 toll-value" style="display:none">' +
                            '<label for="valor">' + v_BaseIndex + '5.1. Valor - R$</label>' +
                            '<input type="text" class="form-control road-toll-value currency-field" placeholder="Digite Aqui" value="' + p_Road.valor + '">' +
                        '</div>' +
                    '</div>';
        }

        function submitForm()
        {
            updateJSONData('federal');
            updateJSONData('estadual');
            updateJSONData('municipal');

            return true;
        }
    </script>
    @include('admin.util.responsibleTeamScript')
    @include('admin.util.revisionScript')
    @include('admin.util.tooltipScript', ['p_Type' => 'A2.1'])
@stop