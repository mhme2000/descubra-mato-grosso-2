@extends('admin.mainTabs')
@section('pageCSS')
    <link href="{{url('/vendor/select2/select2.min.css')}}" rel="stylesheet" />
    <style type="text/css">
        .full-width
        {
            width: 100%;
        }
        .align-center
        {
            text-align: center;
        }

        .margin-auto
        {
            margin: auto;
        }
    </style>
@stop
@section('panel-header')
    {{ $p_Route == null ? 'Cadastro' : 'Edição' }} de roteiro
    <ul class="nav panel-tabs-border panel-tabs">
        <li class="active">
            <a href="#tab0" data-toggle="tab" aria-expanded="true">Português</a>
        </li>
        <li class="">
            <a href="#tab1" data-toggle="tab" aria-expanded="false">Inglês</a>
        </li>
        <li class="">
            <a href="#tab2" data-toggle="tab" aria-expanded="false">Espanhol</a>
        </li>
        <li class="">
            <a href="#tab3" data-toggle="tab" aria-expanded="false">Francês</a>
        </li>
    </ul>
@stop
@section('content')
    @if(!\App\UserType::isParceiro())
    {!! Form::open(array('id' => 'mainForm', 'url'=> url('/admin/roteiros'), 'onsubmit' => 'return submitForm()', 'files' => true)) !!}
    @endif
    <div class="tab-content pn br-n">
        @if($p_Route != null)
            <input type="hidden" name="id" value="{{$p_Route->id}}">
        @endif
        <input type="hidden" id="form_nome_pt" name="roteiro[nome_pt]">
        <input type="hidden" id="form_nomes" name="roteiro[nome]">
        <input type="hidden" id="form_descricoes" name="roteiro[descricao]">
        <input type="hidden" id="form_descricoes_curtas" name="roteiro[descricao_curta]">
        <input type="hidden" id="form_depoimentos" name="roteiro[depoimento]">
        <input type="hidden" id="form_depoimentos_autores" name="roteiro[depoimento_autor]">

        <?php
            if($p_Route != null){
                $v_EmptyData = ['pt'=>'', 'en'=>'', 'es'=>'', 'fr'=>''];
                $v_Names = $p_Route->nome != null ? json_decode($p_Route->nome,1) : $v_EmptyData;
                $v_Descriptions = $p_Route->descricao != null ? json_decode($p_Route->descricao,1) : $v_EmptyData;
                $v_ShortDescriptions = $p_Route->descricao_curta != null ? json_decode($p_Route->descricao_curta,1) : $v_EmptyData;
                $v_Testimonials = $p_Route->depoimento != null ? json_decode($p_Route->depoimento,1) : $v_EmptyData;
                $v_TestimonialAuthors = $p_Route->depoimento_autor != null ? json_decode($p_Route->depoimento_autor,1) : $v_EmptyData;
            }
            $v_Languages = ['pt', 'en', 'es', 'fr'];
        ?>

        @if($p_Route != null)
            <input type="hidden" name="roteiro[id]" value="{{$p_Route->id}}">
        @endif
        @foreach($v_Languages as $c_Index => $c_Language)
            <div id="{{'tab' . $c_Index}}" class="tab-pane {{ $c_Index == 0 ? 'active' : '' }}">
                @if($c_Index == 0)
                    <div class="row">
                        <div class="col-sm-4 fixed-photo-div">
                            <div class="camera" style="{{$p_CoverPhoto == null ? '' : 'background-image:url(' . $p_CoverPhoto->url . ');'}}">
                                <img class="upload-btn-icon" style="{{$p_CoverPhoto == null ? '' : 'display:none'}}" src="{{url('/assets/img/camera.png')}}" alt="">
                                <h4 class="upload-btn-text" style="{{$p_CoverPhoto == null ? '' : 'display:none'}}">Foto de<br>capa</h4>
                                <input class="photo-upload-input" name="photo[file][]" type="file" accept="image/gif, image/jpg, image/jpeg, image/png" onchange="onChangeImage(this)" {{$p_CoverPhoto == null ? 'required' : ''}}>
                                <input type="hidden" name="photo[type][]" value="cover">
                                <input type="hidden" name="photo[id][]" value="{{$p_CoverPhoto == null ? '' : $p_CoverPhoto->id}}">
                                @if($p_CoverPhoto)
                                    <a title="Baixar" href="{{$p_CoverPhoto->url}}" class="btn btn-success download-photo-btn" download><i class="fa fa-download"></i></a>
                                @endif
                            </div>
                        </div>
                        <div class="col-sm-4 fixed-photo-div">
                            <div class="camera" style="{{$p_MapPhoto == null ? '' : 'background-image:url(' . $p_MapPhoto->url . ');'}}">
                                <img class="upload-btn-icon" style="{{$p_MapPhoto == null ? '' : 'display:none'}}" src="{{url('/assets/img/camera.png')}}" alt="">
                                <h4 class="upload-btn-text" style="{{$p_MapPhoto == null ? '' : 'display:none'}}">Mapa</h4>
                                <input class="photo-upload-input" name="photo[file][]" type="file" accept="image/gif, image/jpg, image/jpeg, image/png" onchange="onChangeImage(this)">
                                <input type="hidden" name="photo[type][]" value="map">
                                <input type="hidden" name="photo[id][]" value="{{$p_MapPhoto == null ? '' : $p_MapPhoto->id}}">
                                @if($p_MapPhoto)
                                    <a title="Baixar" href="{{$p_MapPhoto->url}}" class="btn btn-success download-photo-btn" download><i class="fa fa-download"></i></a>
                                @endif
                                <a title="Excluir" type="button" class="btn btn-success remove-photo" onclick="removePhoto(this)"><i class="fa fa-trash-o"></i></a>
                            </div>
                        </div>
                        <div class="col-sm-4 fixed-photo-div">
                            <div class="camera" style="{{$p_TestimonialPhoto == null ? '' : 'background-image:url(' . $p_TestimonialPhoto->url . ');'}}">
                                <img class="upload-btn-icon" style="{{$p_TestimonialPhoto == null ? '' : 'display:none'}}" src="{{url('/assets/img/camera.png')}}" alt="">
                                <h4 class="upload-btn-text" style="{{$p_TestimonialPhoto == null ? '' : 'display:none'}}">Foto<br>depoimento</h4>
                                <input class="photo-upload-input" name="photo[file][]" type="file" accept="image/gif, image/jpg, image/jpeg, image/png" onchange="onChangeImage(this)">
                                <input type="hidden" name="photo[type][]" value="testimonial">
                                <input type="hidden" name="photo[id][]" value="{{$p_TestimonialPhoto == null ? '' : $p_TestimonialPhoto->id}}">
                                @if($p_TestimonialPhoto)
                                    <a title="Baixar" href="{{$p_TestimonialPhoto->url}}" class="btn btn-success download-photo-btn" download><i class="fa fa-download"></i></a>
                                @endif
                                <a title="Excluir" type="button" class="btn btn-success remove-photo" onclick="removePhoto(this)"><i class="fa fa-trash-o"></i></a>
                            </div>
                        </div>
                    </div>
                @endif
                <div class="row">
                    <div class="form-group col-sm-12">
                        <label for="nome_{{$c_Language}}">Nome<span class="mandatory-field">*</span></label>
                        <input type="text" class="form-control" id="nome_{{$c_Language}}" placeholder="Digite Aqui" value="{{$p_Route == null ? '' : $v_Names[$c_Language]}}" {{$c_Index == 0 ? 'required' : ''}}>
                    </div>
                    @if($c_Index == 0)
                        <div class="form-group col-sm-6">
                            <label for="destaque">Destaque?</label>
                            <p><input type="checkbox" value="1" name="roteiro[destaque]" id="destaque" class="ml5 mt10" {{($p_Route == null || $p_Route->destaque == 0) ? '' : 'checked'}}></p>
                        </div>
                        <div class="form-group col-sm-12">
                            <label for="destinos_participantes">Destinos participantes <i>(permite mais de uma opção)</i><span class="mandatory-field">*</span></label>
                            {!! Form::select('destinos_participantes[]', $p_Destinations, $p_RouteDestinations, ['id' => 'destinos_participantes', 'class' => 'form-control select2', 'required' => 'required', 'style' => 'width: 100%', 'multiple' => 'multiple']) !!}
                        </div>
                    @endif
                    <div class="form-group col-sm-12">
                        <label for="depoimento_{{$c_Language}}">Depoimento</label>
                        <textarea id="depoimento_{{$c_Language}}" class="form-control" rows="4" placeholder="Digite Aqui">{{$p_Route == null ? '' : $v_Testimonials[$c_Language]}}</textarea>
                    </div>
                    <div class="form-group col-sm-12">
                        <label for="depoimento_autor_{{$c_Language}}">Autor do depoimento</label>
                        <input type="text" class="form-control" id="depoimento_autor_{{$c_Language}}" placeholder="Digite Aqui" value="{{$p_Route == null ? '' : $v_TestimonialAuthors[$c_Language]}}">
                    </div>
                    <div class="form-group col-sm-12">
                        <label for="descricao_curta_{{$c_Language}}">Descrição curta</label>
                        <textarea id="descricao_curta_{{$c_Language}}" class="form-control" rows="4" placeholder="Digite Aqui">{{$p_Route == null ? '' : $v_ShortDescriptions[$c_Language]}}</textarea>
                    </div>

                    <div class="form-group col-sm-12">
                        <label for="descricao_{{$c_Language}}">Descrição<span class="mandatory-field">*</span></label>
                        <textarea id="descricao_{{$c_Language}}" class="form-control" rows="4" placeholder="Digite Aqui" {{$c_Index == 0 ? 'required' : ''}}>{{$p_Route == null ? '' : $v_Descriptions[$c_Language]}}</textarea>
                    </div>

                    @if($c_Index == 0)
                        <div class="form-group col-sm-6">
                            <label for="site">Site</label>
                            <input type="url"  name="roteiro[site]" class="form-control" id="site" placeholder="Digite Aqui" value="{{$p_Route == null ? '' : $p_Route->site}}">
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="telefone">Telefone (exemplo: (DDD) 9 9999-9999 / (DDD) 9999-9999)</label>
                            <input type="text" name="roteiro[telefone]" class="form-control phone-field" id="telefone" placeholder="Digite Aqui" value="{{$p_Route == null ? '' : $p_Route->telefone}}">
                            <input type="hidden" value="">
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="email">Email</label>
                            <input type="email" name="roteiro[email]" class="form-control" id="email" placeholder="Digite Aqui" value="{{$p_Route == null ? '' : $p_Route->email}}">
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="facebook">Facebook</label>
                            <input type="url"  name="roteiro[facebook]" class="form-control" id="facebook" placeholder="Digite Aqui" value="{{$p_Route == null ? '' : $p_Route->facebook}}">
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="instagram">Instagram</label>
                            <input type="url"  name="roteiro[instagram]" class="form-control" id="instagram" placeholder="Digite Aqui" value="{{$p_Route == null ? '' : $p_Route->instagram}}">
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="twitter">Twitter</label>
                            <input type="url"  name="roteiro[twitter]" class="form-control" id="twitter" placeholder="Digite Aqui" value="{{$p_Route == null ? '' : $p_Route->twitter}}">
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="youtube">Youtube</label>
                            <input type="url"  name="roteiro[youtube]" class="form-control" id="youtube" placeholder="Digite Aqui" value="{{$p_Route == null ? '' : $p_Route->youtube}}">
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="flickr">Flickr</label>
                            <input type="url"  name="roteiro[flickr]" class="form-control" id="flickr" placeholder="Digite Aqui" value="{{$p_Route == null ? '' : $p_Route->flickr}}">
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="minas_360">Link para Minas em 360º</label>
                            <input type="url"  name="roteiro[minas_360]" class="form-control" id="minas_360" placeholder="Digite Aqui" value="{{$p_Route == null ? '' : $p_Route->minas_360}}">
                        </div>
                    @endif

                    @if($c_Index == 0)
                        <div class="form-group col-sm-12">
                            <label for="tipos_viagem">Tipo de polo <i>(permite mais de uma opção)</i></label>
                            {{-- $p_SelectedCategories é um array com as chaves das categorias selecionadas --}}
                            {!! Form::select('tipos_viagem[]', $p_TripTypes, $p_SelectedTripTypes, ['id' => 'tipos_viagem', 'class' => 'form-control select2', 'style' => 'width: 100%', 'multiple' => 'multiple']) !!}
                        </div>
                        <div class="form-group col-sm-12">
                            <?php
                                $v_TypeOptions = [
                                    'Aventura' => 'Aventura',
                                    'Bem-Estar' => 'Bem-Estar',
                                    'Cultura' => 'Cultura',
                                    'Ecoturismo' => 'Ecoturismo',
                                    'Gastronomico' => 'Gastronomico',
                                    'Negócios e Eventos' => 'Negócios e Eventos',
                                    'Pesca' => 'Pesca',
                                    'Religioso' => 'Religioso',
                                    'Ruralidade' => 'Ruralidade',
                                ];
                                $v_SelectedTypes = $p_Route == null ? [] : explode(';', $p_Route->tipo_roteiro);
                            ?>
                            <label for="tipo_roteiro">Tipo de Roteiro <i>(permite mais de uma opção)</i></label>
                            {!! Form::select('tipo_roteiro[]', $v_TypeOptions, $v_SelectedTypes, ['id' => 'tipo_roteiro', 'class' => 'form-control select2', 'style' => 'width: 100%', 'multiple' => 'multiple']) !!}
                        </div>

                        <div class="form-group col-sm-12">
                            <label for="hashtags">Palavras-chave<span class="mandatory-field">*</span></label>
                            {!! Form::select('hashtags[]', $p_Hashtags, array_keys($p_Hashtags), ['id' => 'hashtags', 'class' => 'form-control select2-custom', 'style' => 'width: 100%', 'required' => 'required', 'multiple' => 'multiple']) !!}
                        </div>
                        <div class="form-group col-sm-12">
                            <h3>Etapas da viagem</h3>
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="dias_duracao">Duração (dias)</label>
                            <input type="number" min="1" name="roteiro[dias_duracao]" class="form-control" id="dias_duracao" placeholder="Digite Aqui" value="{{$p_Route == null ? '1' : $p_Route->dias_duracao}}">
                        </div>
                        <div class="days-hidden-inputs-div">
                        </div>
                    @endif

                    @if(count($p_Days))
                        @foreach($p_Days as $c_DayIndex => $c_Day)
                            <?php $v_DayDescription = json_decode($c_Day->descricao,1); ?>
                            <div class="form-group col-sm-12 day-description">
                                <label for="descricao_dia_{{$c_Language . '_' . $c_Index . $c_DayIndex}}">Detalhamento dia {{ ($c_Day->dia) }}</label>
                                <textarea class="form-control descricao_dia_{{$c_Language . '_' . $c_DayIndex}}" rows="2" id="descricao_dia_{{$c_Language . '_' . $c_Index . $c_DayIndex}}" placeholder="Digite Aqui">{{$v_DayDescription[$c_Language]}}</textarea>
                            </div>
                        @endforeach
                    @endif

                    <div class="hidden day-description-guideline"></div>

                    @if($c_Index == 0)
                        @include('admin.util.photos', ['p_PhotoGallery' => $p_PhotoGallery, 'p_Cover' => false])
                    @endif
                </div>
            </div>
        @endforeach
        @if(!\App\UserType::isParceiro())
        <div class="row">
            <div class="form-group col-sm-12">
                <input type="submit" class="btn btn-default mt15 mb25 pull-right" value="Salvar">
            </div>
        </div>
        @endif
    </div>
    @if(!\App\UserType::isParceiro())
    {!! Form::close() !!}
    @endif
@stop
@section('pageScript')
    <script src="{{url('/vendor/select2/select2.min.js')}}" type="text/javascript"></script>
    <script src="{{url('/vendor/select2/i18n/pt-BR.js')}}" type="text/javascript"></script>
    <script src="{{url('/assets/js/jquery.inputmask.js')}}" type="text/javascript"></script>
    @include('admin.util.photosScript', ['p_MaxPhotoCount' => -1])
    <script>
        $(document).ready(function()
        {
            $(".select2").select2({language:'pt-BR'});
            $(".select2-custom").select2({language:'pt-BR', tags: true});

            $('.phone-field').keypress(function (e) {
                var key = e.keyCode || e.charCode;
                if( key == 8 || key == 37 || key == 39 || key == 46 )
                    return true;

                var regex = new RegExp(/^[\d\s\(\)\-\+\.\,\;\\\/]+$/);
                var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
                if (regex.test(str))
                    return true;

                e.preventDefault();
                return false;
            });
            $('input[type="url"]').focus(function(){
                if(this.value == '')
                    this.value = 'http://';
            }).focusout(function(){
                if(this.value == 'http://')
                    this.value = '';
            });

            $('#dias_duracao').change(function(){
                routeDayDescription($(this).val());
            }).trigger('change');
        });
        var v_Languages = ['pt', 'en', 'es', 'fr'];
        var v_DayInputHidden = '<input type="hidden" class="day-description-hidden" name="dias[]">';
        function routeDayDescription(p_DayCount)
        {
            var v_HiddenInputsHtml = '';
            for(v_Index = 0; v_Index < p_DayCount; v_Index++)
                v_HiddenInputsHtml += v_DayInputHidden;

            $('.days-hidden-inputs-div').html(v_HiddenInputsHtml);
            $('.tab-pane').each(function (c_Key, c_Field)
            {
                var v_DayCounter = $(c_Field).find('.day-description').length;
                if(v_DayCounter != p_DayCount)
                {
                    while(v_DayCounter > p_DayCount)
                    {
                        $(c_Field).find('.day-description:last').remove();
                        v_DayCounter--;
                    }
                    while(v_DayCounter < p_DayCount)
                    {
                        addRouteDayDescription(c_Key, c_Field, v_DayCounter, v_Languages[c_Key]);
                        v_DayCounter++;
                    }
                }
            });
        }
        function addRouteDayDescription(p_Index, p_Panel, p_DayIndex, p_Language)
        {
            var v_Div = (
                    '<div class="form-group col-sm-12 day-description">' +
                        '<label for="descricao_dia_' + p_Language + '_' + p_Index + p_DayIndex + '">Detalhamento dia ' + (p_DayIndex + 1) + '</label>' +
                        '<textarea class="form-control descricao_dia_' + p_Language + '_' + p_DayIndex + '" rows="2" id="descricao_dia_' + p_Language + '_' + p_Index + p_DayIndex + '" placeholder="Digite Aqui"></textarea>' +
                    '</div>'
            );
            $(p_Panel).find('.day-description-guideline').before(v_Div);
        }

        function submitForm()
        {
            var v_NumeroDias = $('#dias_duracao').val();
            var v_DiasJson = [];
            for(v_Index = 0; v_Index < v_NumeroDias; v_Index++)
                v_DiasJson[v_Index] = {pt:'',en:'',es:'',fr:''};

            var v_NamesJson = {};
            var v_DescriptionsJson = {};
            var v_ShortDescriptionsJson = {};
            var v_TestimonialsJson = {};
            var v_TestimonialAuthorsJson = {};
            @foreach($v_Languages as $c_Language)
                v_NamesJson.{{$c_Language}} = $('#nome_{{$c_Language}}').val();
                v_DescriptionsJson.{{$c_Language}} = $('#descricao_{{$c_Language}}').val();
                v_ShortDescriptionsJson.{{$c_Language}} = $('#descricao_curta_{{$c_Language}}').val();
                v_TestimonialsJson.{{$c_Language}} = $('#depoimento_{{$c_Language}}').val();
                v_TestimonialAuthorsJson.{{$c_Language}} = $('#depoimento_autor_{{$c_Language}}').val();

                for(v_Index = 0; v_Index < v_NumeroDias; v_Index++)
                    v_DiasJson[v_Index].{{$c_Language}} = $('.descricao_dia_{{$c_Language}}_' + v_Index).val();
            @endforeach

            $('#form_nome_pt').val(v_NamesJson.pt);
            $('#form_nomes').val(JSON.stringify(v_NamesJson));
            $('#form_descricoes').val(JSON.stringify(v_DescriptionsJson));
            $('#form_descricoes_curtas').val(JSON.stringify(v_ShortDescriptionsJson));
            $('#form_depoimentos').val(JSON.stringify(v_TestimonialsJson));
            $('#form_depoimentos_autores').val(JSON.stringify(v_TestimonialAuthorsJson));

            for(v_Index = 0; v_Index < v_NumeroDias; v_Index++)
                $('.day-description-hidden:eq('+v_Index+')').val(JSON.stringify(v_DiasJson[v_Index]));

            return true;
        }
    </script>
@stop