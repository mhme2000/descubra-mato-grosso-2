@extends('admin.util.listDT', ['p_HasDateFilter' => true])
@section('list-css')
@stop
@section('panel-header')
    Blog
    <a href="{{ url('admin/blog/editar') }}">
        <button class="btn btn-success pull-right" title="Novo artigo">
            <i class="fa fa-plus"></i>
        </button>
    </a>
@stop
@section('list-table-head')
    <tr>
        <th>Data</th>
        <th>Atualizado em</th>
        <th>Título</th>
        <th>Status</th>
        <th>Ações</th>
    </tr>
    <tr>
        <td><input class="form-control dateInput" type="text" placeholder="Buscar"></td>
        <td><input class="form-control dateInput" type="text" placeholder="Buscar"></td>
        <td><input type="text" placeholder="Buscar" class="form-control" style="font-weight:normal"/></td>
        <td>{!! Form::select('', ['' => 'Selecione', 1 => 'Ativo', 0 => 'Desativado'], null, ['class' => 'form-control']) !!}</td>
        <td></td>
    </tr>
@stop
@section('list-table-dt-url')
    url: "{{ url('/admin/dt/blog')}}"
@stop
@section('list-table-initial-sorting')
    "aaSorting": [[ 0, "desc" ]],
@stop
@section('list-table-sortable-columns')
    {"bSortable": true},
    {"bSortable": true},
    {"bSortable": true},
    {"bSortable": true},
    {"bSortable": false}
@stop
@section('list-js')
@stop