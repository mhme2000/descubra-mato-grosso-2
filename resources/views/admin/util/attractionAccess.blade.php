<div class="form-group col-sm-12">
    <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Meios de acesso</h3>
</div>
<input type="hidden" id="acesso" name="formulario[acesso]" value="{{$p_Form == null ? '' : $p_Form->acesso}}">
<div class="acesso-fields">
    <?php
        $v_CommonStates = [
                '' => '',
                'Não se aplica' => 'Não se aplica',
                'Ruim' => 'Ruim',
                'Regular' => 'Regular',
                'Bom' => 'Bom'
        ];
        $v_SinalizationStates = [
                '' => '',
                'Não sinalizado' => 'Não sinalizado',
                'Mal sinalizado' => 'Mal sinalizado',
                'Bem sinalizado' => 'Bem sinalizado'
        ];
        $v_AccessFields = [
                'A pé',
                'Ciclovia',
                'Via terrestre',
                'Ferrovia',
                'Hidrovia fluvial/lacustre',
                'Aéreo'
        ];
    ?>
    @foreach($v_AccessFields as $c_Field)
    <div class="form-group col-sm-6">
        <label>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}{{$c_Field}}
            @if(isset($p_NaturalAttraction))
                <span class="mandatory-field">*</span>
            @endif
        </label>
        {!! Form::select('', $v_CommonStates, null, ['rel' => $c_Field, 'class' => 'form-control field', 'style' => 'width: 100%', isset($p_NaturalAttraction) ? 'required' : '']) !!}
    </div>
    @endforeach
    <div class="form-group col-sm-6">
        <label>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Sinalização de acesso<span class="mandatory-field">*</span></label>
        {!! Form::select('', $v_SinalizationStates, null, ['rel' => 'Sinalização de acesso', 'class' => 'form-control field', 'required' => 'required', 'style' => 'width: 100%']) !!}
    </div>
    @if(!isset($p_NaturalAttraction))
    <div class="form-group col-sm-12">
        <label>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Outros</label>
        <textarea rel="Outros" class="form-control field" rows="4" placeholder="Digite Aqui"></textarea>
    </div>
    @endif
</div>

<div class="form-group col-sm-12">
    <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Via terrestre</h3>
</div>
<input type="hidden" id="via_terrestre" name="formulario[via_terrestre]" value="{{$p_Form == null ? '' : $p_Form->via_terrestre}}">
<div class="via-terrestre-fields">
    <?php
        $v_PavementStates = [
            '' => '',
            'Não se aplica' => 'Não se aplica',
            'Não pavimentada' => 'Não pavimentada',
            'Parcialmente pavimentada' => 'Parcialmente pavimentada',
            'Totalmente pavimentada' => 'Totalmente pavimentada'
        ];
    ?>
    <div class="form-group col-sm-6">
        <label>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Rural<span class="mandatory-field">*</span></label>
        {!! Form::select('', $v_PavementStates, null, array('rel' => 'Rural', 'class' => 'form-control field', 'required' => 'required', 'style' => 'width: 100%')) !!}
    </div>
    <div class="form-group col-sm-6">
        <?php
        $v_PavementTypes = [
                '' => '',
                'Asfáltica' => 'Asfáltica',
                'Concreto' => 'Concreto',
                'Paralelepípedo' => 'Paralelepípedo',
                'Saibro' => 'Saibro',
                'Asfalto ecológico' => 'Asfalto ecológico',
                'Chão batido' => 'Chão batido'
        ];
        ?>
        <label>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Pavimentação rural</label>
        {!! Form::select('', $v_PavementTypes, null, array('rel' => 'Pavimentação rural', 'class' => 'form-control field', 'style' => 'width: 100%')) !!}
    </div>
    <div class="form-group col-sm-6">
        <label>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Urbana<span class="mandatory-field">*</span></label>
        {!! Form::select('', $v_PavementStates, null, array('rel' => 'Urbana', 'class' => 'form-control field', 'required' => 'required', 'style' => 'width: 100%')) !!}
    </div>
    <div class="form-group col-sm-6">
        <label>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Pavimentação urbana</label>
        {!! Form::select('', $v_PavementTypes, null, array('rel' => 'Pavimentação urbana', 'class' => 'form-control field', 'style' => 'width: 100%')) !!}
    </div>
</div>

<div class="form-group col-sm-12">
    <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Transportes para o atrativo</h3>
</div>
<input type="hidden" id="transporte" name="formulario[transporte]" value="{{$p_Form == null ? '' : $p_Form->transporte}}">
<div class="transporte-fields">
    <div class="form-group col-sm-12 mandatory">
        <?php
            $v_QualityOptions = [
                    'Fretado' => 'Fretado',
                    'Particular' => 'Particular',
                    'Público' => 'Público'
            ];
        ?>
        <label>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Tipo de transporte <i>(permite mais de uma opção)</i></label>
        {!! Form::select('', $v_QualityOptions, null, ['rel' => 'Tipo de transporte', 'class' => 'form-control field select2', 'style' => 'width: 100%', 'multiple' => 'multiple']) !!}
    </div>
    <div class="form-group col-sm-12">
        <label>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Observação</label>
        <textarea rel="Observação" class="form-control field" rows="4" placeholder="Digite Aqui"></textarea>
    </div>
</div>