<script>
    $(document).ready(function()
    {
        $('#visitantes_epoca_maior_fluxo, #visitantes_epoca_menor_fluxo').change(function(){
            var v_Field = this;
            var v_Selection = $(v_Field).val();
            if($.inArray('Todos', v_Selection) > -1)
            {
                if($(v_Field).find('option:selected').length == 13)
                    $(v_Field).select2("val", "");
                else
                    $(v_Field).select2("val", ['1','2','3','4','5','6','7','8','9','10','11','12']);
            }
        });
    });
</script>