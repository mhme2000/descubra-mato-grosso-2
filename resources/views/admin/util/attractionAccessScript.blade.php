<script>
    $(document).ready(function()
    {
        carregaCamposAcesso('acesso', 'acesso-fields');
        carregaCamposAcesso('via_terrestre', 'via-terrestre-fields');
        carregaCamposAcesso('transporte', 'transporte-fields');
    });

    function carregaCamposAcesso(p_Id, p_DivClass)
    {
        var v_Dados = $('#'+p_Id).val();
        if(v_Dados.length > 0)
        {
            v_Dados = JSON.parse(v_Dados);
            $(v_Dados).each(function(){
                if(this.tipo == 'checkbox')
                    $('.'+p_DivClass+' .field[rel="'+this.nome+'"]').prop('checked',this.valor);
                else if(this.tipo == 'select2')
                    $('.'+p_DivClass+' .field[rel="'+this.nome+'"]').select2("val", this.valor);
                else
                    $('.'+p_DivClass+' .field[rel="'+this.nome+'"]').val(this.valor);
            });
        }
    }

    function processaDadosAcesso()
    {
        processaCamposAcesso('acesso', 'acesso-fields');
        processaCamposAcesso('via_terrestre', 'via-terrestre-fields');
        processaCamposAcesso('transporte', 'transporte-fields');
    }

    function processaCamposAcesso(p_Id, p_DivClass)
    {
        var v_Dados = [];
        $('.'+p_DivClass+' .field').each(function(){
            v_Dados.push({
                nome:$(this).attr('rel'),
                tipo:$(this).hasClass('checkbox') ? 'checkbox' : ($(this).hasClass('select2') ? 'select2' : 'text-or-select'),
                valor:$(this).hasClass('checkbox') ? $(this).is(':checked') : $(this).val()
            });
        });
        $('#'+p_Id).val(JSON.stringify(v_Dados));
    }
</script>