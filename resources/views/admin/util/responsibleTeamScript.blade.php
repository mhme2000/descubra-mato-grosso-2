<script>
    var v_ResponsibleTeamsData;
    $(document).ready(function(){
        $('.email-field').keypress(function (e) {
            var regex = new RegExp(/^[a-zA-Z\d\_\-\.\@\;]+$/);
            var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
            if (regex.test(str))
                return true;

            e.preventDefault();
            return false;
        });

        @if(\App\UserType::isAdmin() || \App\UserType::isMaster() || \App\UserType::isCircuito() || \App\UserType::isMunicipio())
        var $v_ResponsibleTeams = $('#responsible_team_data');
        if($v_ResponsibleTeams.length){
            v_ResponsibleTeamsData = JSON.parse($('#responsible_team_data').val());
            var $v_CityField = $('#city_id');
            $v_CityField.change(function(){
                updateTeamsByCity(v_ResponsibleTeamsData, $v_CityField.val());
            });
            updateTeamsByCity(v_ResponsibleTeamsData, $v_CityField.val());
            $('#responsible_team_select').change(function(){
                var v_Id = this.value;
                if(v_Id != ''){
                    var v_Team = findTeamById(v_ResponsibleTeamsData, v_Id);
                    $('#equipe_responsavel').val(v_Team.responsavel);
                    $('#equipe_responsavel_instituicao').val(v_Team.instituicao);
                    $('#equipe_responsavel_telefone').val(v_Team.telefone);
                    $('#equipe_responsavel_email').val(v_Team.email);
                }
            });
        }
        @endif
    });

    function findTeamById(p_Array, p_Id){
        return $.grep(p_Array, function(e){ return e.id == p_Id; })[0];
    }

    function updateTeamsByCity(p_Array, p_CityId){
        var v_NewOptions = '<option value=""></option>';
        var $v_ResponsibleTeamsSelect = $('#responsible_team_select');
        var v_LastVal = $v_ResponsibleTeamsSelect.val();
        $(p_Array).each(function(){
            if(this.city_id == p_CityId){
                v_NewOptions += '<option value="' + this.id + '">Responsável: ' + this.responsavel + ' - Instituição: ' + this.instituicao + ' - Município: ' + this.cidade + '</option>';
            }
        });
        $v_ResponsibleTeamsSelect.html(v_NewOptions).select2("val", v_LastVal);
    }
</script>