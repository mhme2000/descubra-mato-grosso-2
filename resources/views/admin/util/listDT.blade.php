@extends('admin.main')
@section('pageCSS')
    <link rel="stylesheet" type="text/css" href="{{url('/vendor/plugins/datatables/media/css/dataTables.bootstrap.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('/vendor/plugins/datatables/media/css/dataTablesTemplate.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('/vendor/plugins/datatables/media/css/dataTables.responsive.css')}}">
    @if($p_HasDateFilter)
    <link rel="stylesheet" type="text/css" href="{{url('/vendor/plugins/daterange/daterangepicker.css')}}">
    @endif
    <style type="text/css">
        div ul li.paginate_button.active a
        {
            background-color: #C30F1B;
            border-color: #C30F1B;
            color: #FFF;
        }
        div ul li.paginate_button.active a:hover
        {
            background-color: #C30F1B;
            border-color: #C30F1B;
            color: #FFF;
        }
        div ul li.paginate_button a, div ul li.paginate_button a:hover
        {
            color: #000;
        }
        td
        {
            text-align: center;
        }
        #responsive-table {
            width: 99.9999% !important;
        }
        thead tr th.select-status {
            width: 100px !important;
        }
        .dtr-data
        {
            display: inline-block;
            word-wrap: break-word;
            word-break: break-all;
        }
        table.dataTable tr.child ul li
        {
            white-space: normal;
        }
        table div.actions-div
        {
            width: 130px;
        }
        table div.actions-div a
        {
            width: 39px;
            margin-right: 3px;
            margin-top: 1px;
            margin-bottom: 1px;
        }
        .width-100-percent {
            width: 100% !important;
        }
        @media (min-width: 500px)
        {
            td
            {
                text-align: center;
            }
        }
        @media (max-width: 320px)
        {
            table div.actions-div
            {
                text-align: center;
            }
            table div.actions-div a
            {
                margin-bottom: 3px;
            }
        }


        .search-cnpj-cadastur{
            vertical-align: top;
            float: right;
        }
        .cnpj-field-with-cadastur {
            width: calc(100% - 45px);
            display:inline-block;
        }
        .new-trade {
            border:1px solid #ebebeb;
        }
        .modal.in .modal-dialog{
            z-index:1090;
        }
    </style>
    @yield('list-css')
@stop
@section('content')
    <div class="row">
        <div class="col-md-12 mt15">
            <div class="panel panel-visible">
                @yield('list-above-table-content')
                <div class="panel-body pn">
                    <div class="adv-table">
                        <table class="table table-striped table-bordered table-hover" id="responsive-table" cellspacing="0" width="100%">
                            <thead>
                                @yield('list-table-head')
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
@section('pageScript')
    <script type="text/javascript" src="{{url('/vendor/plugins/datatables/media/js/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript" src="{{url('/vendor/plugins/datatables/media/js/dataTables.bootstrap.js')}}"></script>
    <script type="text/javascript" src="{{url('/vendor/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js')}}"></script>
    <script type="text/javascript" src="{{url('/vendor/plugins/datatables/media/js/dataTables.responsive.js')}}"></script>
    <script type="text/javascript" src="{{url('/assets/js/jquery.popconfirm.js')}}"></script>
    @if($p_HasDateFilter)
    <script type="text/javascript" src="{{url('/vendor/plugins/daterange/moment.min.js')}}"></script>
    <script type="text/javascript" src="{{url('/vendor/plugins/datatables/extensions/TableTools/js/datetime-moment.js')}}"></script>
    <script type="text/javascript" src="{{url('/vendor/plugins/daterange/daterangepicker.js')}}"></script>
    @endif
    <script type="text/javascript">
    $(document).ready(function(){
        var v_Timer = null;
        /* DataTables */
        var v_Table = $('#responsive-table').DataTable({
            serverSide: true,
            ajax:
            {
                @yield('list-table-dt-url')
            },
            responsive: true,
            "aLengthMenu": [[25, 50, 100, 250, 500, -1], [25, 50, 100, 250, 500, "Todos"]],
            "sDom": "<'dt-panelmenu clearfix'lTr>t<'dt-panelfooter clearfix'ip>",
            tableTools: {
                "aButtons": []
            },
            @yield('list-table-initial-sorting')
            "orderCellsTop": true,
            "sScrollX": "100%",
            "bScrollCollapse": true,
            "bAutoWidth": true,
            "oLanguage":
            {
                "sEmptyTable": "Nenhum registro encontrado",
                "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
                "sInfoFiltered": "(Filtrados de _MAX_ registros)",
                "sInfoPostFix": "",
                "sInfoThousands": ".",
                "sLengthMenu": "_MENU_ resultados por página",
                "sLoadingRecords": "Carregando...",
                "sProcessing": "Processando...",
                "sZeroRecords": "Nenhum registro encontrado",
                "sSearch": "Pesquisar",
                "oPaginate":
                {
                    "sNext": "",
                    "sPrevious": "",
                    "sFirst": "Primeiro",
                    "sLast": "Último"
                },
                "oAria":
                {
                    "sSortAscending": ": Ordenar colunas de forma ascendente",
                    "sSortDescending": ": Ordenar colunas de forma descendente"
                }
            },
            "aoColumns": [
                @yield('list-table-sortable-columns')
            ]
        });

        v_Table.columns().eq( 0 ).each( function ( colIdx )
        {
            $('.table.dataTable thead td:eq('+colIdx+') input').on('keyup', function(e) {
                var code = e.keyCode || e.which;
                if (code != 9 && code != 16 && code != 17 && code != 18 && code != 20 && code != 27 && code != 35 && code != 36 && code != 37 && code != 38 && code != 39 && code != 40)
                {
                    var v_Value = this.value;
                    var v_DateNumbers = v_Value.replace(/\D/g,'');
                    if($(this).hasClass('dateInput') && v_DateNumbers.length != 16)
                        v_Value = '';
                    clearInterval(v_Timer);  //clear any interval on key up
                    v_Timer = setTimeout(function() { //then give it a second to see if the user is finished
                        v_Table.column( colIdx ).search( v_Value ).draw();
                    }, 250);
                }
            }).on('keydown', function(e) {
                var code = e.keyCode || e.which;
                if (code == '9')
                {
                    var v_Value = this.value;
                    var v_DateNumbers = v_Value.replace(/\D/g,'');
                    if($(this).hasClass('dateInput') && v_DateNumbers.length != 16)
                        v_Value = '';
                    clearInterval(v_Timer);  //clear any interval on key up
                    v_Timer = setTimeout(function() { //then give it a second to see if the user is finished
                        v_Table.column( colIdx ).search(v_Value).draw();
                    }, 1000);
                }
            });
            $('.table.dataTable thead td:eq('+colIdx+') select').on('change', function() {
                v_Table.column( colIdx ).search( this.value ).draw();
            });

            @if($p_HasDateFilter)
            $('.table.dataTable thead td:eq('+colIdx+') .dateInput').on('blur', function() {
                var v_Value = this.value;
                var v_DateNumbers = v_Value.replace(/\D/g,'');
                if(v_DateNumbers.length != 16)
                    v_Value = '';
                v_Table.column( colIdx ).search(v_Value).draw();
            });
            @endif
        });

        @if($p_HasDateFilter)
        // daterange plugin options
        $('.dateInput').daterangepicker({
            "autoApply": true,
            "locale": {
                "format": "DD/MM/YYYY",
                "separator": " - ",
                "applyLabel": "Selecionar",
                "cancelLabel": "Cancelar",
                "fromLabel": "De",
                "toLabel": "Até",
                "daysOfWeek": [
                    "D",
                    "S",
                    "T",
                    "Q",
                    "Q",
                    "S",
                    "S"
                ],
                "customRangeLabel": "Intervalo",
                "monthNames": [
                    "Janeiro",
                    "Fevereiro",
                    "Março",
                    "Abril",
                    "Maio",
                    "Junho",
                    "Julho",
                    "Agosto",
                    "Setembro",
                    "Outubro",
                    "Novembro",
                    "Dezembro"
                ],
                "firstDay": 1
            },
            ranges: {
                'Hoje': [moment(), moment()],
                'Ontem': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Últimos 7 dias': [moment().subtract(6, 'days'), moment()],
                'Últimos 30 dias': [moment().subtract(29, 'days'), moment()],
                'Este Mês': [moment().startOf('month'), moment().endOf('month')],
                'Mês Passado': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            "linkedCalendars": false,
            "opens": "center",
            "autoUpdateInput": false
        }).on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY')).blur();
        }).mask('99/99/9999 - 99/99/9999');
        @endif

        $('#responsive-table').on('draw.dt', function (){
            $('.delete-btn').removeClass('delete-btn').popConfirm({
                title: "Confirmar exclusão",
                content: "Tem certeza de que deseja excluir esta entrada? Essa ação é irreversível",
                placement: "left",
                yesBtn: "Sim",
                noBtn: "Não"
            });
        });

        $('.panel-body.pn select[name="responsive_table_length"]').addClass('form-control input-sm');
        $('.dataTables_scrollHeadInner, .table').addClass('width-100-percent');

        loadFilters();
        var e = jQuery.Event('keyup');
        e.which = 1; // # Some key code value
        $('.table.dataTable thead td').find('input, select').each(function(){
            if($(this).val() != '')
                $(this).trigger(e).change();
        });
    });
    $(window).unload(function(){
        storeFilters();
    });

    function loadFilters(){
        var v_FilterData = JSON.parse(localStorage.getItem('adminFilters'));
        if(v_FilterData != null){
            var v_PreviousUrl = '{{URL::previous()}}';
            var v_CurrentUrl = '{{Request::url()}}';
            if(v_PreviousUrl != v_CurrentUrl && v_PreviousUrl.indexOf(v_CurrentUrl) == 0 && v_FilterData.url == v_CurrentUrl && v_FilterData.user == '{{Auth::id()}}'){
                $('.table.dataTable thead td').find('input, select').each(function(c_Index){
                    $(this).val(v_FilterData.filters[c_Index]);
                });
            }
            else {
                localStorage.removeItem('adminFilters');
            }
        }
    }

    function storeFilters() {
        var v_Filters = [];
        $('.table.dataTable thead td').find('input, select').each(function(){
            v_Filters.push($(this).val());
        });
        var v_FilterData = {url: '{{Request::url()}}',user:'{{Auth::id()}}',filters:v_Filters};

        localStorage.setItem('adminFilters', JSON.stringify(v_FilterData));
    }

    {{--function authorizeEvent(p_CNPJ, p_Index, p_Btn)--}}
    {{--{--}}
        {{--var v_Btn = p_Btn;--}}
        {{--var v_Index = p_Index;--}}
        {{--$.get("{{url('/admin/bands/authorize')}}/" + p_CNPJ, function(){--}}
        {{--}).done(function(data){--}}
            {{--if (data.error == 'ok')--}}
            {{--{--}}
                {{--var v_Table = $('#responsive-table').dataTable()--}}
                {{--v_Table.fnUpdate((data.status ? 'Autorizada' : 'Pendente'), v_Index, 1);--}}
            {{--}--}}
            {{--else--}}
                {{--$('section.panel.panel-visible header').after('<div class="alert alert-danger alert-block alert-dismissable fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><p>' + data.error + '</p></div>');--}}
        {{--}).error(function(){--}}
        {{--});--}}
    {{--}--}}
    </script>

    @yield('list-js')
@stop