@extends('admin.main')
@section('pageCSS')
    <link rel="stylesheet" type="text/css" href="{{url('/vendor/plugins/datatables/media/css/dataTables.bootstrap.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('/vendor/plugins/datatables/media/css/dataTablesTemplate.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('/vendor/plugins/datatables/media/css/dataTables.responsive.css')}}">
    <style type="text/css">
        div ul li.paginate_button.active a
        {
            background-color: #C30F1B;
            border-color: #C30F1B;
            color: #FFF;
        }
        div ul li.paginate_button.active a:hover
        {
            background-color: #C30F1B;
            border-color: #C30F1B;
            color: #FFF;
        }
        div ul li.paginate_button a, div ul li.paginate_button a:hover
        {
            color: #000;
        }
        td
        {
            text-align: center;
        }
        #responsive-table {
            width: 99.9999% !important;
        }
        thead tr th.select-status {
            width: 100px !important;
        }
        .dtr-data
        {
            display: inline-block;
            word-wrap: break-word;
            word-break: break-all;
        }
        table.dataTable tr.child ul li
        {
            white-space: normal;
        }
        table div.actions-div a
        {
            width: 39px;
            margin-right: 3px;
            margin-top: 1px;
            margin-bottom: 1px;
        }
        @media (min-width: 500px)
        {
            td
            {
                text-align: center;
            }
        }
        @media (max-width: 320px)
        {
            table div.actions-div
            {
                text-align: center;
            }
            table div.actions-div a
            {
                margin-bottom: 3px;
            }
        }
    </style>
    @yield('list-css')
@stop
@section('content')
    <div class="row">
        <div class="col-md-12 mt15">
            <div class="panel panel-visible">
                <div class="panel-body pn">
                    <div class="adv-table">
                        <table class="table table-striped table-bordered table-hover" id="responsive-table" cellspacing="0" width="100%">
                            <thead>
                                @yield('list-table-head')
                            </thead>
                            <tbody>
                                @yield('list-table-rows')
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('pageScript')
    <script type="text/javascript" src="{{url('/vendor/plugins/datatables/media/js/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript" src="{{url('/vendor/plugins/datatables/media/js/dataTables.bootstrap.js')}}"></script>
    <script type="text/javascript" src="{{url('/vendor/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js')}}"></script>
    <script type="text/javascript" src="{{url('/vendor/plugins/datatables/media/js/dataTables.responsive.js')}}"></script>
    <script type="text/javascript" src="{{url('/assets/js/jquery.popconfirm.js')}}"></script>
    @if($p_HasDateFilter || isset($p_HasDateTimeFilter))
    <script type="text/javascript" src="{{url('/vendor/plugins/datatables/extensions/TableTools/js/moment.min.js')}}"></script>
    <script type="text/javascript" src="{{url('/vendor/plugins/datatables/extensions/TableTools/js/datetime-moment.js')}}"></script>
    @endif
    <script type="text/javascript">
    $(document).ready(function()
    {
        @if($p_HasDateFilter)
        $.fn.dataTable.moment('DD/MM/YYYY');
        @elseif(isset($p_HasDateTimeFilter))
        $.fn.dataTable.moment('DD/MM/YY - HH:mm');
        @endif
        /* DataTables */
        var v_Table = $('#responsive-table').DataTable({
            responsive: true,
            "fnDrawCallback" : function() {
                setPopConfirm()
            },
            "aLengthMenu": [[25, 50, 100, 250, 500, -1], [25, 50, 100, 250, 500, "Todos"]],
            "sDom": '<"dt-panelmenu clearfix"lfr>t<"dt-panelfooter clearfix"ip>',
            tableTools: {
                "aButtons": []
            },
            @yield('list-table-initial-sorting')
            "bAutoWidth": true,
            "sScrollX": "100%",
            "bScrollCollapse": true,
            "oLanguage":
            {
                "sEmptyTable": "Nenhum registro encontrado",
                "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
                "sInfoFiltered": "(Filtrados de _MAX_ registros)",
                "sInfoPostFix": "",
                "sInfoThousands": ".",
                "sLengthMenu": "_MENU_ resultados por página",
                "sLoadingRecords": "Carregando...",
                "sProcessing": "Processando...",
                "sZeroRecords": "Nenhum registro encontrado",
                "sSearch": "Pesquisar",
                "oPaginate":
                {
                    "sNext": "",
                    "sPrevious": "",
                    "sFirst": "Primeiro",
                    "sLast": "Último"
                },
                "oAria":
                {
                    "sSortAscending": ": Ordenar colunas de forma ascendente",
                    "sSortDescending": ": Ordenar colunas de forma descendente"
                }
            },
            "aoColumns": [
                @yield('list-table-sortable-columns')
            ]
        });

        function setPopConfirm()
        {
            $('.delete-btn').removeClass('delete-btn').popConfirm({
                title: "Confirmar exclusão",
                content: "Tem certeza de que deseja excluir esta entrada? Essa ação é irreversível",
                placement: "left",
                yesBtn: "Sim",
                noBtn: "Não"
            });
        };

        var v_FilterInput = $('.adv-table input[aria-controls="responsive-table"]').appendTo('.adv-table #responsive-table_filter');
        v_FilterInput.attr('placeholder', 'Buscar');
        $('.adv-table #responsive-table_filter label').text('');
        v_FilterInput.appendTo('.adv-table #responsive-table_filter label');
    });

    {{--function authorizeEvent(p_Id, p_Index, p_Btn)--}}
    {{--{--}}
        {{--var v_Btn = p_Btn;--}}
        {{--var v_Index = p_Index;--}}
        {{--$.get("{{url('/admin/workshops/authorize')}}/" + p_Id, function(){--}}
        {{--}).done(function(data){--}}
            {{--if (data.error == 'ok')--}}
            {{--{--}}
                {{--$(v_Btn).attr('title', data.status ? 'Desativar' : 'Autorizar');--}}
                {{--$(v_Btn).html('<i class="fa fa-' + (data.status ? 'times' : 'check') + '"></i>');--}}
                {{--var v_Table = $('#responsive-table').dataTable()--}}
                {{--v_Table.fnUpdate((data.status ? 'Autorizada' : 'Pendente'), v_Index, 3);--}}
            {{--}--}}
            {{--else--}}
                {{--$('section.panel.panel-visible header').after('<div class="alert alert-danger alert-block alert-dismissable fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><p>' + data.error + '</p></div>');--}}
        {{--}).error(function(){--}}
        {{--});--}}
    {{--}--}}
    </script>

    @yield('list-js')
@stop