<script>
    @if(\App\UserType::isTrade())
    var v_CadasturToken = '';

    /*Remover este trecho*/
    //var v_CadasturUser = "{{\App\Parameter::getParameterByKey('cadastur_login')}}";
    //var v_CadasturPwd = "{{\App\Parameter::getParameterByKey('cadastur_senha')}}";
    /*----*/
    
    var v_CadasturUrl = "{{\App\Parameter::getParameterByKey('cadastur_url')}}";
    var v_CadasturPwd = "{{\App\Parameter::getParameterByKey('cadastur_senha')}}";
    var v_url = '';

    var v_DisponibilidadeCNPJ;
    var v_CNPJ;
    var v_CadasturTipoAtividade;
    var v_CadasturValidade = null;
    
        var v_CadasturTrAtiv = new Array();


    /*
     1-Acampamento Turístico
     2-Agência de Turismo
     3-Meio de Hospedagem
     4-Organizadora de Eventos
     5-Parque Temático
     6-Transportadora Turística
     7-Empreendimento de Apoio ao Turismo Náutico ou à Pesca Desportiva
     8-Casa de Espetáculos e Equipamento de Animação Turística
     9-Centro de Convenções
     10-Prestador Especializado em Segmentos Turísticos
     11-Prestador de Infraestrutura de Apoio para Eventos
     12-Locadora de Veículos para Turistas
     13-Parque Aquático e Empreendimento de Lazer
     14-Restaurante, Cafeteria, Bar e Similares
     */

    //Array para converter tipos de atividades cadastradas no BD para o cadastur 
    v_CadasturTrAtiv[10] = 2;
    v_CadasturTrAtiv[15] = 14;
    v_CadasturTrAtiv[20] = 3;
    v_CadasturTrAtiv[25] = 9;
    v_CadasturTrAtiv[30] = 6;
    v_CadasturTrAtiv[35] = 1;
    v_CadasturTrAtiv[40] = 11;
    v_CadasturTrAtiv[45] = 7;
    v_CadasturTrAtiv[55] = 8;
    v_CadasturTrAtiv[60] = 5;
    v_CadasturTrAtiv[65] = 13;
    v_CadasturTrAtiv[70] = 13;
    v_CadasturTrAtiv[75] = 10;
    v_CadasturTrAtiv[80] = 4;
    v_CadasturTrAtiv[85] = 12;
    
    
    
    $(document).ready(function(){
        $('.cnpj-field').mask('99.999.999/9999-99');
        $('.cnpj-field').keypress(function(e)
        {
            code = (e.keyCode ? e.keyCode : e.which);
            if (code == 13)
                searchData();
        });
        localStorage.removeItem("newTradeItem");

        getCadasturToken(function(){}, null);
    });

    function getCadasturToken(p_SuccessFunc, p_ErrorMessage) {
        /*-- Remover este trecho --*/
        /*
        if(v_CadasturToken.length == 0 && v_CadasturUser.length && v_CadasturPwd.length) {
            $.ajax({
                url: 'http://www.cadastur.turismo.gov.br/cadastur/rest/autenticacao/obterToken?login='+v_CadasturUser+'&'+v_CadasturPwd+'&nuPerfil=710',
                type: 'GET',
                success: function (data) {
                    v_CadasturToken = data.token;
                    p_SuccessFunc();
                },
                error: function(){
                    if(p_ErrorMessage != null) {
                        @if($p_RequiresCadastur)
                        showErrorDialog(p_ErrorMessage);
                        $('.search-cnpj-cadastur i').show();
                        $('.search-cnpj-cadastur img').hide();
                        @else
                        p_SuccessFunc();
                        @endif
                    }
                }
            });
        }
        else if(v_CadasturToken.length) {
        */
        /*------*/
            p_SuccessFunc();
             /*-- Remover este trecho --*/
             /*
        }
        */
        /*------*/
    }

    function searchData()
    {
        v_CNPJ = $('#search-cnpj').val().replace(/[^\d]+/g,'');
        v_CadasturTipoAtividade = $('#tipo_atividade_cadastur').val();
        v_DisponibilidadeCNPJ = -1;
        if(validateCNPJ(v_CNPJ) && v_CadasturTipoAtividade.length) {
            $('.search-cnpj-cadastur i').hide();
            $('.search-cnpj-cadastur img').show();

            $.get("{{url('/admin/consulta-cnpj?tipo=' . $p_Type)}}&cnpj=" + v_CNPJ, function(){
            }).done(function(data){
                v_DisponibilidadeCNPJ = data;

                @if($p_RequiresCadastur)
                getCadasturToken(loadCadasturData, 'Não foi possível realizar essa operação. Tente novamente mais tarde.');
                @else
                getCadasturToken(finishedSearchSuccess, 'Não foi possível realizar essa operação. Tente novamente mais tarde.');
                @endif
            }).error(function(){
                $('.search-cnpj-cadastur i').show();
                $('.search-cnpj-cadastur img').hide();
            });
        }
        else if(v_CadasturTipoAtividade.length == 0) {
            alert("Selecione o tipo de atividade!");
        }
        else if(!validateCNPJ(v_CNPJ)) {
            alert("CNPJ inválido!");
        }
    }

    function loadCadasturData()
    {
        v_url = v_CadasturUrl.replace(/\s/g, '') + v_CadasturPwd.replace(/\s/g, '') + '/cnpj/' + v_CNPJ + '/tipoAtividade/' + v_CadasturTrAtiv[v_CadasturTipoAtividade];

        
        $.ajax({
            /*--Remove este trecho--*/
            //url: 'http://www.cadastur.turismo.gov.br/cadastur/rest/integracaoRegional/validadeCadastro?coCnpj='+v_CNPJ+'&nuTipoAtividade='+v_CadasturTipoAtividade,
            /*---*/
            url: v_url,
            type: 'GET',
            /*--Remove este trecho*/
            //headers: {
            //    'Authorization':v_CadasturToken
            //},
            /*--*/
            success: function (data) {
                    if(!data.cod) {
                        /*--Remove este trecho*/
                        //v_CadasturValidade = data.dto.dtValidade;
                        /*---*/
                        v_CadasturValidade = data.validadeCertificado;
                        finishedSearchSuccess();
                    }
                    else {
                        v_CadasturValidade = null;
                        showErrorDialog('Não é possível prosseguir com o cadastro no Portal sem um registro ATIVO no CADASTUR \n\nSaiba mais: www.cadastur.turismo.mg.gov.br');
                    }
                    $('.search-cnpj-cadastur i').show();
                    $('.search-cnpj-cadastur img').hide();
                },
            error: function(){
                showErrorDialog('Não foi possível realizar essa operação. Tente novamente mais tarde.');
                $('.search-cnpj-cadastur i').show();
                $('.search-cnpj-cadastur img').hide();
//                $('.search-cnpj-cadastur').css("background","");
            }
        });
    }

    function finishedSearchSuccess() {
        if(v_DisponibilidadeCNPJ == -1) { //disponivel
            localStorage.setItem("newTradeItem",JSON.stringify({cnpj:v_CNPJ, tipoAtividade:v_CadasturTipoAtividade, token:v_CadasturToken, validade:v_CadasturValidade}));
            location.href = '{{ $p_EditUrl }}';
        }
        else if(v_DisponibilidadeCNPJ == -2) { //ja solicitado
            showErrorDialog('CNPJ já cadastrado! Favor aguardar aprovação da SECRETARIA ADJUNTA DE TURISMO para liberação da posse do mesmo.');
        }
        else if(v_DisponibilidadeCNPJ == 0) {
            showConfirmationDialog('CNPJ já cadastrado no portal, deseja solicitar a posse do mesmo?', function(){
                $('#cnpj').val(v_CNPJ);
                $('#mainForm').submit();
            });
        }
        else { //indisponivel? (ja tem usuario)
            showErrorDialog('Já existe um usuário responsável por este CNPJ no Portal.');
        }
        $('.search-cnpj-cadastur i').show();
        $('.search-cnpj-cadastur img').hide();
    }

    function showConfirmationDialog(p_Message, p_ConfirmFunction) {
        $.confirm({
            text: p_Message,
            title: 'Atenção',
            confirm: function() {
                p_ConfirmFunction();
            },
            confirmButton: 'Sim',
            cancelButton: 'Não'
        });
    }

    function showErrorDialog(p_Message) {
        $.confirm({
            text: p_Message,
            title: 'Atenção',
            confirmButton: 'OK',
            cancelButton: 'Não',
            cancelButtonClass:'hidden'
        });
    }

    function validateCNPJ(p_Value)
    {
        var p_Cnpj = p_Value.replace(/[^\d]+/g,'');
        if(p_Cnpj == '')
            return true; //not mandatory

        if (p_Cnpj.length != 14)
            return false;

        if (p_Cnpj == "00000000000000" || p_Cnpj == "11111111111111" || p_Cnpj == "22222222222222" || p_Cnpj == "33333333333333" || p_Cnpj == "44444444444444" ||
                p_Cnpj == "55555555555555" || p_Cnpj == "66666666666666" || p_Cnpj == "77777777777777" || p_Cnpj == "88888888888888" || p_Cnpj == "99999999999999")
            return false;

        var v_Size = p_Cnpj.length - 2;
        var v_Numbers = p_Cnpj.substring(0,v_Size);
        var v_Digits = p_Cnpj.substring(v_Size);
        var v_Sum = 0;
        var v_Pos = v_Size - 7;
        for (i = v_Size; i >= 1; i--)
        {
            v_Sum += v_Numbers.charAt(v_Size - i) * v_Pos--;
            if (v_Pos < 2)
                v_Pos = 9;
        }
        var v_Result = v_Sum % 11 < 2 ? 0 : 11 - v_Sum % 11;
        if (v_Result != v_Digits.charAt(0))
            return false;

        v_Size = v_Size + 1;
        v_Numbers = p_Cnpj.substring(0,v_Size);
        v_Sum = 0;
        v_Pos = v_Size - 7;
        for (i = v_Size; i >= 1; i--)
        {
            v_Sum += v_Numbers.charAt(v_Size - i) * v_Pos--;
            if (v_Pos < 2)
                v_Pos = 9;
        }
        v_Result = v_Sum % 11 < 2 ? 0 : 11 - v_Sum % 11;
        return v_Result == v_Digits.charAt(1);
    }
    @endif
</script>