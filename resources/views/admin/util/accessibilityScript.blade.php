<script>
    $(document).ready(function()
    {
        $('.accessibility-fields-container').hide();
        $('#possui_acessibilidade').change(function(){
            if($(this).is(':checked'))
                $('.accessibility-fields-container').show();
            else
                $('.accessibility-fields-container').hide();
        });
        carregaDadosAcessibilidade();
    });

    function carregaDadosAcessibilidade()
    {
        var v_Acessibilidade = $('#acessibilidade').val();
        if(v_Acessibilidade.length > 0)
        {
            v_Acessibilidade = JSON.parse(v_Acessibilidade);
            $('#possui_acessibilidade').prop("checked", v_Acessibilidade.possui).trigger('change');
            $(v_Acessibilidade.campos).each(function(){
                if(this.tipo == 'text')
                    $('.accessibility-fields .form-control[rel="'+this.nome+'"]').val(this.valor);
                else{
                    var $v_Field = $('.accessibility-fields .form-control[rel="'+this.nome+'"]');
                    if($v_Field.length)
                        $v_Field.select2("val", this.valor);
                }
            });
        }
    }

    function processaDadosAcessibilidade()
    {
        var v_Acessibilidade = {possui:$('#possui_acessibilidade').is(':checked'), campos:[]};
        $('.accessibility-fields .form-control').each(function(){
            v_Acessibilidade.campos.push({
                nome:$(this).attr('rel'),
                tipo:$(this).hasClass('select2') ? 'select' : 'text',
                valor:$(this).val()
            });
        });
        $('#acessibilidade').val(JSON.stringify(v_Acessibilidade));
    }
</script>