<input id="dadosTooltips" value="{{\App\Parameter::getParameterByKey('tooltips-' . $p_Type)}}">
<script type="text/javascript" src="{{url('/vendor/plugins/tooltipster/tooltipster.bundle.min.js')}}"></script>
<script>
	var m_Tooltips;
    $(document).ready(function()
    {
		m_Tooltips = $('#dadosTooltips').val();
        if(m_Tooltips.length > 0) {
            m_Tooltips = JSON.parse(m_Tooltips);
            var $v_Containers = $('#mainForm, #physicalSpaceModal, #myModal');
            $v_Containers.find('label, h3, h4').tooltipster({maxWidth: 480});
            $v_Containers.on('mouseenter', 'label, h3, h4', function(){
                var v_SearchString = $(this).text().replace(/\*$/, '').trim();
                var v_Tooltip = findTooltipByFieldName(v_SearchString);
                if(v_Tooltip == undefined){
                    v_SearchString = v_SearchString.split(/\d\. /);
                    if(v_SearchString.length > 1){
                        v_SearchString = v_SearchString[1];
                        v_Tooltip = findTooltipByFieldName($(this).text().replace(/\*$/, '').trim());
                    }
                }
                if(v_Tooltip != undefined && v_Tooltip.text.length > 0) {
                    if(!$(this).hasClass("tooltipstered"))
                        $(this).tooltipster({maxWidth: 480});
                    $(this).tooltipster('content', v_Tooltip.text);
                    $(this).tooltipster('show');
                }
            }).on('mouseleave', 'label, h3, h4', function(){
                if($(this).hasClass("tooltipstered"))
                    $(this).tooltipster('hide');
            });
        }
    });

    function findTooltipByFieldName(p_FieldName) {
        return $.grep(m_Tooltips, function(e){ return e.name.replace(/\*$/, '').trim() == p_FieldName; })[0];
    }
</script>