<div class="form-group col-sm-12">
    <label for="visitantes_epoca_maior_fluxo">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Época do maior fluxo de visitação <i>(permite mais de uma opção)</i></label>
    <?php $v_MonthRange = $p_Form == null ? [] : explode(';', $p_Form->visitantes_epoca_maior_fluxo); ?>
    {!! Form::select('formulario[visitantes_epoca_maior_fluxo][]', \App\Http\Controllers\BaseController::$m_Months, $v_MonthRange, array('id' => 'visitantes_epoca_maior_fluxo', 'class' => 'form-control select2', 'multiple' => 'multiple', 'style' => 'width: 100%')) !!}
</div>
<div class="form-group col-sm-12">
    <label for="visitantes_epoca_menor_fluxo">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Época do menor fluxo de visitação <i>(permite mais de uma opção)</i></label>
    <?php $v_MonthRange = $p_Form == null ? [] : explode(';', $p_Form->visitantes_epoca_menor_fluxo); ?>
    {!! Form::select('formulario[visitantes_epoca_menor_fluxo][]', \App\Http\Controllers\BaseController::$m_Months, $v_MonthRange, array('id' => 'visitantes_epoca_menor_fluxo', 'class' => 'form-control select2', 'multiple' => 'multiple', 'style' => 'width: 100%')) !!}
</div>



