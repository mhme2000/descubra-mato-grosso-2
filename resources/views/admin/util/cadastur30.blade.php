<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script>
    function axiosSearchData () {
        // Recupera o value do input cep
        let cnpj = document.getElementById('cnpj').value.replace(/[^\d]+/g,'')
        console.log(cnpj)
        if (cnpj.length < 14) {
            alert('Por favor insira um CNPJ válido!')
        } 
        // Inicia requisição AJAX com o axios
        axios.get(`http://webservicemt.turismo.gov.br/mt-backend/rest/prestador/CPF_CNPJ/${cnpj}/chaveAcesso/e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855`)
                .then(response => {
                    const dados = response.data
                    localStorage.setItem('CadasturCNPJ', `${cnpj}`)
                    localStorage.setItem('CadasturValidade', `${dados.situacao}`)
                    console.log(dados)
                    alert('CNPJ encontrado!')
                })
                .catch(error => {
                    alert("Por favor insira um cnpj válido!")
                })
        
    }

</script>
<script>
function returnaSituacao() {
    return localStorage.getItem('CadasturValidade')
}
</script>

<!-- <script>
    var v_CadasturUrl = "http://webservicemt.turismo.gov.br/mt-backend/rest/prestador";
    var v_CadasturPwd = "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855";
    var v_url = '';
    var v_DisponibilidadeCNPJ;
    var v_CNPJ;
    var v_CadasturTipoAtividade;
    var v_CadasturValidade = null;  
    $(document).ready(function(){
        $('.cnpj-field').mask('99.999.999/9999-99');
        $('.cnpj-field').keypress(function(e)
        {
                searchData();
        });
        
    });

    function validateCNPJ(p_Value)
    {
        var p_Cnpj = p_Value.replace(/[^\d]+/g,'');
        if(p_Cnpj == '')
            return true; //not mandatory

        if (p_Cnpj.length != 14)
            return false;

        if (p_Cnpj == "00000000000000" || p_Cnpj == "11111111111111" || p_Cnpj == "22222222222222" || p_Cnpj == "33333333333333" || p_Cnpj == "44444444444444" ||
                p_Cnpj == "55555555555555" || p_Cnpj == "66666666666666" || p_Cnpj == "77777777777777" || p_Cnpj == "88888888888888" || p_Cnpj == "99999999999999")
            return false;

        var v_Size = p_Cnpj.length - 2;
        var v_Numbers = p_Cnpj.substring(0,v_Size);
        var v_Digits = p_Cnpj.substring(v_Size);
        var v_Sum = 0;
        var v_Pos = v_Size - 7;
        for (i = v_Size; i >= 1; i--)
        {
            v_Sum += v_Numbers.charAt(v_Size - i) * v_Pos--;
            if (v_Pos < 2)
                v_Pos = 9;
        }
        var v_Result = v_Sum % 11 < 2 ? 0 : 11 - v_Sum % 11;
        if (v_Result != v_Digits.charAt(0))
            return false;

        v_Size = v_Size + 1;
        v_Numbers = p_Cnpj.substring(0,v_Size);
        v_Sum = 0;
        v_Pos = v_Size - 7;
        for (i = v_Size; i >= 1; i--)
        {
            v_Sum += v_Numbers.charAt(v_Size - i) * v_Pos--;
            if (v_Pos < 2)
                v_Pos = 9;
        }
        v_Result = v_Sum % 11 < 2 ? 0 : 11 - v_Sum % 11;
        return v_Result == v_Digits.charAt(1);
    }

    function searchData()
    {
        v_CNPJ = $('#search-cnpj').val().replace(/[^\d]+/g,'');
        v_DisponibilidadeCNPJ = -1;
        if(validateCNPJ(v_CNPJ)) {
            $('.search-cnpj-cadastur i').hide();
            $('.search-cnpj-cadastur img').show();

            $.get("http://www.descubramatogrosso.com.br/admin/consulta-cnpj?tipo=B2&cnpj=" + v_CNPJ, function(){
            }).done(function(data){
                v_DisponibilidadeCNPJ = data;
                            }).error(function(){
                $('.search-cnpj-cadastur i').show();
                $('.search-cnpj-cadastur img').hide();
            });
        }
        else if(!validateCNPJ(v_CNPJ)) {
            alert("CNPJ inválido!");
        }
    }

    function loadCadasturData()
    {
        v_url = v_CadasturUrl + '/CPF_CNPJ/' + v_CNPJ + '/chaveAcesso/' + v_CadasturPwd;

        
        $.ajax({
            url: v_url,
            type: 'GET',
            success: function (data) {
                    if(!data.cod) {
                        v_CadasturValidade = data.dataFimVigencia;
                        finishedSearchSuccess();
                    }
                    else {
                        v_CadasturValidade = null;
                        showErrorDialog('Não é possível prosseguir com o cadastro no Portal sem um registro ATIVO no CADASTUR \n\nSaiba mais: cadastur.turismo.gov.br');
                    }
                    $('.search-cnpj-cadastur i').show();
                    $('.search-cnpj-cadastur img').hide();
                },
                
            error: function(){
                showErrorDialog('Não foi possível realizar essa operação. Tente novamente mais tarde.');
                $('.search-cnpj-cadastur i').show();
                $('.search-cnpj-cadastur img').hide();
            }
        });
    }

    function finishedSearchSuccess() {
        if(v_DisponibilidadeCNPJ == -1) { //disponivel
            location.href = 'http://www.descubramatogrosso.com.br/admin/inventario/servicos-alimentos/editar';
        }
        else if(v_DisponibilidadeCNPJ == -2) { //ja solicitado
            showErrorDialog('CNPJ já cadastrado! Favor aguardar aprovação da SETUR para liberação da posse do mesmo.');
        }
        else if(v_DisponibilidadeCNPJ == 0) {
            showConfirmationDialog('CNPJ já cadastrado no portal, deseja solicitar a posse do mesmo?', function(){
                $('#cnpj').val(v_CNPJ);
                $('#mainForm').submit();
            });
        }
        else { //indisponivel? (ja tem usuario)
            showErrorDialog('Já existe um usuário responsável por este CNPJ no Portal.');
        }
        $('.search-cnpj-cadastur i').show();
        $('.search-cnpj-cadastur img').hide();
    }

    function showConfirmationDialog(p_Message, p_ConfirmFunction) {
        $.confirm({
            text: p_Message,
            title: 'Atenção',
            confirm: function() {
                p_ConfirmFunction();
            },
            confirmButton: 'Sim',
            cancelButton: 'Não'
        });
    }

    function showErrorDialog(p_Message) {
        $.confirm({
            text: p_Message,
            title: 'Atenção',
            confirmButton: 'OK',
            cancelButton: 'Não',
            cancelButtonClass:'hidden'
        });
    }

    
    </script> -->



<!-- <script>
    //Script integração CADASTUR

    function indicaVenctoCADASTUR() {
        try {

        //Espera-se que a data esteja no formado yyyy-mm-dd hh:mm:ss
        if ($("[name='formulario[validade_cadastur]']").val() != '') {

            //console.log($("[name='formulario[validade_cadastur]']").val());
                dataCadastur = $("[name='formulario[validade_cadastur]']").val().split(' ')[0].split('-');


            if (dataCadastur.length != 3) {
                dataCadastur = $("[name='formulario[validade_cadastur]']").val().split('/');
                data = new Date(dataCadastur[2], dataCadastur[1] - 1, dataCadastur[0]);
            } else {
                data = new Date(dataCadastur[0], dataCadastur[1] - 1, dataCadastur[2]);
            }

            if (data < new Date()) {
                $("[for='registro_cadastur']").html('2.6. Número de registro do CADASTUR | <small style="color:  red;"> Vencído em: ' + data.toLocaleDateString('pt-BR') + '</small>');
            } else {
                $("[for='registro_cadastur']").html('2.6. Número de registro do CADASTUR | <small style="color:  green;"> Válido até: ' + data.toLocaleDateString('pt-BR') + '</small>');
            }

        } else {
            $("[for='registro_cadastur']").html("2.6. Número de registro do CADASTUR");
        }

        } catch(err) {
            $("[for='registro_cadastur']").html("2.6. Número de registro do CADASTUR");
        }




        //
        //
//$("[for='registro_cadastur']").html();
        //"2.6. Número de registro do CADASTUR"
    }

    function loadCadasturData() {
        var v_CNPJ = $('#cnpj').val().replace(/[^\d]+/g, '');

        if (v_CNPJ.length) {
            $('#searchCnpjCadasturBtn i').hide();
            $('#searchCnpjCadasturBtn img').show();

            v_url = v_CadasturUrl.replace(/\s/g, '') + v_CadasturPwd.replace(/\s/g, '') + '/cnpj/' + v_CNPJ + '/tipoAtividade/' + v_CadasturTrAtiv[v_CadasturTipoAtividade];

            $.ajax({
                url: ,
                type: 'GET',
                success: function (data) {
                    $('#searchCnpjCadasturBtn i').show();
                    $('#searchCnpjCadasturBtn img').hide();

                    if (typeof data['cod'] == "undefined") {

                        //transforma data em array sando como referência /
                        sDate = data['validadeCertificado'].split("/");
                        //Cria data usando como parâmetro os dados do array
                        certDate = new Date(sDate[2], sDate[1] - 1, sDate[0]);

                        if ((data['situacaoAtividadeTuristica'] && data['situacaoAtividadeTuristica'] == 'Vencido') || (certDate < new Date())) {
                            alert('CNPJ possui um cadastro VENCIDO no CADASTUR. Para prosseguir preencha os dados manualmente. \n\nSaiba mais: www.cadastur.turismo.mg.gov.br');
                            $('input[name="formulario[tem_cadastur]"]').val(0);
                            $('input[name="formulario[validade_cadastur]"]').val('');
                            $('input[name="formulario[registro_cadastur]"]').val('');
                            indicaVenctoCADASTUR();
                            return 0;
                        }

                        if (data['nuAtividadePj'])
                            $('input[name="formulario[tem_cadastur]"]').val(1);
                        if (data['validadeCertificado'])
                            $('input[name="formulario[validade_cadastur]"]').val(data['validadeCertificado'].split('/')[2] + '-' + data['validadeCertificado'].split('/')[1] + '-' + data['validadeCertificado'].split('/')[0]);
                        if (data['nuAtividadePj'])
                            $('input[name="formulario[registro_cadastur]"]').val(data['nuAtividadePj']);
                        if (data['noFantasia'])
                            $('input[name="formulario[nome_fantasia]"]').val(data['noFantasia'].toLowerCase().trim());
                        if (data['noRazaoSocial'])
                            $('input[name="formulario[nome_juridico]"]').val(data['noRazaoSocial'].toLowerCase().trim());

                        //Trata data de abertura
                        if (data['dtAbertura'].indexOf('.') >= 0) {
                            data['dtAbertura'] = data['dtAbertura'].split('.')[2] + '/' + data['dtAbertura'].split('.')[1] + '/' + data['dtAbertura'].split('.')[0];
                        }

                        if (data['dtAbertura'])
                            $('input[name="formulario[inicio_atividade]"]').val(data['dtAbertura']);
                        if (data['noWebsite'])
                            $('input[name="formulario[site]"]').val(data['noWebsite'].toLowerCase().trim());
                        if (data['telefoneInstitucional'])
                            $('input[name="formulario[telefone]"]').val(data['telefoneInstitucional']).blur();
                        if (data['noEmail'])
                            $('input[name="formulario[email]"]').val(data['noEmail'].toLowerCase().trim());
                        if (data['coCep'])
                            $('input[name="formulario[cep]"]').val(data['coCep']).blur();
                        if (data['noBairro'])
                            $('input[name="formulario[bairro]"]').val(data['noBairro'].toLowerCase().trim());
                        if (data['deLogradouro'])
                            $('input[name="formulario[logradouro]"]').val(data['deLogradouro'].toLowerCase().trim());
                        //if (data['nuLogradouro']) $('input[name="formulario[numero]"]').val(data['nuLogradouro']);
                        if (data['nuQtdUnidHabitacionais'])
                            $('input[rel="Número de UHs - Total"]').val(data['nuQtdUnidHabitacionais']);
                        if (data['nuQtdLeitos'])
                            $('input[rel="Número de Leitos - Total"]').val(data['nuQtdLeitos']);
                        if (data['noFantasia'])
                            $('input[name="formulario[nome_fantasia]"]').val(data['noFantasia'].toLowerCase().trim());
                        if (data['nuUhsAcessiveis'])
                            $('input[rel="Número de UHs - UHs adaptadas para pessoas com deficiência"]').val(data['nuUhsAcessiveis']);

                    } else {
                        if (data['msg'] == "Prestador nao encontrado") {
                            alert('CNPJ não encontrado no CADASTUR. Preencha os dados manualmente para prosseguir. \n\nSaiba mais: www.cadastur.turismo.mg.gov.br');
                            $('input[name="formulario[tem_cadastur]"]').val(0);
                            $('input[name="formulario[validade_cadastur]"]').val('');
                            $('input[name="formulario[registro_cadastur]"]').val('');
                        } else {
                            alert("Não foi possível realizar essa operação. Preencha os dados manualmente, ou tente novamente mais tarde.");
                            $('input[name="formulario[tem_cadastur]"]').val(0);
                            $('input[name="formulario[validade_cadastur]"]').val('');
                            $('input[name="formulario[registro_cadastur]"]').val('');
                        }
                        console.log(v_url);
                        console.log(data['msg']);
                    }
                    indicaVenctoCADASTUR();
                },
                erro: function () {
                    alert("Não foi possível realizar essa operação. Preencha os dados manualmente, ou tente novamente mais tarde.");
                    $('#searchCnpjCadasturBtn i').show();
                    $('#searchCnpjCadasturBtn img').hide();
                    indicaVenctoCADASTUR();
                }
            });
        } else {
            $('input[name="formulario[tem_cadastur]"]').val(0);
            $('input[name="formulario[validade_cadastur]"]').val('');
            $('input[name="formulario[registro_cadastur]"]').val('');

            console.log(v_url);
            console.log(data['msg']);

        }
    }

    $('#cnpj').on('change', function () {
        var v_CNPJ = $('#cnpj').val().replace(/[^\d]+/g, '');
        if (v_CNPJ == '') {
            $('input[name="formulario[tem_cadastur]"]').val(0);
            $('input[name="formulario[validade_cadastur]"]').val('');
            $('input[name="formulario[registro_cadastur]"]').val('');
            indicaVenctoCADASTUR();
        } else {
            loadCadasturData();
        }

    });


    $(document).ready(function () {

        indicaVenctoCADASTUR();
        

    });
</script> -->