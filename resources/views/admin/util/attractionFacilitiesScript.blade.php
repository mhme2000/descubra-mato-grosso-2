<script type="text/javascript" src="{{url('/vendor/plugins/datepicker/js/bootstrap-datetimepicker.js')}}"></script>
@if(!isset($p_Translation))
    @include('admin.util.workingPeriodScript')
@else
    @include('admin.util.workingPeriodScript', ['p_Translation' => $p_Translation])
@endif
<script>
    $(document).ready(function()
    {
        $('.time-field').mask('99:99')
        .datetimepicker({
            pickDate: false,
            format: 'HH:mm',
            icons: {
                up: 'fa fa-chevron-up',
                down: 'fa fa-chevron-down'
            }
        });

        carregaDadosFacilidades();
        carregaIdiomasVisitacao();

        $('#visita_tipo').change(function(){
            if($(this).val() != null && $(this).val().indexOf('Guiada') > -1)
            {
                $('.guided-visitation-fields').show();
                $('.guided-visitation-fields.mandatory .form-control').attr('required', true);
            }

            else
            {
                $('.guided-visitation-fields').hide();
                $('.guided-visitation-fields.mandatory .form-control').removeAttr('required');
            }
        }).change();

        $('#entrada_tipo').change(function(){
            if($(this).val() == 'Paga')
            {
                $('.paid-entrance-fields').show();
                $('.paid-entrance-fields.mandatory .form-control').attr('required', true);
            }

            else
            {
                $('.paid-entrance-fields').hide();
                $('.paid-entrance-fields.mandatory .form-control').removeAttr('required');
            }
        }).change();

        $('#necessaria_autorizacao_previa').change(function(){
            if($(this).is(':checked'))
            {
                $('.necessaria-autorizacao-fields').show();
                $('.necessaria-autorizacao-fields.mandatory .form-control').attr('required', true);
            }

            else
            {
                $('.necessaria-autorizacao-fields').hide();
                $('.necessaria-autorizacao-fields.mandatory .form-control').removeAttr('required');
            }
        }).change();

        $('#limite_numero_visitantes').change(function(){
            if($(this).is(':checked'))
            {
                $('.limite-numero-visitantes-fields').show();
                $('.limite-numero-visitantes-fields.mandatory .form-control').attr('required', true);
            }

            else
            {
                $('.limite-numero-visitantes-fields').hide();
                $('.limite-numero-visitantes-fields.mandatory .form-control').removeAttr('required');
            }
        }).change();

        var v_PaymentMethods = $('#formas_pagamento').val();
        if(v_PaymentMethods != undefined && v_PaymentMethods.length > 0)
        {
            v_PaymentMethods = JSON.parse(v_PaymentMethods);
            var v_SelectedItems = [];
            $(v_PaymentMethods).each(function(){
                v_SelectedItems.push(this.id);
            });
            $('#formas_pagamento_select').select2("val", v_SelectedItems);
        }
    });

    function carregaDadosFacilidades()
    {
        carregaCamposFacilidade('entrada_atrativo', 'entrada-atrativo-fields');
        carregaCamposFacilidade('acesso_mais_utilizado', 'acesso-mais-utilizado-fields');
        carregaCamposFacilidade('servicos_equipamentos', 'servicos-equipamentos-fields');
    }

    function carregaIdiomasVisitacao()
    {
        var v_Dados = $('#visita_idiomas').val();
        if(v_Dados.length > 0)
        {
            v_Dados = JSON.parse(v_Dados);
            var v_SelectedItems = [];
            $(v_Dados).each(function(){
                v_SelectedItems.push(this.id);
            });
            $('#visita_idiomas_select').select2("val", v_SelectedItems);
        }
    }

    function carregaCamposFacilidade(p_Id, p_DivClass)
    {
        var v_Dados = $('#'+p_Id).val();
        if(v_Dados.length > 0)
        {
            v_Dados = JSON.parse(v_Dados);
            $(v_Dados).each(function(){
                if(this.tipo == 'checkbox')
                    $('.'+p_DivClass+' .field[rel="'+this.nome+'"]').prop('checked',this.valor);
                else if(this.tipo == 'select2')
                    $('.'+p_DivClass+' .field[rel="'+this.nome+'"]').select2("val", this.valor);
                else
                    $('.'+p_DivClass+' .field[rel="'+this.nome+'"]').val(this.valor);
            });
        }
    }

    function processaDadosFacilidades()
    {
        processaDadosFuncionamentos();
        processaIdiomasVisitacao();
        processaCamposFacilidade('entrada_atrativo', 'entrada-atrativo-fields');
        processaCamposFacilidade('acesso_mais_utilizado', 'acesso-mais-utilizado-fields');
        processaCamposFacilidade('servicos_equipamentos', 'servicos-equipamentos-fields');

        @if(isset($p_Translation))
            <?php $v_Languages = ['pt', 'en', 'es', 'fr']; ?>

            var v_ActivitiesJson = {};
            var v_PreviousAuthorizationTypeJson = {};
            @foreach($v_Languages as $c_Language)
                v_ActivitiesJson.{{$c_Language}} = $('#atividades_realizadas_{{$c_Language}}').val();
                v_PreviousAuthorizationTypeJson.{{$c_Language}} = $('#autorizacao_previa_tipo_{{$c_Language}}').val();
            @endforeach
            $('#atividades_realizadas').val(JSON.stringify(v_ActivitiesJson));
            $('#autorizacao_previa_tipo').val(JSON.stringify(v_PreviousAuthorizationTypeJson));
        @endif

        var v_PaymentMethods = [];
        var $v_PaymentMethodsField = $('#formas_pagamento');
        if($v_PaymentMethodsField.length){
            $('#formas_pagamento_select option:selected').each(function(){
                v_PaymentMethods.push({
                    id:$(this).attr('value'),
                    nome:$(this).text()
                });
            });
            $('#formas_pagamento').val(JSON.stringify(v_PaymentMethods));
        }
    }

    function processaCamposFacilidade(p_Id, p_DivClass)
    {
        var v_Dados = [];
        $('.'+p_DivClass+' .field').each(function(){
            v_Dados.push({
                nome:$(this).attr('rel'),
                tipo:$(this).hasClass('checkbox') ? 'checkbox' : ($(this).hasClass('select2') ? 'select2' : 'text-or-select'),
                valor:$(this).hasClass('checkbox') ? $(this).is(':checked') : $(this).val()
            });
        });
        $('#'+p_Id).val(JSON.stringify(v_Dados));
    }

    function processaIdiomasVisitacao()
    {
        var v_Dados = [];
        $('#visita_idiomas_select option:selected').each(function(){
            v_Dados.push({
                id:$(this).attr('value'),
                nome:$(this).text()
            });
        });
        $('#visita_idiomas').val(JSON.stringify(v_Dados));
    }
</script>