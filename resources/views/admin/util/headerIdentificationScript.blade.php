<script type="text/javascript" src="{{url('/vendor/plugins/datepicker/js/bootstrap-datetimepicker.js')}}"></script>
<script>
    @if(isset($p_CadasturActivities))
    var v_CadasturActivities = {!! json_encode($p_CadasturActivities) !!}

    function getActivityByType(p_Type){
        return $.grep(v_CadasturActivities, function(e){ return e.type_id == p_Type; })[0];
    }
    @endif

    $(document).ready(function()
    {
        $('.date-field')
        .datetimepicker({
            pickTime: false,
            format: "DD/MM/YYYY"
        }).mask('99/99/9999');

        $('.cep-field').mask('99.999-999');
        $('#inicio_atividade').mask('99/9999');

        $('.cnpj-field').mask('99.999.999/9999-99');

        $('#city_id').change(function(){
            $.get("{{url('/admin/distritosMunicipio?city_id=')}}" + $(this).val(), function(){
            }).done(function(data){
                if (data.error == 'ok') {
                    var v_LastVal = $('#district_id').val();
                    var v_DataString = '';
                    $.each(data.data, function (c_Key, c_Field)
                    {
                        v_DataString += '<option value="' + c_Key + '">' + c_Field + '</option>';
                    });

                    $('#district_id').html('<option value=""></option>' + v_DataString);
                    if(data.data.length == 0)
                        $('#district_id').select2("val", "");
                    else
                        $('#district_id').select2("val", v_LastVal);
                }
                else
                    $('#district_id').html('<option value=""></option>').select2("val", "");
            }).error(function(){
            });
        }).change();

        $('#latitude, #longitude').focus(function() {
            $(this).on('mousewheel.disableScroll', function(e) {
                e.preventDefault();
            })
        }).blur(function() {
            $(this).off('mousewheel.disableScroll');
        }).keydown(function(e) {
            if (e.keyCode === 38 || e.keyCode === 40)
                e.preventDefault();
        });



        @if(isset($p_CadasturActivities))
            $('#type_id').change(function(){
                var v_Activity = getActivityByType(this.value);
                $('#tipo_atividade_cadastur').val(v_Activity == undefined ? '' : v_Activity.cadastur_activity_id);
            }).change();

            @if(!isset($p_WithoutSubtype))
                $('#sub_type_id').change(function(){
                    var v_Type = this.value == '' ? $('#type_id').val() : this.value;
                    var v_Activity = getActivityByType(v_Type);
                    $('#tipo_atividade_cadastur').val(v_Activity == undefined ? '' : v_Activity.cadastur_activity_id);
                }).change();
            @endif
        @endif

        @if(!isset($p_WithoutSubtype))
        $('#type_id').change(function(){
            $.get("{{url('/admin/subtipos?super_type_id=')}}" + $(this).val(), function(){
            }).done(function(data){
                if (data.error == 'ok')
                {
                    var v_LastVal = $('#sub_type_id').val();
                    var v_DataString = '';
                    $.each(data.data, function (c_Key, c_Field)
                    {
                        v_DataString += '<option value="' + c_Key + '">' + c_Field + '</option>';
                    });

                    $('#sub_type_id').html(v_DataString);
                    if(data.data.length == 0)
                        $('#sub_type_id').select2("val", "");
                    else
                        $('#sub_type_id').select2("val", v_LastVal);
                }
                else
                    $('#sub_type_id').html('').select2("val", "");
            }).error(function(){
            });
        }).change();
        @endif

        $('#latitude_longitude_decimal').keyup(function(){
            var v_Value = this.value;
            if(v_Value.length){
                v_Value = v_Value.split(',');
                var v_Latitude = v_Value[0].trim();
                var v_Longitude = v_Value[1].trim();
                if(v_Latitude.length && v_Longitude.length){
                    $('#latitude').val(v_Latitude);
                    $('#longitude').val(v_Longitude).change();
                }
            }
        });
    });


    function validateCNPJ(p_Value)
    {
        var p_Cnpj = p_Value.replace(/[^\d]+/g,'');
        if(p_Cnpj == '')
            return true; //not mandatory

        if (p_Cnpj.length != 14)
            return false;

        if (p_Cnpj == "00000000000000" || p_Cnpj == "11111111111111" || p_Cnpj == "22222222222222" || p_Cnpj == "33333333333333" || p_Cnpj == "44444444444444" ||
                p_Cnpj == "55555555555555" || p_Cnpj == "66666666666666" || p_Cnpj == "77777777777777" || p_Cnpj == "88888888888888" || p_Cnpj == "99999999999999")
            return false;

        var v_Size = p_Cnpj.length - 2;
        var v_Numbers = p_Cnpj.substring(0,v_Size);
        var v_Digits = p_Cnpj.substring(v_Size);
        var v_Sum = 0;
        var v_Pos = v_Size - 7;
        for (i = v_Size; i >= 1; i--)
        {
            v_Sum += v_Numbers.charAt(v_Size - i) * v_Pos--;
            if (v_Pos < 2)
                v_Pos = 9;
        }
        var v_Result = v_Sum % 11 < 2 ? 0 : 11 - v_Sum % 11;
        if (v_Result != v_Digits.charAt(0))
            return false;

        v_Size = v_Size + 1;
        v_Numbers = p_Cnpj.substring(0,v_Size);
        v_Sum = 0;
        v_Pos = v_Size - 7;
        for (i = v_Size; i >= 1; i--)
        {
            v_Sum += v_Numbers.charAt(v_Size - i) * v_Pos--;
            if (v_Pos < 2)
                v_Pos = 9;
        }
        v_Result = v_Sum % 11 < 2 ? 0 : 11 - v_Sum % 11;
        return v_Result == v_Digits.charAt(1);
    }
</script>