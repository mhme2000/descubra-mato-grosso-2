<input type="hidden" id="hashtags" name="formulario[hashtags]">
<div class="form-group col-sm-12">
    <label for="hashtags">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Palavras-chave</label>
    <?php
        $v_Hashtags = $p_Form != null && $p_Form->hashtags != null ? explode(';', $p_Form->hashtags) : [];
        $v_HashtagOptions = [];
        foreach($v_Hashtags as $c_Key)
            $v_HashtagOptions[$c_Key] = $c_Key;
    ?>
    {!! Form::select('', $v_HashtagOptions, $v_Hashtags, ['id' => 'hashtags_select', 'class' => 'form-control select2-custom', 'style' => 'width: 100%', 'multiple' => 'multiple']) !!}
</div>