<div class="form-group col-sm-12">
    <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Equipe responsável pelo preenchimento de dados</h3>
</div>
@if(\App\UserType::isAdmin() || \App\UserType::isMaster() || \App\UserType::isCircuito() || \App\UserType::isMunicipio())
    <?php $v_TeamsData = \App\ResponsibleTeam::getTeamsData(); ?>
    @if(count($v_TeamsData))
        <div class="form-group col-sm-12">
            <label for="equipe_responsavel">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Equipe</label>
            <input type="hidden" id="responsible_team_data" value="{{json_encode($v_TeamsData)}}">
            {!! Form::select('', ['' => ''], null, ['id' => 'responsible_team_select', 'class' => 'form-control select2', 'style' => 'width: 100%']) !!}
        </div>
    @endif
@endif
<div class="form-group col-sm-6">
    <label for="equipe_responsavel">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Responsável<span class="mandatory-field">*</span></label>
    <input type="text" name="formulario[equipe_responsavel_responsavel]" class="form-control" id="equipe_responsavel" placeholder="Digite Aqui" value="{{$p_Form == null ? '' : $p_Form->equipe_responsavel_responsavel}}" required>
</div>
<div class="form-group col-sm-6">
    <label for="equipe_responsavel_instituicao">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Instituição<span class="mandatory-field">*</span></label>
    <input type="text" name="formulario[equipe_responsavel_instituicao]" class="form-control" id="equipe_responsavel_instituicao" placeholder="Digite Aqui" value="{{$p_Form == null ? '' : $p_Form->equipe_responsavel_instituicao}}" required>
</div>
<div class="form-group col-sm-6">
    <label for="equipe_responsavel_telefone">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Telefone (exemplo: (DDD) 9 9999-9999 / (DDD) 9999-9999)<span class="mandatory-field">*</span></label>
    <input type="text" name="formulario[equipe_responsavel_telefone]" class="form-control phone-field" id="equipe_responsavel_telefone" placeholder="Digite Aqui" value="{{$p_Form == null ? '' : $p_Form->equipe_responsavel_telefone}}" required>
</div>
<div class="form-group col-sm-6">
    <label for="equipe_responsavel_email">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Email<span class="mandatory-field">*</span></label>
    <input type="text" name="formulario[equipe_responsavel_email]" class="email-field form-control" id="equipe_responsavel_email" placeholder="Digite Aqui" value="{{$p_Form == null ? '' : $p_Form->equipe_responsavel_email}}" required>
</div>
<div class="form-group col-sm-12">
    <label for="equipe_responsavel_observacao">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Observação</label>
    <textarea name="formulario[equipe_responsavel_observacao]" id="equipe_responsavel_observacao" class="form-control" rows="4" placeholder="Digite Aqui">{{$p_Form == null ? '' : $p_Form->equipe_responsavel_observacao}}</textarea>
</div>