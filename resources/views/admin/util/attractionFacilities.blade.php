@if(!isset($p_Translation) || (isset($p_TabLanguage) && $p_TabLanguage == 'pt'))
<link rel="stylesheet" type="text/css" href="{{url('/vendor/plugins/datepicker/css/bootstrap-datetimepicker.css')}}">

<div class="form-group col-sm-12">
    <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Entrada do atrativo</h3>
</div>
<input type="hidden" id="entrada_atrativo" name="formulario[entrada_atrativo]" value="{{$p_Form == null ? '' : $p_Form->entrada_atrativo}}">
<div class="entrada-atrativo-fields">
    <?php
        $v_EntradaFields = [
            'Centro de recepção',
            'Posto de informações',
            'Portaria principal',
            'Guarita'
        ];
    ?>
    @foreach($v_EntradaFields as $c_Field)
        <div class="form-group col-sm-3">
            <label>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}{{$c_Field}}?</label>
            <p><input type="checkbox" value="1" rel="{{$c_Field}}" class="ml5 mt10 checkbox field"></p>
        </div>
    @endforeach
</div>
@endif

@if(!isset($p_Translation))
    @include('admin.util.workingPeriod', ['p_Form' => $p_Form, 'p_ClosedFields' => true])
@else
    @include('admin.util.workingPeriod', ['p_Form' => $p_Form, 'p_Translation' => $p_Translation, 'p_TabLanguage' => $p_TabLanguage, 'p_ClosedFields' => true])
@endif

<div class="form-group col-sm-12 mt25">
    <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Visitação</h3>
</div>
@if(!isset($p_Translation) || (isset($p_TabLanguage) && $p_TabLanguage == 'pt'))
<div class="form-group col-sm-6">
    <label for="visita_duracao_media">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Duração média da visita (horas)<span class="mandatory-field">*</span></label>
    <input name="formulario[visita_duracao_media]" type="text" class="form-control time-field" placeholder="Digite Aqui" value="{{$p_Form == null ? '' : $p_Form->visita_duracao_media}}" required>
</div>
<div class="form-group col-sm-6">
    <?php
        $v_VisitationTypes = [
                'Não guiada' => 'Não guiada',
                'Auto-guiada' => 'Auto-guiada',
                'Guiada' => 'Guiada'
        ];
        $v_SelectedTypes = $p_Form == null ? [] : explode(';', $p_Form->visita_tipo);
    ?>
    <label for="visita_tipo">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Tipo de visita <i>(permite mais de uma opção)</i><span class="mandatory-field">*</span></label>
    {!! Form::select('formulario[visita_tipo][]', $v_VisitationTypes, $v_SelectedTypes, ['id' => 'visita_tipo', 'class' => 'form-control select2', 'multiple' => 'multiple', 'style' => 'width: 100%', 'required' => 'required']) !!}
</div>
<div class="form-group col-sm-6 mandatory guided-visitation-fields">
    <?php
        $v_VisitationGuideTypes = [
            'Inexistente' => 'Inexistente',
            'Gratuito' => 'Gratuito',
            'Pago' => 'Pago'
        ];
    ?>
    <label for="visita_guia">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(2)}}Guia de visitação<span class="mandatory-field">*</span></label>
    {!! Form::select('formulario[visita_guia]', $v_VisitationGuideTypes, $p_Form == null ? '' : $p_Form->visita_guia, ['id' => 'visita_guia', 'class' => 'form-control', 'style' => 'width: 100%']) !!}
</div>

<div class="form-group col-sm-12 mandatory guided-visitation-fields">
    <label for="visita_idiomas">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(2)}}Idiomas da visitação <i>(permite mais de uma opção)</i><span class="mandatory-field">*</span></label>
    <input type="hidden" id="visita_idiomas" name="formulario[visita_idiomas]" value="{{$p_Form == null ? '' : $p_Form->visita_idiomas}}">
    {!! Form::select('', $p_Languages, null, array('id' => 'visita_idiomas_select', 'class' => 'form-control select2', 'multiple' => 'multiple', 'style' => 'width: 100%')) !!}
</div>
<div class="form-group col-sm-12 guided-visitation-fields">
    <label for="visita_outros_idiomas">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(2)}}Outros idiomas</label>
    <textarea name="formulario[visita_outros_idiomas]" id="visita_outros_idiomas" class="form-control" rows="4" placeholder="Digite Aqui">{{$p_Form == null ? '' : $p_Form->visita_outros_idiomas}}</textarea>
</div>

<div class="form-group col-sm-6">
    <?php
        $v_EntranceTypes = [
            ''=>'',
            'Gratuita' => 'Gratuita',
            'Paga' => 'Paga'
        ];
    ?>
    <label for="entrada_tipo">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Entrada<span class="mandatory-field">*</span></label>
    {!! Form::select('formulario[entrada_tipo]', $v_EntranceTypes, $p_Form == null ? '' : $p_Form->entrada_tipo, array('id' => 'entrada_tipo', 'class' => 'form-control', 'style' => 'width: 100%', 'required' => 'required')) !!}
</div>
<div class="form-group col-sm-6 mandatory paid-entrance-fields">
    <label for="entrada_valor">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(2)}}Valor - R$<span class="mandatory-field">*</span></label>
    <input type="text" name="formulario[entrada_valor]" class="form-control currency-field" id="entrada_valor" placeholder="Digite Aqui" value="{{$p_Form == null ? '' : $p_Form->entrada_valor}}">
</div>
<div class="form-group col-sm-12">
    <label for="formas_pagamento">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Formas de pagamento <i>(permite mais de uma opção)</i></label>
    <input type="hidden" id="formas_pagamento" name="formulario[formas_pagamento]" value="{{$p_Attraction == null ? '' : $p_Attraction->formas_pagamento}}">
    {!! Form::select('', $p_PaymentMethods, null, ['id' => 'formas_pagamento_select', 'class' => 'form-control select2', 'style' => 'width: 100%', 'multiple' => 'multiple']) !!}
</div>
<div class="form-group col-sm-12">
    <label for="necessaria_autorizacao_previa">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}É necessário ter autorização prévia para visitação?</label>
    <p><input type="checkbox" value="1" name="formulario[necessaria_autorizacao_previa]" id="necessaria_autorizacao_previa" class="ml5 mt10" {{$p_Form != null && $p_Form->necessaria_autorizacao_previa == 1 ? 'checked' : ''}}></p>
</div>
@endif

@if(!isset($p_Translation))
    <div class="form-group col-sm-12 mandatory necessaria-autorizacao-fields">
        <label for="autorizacao_previa_tipo">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(2)}}De qual tipo?<span class="mandatory-field">*</span></label>
        <textarea name="formulario[autorizacao_previa_tipo]" id="autorizacao_previa_tipo" class="form-control" rows="4" placeholder="Digite Aqui">{{$p_Form == null ? '' : $p_Form->autorizacao_previa_tipo}}</textarea>
    </div>
@else
    <?php
        if($p_Form != null){
            $v_EmptyData = ['pt'=>'', 'en'=>'', 'es'=>'', 'fr'=>''];
            $v_Activities = $p_Form->atividades_realizadas != null ? json_decode($p_Form->atividades_realizadas,1) : $v_EmptyData;
            $v_PreviousAuthorizationType = $p_Form->autorizacao_previa_tipo != null ? json_decode($p_Form->autorizacao_previa_tipo,1) : $v_EmptyData;
        }
    ?>
    @if($p_TabLanguage == 'pt')
    <input type="hidden" id="autorizacao_previa_tipo" name="formulario[autorizacao_previa_tipo]">
    @endif
    <div class="form-group col-sm-12 {{$p_TabLanguage == 'pt' ? 'mandatory' : ''}} necessaria-autorizacao-fields">
        @if($p_TabLanguage == 'pt')
            <label for="autorizacao_previa_tipo_{{$p_TabLanguage}}">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(2)}}De qual tipo?<span class="mandatory-field">*</span></label>
        @else
            <label for="autorizacao_previa_tipo_{{$p_TabLanguage}}">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}É necessário ter autorização prévia para visitação - De qual tipo?</label>
        @endif
        <textarea id="autorizacao_previa_tipo_{{$p_TabLanguage}}" class="form-control" rows="4" placeholder="Digite Aqui">{{$p_Form == null ? '' : $v_PreviousAuthorizationType[$p_TabLanguage]}}</textarea>
    </div>
@endif

@if(!isset($p_Translation) || (isset($p_TabLanguage) && $p_TabLanguage == 'pt'))
<div class="form-group col-sm-12">
    <label for="limite_numero_visitantes">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Limite no número de visitantes?</label>
    <p><input type="checkbox" value="1" name="formulario[limite_numero_visitantes]" id="limite_numero_visitantes" class="ml5 mt10" {{$p_Form != null && $p_Form->limite_numero_visitantes == 1 ? 'checked' : ''}}></p>
</div>
<div class="form-group col-sm-12 mandatory limite-numero-visitantes-fields">
    <label for="limite_numero_visitantes_quantidade">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(2)}}Qual?<span class="mandatory-field">*</span></label>
    <input type="text" name="formulario[limite_numero_visitantes_quantidade]" class="form-control integer-field" id="limite_numero_visitantes_quantidade" placeholder="Digite Aqui" value="{{$p_Form == null ? '' : $p_Form->limite_numero_visitantes_quantidade}}">
</div>

<input type="hidden" id="acesso_mais_utilizado" name="formulario[acesso_mais_utilizado]" value="{{$p_Form == null ? '' : $p_Form->acesso_mais_utilizado}}">
<div class="acesso-mais-utilizado-fields">
    <div class="form-group col-sm-6">
        <label>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Permite a expansão no volume de visitantes?</label>
        <p><input type="checkbox" value="1" rel="Permite a expansão no volume de visitantes" class="ml5 mt10 checkbox field"></p>
    </div>
</div>

@include('admin.util.attractionGeneralInfo', ['p_Form' => $p_Form])

<div class="form-group col-sm-6">
    <label for="visitantes_numero_anual">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Número anual de visitantes</label>
    <input type="text" name="formulario[visitantes_numero_anual]" class="form-control integer-field" id="visitantes_numero_anual" placeholder="Digite Aqui" value="{{$p_Form == null ? '' : $p_Form->visitantes_numero_anual}}">
</div>
<div class="form-group col-sm-12">
    <?php
        $v_VisitorOrigins = [
            'Municipal' => 'Municipal',
            'Entorno regional' => 'Entorno regional',
            'Nacional' => 'Nacional',
            'Internacional' => 'Internacional'
        ];
    ?>
    <label for="visitantes_origem">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Procedência dos visitantes <i>(permite mais de uma opção)</i></label>
    <input type="hidden" id="visitantes_origem" name="formulario[visitantes_origem]" value="{{$p_Form == null ? '' : $p_Form->visitantes_origem}}">
    {!! Form::select('', $v_VisitorOrigins, null, ['id' => 'visitantes_origem_select', 'class' => 'form-control select2', 'multiple' => 'multiple', 'style' => 'width: 100%']) !!}
</div>
<div class="form-group col-sm-12">
    <label for="visitantes_origem_internacionais">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Origem dos turistas internacionais <i>(permite mais de uma opção)</i></label>
    <input type="hidden" id="visitantes_origem_internacionais" name="formulario[visitantes_origem_internacionais]" value="{{$p_Form == null ? '' : $p_Form->visitantes_origem_internacionais}}">
    {!! Form::select('', $p_Countries, null, ['id' => 'visitantes_origem_internacionais_select', 'class' => 'form-control select2', 'multiple' => 'multiple', 'style' => 'width: 100%']) !!}
</div>


<div class="form-group col-sm-12">
    <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Serviços e equipamentos no atrativo</h3>
</div>
<input type="hidden" id="servicos_equipamentos" name="formulario[servicos_equipamentos]" value="{{$p_Form == null ? '' : $p_Form->servicos_equipamentos}}">
<div class="servicos-equipamentos-fields">
    @if(!isset($p_RealizacaoContemporanea))
        <div class="form-group col-sm-6">
            <label>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Informações ao visitante?</label>
            <p><input type="checkbox" value="1" rel="Informações ao visitante" class="ml5 mt10 checkbox field"></p>
        </div>
        <div class="form-group col-sm-12">
            <label>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Informações em outras línguas <i>(permite mais de uma opção)</i></label>
            {!! Form::select('', $p_LanguageNames, null, ['rel' => 'Informações em outras línguas', 'class' => 'form-control field select2', 'multiple' => 'multiple', 'style' => 'width: 100%']) !!}
        </div>
    @endif
    @if(isset($p_EcologyInstructions))
        <div class="form-group col-sm-3">
            <label>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Instruções histórico/cultural?</label>
            <p><input type="checkbox" value="1" rel="Instruções histórico/cultural" class="ml5 mt10 checkbox field"></p>
        </div>
    @endif
    <?php
    $v_ServicosEquipamentosFields = [
            'Instalações sanitárias',
            'Espaço para alimentação',
            'Hospedagem',
            'Atividades comerciais',
            'Restaurante/Lanchonete'
    ];
    ?>
    @foreach($v_ServicosEquipamentosFields as $c_Field)
        <div class="form-group col-sm-3">
            <label>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}{{$c_Field}}?</label>
            <p><input type="checkbox" value="1" rel="{{$c_Field}}" class="ml5 mt10 checkbox field"></p>
        </div>
    @endforeach
</div>
@endif

<div class="form-group col-sm-12">
    <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Atividades realizadas no atrativo</h3>
</div>
@if(!isset($p_Translation))
    <div class="form-group col-sm-12">
        <label for="atividades_realizadas">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Quais atividades são realizadas no atrativo?<span class="mandatory-field">*</span></label>
        <textarea name="formulario[atividades_realizadas]" id="atividades_realizadas" class="form-control" rows="4" placeholder="Digite Aqui" required>{{$p_Form == null ? '' : $p_Form->atividades_realizadas}}</textarea>
    </div>
@else
    @if($p_TabLanguage == 'pt')
    <input type="hidden" id="atividades_realizadas" name="formulario[atividades_realizadas]">
    @endif
    <div class="form-group col-sm-12">
        <label for="atividades_realizadas_{{$p_TabLanguage}}">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Quais atividades são realizadas no atrativo?
            @if($p_TabLanguage == 'pt')
                <span class="mandatory-field">*</span>
            @endif
        </label>
        <textarea id="atividades_realizadas_{{$p_TabLanguage}}" class="form-control" rows="4" placeholder="Digite Aqui" {{$p_TabLanguage == 'pt' ? 'required' : ''}}>{{$p_Form == null ? '' : $v_Activities[$p_TabLanguage]}}</textarea>
    </div>
@endif