@if(!isset($p_Translation) || (isset($p_TabLanguage) && $p_TabLanguage == 'pt'))
<div class="form-group col-sm-12">
    <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Acessibilidade</h3>
</div>
<input type="hidden" id="acessibilidade" name="formulario[acessibilidade]" value="{{$p_Form == null ? '' : $p_Form->acessibilidade}}">
<div class="form-group col-sm-12">
    <label>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Possui alguma facilidade para pessoas com deficiência ou mobilidade reduzida?<input id="possui_acessibilidade" type="checkbox" value="1" class="ml5"></label>
</div>

<?php
    $v_ExternalRoute = [
            'Não'=>'Não',
            'Estacionamento'=>'Estacionamento',
            'Calçada rebaixada'=>'Calçada rebaixada',
            'Faixa de pedestre'=>'Faixa de pedestre',
            'Rampa'=>'Rampa',
            'Semáforo sonoro'=>'Semáforo sonoro',
            'Piso tátil de alerta'=>'Piso tátil de alerta',
            'Piso regular e antiderrapante'=>'Piso regular e antiderrapante',
            'Livre de obstáculos'=>'Livre de obstáculos'
    ];
    $v_BoardingLanding = [
            'Não'=>'Não',
            'Sinalizado'=>'Sinalizado',
            'Com acesso em nível'=>'Com acesso em nível'
    ];
    $v_ParkingSpot = [
            'Não'=>'Não',
            'Sinalizada'=>'Sinalizada',
            'Com acesso em nível'=>'Com acesso em nível',
            'Alargada para cadeira de rodas'=>'Alargada para cadeira de rodas',
            'Rampa de acesso à calçada'=>'Rampa de acesso à calçada'
    ];
    $v_WheelchairAccess = [
            'Não'=>'Não',
            'Rampa'=>'Rampa',
            'Elevador'=>'Elevador',
            'Plataforma elevatória'=>'Plataforma elevatória',
            'Com circulação entre mobiliário'=>'Com circulação entre mobiliário',
            'Porta larga'=>'Porta larga',
            'Piso regular/antiderrapante'=>'Piso regular/antiderrapante'
    ];
    $v_Stairs = [
            'Não'=>'Não',
            'Corrimão'=>'Corrimão',
            'Patamar para descanso'=>'Patamar para descanso',
            'Sinalização tátil de alerta'=>'Sinalização tátil de alerta',
            'Piso antiderrapante'=>'Piso antiderrapante'
    ];
    $v_Elevator = [
            'Não'=>'Não',
            'Sinalizado em Braille'=>'Sinalizado em Braille',
            'Dispositivo sonoro'=>'Dispositivo sonoro',
            'Dispositivo luminoso'=>'Dispositivo luminoso',
            'Sensor eletrônico (porta)'=>'Sensor eletrônico (porta)'
    ];
    $v_Communication = [
            'Não'=>'Não',
            'Texto informativo em Braille'=>'Texto informativo em Braille',
            'Texto informativo em fonte ampliada'=>'Texto informativo em fonte ampliada',
            'Intérprete em Libras (língua brasileira de sinais)'=>'Intérprete em Libras (língua brasileira de sinais)'
    ];
    $v_SpecialSeatObese = [
            'Não'=>'Não',
            'Sinalizado'=>'Sinalizado'
    ];
    $v_Bathroom = [
            'Não'=>'Não',
            'Barra de apoio'=>'Barra de apoio',
            'Porta larga suficiente para entrada de cadeira de rodas'=>'Porta larga suficiente para entrada de cadeira de rodas',
            'Giro para cadeira de rodas'=>'Giro para cadeira de rodas',
            'Acesso para cadeira de rodas'=>'Acesso para cadeira de rodas',
            'Pia rebaixada'=>'Pia rebaixada',
            'Espelho rebaixado ou com ângulo de alcance visual'=>'Espelho rebaixado ou com ângulo de alcance visual',
            'Boxe ou banheira adaptada'=>'Boxe ou banheira adaptada',
            'Torneira monocomando/alavanca'=>'Torneira monocomando/alavanca',
            'Porta larga o suficiente para entrada de obesos'=>'Porta larga o suficiente para entrada de obesos',
            'Assento especial para obesos'=>'Assento especial para obesos'
    ];
    $v_FieldsOptionsArray = [
            $v_ExternalRoute,
            $v_BoardingLanding,
            $v_ParkingSpot,
            $v_WheelchairAccess,
            $v_Stairs,
            $v_Elevator,
            $v_Communication,
            $v_SpecialSeatObese,
            $v_Bathroom
    ];
    $v_FieldsNamesArray = [
            'Rota externa acessível',
            'Local de embarque e desembarque',
            'Vaga em estacionamento',
            'Área de circulação/acesso interno para cadeiras de rodas/Lazer e UHs',
            'Escada',
            'Elevador',
            'Comunicação',
            'Assento especial para obesos',
            'Sanitário'
    ];
?>
<div class="accessibility-fields-container">
    <div class="accessibility-fields">
        @foreach($v_FieldsNamesArray as $c_FieldIndex => $c_Field)
            <div class="form-group col-sm-12">
                <label>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(2)}}{{$c_Field}} <i>(permite mais de uma opção)</i></label>
                {!! Form::select('', $v_FieldsOptionsArray[$c_FieldIndex], null, ['rel'=>$c_Field,'class' => 'form-control select2', 'style' => 'width: 100%', 'multiple' => 'multiple']) !!}
            </div>
            @if($c_FieldIndex == 0)
                @if(!isset($p_Translation))
                    <div class="form-group col-sm-12">
                        <label>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(2)}}Outras</label>
                        <textarea class="form-control" rows="4" placeholder="Digite Aqui" rel="Rota externa acessível - Outras"></textarea>
                    </div>
                @else
                    <div class="form-group col-sm-12">
                        <label>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(2)}}Outras</label>
                        <textarea class="form-control" rows="4" placeholder="Digite Aqui" rel="Rota externa acessível - Outras - {{$p_TabLanguage}}"></textarea>
                    </div>
                @endif
            @endif
        @endforeach
    </div>
</div>
@endif

<div class="accessibility-fields-container">
    <div class="accessibility-fields">
        @if(!isset($p_Translation))
            <div class="form-group col-sm-12">
                <label>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(2)}}Observações complementares</label>
                <textarea class="form-control" rows="4" placeholder="Digite Aqui" rel="Observações complementares"></textarea>
            </div>
        @else
            @if($p_TabLanguage != 'pt')
            <div class="form-group col-sm-12">
                <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Acessibilidade</h3>
            </div>
            <div class="form-group col-sm-12" data-index="{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}">
                <label>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(2)}}Rota externa acessível - Outras</label>
                <textarea class="form-control" rows="4" placeholder="Digite Aqui" rel="Rota externa acessível - Outras - {{$p_TabLanguage}}"></textarea>
            </div>
            @endif
            <div class="form-group col-sm-12">
                <label>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(2)}}Observações complementares</label>
                <textarea class="form-control" rows="4" placeholder="Digite Aqui" rel="Observações complementares - {{$p_TabLanguage}}"></textarea>
            </div>
        @endif
    </div>
</div>
