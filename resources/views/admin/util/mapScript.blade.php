<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?language=pt-BR&key=AIzaSyBLc_8beOK5wVSM2NeSk1Bl4mLwtq6jtGw"></script>
<script>
    // MAP
    var g_Map;
    var g_Geocoder;
    var g_MapCenter = new google.maps.LatLng(-13.275734, -56.024483);
    var g_Marker = null;
    var g_LatitudeInput;
    var g_LongitudeInput;
    var g_LatitudeLongitudeInput;
    var g_InitializingMap = true;
    $(document).ready(function(){
        initializeMap();
        $('#map_canvas').parent().addClass('map-canvas-parent hidden-print');

        $('#latitude, #longitude').change(function(){
            updateMarkerPosition(0);
        });

        $('.panel-title .btn[onclick="print()"]').attr('onclick', 'adjustBeforePrint()');

        $('.panel-tabs li a').click(function (){
            if ($(this).attr('href') == '#tab0'){
                $('.panel-title .btn[onclick="print()"]').attr('onclick', 'adjustBeforePrint()');
            }
            else
                $('.panel-title .btn[onclick="adjustBeforePrint()"]').attr('onclick', 'print()');
        });
    });

    function adjustBeforePrint(){
        $('#mainForm').before($('#map_canvas'));
        $('#map_canvas').width('660px').css('margin-left','10px');
        google.maps.event.trigger(g_Map,'resize');
        g_Map.setCenter(g_MapCenter);
        updateMarkerPosition(0);
        setTimeout(function () {
            print();
            setTimeout(function () {
                adjustAfterPrint();
            },300);
        },50);
    }

    function adjustAfterPrint(){
        $('.map-canvas-parent').append($('#map_canvas'));
        $('#map_canvas').width('100%').css('margin-left','0');
        google.maps.event.trigger(g_Map,'resize');
        g_Map.setCenter(g_MapCenter);
        updateMarkerPosition(0);
    }
    function initializeMap(){
        g_Geocoder = new google.maps.Geocoder();
        g_LatitudeInput = $('#latitude');
        g_LongitudeInput = $('#longitude');
        g_LatitudeLongitudeInput = $('#latitude_longitude_decimal');
        var v_MapCanvas = document.getElementById('map_canvas');
        var v_MapOptions = {
            center: g_MapCenter,
            language: 'pt-BR',
            zoom: 6,
            minZoom: 6,
            mapTypeControlOptions: {
                mapTypeIds: []
            },
            navigationControl: true,
            scrollwheel: false,
            streetViewControl: true
        };
        g_Map = new google.maps.Map(v_MapCanvas, v_MapOptions);

        var v_CustomMapType = new google.maps.StyledMapType([],{name: 'Custom Style'});
        var v_CustomMapTypeId = 'custom_style';

        g_Map.mapTypes.set(v_CustomMapTypeId, v_CustomMapType);
        g_Map.setMapTypeId(v_CustomMapTypeId);

        updateMarkerPosition(0);
    }

    function updateMarkerPosition(p_FromAddress) {
        if(p_FromAddress == 0 && g_LatitudeInput.val() != '' && g_LongitudeInput.val() != ''){
            var v_LatLng = new google.maps.LatLng(g_LatitudeInput.val(), g_LongitudeInput.val());
            setMarker(v_LatLng);
            if(g_InitializingMap)
                g_InitializingMap = false;
        }
        else {
            var v_Address;
            var v_Cep = $('#cep').val().replace('.','');
            v_Address = v_Cep == '' ? '' : v_Cep + ', ';
            var v_Neighborhood = $('#bairro').val();
            v_Address += v_Neighborhood == '' ? '' : v_Neighborhood + ', ';
            var v_Street = $('#logradouro').val();
            v_Address += v_Street == '' ? '' : v_Street + ', ';
            var v_StreetNumber = $('#numero').val();
            v_Address += v_StreetNumber == '' ? '' : v_StreetNumber + ', ';
            if(v_Address != ''){
                var v_City = $('#city_id option:selected').text();
                v_Address += v_City == '' ? '' : v_City + ', ';
                g_Geocoder.geocode({'address': v_Address + 'MT, Brasil'}, function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        setMarker(results[0].geometry.location);
                        updateLatitudeLongitude();
                    }
                    else {
                        if(!g_InitializingMap){
                            console.log('Geolocalização não encontrada. Motivo: ' + status);
                            alert('Geolocalização não encontrada');
                        }
                    }
                    if(g_InitializingMap)
                        g_InitializingMap = 0;
                });
            }
        }
    }

    function setMarker(p_Position) {
        g_Map.setCenter(p_Position);
        g_Map.setZoom(16);
        if(g_Marker == null){
            g_Marker = new google.maps.Marker({
                position: p_Position,
                map: g_Map,
                draggable: true
            });

            g_Marker.addListener('dragend', function() {
                updateLatitudeLongitude();
            });
        }
        else{
            g_Marker.setPosition(p_Position);
        }
    }

    function updateLatitudeLongitude(){
        var v_Lat = Math.round(g_Marker.position.lat() * 10000000)/10000000;
        var v_Long = Math.round(g_Marker.position.lng() * 10000000)/10000000;
        g_LatitudeLongitudeInput.val(v_Lat + ', ' + v_Long);
        g_LatitudeInput.val(v_Lat);
        g_LongitudeInput.val(v_Long);
    }
</script>