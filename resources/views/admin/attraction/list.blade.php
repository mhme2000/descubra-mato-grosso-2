@extends('admin.util.listDT', ['p_HasDateFilter' => false])
@section('list-css')
@stop
@section('panel-header')
    {{ \App\Http\Controllers\AttractionController::$m_InventoryItemDescriptions[$p_InventoryItem] }} - {{ $p_City->name }}
    <a href="{{ url('admin/cidades/' . $p_City->id . '/atracoes/' . $p_InventoryItem . '/editar') }}">
        <button class="btn btn-success pull-right" title="Nova atração">
            <i class="fa fa-plus"></i>
        </button>
    </a>
@stop
@section('list-table-head')
    <tr>
        <th>Nome</th>
        <th>Ações</th>
    </tr>
    <tr>
        <td><input type="text" placeholder="Buscar" class="form-control" style="font-weight:normal"/></td>
        <td></td>
    </tr>
@stop
@section('list-table-dt-url')
    url: "{{ url('/admin/dt/cidades/' . $p_City->id . '/atracoes/' . $p_InventoryItem)}}"
@stop
@section('list-table-initial-sorting')
    "aaSorting": [[ 0, "asc" ]],
@stop
@section('list-table-sortable-columns')
    {"bSortable": true},
    {"bSortable": false}
@stop
@section('list-js')
@stop