@extends('admin.base')
@section('main-content')
<div class="row">
    <div class="col-sm-12">
        <section class="panel panel-visible">
            <header class="panel-heading br-b-n">
                <div class="panel-title">
                    @yield('panel-header')
                </div>
            </header>
            <div class="panel-body">
                @yield('content')
            </div>
        </section>
    </div>
</div>
@stop

