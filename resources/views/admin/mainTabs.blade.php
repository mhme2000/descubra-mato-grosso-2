@extends('admin.base')
@section('main-content')
<div class="row">
    <div class="col-sm-12">
        <section class="panel panel-visible">
            <header class="panel-heading" style="display: flex; align-items: center; height: 40px">
                <div class="panel-title">
                    @yield('panel-header')
                </div>
            </header>
            <div class="panel-body">
                @yield('content')
            </div>
        </section>
    </div>
</div>
@stop

