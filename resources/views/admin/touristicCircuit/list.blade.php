@extends('admin.util.listDT', ['p_HasDateFilter' => false])
@section('list-css')
@stop
@section('panel-header')
    Regiões Turísticas
    @if(!\App\UserType::isParceiro())
    <a href="{{ url('admin/circuitos/editar') }}">
        <button class="btn btn-success pull-right" title="Novo circuito">
            <i class="fa fa-plus"></i>
        </button>
    </a>
    @endif
@stop
@section('list-table-head')
    <tr>
        <th>Nome</th>
        <th>Município</th>
        <th>Técnico responsável</th>
        <th>Status</th>
        <th>Ações</th>
    </tr>
    <tr>
        <td><input type="text" placeholder="Buscar" class="form-control" style="font-weight:normal"/></td>
        <td>{!! Form::select('', ['' => 'Selecione'] + $p_Cities, null, ['class' => 'form-control']) !!}</td>
        <td><input type="text" placeholder="Buscar" class="form-control" style="font-weight:normal"/></td>
        <td>{!! Form::select('', ['' => 'Selecione'] + \App\Http\Controllers\CircuitController::$m_CircuitStatus, null, ['class' => 'form-control']) !!}</td>
        <td></td>
    </tr>
@stop
@section('list-table-dt-url')
    url: "{{ url('/admin/dt/circuitos')}}"
@stop
@section('list-table-initial-sorting')
    "aaSorting": [[ 0, "asc" ]],
@stop
@section('list-table-sortable-columns')
    {"bSortable": true},
    {"bSortable": true},
    {"bSortable": true},
    {"bSortable": true},
    {"bSortable": false}
@stop
@section('list-js')
@stop