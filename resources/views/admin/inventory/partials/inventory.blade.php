<div class="col-sm-12">
    <div class="panel panel-visible">
        <div class="panel-body pn">
            <div class="adv-table">
                <table class="table table-striped table-bordered table-hover responsive-table" id="responsive-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Nome</th>
                            <th>Popular</th>
                            <th>Município</th>
                            <th>Item</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($p_Inventory as $c_Inventory)
                            @foreach($c_Inventory['items'] as $c_Item)
                            
                            <tr>
                                <td>
                                    <a href="{{url('admin/inventario/' . $c_Inventory['path'] . '/editar/' . $c_Item->item_id)}}" title="Editar">
                                      @if(isset($c_Item->nome) && trim($c_Item->nome)!='')      
                                            {{$c_Item->nome}} 
                                      @else
                                        @if (isset($c_Item->nome_fantasia)){{$c_Item->nome_fantasia}}@endif
                                        @if (isset($c_Item->nome_popular) && isset(json_decode($c_Item->nome_popular)->pt)){{json_decode($c_Item->nome_popular)->pt}}@endif
                                      @endif
                                    </a>
                                </td>
                                <td>
                                    @if (isset($c_Item->nome_fantasia)){{$c_Item->nome_fantasia}}@endif
                                    @if (isset($c_Item->nome_popular) && isset(json_decode($c_Item->nome_popular)->pt)){{json_decode($c_Item->nome_popular)->pt}}@else {{$c_Item->nome_popular}} @endif
                                </td>
                                <td>{{$c_Item->cidade}}</td>
                                <td>
                                    <a href="{{url('admin/inventario/' . $c_Inventory['path'])}}" title="Editar">
                                        {{$c_Inventory['name']}}
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>