@extends('admin.util.listDT', ['p_HasDateFilter' => true])
@section('list-css')
    <style>
        table th, table td {
            font-size:11px;
        }
        .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
            padding: 8px 2px;
        }
        table div.actions-div {
            width: 128px;
        }
    </style>
@stop
@section('panel-header')
    Mídias doadas
@stop
@section('list-table-head')
    <tr>
        <th>Tipo</th>
        <th>Data</th>
        <th>Município</th>
        <th>Descrição</th>
        <th>Nome</th>
        <th>Telefone</th>
        <th>Ações</th>
    </tr>
    <tr>
        <td class="table-filter">{!! Form::select('tipo_midia', ['' => 'Selecione', 'Áudio' => 'Áudio', 'Imagem' => 'Imagem', 'Vídeo' => 'Vídeo'], null, ['class' => 'form-control']) !!}</td>
        <td class="table-filter"><input name="created_at" class="form-control dateInput" type="text" placeholder="Buscar"></td>
        <td class="table-filter"><input name="municipio" type="text" placeholder="Buscar" class="form-control" style="font-weight:normal"/></td>
        <td class="table-filter"><input name="descricao" type="text" placeholder="Buscar" class="form-control" style="font-weight:normal"/></td>
        <td class="table-filter"><input name="nome" type="text" placeholder="Buscar" class="form-control" style="font-weight:normal"/></td>
        <td class="table-filter"><input name="telefone" type="text" placeholder="Buscar" class="form-control" style="font-weight:normal"/></td>
        <td>
            {!! Form::open(['id' => 'mainForm', 'method' => 'GET', 'target' => '_blank', 'url'=> url('/admin/relatorios/doacoes/gerar'), 'onsubmit' => 'return processFilters()']) !!}
                <div id="reportFilters"></div>
                <button class="btn btn-success" title="Gerar relatório"><i class="imoon imoon-file-excel"></i></button>
            {!! Form::close() !!}
        </td>
    </tr>
@stop
@section('list-table-dt-url')
    url: "{{ url('/admin/dt/doacoes')}}"
@stop
@section('list-table-initial-sorting')
    "aaSorting": [[ 3, "desc" ]],
@stop
@section('list-table-sortable-columns')
    {"bSortable": true},
    {"bSortable": true},
    {"bSortable": true},
    {"bSortable": true},
    {"bSortable": true},
    {"bSortable": true},
    {"bSortable": false}
@stop
@section('list-js')
    <script>
        function processFilters() {
            var $v_ReportFilterDiv = $('.dataTables_scrollHeadInner #reportFilters'),
                v_HTML = '';
            $('.dataTables_scrollHeadInner .table-filter select, .dataTables_scrollHeadInner .table-filter input').each(function(){
                v_HTML += '<input type="hidden" name="'+$(this).attr('name')+'" value="'+$(this).val()+'">';
            });
            $v_ReportFilterDiv.html(v_HTML);
            return true;
        }
    </script>
@stop