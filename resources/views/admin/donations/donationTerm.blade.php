<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <style>
        body {
            margin:0;
        }
        h3{
            text-align: center;
            margin-bottom: 45px;
        }
        p{
            margin: 9px 0;
            text-align: justify;
        }
    </style>

</head>
<body>
    <div>

        <h3>TERMO DE DOAÇÃO DE MÍDIA</h3>

        <p>Eu, &nbsp;<strong>{{$p_Donation->nome}}</strong>, inscrito no CPF sob o número &nbsp;<strong>{{$p_Donation->documento}}</strong>, portador do e-mail &nbsp;<strong>{{$p_Donation->email}}</strong> e telefone &nbsp;<strong>{{$p_Donation->telefone}}</strong> concedo a doação da mídia &nbsp;<strong>{{$p_Donation->tipo_midia}}</strong>, de acordo com os padrões de licença do <i>Creative Commons</i>, à &nbsp;<strong>SECRETARIA DE TURISMO DE ESTADO DE MINAS GERAIS</strong>, inscrita no CNPJ sob o número 03.500.589/0001-85.</p>
        <br>
        <p>Cujo arquivo de mídia doado foi nomeado como &nbsp;<strong>{{$p_Donation->nome_original_arquivo}}</strong>, enviado a partir do IP &nbsp;<strong>{{$p_Donation->ip}}</strong>, em &nbsp;<strong>{{$p_Donation->created_at->format('H:i:s d/m/Y')}}</strong> no Portal de Turismo de Minas Gerais, <a href="http://www.minasgerais.com.br">www.minasgerais.com.br.</a></p>

        <br><br>
        <p>Regras da doação:</p>

        <?php
            \App::setLocale('pt');
            $v_AllowSharingOptions = [0 => 'Não',
                                      1 => 'Sim',
                                      2 => 'Sim, desde que outros compartilhem igual'];
            if($p_Donation->permite_compartilhar == 2) {
                if($p_Donation->permite_uso_comercial == 1)
                    $v_LicenseType = trans('donateMedia.attribution4_international');
                else
                    $v_LicenseType = trans('donateMedia.attribution_non_commercial4_international');
            }
            else if($p_Donation->permite_compartilhar == 1) {
                if($p_Donation->permite_uso_comercial == 1) {
                    $v_LicenseType = trans('donateMedia.attribution_share_like4_international');
                    $v_LicenseTypeSubtitle = trans('donateMedia.free_culture_license');
                }
                else
                    $v_LicenseType = trans('donateMedia.attribution_non_commercial_share_like4_international');
            }
            else {
                if($p_Donation->permite_uso_comercial == 1)
                    $v_LicenseType = trans('donateMedia.attribution_no_derivations4_international');
                else
                    $v_LicenseType = trans('donateMedia.attribution_non_commercial_no_derivations4_international');
            }
        ?>
        <ul>
            <li>Permitir que adaptações do seu trabalho sejam compartilhadas? {{$v_AllowSharingOptions[$p_Donation->permite_compartilhar]}}</li>
            <li>Permitir usos comerciais do seu trabalho? {{$p_Donation->permite_uso_comercial == 1 ? 'Sim' : 'Não'}}</li>
            <li>Licença selecionada: {{$v_LicenseType}}</li>
        </ul>

        <br>
        <p>Todas as licenças serão utilizadas por um período de até 100 (cem) anos a partir da data de doação.</p>
        <br>
        <p>Declaro para devidos fins que minha participação é voluntária e sem ônus.</p>
        <br>
        <p>Belo Horizonte, {{$p_Donation->created_at->format('d/m/Y')}}.</p>
        <p><strong>{{$p_Donation->nome}}</strong></p>

        <br><br>
        <p>Saiba mais sobre as licenças do <i>Creative Commons</i> em: <a href="https://br.creativecommons.org/licencas/">https://br.creativecommons.org/licencas/</a></p>
    </div>
</body>
</html>
