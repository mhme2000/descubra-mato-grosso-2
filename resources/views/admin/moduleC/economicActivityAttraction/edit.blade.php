@extends('admin.main')
@section('pageCSS')
    <link href="{{url('/vendor/select2/select2.min.css')}}" rel="stylesheet" />
    <style type="text/css">
        .align-center
        {
            text-align: center;
        }
        .margin-auto
        {
            margin: auto;
        }
        .panel-title > a.btn-success{
            color: white!important;
        }
    </style>
@stop
@section('panel-header')
    C3 - Atividade econômica/produção associada ao turismo
    <a title="Imprimir" type="button" class="btn btn-success pull-right" onclick="print()">
        <i class="fa fa-print"></i>
    </a>
@stop
@section('content')
    <?php \App\BaseInventoryModel::startFormFieldIndexing(); ?>
    <div class="row">
        <div class="mb20 col-sm-12 visible-print">
            <h2>C3 - Atividade econômica/produção associada ao turismo</h2>
        </div>
        @if(!\App\UserType::isParceiro())
        {!! Form::open(['id' => 'mainForm', 'url'=> url('/admin/inventario/atrativos-atividades-economicas'), 'onsubmit' => 'return submitForm()', 'files' => true]) !!}
        @else
        <div id="mainForm">
        @endif

            @include('admin.util.attractionHeaderIdentification', ['p_Form' => $p_Attraction, 'p_Types' => $p_Types, 'p_OrganizationNotRequired' => true])

        <div class="form-group col-sm-12">
            <label for="setor_responsavel_visitacao">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Setor responsável pela visitação<span class="mandatory-field">*</span></label>
            <textarea name="formulario[setor_responsavel_visitacao]" id="setor_responsavel_visitacao" class="form-control" rows="4" placeholder="Digite Aqui" required>{{$p_Attraction == null ? '' : $p_Attraction->setor_responsavel_visitacao}}</textarea>
        </div>

        @include('admin.util.photos', ['p_CoverPhoto' => $p_CoverPhoto, 'p_PhotoGallery' => $p_PhotoGallery, 'p_Cover' => true])


        @include('admin.util.attractionAccess', ['p_Form' => $p_Attraction])

        <div class="form-group col-sm-12">
            <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Proteção</h3>
        </div>
        <input type="hidden" id="protecao" name="formulario[protecao]" value="{{$p_Attraction == null ? '' : $p_Attraction->protecao}}">
        <div class="protecao-fields">
            <div class="form-group col-sm-12">
                <label>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Existe dispositivo legal (lei, decreto, norma, etc) que trate da criação, proteção ou restrição referentes ao atrativo?</label>
                <p><input id="existe_dispositivo_legal" type="checkbox" value="1" rel="Existe dispositivo legal" class="ml5 mt10 checkbox field"></p>
            </div>
            <div class="form-group col-sm-12 mandatory dispositivo-legal-fields">
                <label>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(2)}}Especifique<span class="mandatory-field">*</span></label>
                <textarea class="form-control field" rel="Existe dispositivo legal - Especifique" rows="4" placeholder="Digite Aqui"></textarea>
            </div>
        </div>

        @include('admin.util.attractionConservation', ['p_Form' => $p_Attraction])

        @include('admin.util.attractionFacilities', ['p_Form' => $p_Attraction, 'p_Languages' => $p_Languages, 'p_LanguageNames' => $p_LanguageNames])

        <div class="form-group col-sm-12">
            <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Dados Complementares</h3>
        </div>
        <div class="form-group col-sm-12">
            <label for="observacoes_complementares">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Descrições e observações complementares</label>
            <textarea name="formulario[observacoes_complementares]" id="observacoes_complementares" class="form-control" rows="4" placeholder="Digite Aqui">{{$p_Attraction == null ? '' : $p_Attraction->observacoes_complementares}}</textarea>
        </div>

        <div class="form-group col-sm-12">
            <label for="possui_espacos_eventos">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Possui espaços de eventos?</label>
            <p><input type="checkbox" value="1" name="formulario[possui_espacos_eventos]" id="possui_espacos_eventos" class="ml5 mt10" {{($p_Attraction == null || $p_Attraction->possui_espacos_eventos == 0) ? '' : 'checked'}}></p>
        </div>

        @include('admin.util.accessibility', ['p_Form' => $p_Attraction])

        <div class="form-group col-sm-12">
            <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Uso interno da SEDEC/TUR</h3>
        </div>

        <input type="hidden" id="tipos_viagem" name="formulario[tipos_viagem]" value="{{$p_Attraction == null ? '' : $p_Attraction->tipos_viagem}}">
        <div class="form-group col-sm-12">
            <label for="tipos_viagem">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Tipo de polo <i>(permite mais de uma opção)</i></label>
            {!! Form::select('', $p_TripTypes, null, ['id' => 'tipos_viagem_select', 'class' => 'form-control select2', 'style' => 'width: 100%', 'multiple' => 'multiple']) !!}
        </div>

        @include('admin.util.revision', ['p_Form' => $p_Attraction])

        @include('admin.util.responsibleTeam', ['p_Form' => $p_Attraction])

        @if(!\App\UserType::isParceiro())
        <div class="form-group col-sm-12">
            <input type="submit" class="btn btn-default mt15 mb25 pull-right" value="Salvar">
        </div>
        {!! Form::close() !!}
        @else
        </div>
        @endif
    </div>
@stop
@section('pageScript')
    <script src="{{url('/vendor/select2/select2.min.js')}}" type="text/javascript"></script>
    <script src="{{url('/vendor/select2/i18n/pt-BR.js')}}" type="text/javascript"></script>
    <script src="{{url('/assets/js/inputmask.js')}}" type="text/javascript"></script>
    <script src="{{url('/assets/js/jquery.inputmask.js')}}" type="text/javascript"></script>
    @include('admin.util.headerIdentificationScript')
    @include('admin.util.attractionConservationScript')
    @include('admin.util.photosScript', ['p_MaxPhotoCount' => -1])
    <script>
        $(document).ready(function()
        {
            $(".select2").select2({language:'pt-BR'});

            $('.phone-field').keypress(function (e) {
                var key = e.keyCode || e.charCode;
                if( key == 8 || key == 37 || key == 39 || key == 46 )
                    return true;

                var regex = new RegExp(/^[\d\s\(\)\-\+\.\,\;\\\/]+$/);
                var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
                if (regex.test(str))
                    return true;

                e.preventDefault();
                return false;
            });
            $('input[type="url"]').focus(function(){
                if(this.value == '')
                    this.value = 'http://';
            }).focusout(function(){
                if(this.value == 'http://')
                    this.value = '';
            });

            carregaDadosProtecao();

            $('#existe_dispositivo_legal').change(function(){
                if($(this).is(":checked"))
                {
                    $('.dispositivo-legal-fields').show();
                    $('.dispositivo-legal-fields.mandatory .form-control').attr('required', true);
                }
                else
                {
                    $('.dispositivo-legal-fields').hide();
                    $('.dispositivo-legal-fields.mandatory .form-control').removeAttr('required');
                }
            }).change();

            var v_VisitorsOrigin = $('#visitantes_origem').val();
            if(v_VisitorsOrigin.length > 0)
            {
                v_VisitorsOrigin = JSON.parse(v_VisitorsOrigin);
                $('#visitantes_origem_select').select2("val", v_VisitorsOrigin);
            }

            var v_TripTypes = $('#tipos_viagem').val();
            if(v_TripTypes.length > 0)
            {
                v_TripTypes = JSON.parse(v_TripTypes);
                $('#tipos_viagem_select').select2("val", v_TripTypes);
            }

            var v_InternationalVisitorsOrigin = $('#visitantes_origem_internacionais').val();
            if(v_InternationalVisitorsOrigin.length > 0)
            {
                try{
                    v_InternationalVisitorsOrigin = JSON.parse(v_InternationalVisitorsOrigin);
                    var v_SelectedItems = [];
                    $(v_InternationalVisitorsOrigin).each(function(){
                        v_SelectedItems.push(this.id);
                    });
                    $('#visitantes_origem_internacionais_select').select2("val", v_SelectedItems);
                } catch (e) {
                }
            }

            $('#equipamento_fechado').change(function(){
                if($(this).is(":checked")) {
                    $('.closed-equipment-fields').show();
                    $('.closed-equipment-fields.mandatory .form-control').attr('required', true);
                }
                else {
                    $('.closed-equipment-fields').hide();
                    $('.closed-equipment-fields.mandatory .form-control').removeAttr('required');
                }
            }).change();
        });

        function carregaDadosProtecao()
        {
            var v_Protecao = $('#protecao').val();
            if(v_Protecao.length > 0)
            {
                try {
                    v_Protecao = JSON.parse(v_Protecao);
                } catch (e) {
                    $('.protecao-fields .field[rel="Existe dispositivo legal - Especifique"]').val(v_Protecao);
                    return false;
                }
                $(v_Protecao).each(function(){
                    if(this.tipo == 'checkbox')
                        $('.protecao-fields .field[rel="'+this.nome+'"]').prop('checked',this.valor);
                    else
                        $('.protecao-fields .field[rel="'+this.nome+'"]').val(this.valor);
                });
            }
        }

        function submitForm()
        {
            if(!validateCNPJ($('.cnpj-field').val()))
            {
                alert('CNPJ inválido!');
                return false;
            }

            $('.cnpj-field').each(function (c_Key, c_Field)
            {
                var v_Value = $(c_Field).val().replace(/[^\d]+/g,'');
                $(c_Field).parent().find('input[type="hidden"]').val(v_Value);
            });

            processaDadosAcesso();

            processaDadosProtecao();

            processaDadosConservacao();

            processaDadosFacilidades();

            processaDadosAcessibilidade();

            $('#visitantes_origem').val(JSON.stringify(getSelectValue($('#visitantes_origem_select').val())));

            var v_InternationalVisitorsOrigin = [];
            $('#visitantes_origem_internacionais_select option:selected').each(function(){
                v_InternationalVisitorsOrigin.push({
                    id:$(this).attr('value'),
                    nome:$(this).text()
                });
            });
            $('#visitantes_origem_internacionais').val(JSON.stringify(v_InternationalVisitorsOrigin));

            $('#tipos_viagem').val(JSON.stringify(getSelectValue($('#tipos_viagem_select').val())));

            return true;
        }

        function getSelectValue(p_SelectValue){
            if(p_SelectValue == null || p_SelectValue == undefined)
                return [];
            else return p_SelectValue;
        }

        function processaDadosProtecao()
        {
            var v_Protecao = [];
            $('.protecao-fields .field').each(function(){
                v_Protecao.push({
                    nome:$(this).attr('rel'),
                    tipo:$(this).hasClass('checkbox') ? 'checkbox' : 'text-or-select',
                    valor:$(this).hasClass('checkbox') ? $(this).is(':checked') : $(this).val()
                });
            });
            $('#protecao').val(JSON.stringify(v_Protecao));
        }
    </script>
    @include('admin.util.mapScript')
    @include('admin.util.attractionAccessScript')
    @include('admin.util.attractionFacilitiesScript')
    @include('admin.util.attractionGeneralInfoScript')
    @include('admin.util.accessibilityScript')
    @include('admin.util.responsibleTeamScript')
    @include('admin.util.revisionScript')
    @include('admin.util.tooltipScript', ['p_Type' => 'C3'])
@stop