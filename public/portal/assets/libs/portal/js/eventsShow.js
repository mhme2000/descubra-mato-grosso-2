

    $('.top-eventos').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 5000,
        prevArrow: '<a data-role="none" class="carousel-control-prev slide-control-prev" aria-label="Previous" tabindex="0" role="button"><span class="carousel-control-prev-icon" aria-hidden="true"></span><span class="sr-only">Previous</span></a>',
        nextArrow: '<a data-role="none" class="carousel-control-next slide-control-next" aria-label="Next" tabindex="0" role="button"><span class="carousel-control-next-icon" aria-hidden="true"></span><span class="sr-only">Next</span></a>',
        arrows: true,
        dots: false,
        pauseOnHover: false,
        responsive: [{
                breakpoint: 990,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
        },{
                
            breakpoint: 768,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1
            }
        }, {
            breakpoint: 520,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }]
    });


$('[data-toggle="popover"]').popover();