function lazyLoadImage(){
    $('img[data-lazyload]').each( function(){
        //* set the img src from data-src
        $( this ).attr( 'src', $( this ).attr( 'data-lazyload' ) );
        }
    );
}

function displayOnLoad(){
    $('.displayOnLoad').each( function(){
        //* set the img src from data-src
        $( this ).removeClass('d-none');
        }
    );

    $('.fadeInOnLoad').fadeIn(400);
    

}



$(document).ready(function () {
    $(window).on('scroll', function () {
        // Faz verificação do tamanho da tela antes de realizar a ação 768 é o tamanho de md do bootstrap medida que
        // ativa o menu "sandwich"
        if ((($(window).width() < 768 && $(window).scrollTop() >= 50) ||
            ($(window).width() > 768 && $(window).scrollTop() >= 110)) &&
            !($('header').hasClass('fixed-top'))) {
            //Faz fadeout do cabeçalho retornado com efeito e fixando-o no topo
            $('header').fadeOut(500, function () {
                $('header').addClass('fixed-top');
                $('header').fadeIn();
            });
        } else if ($(window).scrollTop() <= 1) {
            //Remove a classe que fixa o cabeçalho no topo deixando-o como parte do documento
            $('header').removeClass('fixed-top');
        }

        //Trata o menu que é exibito na parte inferior da tela em versões mobile e tela diminuida
        //faz com que ao alcançar o scroll máximo da tela o menu fique ocuto
        if ($(window).scrollTop() == $(document).height() - $(window).height()) {
            $('.menu-mobile').fadeOut(500, function () {
                $('.menu-mobile').removeClass('d-block');
                $('.menu-mobile').addClass('d-none');
            });
        } else {
            $('.menu-mobile').removeClass('d-none')
            $('.menu-mobile').addClass('d-block');
        }
    });

    $('[data-toggle="tooltip"]').tooltip();

});

if ($('#reportrange').length){

$(function() {
    
    if ($('#reportrange').val() == ''){
        var start = moment();
        var end = moment().add(180, 'days');    
    } else {
        var start = moment($('#reportrange').val().split("-")[0], 'DD/MM/YYYY');
        var end = moment($('#reportrange').val().split("-")[1], 'DD/MM/YYYY');
    }
    
    function cb(start, end) {

        $('#reportrange span').html(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
    }
    
    $('#reportrange').daterangepicker({
            "locale": {
            "format": "DD/MM/YYYY",
            "separator": " - ",
            "applyLabel": "Ok",
            "cancelLabel": "Cancelar",
            "fromLabel": "Para",
            "toLabel": "de",
            "customRangeLabel": "Definir",
            "weekLabel": "S",
            "daysOfWeek": [
                "D",
                "S",
                "T",
                "Q",
                "Q",
                "S",
                "S"
            ],
            "monthNames": [
                "Janeiro",
                "Fevereiro",
                "Março",
                "Abril",
                "Maio",
                "Junho",
                "Julho",
                "Agosto",
                "Setembro",
                "Outubro",
                "Novembro",
                "Dezembro"
            ],
            "firstDay": 1
        },
        "minDate": moment("01/01/2017"),
        //"maxDate": moment().add(180, 'days'),
        "opens": 'left',
        "startDate": start,
        "endDate": end,
        ranges: {
           'Todas as Datas' : [moment("01/01/2017"), moment().add(730, 'days')],  
           'Hoje': [moment(), moment()],
           'Amanhã': [moment().add(1, 'days'), moment().add(1, 'days')],
           'Próximos 7 dias': [moment(), moment().add(6, 'days')],
           'Próximos 30 dias': [moment(), moment().add(29, 'days')],
           'Próximos 60 dias': [moment(), moment().add(60, 'days')],
           'Este Mês': [moment().startOf('month'), moment().endOf('month')],
           'Próximo Mês': [moment().add(1, 'month').startOf('month'), moment().add(1, 'month').endOf('month')],
           'Neste ano': [moment().startOf('year'), moment().add(1, 'month').endOf('year')]
        }
    }, cb);
    
    cb(start, end);

    });
}

$(window).on('load', function() {
    //Carrega imagens após load
    lazyLoadImage();

    //Exibe Objetos após carga
    displayOnLoad();

});
