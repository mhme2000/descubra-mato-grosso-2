function getDirections(p_origin, p_destination, p_mode){
    var directionsService = new google.maps.DirectionsService();
    var directionsRequest;
    if(p_mode === "car"){
        directionsRequest = {
            origin: p_origin,
            destination: p_destination,
            travelMode: google.maps.DirectionsTravelMode.DRIVING,
            unitSystem: google.maps.UnitSystem.METRIC,
            region: "br",
            language: "pt-BR"
        };
    }else if(p_mode === "bus"){
        directionsRequest = {
            origin: p_origin,
            destination: p_destination,
            travelMode: google.maps.DirectionsTravelMode.TRANSIT,
            unitSystem: google.maps.UnitSystem.METRIC,
            region: "br",
            language: "pt-BR",
            transitOptions: {
                modes: [google.maps.TransitMode.BUS]
            }
        };
    }
    directionsService.route(directionsRequest, function (response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            var distance =  response.routes[0].legs[0].distance.text;
            var duration = response.routes[0].legs[0].duration.text;
            var origin = response.routes[0].legs[0].start_address;
            var destination = response.routes[0].legs[0].end_address;
            var info = "Origem:" + origin + "\nDestino:" + destination
                + "\nDistÃ¢ncia:" + distance + "\n" + "DuraÃ§Ã£o de :" + duration;
            var routeResult = response.routes[0];
            routeResult.legs.forEach(function(leg){
                leg.steps.forEach(function(step){
                });
            });
            var linkGoogleMaps = "https://www.google.com.br/maps/dir/" + origin + "/" + destination;
        }
        else{}
    });
}

var test_origin = "Belo+Horizonte";
var test_destiantion = "Betim";
var test_mode = "car";

getDirections(test_origin,test_destiantion,test_mode);