$(document).ready(function(){

    function loadMap(){

    $('#map_canvas').gmap3({
            marker:{
                latLng: [-20.127883, -44.215284],
                options: {
                    // icon: new google.maps.MarkerImage(
                    //     "http://www.usc.br/wp-content/themes/usc/images/usc_pin.png",
                    //     new google.maps.Size(32, 44, "px", "px")
                    // )
                }
            },
            map:{
                options:{
                    zoom: 16,
                    center:[-20.127883, -44.215284], 
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    mapTypeControl: true,
                    mapTypeControlOptions: {
                      style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
                    },
                    navigationControl: false,
                    scrollwheel: false,
                    streetViewControl: false
                }
            }
        });
    }

    loadMap();
});