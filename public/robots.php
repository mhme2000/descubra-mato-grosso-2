<?php

// Make it a plain text file
header('Content-Type:text/plain');

// Output based on HTTP host
if($_SERVER['HTTP_HOST'] == 'minasgerais.com.br') {

    // Enter your test site robots.txt here

?>
User-agent: *
Disallow: /admin/
Disallow: /en/
Disallow: /fr/
Disallow: /es/
Sitemap: http://www.minasgerais.com.br/portal/sitemap.txt
<?php

}
else {

    // Enter your live site robots.txt here

?>
User-agent: *
Disallow: /admin/
Disallow: /en/
Disallow: /fr/
Disallow: /es/
Sitemap: http://www.minasgerais.com.br/portal/sitemap.txt
<?php    

}

?>